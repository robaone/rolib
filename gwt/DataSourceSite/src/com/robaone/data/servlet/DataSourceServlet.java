package com.robaone.data.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.robaone.api.data.AppDatabase;
import com.robaone.api.json.DSResponse;

/**
 * Servlet implementation class DataSourceServlet
 */
@WebServlet("/data")
public class DataSourceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataSourceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		try{
			response.setContentType("text/plain");
			DSResponse<JSONObject> dsr = new DSResponse<JSONObject>();
			JSONObject jo = new JSONObject("{\"test\":\"Hello World\"}");
			jo.put("timestamp", new java.util.Date().getTime());
			dsr.getResponse().addData(jo);
			response.getOutputStream().write(new JSONObject(dsr).toString(3).getBytes());
			AppDatabase.writeLog(new JSONObject(dsr).toString());
		}catch(Exception e){
			response.getOutputStream().write(("{\"response\":{\"status\":\"1\",\"error\":\""+e.getClass().getName()+"\"}}").getBytes());
		}
	}

}
