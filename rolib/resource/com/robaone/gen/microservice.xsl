﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:text>package </xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text>;

import java.util.Properties;

import javax.jms.Destination;
import javax.jms.Message;

import com.citizensrx.util.jms.ListeningServiceTemplate;
import com.citizensrx.util.jms.JMSServiceHandler;
import com.robaone.script.ScriptBootstrap;

public class </xsl:text>
    <xsl:variable name="className" select="/node[@name='json']/node[@name='info']/node[@name='title']"/>
    <xsl:value-of select="$className"/>
    <xsl:text><![CDATA[ extends ListeningServiceTemplate {

	private Properties properties;
	private ScriptBootstrap script;
	private ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactory factory;
	public ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[(){
		
	}
	@Override
	public void onReceive(Message message) throws Exception {
		this.writeLog("Received message <"+message+">");
		Destination replyTo = message.getJMSReplyTo();
		if(replyTo != null){
			this.writeLog("Replying to <"+replyTo.toString()+">");
			try{
				handleRequest(message,replyTo);
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			this.writeLog("No ReplyTo address");
		}
	}
	public void writeLog(String str) {
		this.getScript().writeLog(str);
	}
	protected ScriptBootstrap getScript() {
		if(this.script == null){
			this.script = new ScriptBootstrap();
		}
		return this.script;
	}
	protected void handleRequest(final Message message,final Destination replyTo) throws Exception {
		]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactory factory = this.getFactory();
		JMSServiceHandler handler = factory.newHandler(message);
	    this.getExecutor().execute(handler);
	}

	public static void main(String[] args) throws Exception {
		if(args.length > 0){
			ScriptBootstrap script = new ScriptBootstrap();
			script.setConfigFile(args, 0);
			script.loadConfig(script.getConfigFile());
			script.loadMnemonics();
			]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[ service = new ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[();
			service.setProperties(script.getConfigProperties());
			service.setTimeout(0);
			service.configJms(service.getProperties());
			service.setDestination(script.getProperty("jms.service.address"));
			service.writeLog("Starting...");
			service.run();
			if(service.getException() != null){
				service.getException().printStackTrace();
				System.exit(1);
			}else{
				System.exit(0);
			}
		}else{
			System.err.println("Not enough arguments");
			System.exit(1);
		}
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	public ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactory getFactory() {
	    if(this.factory == null){
			this.setFactory(new ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactoryImpl());
			this.getFactory().setConnectionManager(getJMSConnectionManager());
			this.getFactory().setProperties(this.getProperties());
		}
		return factory;
	}
	public void setFactory(]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactory factory) {
		this.factory = factory;
	}

}]]></xsl:text>
  </xsl:template>
</xsl:stylesheet>
