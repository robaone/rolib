﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:variable name="className" select="/node[@name='json']/node[@name='info']/node[@name='title']"/>
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text>.handlers<![CDATA[;

import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

import ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text><![CDATA[.]]></xsl:text>
<xsl:value-of select="$className"/>
<xsl:text><![CDATA[Handler;
import com.citizensrx.util.jms.JMSConnectionBlock;

public class InvalidJmsTypeHandler extends ]]></xsl:text>
<xsl:value-of select="$className"/>
<xsl:text><![CDATA[Handler {

	@Override
	public void run() {
		try {
			new JMSConnectionBlock<Message>(){

				@Override
				public void onMessageReceived(Message message) throws Exception {

				}

				@Override
				protected void run() throws Exception {
					if(getRequest().getJMSReplyTo() != null){
						TextMessage message = this.getSession().createTextMessage();
						MessageProducer producer = this.createProducer(getRequest().getJMSReplyTo());
						message.setJMSType("error");
						message.setStringProperty("message", "Invalid JMSType");
						producer.send(message);
					}
				}

			}.run(this.getConnectionManager());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(Message message) throws Exception {
		this.setRequest(message);
	}

}]]></xsl:text>
  </xsl:template>
</xsl:stylesheet>
