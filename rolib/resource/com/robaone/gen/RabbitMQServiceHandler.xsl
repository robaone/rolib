﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/service/package"/>
    <xsl:text><![CDATA[;

import java.util.Properties;

import org.json.JSONObject;

import com.robaone.jms.ROJsonMessageHandler;

abstract public class ]]></xsl:text>
    <xsl:value-of select="/service/name"/>
    <xsl:text><![CDATA[Handler implements ROJsonMessageHandler {
	private Properties properties;
	private JSONObject request;
	private JSONObject response;

	@Override
	public void setRequest(JSONObject message_jo) {
		this.request = message_jo;
	}

	public JSONObject getRequest() {
		return this.request;
	}

	@Override
	public JSONObject getResponse() {
		return this.response;
	}

	protected void setResponse(JSONObject resp) {
		this.response = resp;
	}

	public void setProperties(Properties configProperties) {
		this.properties = configProperties;
	}

	public Properties getProperties() {
		return this.properties;
	}

}
]]></xsl:text>
  </xsl:template>
</xsl:stylesheet>
