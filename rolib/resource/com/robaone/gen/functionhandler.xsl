﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
<xsl:param name="path"/>
  <xsl:template match="/">
    <xsl:variable name="className" select="/node[@name='json']/node[@name='info']/node[@name='title']"/>
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text>.handlers<![CDATA[;

import javax.jms.Message;

import com.citizensrx.util.jms.JMSServiceHandler;
import com.citizensrx.util.jms.JMSExecutor;
import com.citizensrx.util.jms.JMSRunner;

public class ]]></xsl:text>
    <xsl:value-of select="$className"/>
    <xsl:call-template name="uppercase">
    	<xsl:with-param name="string" select="substring($path,2)"/>
    </xsl:call-template>
    <xsl:text><![CDATA[Handler extends JMSServiceHandler {
	private JMSRunner runner;
	private JMSExecutor executor;
	
	@Override
	public void run() {
		try {
			JMSExecutor jmsExecutor = this.getExecutor();
			jmsExecutor.setRequest(getRequest());
			jmsExecutor.run(this.getConnectionManager());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(Message message) throws Exception {
		this.setRequest(message);
	}
	
	public JMSRunner getRunner(){
		return this.runner;
	}
	
	public void setRunner(JMSRunner runner){
		this.runner = runner;
	}

	public JMSExecutor getExecutor() {
		if(this.executor == null){
			this.setExecutor(new JMSExecutor());
			this.getExecutor().setRunner(createHandler());
		}
		return executor;
	}
	
	private JMSRunner createHandler() {
		]]></xsl:text>
	<xsl:variable name="implementation" select="/node[@name='json']/node[@name='paths']/node[@name=$path]/node[@name='post']/node[@name='x-handler']"/>
	<xsl:choose>
		<xsl:when test="string-length($implementation) &gt; 0">
			<xsl:value-of select="$implementation"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>return null;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text><![CDATA[
	}

	public void setExecutor(JMSExecutor executor) {
		this.executor = executor;
	}
}]]></xsl:text>
  </xsl:template>
  <xsl:template name="uppercase">
  	<xsl:param name="string"/>
  	<xsl:variable name="first_letter" select="(substring($string,1,1))"/>
  	<xsl:value-of select="translate($first_letter,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  	<xsl:value-of select="substring($string,2)"/>
  </xsl:template>
</xsl:stylesheet>
