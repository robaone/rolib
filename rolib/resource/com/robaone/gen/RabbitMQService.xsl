﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/service/package"/>
    <xsl:text><![CDATA[;

import com.robaone.jms.AMQPRabbitMQConnectionManager;
import com.robaone.jms.ROJsonMessageHandler;
import com.robaone.jms.RabbitMQConnectionManager;
import com.robaone.jms.RabbitMQReceive;
import com.robaone.script.ScriptBootstrap;

public class ]]></xsl:text>
    <xsl:value-of select="/service/name"/>
    <xsl:text><![CDATA[ {
	private ScriptBootstrap script;
	private RabbitMQReceive receiver;
	private RabbitMQConnectionManager connectionManager;
	private ROJsonMessageHandler handler;
	public static void main(String[] args) {
		]]></xsl:text>
    <xsl:value-of select="/service/name"/>
    <xsl:text><![CDATA[ main = null;
		try{
			main = new ]]></xsl:text>
    <xsl:value-of select="/service/name"/>
    <xsl:text><![CDATA[();
			main.getScript().setConfigFile(args[0]);
			main.getScript().loadConfig(main.getScript().getConfigFile());
			main.getScript().loadMnemonics();
			main.getReceiver().setLogWriter(System.out);
			main.getReceiver().setHandler(main.getHandler());
			main.getReceiver().setQueue(main.getScript().getProperty("queue"));
			main.getReceiver().run(main.getConnectionManager());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	protected RabbitMQConnectionManager getConnectionManager() throws Exception {
		if(this.connectionManager == null){
			AMQPRabbitMQConnectionManager cman = new AMQPRabbitMQConnectionManager();
			cman.setHost(this.getScript().getProperty("jms.host"));
			cman.setPassword(this.getScript().getProperty("jms.password"));
			cman.setUsername(this.getScript().getProperty("jms.username"));
			this.setConnectionManager(cman);
		}
		return this.connectionManager;
	}
	protected void setConnectionManager(RabbitMQConnectionManager cman){
		this.connectionManager = cman;
	}
	public ScriptBootstrap getScript() {
		if(this.script == null){
			this.setScript(new ScriptBootstrap());
		}
		return script;
	}
	public void setScript(ScriptBootstrap script) {
		this.script = script;
	}
	public RabbitMQReceive getReceiver() {
		if(this.receiver == null){
			this.setReceiver(new RabbitMQReceive());
		}
		return receiver;
	}
	public void setReceiver(RabbitMQReceive receiver) {
		this.receiver = receiver;
	}
	public ROJsonMessageHandler getHandler() throws Exception {
		if(this.handler == null){
			]]></xsl:text>
    <xsl:value-of select="/service/name"/>
    <xsl:text><![CDATA[Handler ]]></xsl:text>
    <xsl:value-of select="translate(/service/name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
    <xsl:text><![CDATA[_handler = new ]]></xsl:text>
    <xsl:value-of select="/service/handler"/>
    <xsl:text><![CDATA[Handler();
			]]></xsl:text>
    <xsl:value-of select="translate(/service/name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
    <xsl:text><![CDATA[_handler.setProperties(this.getScript().getConfigProperties());
			this.setHandler(]]></xsl:text>
    <xsl:value-of select="translate(/service/name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
    <xsl:text><![CDATA[_handler);
		}
		return handler;
	}
	public void setHandler(ROJsonMessageHandler handler) {
		this.handler = handler;
	}

}
]]></xsl:text>
  </xsl:template>
</xsl:stylesheet>
