﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:variable name="className" select="/node[@name='json']/node[@name='info']/node[@name='title']"/>
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text><![CDATA[;

import java.util.Properties;
import javax.jms.Message;
import com.robaone.api.business.FieldValidator;
import ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text><![CDATA[.handlers.*;
import com.citizensrx.util.jms.InvalidJmsTypeHandler;
import com.citizensrx.util.jms.JMSConnectionManager;
import com.citizensrx.util.jms.JMSServiceHandler;

public class ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactoryImpl implements
		]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactory {

	private JMSConnectionManager connectionManager;
	private Properties properties;

	@Override
	public JMSServiceHandler newHandler(Message message) throws Exception {
		String jmstype = message.getJMSType();
		JMSServiceHandler handler = null;
		if(FieldValidator.exists(jmstype)){
			switch(jmstype){]]></xsl:text>
	  <xsl:for-each select="/node[@name='json']/node[@name = 'paths']/node"><xsl:text><![CDATA[
			case "]]></xsl:text><xsl:value-of select="substring(@name,2)"/><xsl:text><![CDATA[":{
				handler = new ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:call-template name="uppercase">
	  	<xsl:with-param name="string" select="substring(@name,2)"/>
	  </xsl:call-template>
	  <xsl:text><![CDATA[Handler();
	  			break;
			}]]></xsl:text>
	   </xsl:for-each><xsl:text><![CDATA[
	        default:{
				handler = new InvalidJmsTypeHandler();
			}
			}	
		}else{
			handler = new InvalidJmsTypeHandler();
		}
		handler.setConnectionManager(getConnectionManager());
		handler.setProperties(getProperties());
		handler.initialize(message);
		return handler;
	}

	public JMSConnectionManager getConnectionManager() {
		return connectionManager;
	}

	public void setConnectionManager(JMSConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
		
}]]></xsl:text>
  </xsl:template>
  <xsl:template name="uppercase">
  	<xsl:param name="string"/>
  	<xsl:variable name="first_letter" select="(substring($string,1,1))"/>
  	<xsl:value-of select="translate($first_letter,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  	<xsl:value-of select="substring($string,2)"/>
  </xsl:template>
</xsl:stylesheet>
