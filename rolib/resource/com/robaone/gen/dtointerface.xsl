﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:param name="dto_name"/>
  <xsl:template match="/">
  	<xsl:variable name="className" select="concat(/node[@name='json']/node[@name='info']/node[@name='title'],$dto_name)"/>
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text>.dto<![CDATA[;

public class ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[ {
]]></xsl:text>
      <xsl:for-each select="/node[@name='json']/node[@name='definitions']/node[@name=$dto_name]/node[@name='properties']/node">
        <xsl:text><![CDATA[
	private ]]></xsl:text>
	    <xsl:call-template name="java-type">
	      <xsl:with-param name="type" select="."/>
	    </xsl:call-template>
	    <xsl:text> </xsl:text>
	    <xsl:value-of select="@name"/>
	    <xsl:text>;</xsl:text>
	  </xsl:for-each><xsl:text><![CDATA[

]]></xsl:text>
	  <xsl:for-each select="/node[@name='json']/node[@name='definitions']/node[@name=$dto_name]/node[@name='properties']/node">
	    <xsl:text><![CDATA[ 	public ]]></xsl:text>
	    <xsl:call-template name="java-type">
	    	<xsl:with-param name="type" select="."/>
	    </xsl:call-template>
	    <xsl:text><![CDATA[ get]]></xsl:text>
	    <xsl:call-template name="uppercase">
	    	<xsl:with-param name="string" select="@name"/>
	    </xsl:call-template>
	    <xsl:text><![CDATA[(){
	    return this.]]></xsl:text>
	    <xsl:value-of select="@name"/>
	    <xsl:text><![CDATA[;
    }
	public void set]]></xsl:text>
	    <xsl:call-template name="uppercase">
	    	<xsl:with-param name="string" select="@name"/>
	    </xsl:call-template>
	    <xsl:text><![CDATA[(]]></xsl:text>
	    <xsl:call-template name="java-type">
	    	<xsl:with-param name="type" select="."/>
	    </xsl:call-template>
	    <xsl:text><![CDATA[ ]]></xsl:text>
	    <xsl:value-of select="@name"/>
	    <xsl:text><![CDATA[){
	    this.]]></xsl:text>
	    <xsl:value-of select="@name"/>
	    <xsl:text> = </xsl:text>
	    <xsl:value-of select="@name"/>
	    <xsl:text><![CDATA[;
    }
]]></xsl:text>
	  </xsl:for-each>
	  <xsl:text><![CDATA[
}]]></xsl:text>
  </xsl:template>
  <xsl:template name="uppercase">
  	<xsl:param name="string"/>
  	<xsl:variable name="first_letter" select="(substring($string,1,1))"/>
  	<xsl:value-of select="translate($first_letter,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  	<xsl:value-of select="substring($string,2)"/>
  </xsl:template>
  <xsl:template name="java-type">
  	<xsl:param name="type"/>
    <xsl:choose>
    	<xsl:when test="$type/node[@name='type'] = 'string'">
    		<xsl:text>String</xsl:text>
    	</xsl:when>
    	<xsl:when test="$type/node[@name='type'] = 'integer'">
    		<xsl:text>Integer</xsl:text>
    	</xsl:when>
    	<xsl:when test="$type/node[@name='type'] = 'array'">
    		<xsl:call-template name="reference-object">
    			<xsl:with-param name="parameter" select="$type/node[@name='items']/node[@name='$ref']"/>
    		</xsl:call-template>
    		<xsl:text>[]</xsl:text>
    	</xsl:when>
    	<xsl:when test="string-length($type/node[@name='$ref']) &gt; 0">
    		<xsl:call-template name="reference-object">
    			<xsl:with-param name="parameter" select="$type/node[@name='$ref']"/>
    		</xsl:call-template>
    	</xsl:when>
    	<xsl:otherwise>
    		<xsl:value-of select="$type/node[@name='type']"/>
    	</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="reference-object">
  	<xsl:param name="parameter"/>
  	<xsl:value-of select="/node[@name='json']/node[@name='info']/node[@name='title']"/>
  	<xsl:value-of select="substring($parameter,15)"/>
  </xsl:template>
</xsl:stylesheet>
