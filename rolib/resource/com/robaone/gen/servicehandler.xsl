﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
<xsl:param name="path"/>
  <xsl:template match="/">
    <xsl:variable name="className" select="/node[@name='json']/node[@name='info']/node[@name='title']"/>
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text><![CDATA[;

import java.util.Properties;
import javax.jms.Message;

import com.citizensrx.util.jms.JMSConnectionManager;

public abstract class ]]></xsl:text>
    <xsl:value-of select="$className"/>
    <xsl:text><![CDATA[Handler implements Runnable {
	
	private JMSConnectionManager connectionManager;
	private Properties properties;
	private Message request;
	
	public abstract void initialize(Message message) throws Exception;

	public JMSConnectionManager getConnectionManager() {
		return connectionManager;
	}

	public void setConnectionManager(JMSConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}
	
	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public Message getRequest() {
		return request;
	}

	public void setRequest(Message request) {
		this.request = request;
	}
	
}]]></xsl:text>
  </xsl:template>
  <xsl:template name="uppercase">
  	<xsl:param name="string"/>
  	<xsl:variable name="first_letter" select="(substring($string,1,1))"/>
  	<xsl:value-of select="translate($first_letter,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  	<xsl:value-of select="substring($string,2)"/>
  </xsl:template>
</xsl:stylesheet>
