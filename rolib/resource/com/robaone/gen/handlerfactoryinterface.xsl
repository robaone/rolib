﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:variable name="className" select="/node[@name='json']/node[@name='info']/node[@name='title']"/>
    <xsl:text><![CDATA[package ]]></xsl:text>
    <xsl:value-of select="/node[@name='json']/node[@name='x-package']"/>
    <xsl:text><![CDATA[;

import java.util.Properties;
import javax.jms.Message;

import com.citizensrx.util.jms.JMSConnectionManager;
import com.citizensrx.util.jms.JMSServiceHandler;

public interface ]]></xsl:text>
	  <xsl:value-of select="$className"/>
	  <xsl:text><![CDATA[HandlerFactory {

	public JMSServiceHandler newHandler(Message message) throws Exception;

	public void setConnectionManager(JMSConnectionManager connectionManager);
	
	public void setProperties(Properties properties);

}]]></xsl:text>
  </xsl:template>
</xsl:stylesheet>
