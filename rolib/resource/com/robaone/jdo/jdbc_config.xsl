﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:text>tables=</xsl:text>
    <xsl:for-each select="/json/*">
      <xsl:if test="position() &gt; 1">
        <xsl:text>,</xsl:text>
      </xsl:if>
      <xsl:value-of select="name()"/>
    </xsl:for-each>
    <xsl:text>

</xsl:text>
    <xsl:for-each select="/json/*">
      <xsl:value-of select="name()"/>
      <xsl:text>.fields=*
</xsl:text>
      <xsl:value-of select="name()"/>
      <xsl:text>.identity=id</xsl:text>
      <xsl:value-of select="name()"/>
      <xsl:text>
</xsl:text>
      <xsl:value-of select="name()"/>
      <xsl:text>.idmanager=identity

</xsl:text>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
