/*
 * Application Javascript
 * Author: Ansel Robateau
 * Feb 22nd 2014
 * http://www.robaone.com
 */
var RO = {};

RO.log = function(message){
  try{
    console.log(message);
  }catch(Exception){}
};

RO.getTimestamp = function(){
  var time = new Date().getTime();
  return time;
};

RO.form = {};
RO.form.setAction = function(form,action){
	$('form[name="'+form+'"]').attr('action',action);
};
RO.form.reset = function(name){
	$('form[name="'+name+'"] .form-error').addClass('hidden');
	$('form[name="'+name+'"] .form-error').text('');
	$('form[name="'+name+'"] input').parent().removeClass('has-error');
	$('form[name="'+name+'"] .alert').addClass('hidden');
	this.deactivate(name);
};
RO.form.success = function(data,form,handler){
	try{
		var obj = jQuery.parseJSON(data);
		RO.form.showFeedback(obj,form,handler);
	}catch(Exception){
		RO.form.showFormError(form,Exception);
		this.activate(form);
	}
};
RO.form.showFieldError = function(form,field,error){
	$('form[name="'+form+'"] input[name="'+field+'"]').parent().addClass('has-error');
	$('form[name="'+form+'"] input[name="'+field+'"] + .alert').text(error);
	$('form[name="'+form+'"] input[name="'+field+'"] + .alert').removeClass('hidden');
};
RO.form.showFormError = function(form,error){
	$('form[name="'+form+'"] .form-error').removeClass('hidden');
	$('form[name="'+form+'"] .form-error').text(error);
};
RO.form.activate = function(form){
	$('form[name="'+form+'"] fieldset').prop('disabled', false);
};
RO.form.deactivate = function(form){
	$('form[name="'+form+'"] fieldset').prop('disabled', true);
};
RO.form.populateFieldErrors = function(form,errors){
	try{
		for(var error in errors){
			var message = errors[error];
			this.showFieldError(form,error,message);
		}
	}catch(Exception){
	}
};
RO.form.showFeedback = function(obj,form,handler){
	try{
		if(obj.status == 0){
			handler(obj);
		}else if(obj.status == 2){
			this.populateFieldErrors(form,obj.errors);
		}else if(obj.error){
			this.showFormError(form,obj.error);
		}else{
			this.showFormError(form,'Unknown error');
		}
	}catch(exception){
		this.showFormError(form,exception);
	}
	this.activate(form);
};