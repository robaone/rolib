package com.robaone.xml;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ROTransformTest.class, XMLPathParserTest.class })
public class AllXMLTests {

}
