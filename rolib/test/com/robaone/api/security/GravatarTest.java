package com.robaone.api.security;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.robaone.api.security.Gravatar.IMGFORMAT;

public class GravatarTest {
	Gravatar gravatar;
	@Before
	public void setUp() throws Exception {
		gravatar = new Gravatar();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetEmail() {
		this.gravatar.setEmail("test@email.com");
		assertEquals("test@email.com",this.gravatar.getEmail());
	}

	@Test
	public void testGetProfileHash() {
		this.gravatar.setEmail("test@email.com");
		String hash = this.gravatar.getProfileHash();
		assertEquals("93942e96f5acd83e2e047ad8fe03114d",hash);
	}

	@Test
	public void testGetProfileImage() throws IOException {
		this.gravatar.setEmail("test@email.com");
		byte[] img_bytes = this.gravatar.getProfileImage();
		assertNotNull(img_bytes);
		assertTrue(img_bytes.length > 2100);
	}

	@Test
	public void testGetProfileImageUrl() {
		this.gravatar.setEmail("test@email.com");
		String url = this.gravatar.getProfileImageUrl();
		assertEquals("http://www.gravatar.com/avatar/93942e96f5acd83e2e047ad8fe03114d.PNG?s=60",url);
	}

	@Test
	public void testGetImageSize() {
		assertEquals(60,this.gravatar.getImageSize());
		this.gravatar.setImageSize(100);
		assertEquals(100,this.gravatar.getImageSize());
	}

	@Test
	public void testGetImageFormat() {
		assertEquals(IMGFORMAT.PNG,this.gravatar.getImageFormat());
		this.gravatar.setImageFormat(IMGFORMAT.JPEG);
		assertEquals(IMGFORMAT.JPEG,this.gravatar.getImageFormat());
	}

	@Test
	public void testGetContentType() {
		assertEquals("image/png",this.gravatar.getContentType());
		this.gravatar.setImageFormat(IMGFORMAT.JPEG);
		assertEquals("image/jpeg",this.gravatar.getContentType());
		this.gravatar.setImageFormat(IMGFORMAT.GIF);
		assertEquals("image/gif",this.gravatar.getContentType());
		this.gravatar.setImageFormat(IMGFORMAT.TIFF);
		assertEquals("image/tiff",this.gravatar.getContentType());
	}

}
