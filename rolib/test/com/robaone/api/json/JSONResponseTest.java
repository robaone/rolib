package com.robaone.api.json;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class JSONResponseTest {
	JSONResponse<String> response;
	@Before
	public void setUp() throws Exception {
		response = new JSONResponse<String>();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToStringError() {
		response = new JSONResponse<String>(){
			public JSONObject toJSONObject(){
				JSONObject mock = Mockito.mock(JSONObject.class);
				Mockito.when(mock.toString()).thenThrow(new JSONException("error"));
				return mock;
			}
		};

		String json = response.toString();
		assertTrue(json.startsWith("com.robaone.api.json.JSONResponseTest"));

	}

	@Test
	public void testToString() {
		String json = response.toString();
		assertNotNull(json);
	}

	@Test
	public void testSetStatus() {
		assertEquals(0,response.getStatus());
		response.setStatus(1);
		assertEquals(1,response.getStatus());
	}

	@Test
	public void testSetError() {
		assertNull(response.getError());
		response.setError("This is my error");
		assertEquals("This is my error",response.getError());
	}

	@Test
	public void testAddError() {
		response.addError("field", "Error");
		assertEquals("Error",response.getErrors().get("field"));
	}

	@Test
	public void testSetErrors() {
		HashMap<String,String> errors = new HashMap<String,String>();
		response.setErrors(errors);
	}

	@Test
	public void testGetData() {
		assertNotNull(response.getData());
		response.addData("data");
		assertEquals("data",response.getData().get(0));
	}

	@Test
	public void testAddData() {
		response.addData("data");
		assertEquals("data",response.getData().get(0));
	}

	@Test
	public void testRecalcEmpty() {
		response.recalc();
		assertEquals(0,response.getTotalRows());
	}

	@Test
	public void testSetEndRow() {
		response.setEndRow(1);
		assertEquals(1,response.getEndRow());
		response.setEndRow(-1);
		assertEquals(0,response.getEndRow());
	}

	@Test
	public void testGetTotalRows() {
		assertEquals(0,response.getTotalRows());
		response.setTotalRows(-2);
		assertEquals(1,response.getTotalRows());
	}

	@Test
	public void testGetPage() {
		assertEquals(0,response.getPage());
		response.setPage(2);
		assertEquals(2,response.getPage());
	}

	@Test
	public void testGetProperties() {
		Properties props = response.getProperties();
		assertNotNull(props);
		props.setProperty("key", "value");
		assertEquals("value",response.getProperties().getProperty("key"));
	}

	@Test
	public void testAddArray () throws Exception {
		response.addArray(new JSONArray("[]"));
		response.addArray(new JSONArray("[\"item1\"]"));
		
	}
}
