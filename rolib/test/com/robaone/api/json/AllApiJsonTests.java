package com.robaone.api.json;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ JSONResponseTest.class })
public class AllApiJsonTests {

}
