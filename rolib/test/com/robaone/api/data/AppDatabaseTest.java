package com.robaone.api.data;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class AppDatabaseTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFileModified() {
		File file = Mockito.mock(File.class);
		Mockito.when(file.lastModified()).thenReturn(AppDatabase.getTimestamp().getTime());
		assertTrue(AppDatabase.fileModified(file));
	}
	
	@Test
	public void testFileNotModified() {
		File file = Mockito.mock(File.class);
		Mockito.when(file.lastModified()).thenReturn((long)0);
		assertFalse(AppDatabase.fileModified(file));
	}

}
