package com.robaone.api.business;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FieldValidatorTest {
	FieldValidator validator;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsURL() {
		String url = "http://www.google.com?search=This%20is%20my%20search";
		assertTrue(FieldValidator.isUrl(url));
	}

	@Test
	public void testIsURLOtherPort() {
		String url = "http://www.google.com:8080?search=This%20is%20my%20search";
		assertTrue(FieldValidator.isUrl(url));
	}
	
	@Test
	public void testIsNotURL() {
		String url = "ssl://www.google.com?search=This%20is%20my%20search";
		assertFalse(FieldValidator.isUrl(url));
	}
}
