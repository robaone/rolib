package com.robaone.api.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FieldValidatorTest.class })
public class AllAPIBusinessTests {

}
