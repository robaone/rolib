package com.robaone.dbase;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.robaone.api.business.XMLDocumentReader;

public class XSLConnectionBlockTest {
	XSLConnectionBlock<String> block;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		block = new XSLConnectionBlock<String>(null, 0, 0){

			@Override
			protected String bindRecord() throws Exception {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		block.setParameter("test", "value");
		block.setParameter("array", "[\"1\",\"2\"]");
		block.setParameter("object", "{\"field\":\"value\"}");
		Document doc = block.createParameterDocument();
		XMLDocumentReader reader = new XMLDocumentReader();
		reader.read(doc);
		System.out.println(reader.toString());
		assertEquals("value",reader.findXPathString("/parameters/object/field/text()"));
	}
	
	@Test
	public void testNotNormalized() throws Exception {
		block.setNormalize(false);
		block.setParameter("test", "value");
		block.setParameter("array", "[\"1\",\"2\"]");
		block.setParameter("object", "{\"field\":\"value\"}");
		Document doc = block.createParameterDocument();
		XMLDocumentReader reader = new XMLDocumentReader();
		reader.read(doc);
		System.out.println(reader.toString());
		assertFalse("value".equals(reader.findXPathString("/parameters/object/field/text()")));
	}
	
	@Test
	public void testGetParameterValue() throws Exception {
		block.setParameter("array", "[\"1\",\"2\"]");
		Node node = Mockito.mock(Node.class);
		Object val = block.getParameterValue(node,"array[0]", "int");
		assertEquals("1",val.toString());
		val = block.getParameterValue(node,"array[1]", "int");
		assertEquals("2",val.toString());
	}
	
	@Test
	public void testGetJSONObjectParameterValue() throws Exception {
		block.setParameter("object", "{\"count\":2}");
		Node node = Mockito.mock(Node.class);
		Object val = block.getParameterValue(node,"object", "string");
		assertEquals("{\"count\":2}",val);
	}
	
	@Test
	public void testGetJSONArrayParameterValue() throws Exception {
		block.setParameter("array", "[\"1\",\"2\"]");
		Node node = Mockito.mock(Node.class);
		Object val = block.getParameterValue(node,"array[0]", "int");
		assertEquals("1",val);
	}

	@Test
	public void testGetNodeParameterValue() throws Exception {
		Node node = Mockito.mock(Node.class);
		XMLDocumentReader reader = Mockito.mock(XMLDocumentReader.class);
		block.setQueryDocument(reader);
		Mockito.when(reader.findXPathString(node, ".")).thenReturn("value");
		Object val = block.getParameterValue(node, "name", "node");
		assertEquals("value",val.toString());
	}
}
