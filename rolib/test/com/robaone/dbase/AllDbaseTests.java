package com.robaone.dbase;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ConnectionBlockTest.class,
	QueryRunnerTest.class, XSLConnectionBlockTest.class })
public class AllDbaseTests {

}
