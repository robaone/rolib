package com.robaone.dbase;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class QueryRunnerTest {
	XMLQueryRunner runner;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ResourceQueryConfig config = new ResourceQueryConfig("test.xml");
		this.runner = new XMLQueryRunner("get",config);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetQueryFile() {
		InputStream in = runner.getQueryFile();
		assertNotNull(in);
	}
	
	@Test
	public void testGetQueryFileInPath() {
		runner.setQueryConfig(new ResourceQueryConfig("/com/robaone/dbase/test.xml"));
		InputStream in = runner.getQueryFile();
		assertNotNull(in);
	}
	
	@Test
	public void testGetQueryFileInPathNotFound() {
		runner.setQueryConfig(new ResourceQueryConfig("/test.xml"));
		InputStream in = runner.getQueryFile();
		assertNull(in);
	}

}
