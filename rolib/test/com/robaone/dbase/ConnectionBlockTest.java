package com.robaone.dbase;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.mysql.jdbc.PreparedStatement;

public class ConnectionBlockTest {
	ConnectionBlock block;
	int counter = 0;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		block = new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				// TODO Auto-generated method stub
				
			}
			
		};
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testExecuteQueryCounted() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		block.setResultSetIndex(3);
		Mockito.when(ps.execute()).thenReturn(false);
		Mockito.when(ps.getMoreResults()).thenReturn(true);
		Mockito.when(ps.getResultSet()).thenReturn(Mockito.mock(ResultSet.class));
		block.setPreparedStatement(ps);
		block.executeQuery();
		Mockito.verify(ps, Mockito.times(3)).getMoreResults();
		assertNotNull(block.getResultSet());
	}
	
	@Test
	public void testExecuteQueryCounted2() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		block.setResultSetIndex(3);
		Mockito.when(ps.execute()).thenReturn(true);
		Mockito.when(ps.getMoreResults()).thenReturn(true);
		Mockito.when(ps.getResultSet()).thenReturn(Mockito.mock(ResultSet.class));
		block.setPreparedStatement(ps);
		block.executeQuery();
		Mockito.verify(ps, Mockito.times(2)).getMoreResults();
		assertNotNull(block.getResultSet());
	}
	
	@Test
	public void testExecuteQuery() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		block.setResultSetIndex(3);
		Mockito.when(ps.execute()).thenReturn(false);
		Mockito.when(ps.getMoreResults()).thenReturn(true);
		Mockito.when(ps.getResultSet()).thenReturn(Mockito.mock(ResultSet.class));
		block.setPreparedStatement(ps);
		block.executeQuery();
		assertNotNull(block.getResultSet());
	}
}
