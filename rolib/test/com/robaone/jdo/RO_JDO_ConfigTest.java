package com.robaone.jdo;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.robaone.dbase.HDBConnectionManager;
import com.robaone.test.BaseTest;

public class RO_JDO_ConfigTest extends BaseTest {
	private static final String CONFIG_INI = "config.ini";
	RO_JDO_Config config;
	PrintStream output;
	TableListQuery query;
	HDBConnectionManager man;
	int exitcode;
	private ArrayList<String> list;
	@Before
	public void setUp() throws Exception {
		config = new RO_JDO_Config(){
			@Override
			protected void exit(int i){
				exitcode = i;
			}
		};
		this.setWorkingFolder(System.getProperty("working.folder", "/tmp"));
		output = Mockito.mock(PrintStream.class);
		query = Mockito.mock(TableListQuery.class);
		config.setTableListQuery(query);
		config.setOutputWriter(output);
		man = Mockito.mock(HDBConnectionManager.class);
		config.setConnectionManager(man);
		list = new ArrayList<String>();
		list.add("mytable");
		Mockito.when(query.getList()).thenReturn(list);
		Properties props = new Properties();
		FileWriter configFile = new FileWriter(new File(this.getResource(CONFIG_INI).toString()));
		props.store(configFile, "generated");
	}

	@After
	public void tearDown() throws Exception {
		this.deleteStagedFile(CONFIG_INI);
	}
	
	@Test
	public void testExecuteNoArgs() {
		String[] args = {};
		config.execute(args);
		assertEquals(0,exitcode);
	}
	
	@Test
	public void testExecuteArgs() throws Exception {
		String[] args = {
				this.getResource(CONFIG_INI).toString()
		};
		config.execute(args);
		assertEquals(0,exitcode);
	}

	@Test
	public void testShowUsage() {
		config.showUsage();
		Mockito.verify(output,Mockito.timeout(1)).println("Usage: [(Optional) config file]");
	}
	
	@Test
	public void testGetOutputWriter() {
		config.setOutputWriter(null);
		PrintStream out = config.getOutputWriter();
		assertNotNull(out);
		PrintStream out2 = config.getOutputWriter();
		assertEquals(out,out2);
	}

}
