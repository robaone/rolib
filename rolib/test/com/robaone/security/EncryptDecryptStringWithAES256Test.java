package com.robaone.security;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EncryptDecryptStringWithAES256Test {
	EncryptDecryptStringWithAES256 encryptor;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		encryptor = new EncryptDecryptStringWithAES256("1citizensrxefaefvawwefasfsfessaffasfe");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEncrypt() throws Exception {
		String pw = "password";
		String pwe = encryptor.encrypt(pw);
		System.out.println("encrypted = "+pwe);
	}

	@Test
	public void testDecrypt() throws Exception {
		String pwe = "M/fkpQtKW8OvyRHYMwap04DaiottLPgCZijea1njBqw=";
		String pw = encryptor.decrypt(pwe);
		System.out.println("password = "+pw);
	}

}
