package com.robaone.security;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MD5HashTest {
	MD5Hash hasher;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		hasher = new MD5Hash();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetHash() throws UnsupportedEncodingException, Exception {
		String teststr = "This is a test";
		String hash = hasher.getHash(teststr.getBytes("UTF-8"));
		System.out.println(hash);
		assertNotNull(hash);
	}
	
	@Test
	public void testGetCheckSum() throws UnsupportedEncodingException, Exception {
		String teststr = "This is a test";
		String hash = hasher.getCheckSum(teststr.getBytes("UTF-8"));
		System.out.println(hash);
		assertNotNull(hash);
	}

}
