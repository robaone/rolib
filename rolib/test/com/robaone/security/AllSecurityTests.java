package com.robaone.security;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ EncryptDecryptStringWithAES256Test.class,
		EncryptDecryptStringWithAES512Test.class,
		EncryptDecryptStringWithAESTest.class })
public class AllSecurityTests {

}
