package com.robaone.security;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EncryptDecryptStringWithAESTest {
	EncryptDecryptStringWithAES encrypter;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.encrypter = new EncryptDecryptStringWithAES("1citizensrx     ".getBytes());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEncrypt() {
		String pw = "password";
		String pwe = this.encrypter.encrypt(pw);
		System.out.println("Encrypted = "+pwe);
		assertEquals("O6NUr2jTO+P7ZL9WMQScJA==",pwe);
	}

	@Test
	public void testDecrypt() {
		String pwe = "O6NUr2jTO+P7ZL9WMQScJA==";
		String pw = this.encrypter.decrypt(pwe);
		System.out.println("Decrypted = "+pw);
		assertEquals("password",pw);
	}

}
