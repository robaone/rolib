package com.robaone.security;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EncryptDecryptStringWithAES512Test {
	private static final String PASSWORD = "password";
	private static final String PWE = "pucnqj3uCkngzqIQ3gCdLwiNqXme4rGX+sdFLVYxJ3kNLf5Pq2Nm6NnqVvzCJ3xFXNYHtP0gp8Z4SkPU+0VnKGrq46Wes0gU3NjRqZtXig0=";
	EncryptDecryptStringWithAES512 encryptor;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		encryptor = new EncryptDecryptStringWithAES512("1citizensrxekkjhlkjhkhuhuhifaefviojoiawwefasfsfessiuhihuhihuihaffasfe");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEncrypt() throws Exception {
		String pw = PASSWORD;
		String pwe = encryptor.encrypt(pw);
		System.out.println("encrypted = "+pwe);
		assertEquals(PWE,pwe);
	}

	@Test
	public void testDecrypt() throws Exception {
		String pwe = PWE;
		String pw = encryptor.decrypt(pwe);
		System.out.println("password = "+pw);
		assertEquals(PASSWORD,pw);
	}

}
