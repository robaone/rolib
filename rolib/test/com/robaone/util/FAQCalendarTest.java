package com.robaone.util;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class FAQCalendarTest {
	private FAQCalendar calendar;
	@Before
	public void setUp() throws Exception {
		setCalendar(new FAQCalendar());
	}

	@Test
	public void testFAQCalendar() {
		assertNotNull(getCalendar());
	}

	@Test
	public void testGetCalendarDay() {
		long calday = getCalendar().getCalendarDay();
		long expected = 2456401;
		assertTrue(calday > expected);
	}

	@Test
	public void testSetCalendarDay() {
		long day = getCalendar().getCalendarDay();
		System.out.println("Calendar day = "+day);
		assertTrue(day > 0);
		getCalendar().setCalendarDay(day + 1);
		day = getCalendar().getCalendarDay();
		System.out.println("Calendar day = "+day);
	}

	@Test
	public void testGetJulianDay() {
		long day = getCalendar().getJulianDay();
		assertTrue(day > 0);
	}

	@Test
	public void testGetJulianHour() {
		long min = getCalendar().getJulianHour();
		assertTrue(min > 0);
	}

	@Test
	public void testGetJulianMinute() {
		long min = getCalendar().getJulianMinute();
		assertTrue(min > 0);
	}

	@Test
	public void testGetJulianSecond() {
		long sec = getCalendar().getJulianSecond();
		assertTrue(sec > 0);
	}

	@Test
	public void testSetJulianDay() {
		getCalendar().setJulianDay(new java.util.Date().getTime());
		int hour = getCalendar().get(Calendar.HOUR);
		int minute = getCalendar().get(Calendar.MINUTE);
		int millisecond = getCalendar().get(Calendar.MILLISECOND);
		int second = getCalendar().get(Calendar.SECOND);
		
		
	}

	@Test
	public void testRound() {
		getCalendar();
		long rounded = FAQCalendar.round(10, 10);
		assertTrue(rounded > 0);
	}

	public FAQCalendar getCalendar() {
		return calendar;
	}

	public void setCalendar(FAQCalendar calendar) {
		this.calendar = calendar;
	}

}
