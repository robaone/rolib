package com.robaone.util;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ZipTest {
	Zip zip;
	@Before
	public void setUp() throws Exception {
		zip = new Zip();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		File folder = new File("/home/arobateau/temp/com");
		zip.setOutputFolder(new File("/home/arobateau/temp"));
		File zipfile = zip.zipFolder(folder);
		assertTrue(zipfile.exists());
	}

}
