package com.robaone.util;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StrmanipTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Pattern p = Pattern.compile("https://www[.]wrike[.]com/open[.]htm[?]id=([0-9]+)");
		String str = "https://www.wrike.com/open.htm?id=37853588";
		Matcher m = p.matcher(str);
		System.out.println("Matches = "+m.find());
		System.out.println(m.group(1));
	}

}
