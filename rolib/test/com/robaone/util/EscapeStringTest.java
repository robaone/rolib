package com.robaone.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EscapeStringTest {
	EscapeString escape;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		escape = new EscapeString();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() {
		String str = "This is my 'string'\nDeal with it \"honey\"!";
		escape.setString(str);
		escape.run();
		String out = escape.getOutput();
		System.out.println(out);
		assertEquals("This is my \\\'string\\\'\\nDeal with it \\\"honey\\\"!",out);
		System.out.println("This is my \'string\'\nDeal with it \"honey\"!");
	}

}
