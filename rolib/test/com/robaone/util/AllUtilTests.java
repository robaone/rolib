package com.robaone.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	BaseConverterTest.class,
	EscapeJavaStringTest.class,
	EscapeStringTest.class,
	FAQCalendarTest.class, 
	INIFileReaderTest.class,
	StrmanipTest.class})
public class AllUtilTests {

}
