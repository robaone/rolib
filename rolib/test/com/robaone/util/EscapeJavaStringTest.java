package com.robaone.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EscapeJavaStringTest {
	EscapeJavaString escape;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		escape = new EscapeJavaString();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() throws Exception {
		String str = "this is a 'test'\n";
		escape.setString(str);
		escape.run();
		String output = escape.getOutput();
		System.out.println(output);
		assertEquals("this is a \\\\'test\\\\'\\\\n",output);
	}

}
