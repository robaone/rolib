package com.robaone.util;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.robaone.test.BaseTest;

public class INIFileReaderTest extends BaseTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	private INIFileReader reader;
	@Before
	public void setUp() throws Exception {
		this.setWorkingFolder(System.getProperty("working.folder"));
		this.stageResource("inifile.ini", "inifile.ini");
		setReader(new INIFileReader(this.getWorkingFolder()+"/inifile.ini"));
	}

	@After
	public void tearDown() throws Exception {
		this.deleteStagedFile("inifile.ini");
	}

	@Test
	public void testINIFileReader() {
		try {
			assertNotNull(new INIFileReader(this.getWorkingFolder()+"/inifile.ini"));
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void testGetValue() {
		try {
			String propertyA = getReader().getValue("propertyA");
			assertEquals("A",propertyA);
			String propertyB = getReader().getValue("propertyB");
			assertEquals("B",propertyB);
			String propertyC = getReader().getValue("propertyC");
			assertEquals("A B \\C",propertyC);
			String propertyD = getReader().getValue("propertyD");
			assertEquals("A B\nC D\nE",propertyD);
			String propertyE = getReader().getValue("propertyE");
			assertEquals("Hello World!",propertyE);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		try{
			String failure = getReader().getValue("failure");
			fail("Should throw an error");
		}catch(Exception e){
			assertEquals(null,e.getMessage());
		}
	}

	@Test
	public void testParseFile() {
		try {
			getReader().ParseFile();
			fail("Should not be able to read");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testProccessString() {
		try {
			getReader().getBuffer().append("A=B");
			getReader().ProccessString();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	public void testProccessStringWithNewLine() {
		try {
			getReader().ProccessString();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	public INIFileReader getReader() {
		return reader;
	}

	public void setReader(INIFileReader reader) {
		this.reader = reader;
	}

}
