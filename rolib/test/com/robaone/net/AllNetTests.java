package com.robaone.net;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PingWebServiceTest.class, ROHttpRequestTest.class, ROQueryStringTest.class })
public class AllNetTests {

}
