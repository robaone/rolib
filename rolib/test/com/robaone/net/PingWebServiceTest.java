package com.robaone.net;

import static org.junit.Assert.*;

import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.robaone.api.business.XMLDocumentReader;
import com.robaone.test.BaseTest;

public class PingWebServiceTest extends BaseTest {
	PingWebService ping;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.setWorkingFolder(System.getProperty("working.folder"));
		ping = new PingWebService(){

			@Override
			public InputStream getXMLScript() {
				InputStream in = PingWebServiceTest.class.getResourceAsStream("pingtest.xml");
				return in;
			}
			
		};
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() throws Exception {
		ping.setConfig(new Properties());
		ping.getConfig().put("output.folder", this.getWorkingFolder());
		ping.initializeClient();
		XMLDocumentReader reader = new XMLDocumentReader();
		reader.read(ping.getXMLScript());
		Document script = reader.getDocument();
		ping.setScript(script);
		String[] args = {};
		ping.setArgs(args);
		ping.run();
		assertNull(ping.getException());
	}

	@Test
	public void testGetOutputPath() throws Exception {
		ping.setConfig(System.getProperties());
		String path = ping.getOutputPath();
		assertEquals("",path);
		System.setProperty("output.folder", "/path");
		path = ping.getOutputPath();
		assertEquals("/path/",path);
		System.setProperty("output.folder", "/path/");
		path = ping.getOutputPath();
		assertEquals("/path/",path);
		Properties props = new Properties();
		props.put("output.folder", "temp");
		ping.setConfig(props);
		path = ping.getOutputPath();
		assertEquals("temp/",path);
	}
}
