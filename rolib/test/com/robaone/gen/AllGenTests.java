package com.robaone.gen;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ROObjectFactoryTest.class })
public class AllGenTests {

}
