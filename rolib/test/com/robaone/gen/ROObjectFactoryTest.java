package com.robaone.gen;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ROObjectFactoryTest {
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstance() {
		ROObjectFactory<String> factory;
		factory = new ROObjectFactory<String>(){
			
			@Override
			protected void initialize(Object... inst) throws Exception {
				
			}
			
		};
		String str = factory.getInstance(String.class);
		assertNotNull(str);
		assertEquals("",str);
	}

	@Test
	public void testGetInstanceSettable() {
		ROObjectFactory<StringBuffer> factory;
		factory = new ROObjectFactory<StringBuffer>(){

			@Override
			protected void initialize(Object... inst) throws Exception {
				if(inst[0] instanceof StringBuffer){
					((StringBuffer)inst[0]).append("Hello World!");
				}
			}
			
		};
		StringBuffer buff = factory.getInstance(StringBuffer.class);
		assertNotNull(buff);
		assertEquals("Hello World!",buff.toString());
	}
}
