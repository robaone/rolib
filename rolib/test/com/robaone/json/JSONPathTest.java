package com.robaone.json;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JSONPathTest {
	JSONPath path;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		path = new JSONPath();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() {
		path.setJson("{\"test\":\"value\"}");
		path.setPath("test");
		path.run();
		assertNull(path.getException());
		String value = path.getValue();
		assertEquals("value",value);
	}
	
	@Test
	public void testRunNotFound() {
		path.setJson("{\"test\":\"value\",\"properties\":{\"head\":\"tail\"}}");
		path.setPath("properties.data");
		path.run();
		assertNotNull(path.getException());
		System.out.println(path.getException().getMessage());
	}

}
