package com.robaone.json;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CSVToJSONTest {
	CSVToJSON converter;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		converter = new CSVToJSON();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() throws Exception {
		InputStream in = this.getClass().getResourceAsStream("test.csv");
		assertNotNull(in);
		converter.setInputStream(in);
		converter.setCsv(converter.getInput());
		converter.run();
		assertNull(converter.getException());
		String json = converter.getJson();
		assertEquals("{\"csv\":[[\"Request ID\",\"Hash\"],[\"0\",\"0a653ed9774c8d45874f1403196ff431\"]]}",json);
	}

}
