package com.robaone.json;

import static org.junit.Assert.*;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class PROPSToJSONTest {
	PROPSToJSON converter;
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		converter = new PROPSToJSON();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() throws JSONException {
		converter.setProperties(System.getProperties());
		converter.run();
		assertNull(converter.getException());
		assertNotNull(converter.getJson());
		JSONObject json = new JSONObject(converter.getJson());
		assertTrue(json.length() > 0);
		assertNotNull(json.get(json.keys().next().toString()));
	}

}
