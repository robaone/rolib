package com.robaone.json;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CSVToJSONTest.class,
	JSONUtilsTest.class, 
	JSONToXMLTest.class, 
	JSONPathTest.class ,
	PROPSToJSONTest.class,
	XMLToJSONTest.class})
public class AllJsonTests {

}
