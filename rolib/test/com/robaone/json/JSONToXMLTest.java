package com.robaone.json;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JSONToXMLTest {
	JSONToXML converter;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		converter = new JSONToXML();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() {
		String str = "{\"field\":\"value\"}";
		converter.setJson(str);
		converter.run();
		String xml = converter.getXml();
		assertNull(converter.getException());
		assertEquals("<json><field>value</field></json>",xml);
	}

}
