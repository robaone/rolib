package com.robaone;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.robaone.api.business.AllAPIBusinessTests;
import com.robaone.api.data.AllApiDataTests;
import com.robaone.api.json.AllApiJsonTests;
import com.robaone.dbase.AllDbaseTests;
import com.robaone.gen.AllGenTests;
import com.robaone.json.AllJsonTests;
import com.robaone.net.AllNetTests;
import com.robaone.security.AllSecurityTests;
import com.robaone.util.AllUtilTests;
import com.robaone.xml.AllXMLTests;

@RunWith(Suite.class)
@SuiteClasses({
	AllAPIBusinessTests.class,
	AllUtilTests.class,
	AllApiDataTests.class,
	AllDbaseTests.class,
	AllGenTests.class,
	AllApiJsonTests.class,
	AllJsonTests.class,
	AllNetTests.class,
	AllSecurityTests.class,
	AllXMLTests.class})
public class AllTests {

}
