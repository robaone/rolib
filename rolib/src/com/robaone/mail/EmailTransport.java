package com.robaone.mail;

import java.io.EOFException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.robaone.api.business.FieldValidator;
import com.robaone.util.LineReader;

public class EmailTransport implements Runnable {
	private String serverName;
	private InputStream emailData;
	private int portNumber = 25;
	private String username;
	private String password;
	private boolean ssl;
	private boolean debug;
	private Exception exception;
	private HashMap<String,String> emailRecord;
	@Override
	public void run() {
		try{
			this.readFile();
			this.sendMail();
		}catch(Exception e){
			this.setException(e);
		}
	}

	protected void readFile() throws Exception {
		LineReader fileReader = new LineReader(this.getEmailData());
		StringBuffer body = new StringBuffer();
		HashMap<String,String> emailFields = new HashMap<String,String>();
		try{
			parseEmailFile(fileReader, body, emailFields);
		}catch(EOFException eof){
			emailFields.put("Body", body.toString());
		}
		this.setEmailRecord(emailFields);
	}


	protected void sendMail() throws MessagingException {
		String  subject = null, from = null;
		String[] to,cc = null,bcc = null;
		String mailhost = null;
		String mailer = "msgsend";
		from = this.getEmailRecord().get("From");
		to = this.getEmailRecord().get("To").split(",");
		try{
			cc = this.getEmailRecord().get("Cc").split(",");
		}catch(Exception e){}
		try{
			bcc = this.getEmailRecord().get("Bcc").split(",");
		}catch(Exception e){}
		subject = this.getEmailRecord().get("Subject");


		/*
		 * Process command line arguments.
		 */
		mailhost = this.getServerName();
		/*
		 * Initialize the JavaMail Session.
		 */
		Properties props = new Properties();
		String prefix = this.isSSL() ? "mail.smtps." : "mail.smtp.";
		if (mailhost != null){
			props.put(prefix+"host", mailhost);
		}
		props.put(prefix+"port", this.getPortNumber());
		if(passwordRequired()){
			props.put(prefix+"auth", true);
			props.put(prefix+"starttls.enable","true");
		}
		if(this.isSSL()){
			props.put("mail.smtps.socketFactory.port", this.getPortNumber());
			props.put("mail.smtps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtps.socketFactory.fallback", "false");
		}

		// Get a Session object
		Session session = null;
		if(passwordRequired()){
			session = Session.getDefaultInstance(props, new Authenticator() {

				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(getUsername(), getPassword());
				}

			});
		}else{
			session = Session.getInstance(props, null);
		}
		session.setDebug(this.isDebug());
		/*
		 * Construct the message and send it.
		 */
		Message msg = new MimeMessage(session);
		if (from != null){
			try{
				InternetAddress addr = new InternetAddress(from);
				msg.setFrom(addr);
			}catch(Exception e){
				msg.setFrom(new InternetAddress(from));
			}
		}else{
			msg.setFrom();
		}
		RecipientType rtype = Message.RecipientType.TO;
		setAddresses(to, msg, rtype);
		if (cc != null){
			setAddresses(cc, msg, Message.RecipientType.CC);
		}
		if (bcc != null){
			setAddresses(bcc, msg, Message.RecipientType.BCC);
		}
		msg.setSubject(subject);

		String text = this.getEmailRecord().get("Body");

		// We need a multipart message to hold the attachment.
		MimeBodyPart mbp1 = new MimeBodyPart();
		String bodyType = this.getEmailRecord().get("Type");
		if(bodyType.equalsIgnoreCase("text/plain")){
			mbp1.setText(text);
		}else{
			mbp1.setContent(text,bodyType);
		}
		MimeMultipart mp = new MimeMultipart();
		mp.addBodyPart(mbp1);

		String attachments = this.getEmailRecord().get("Attachments");
		String[] attachmentNames = {};
		try{
			attachmentNames = attachments.split(",");
		}catch(Exception e){}
		for(int index = 0;index < attachmentNames.length;index++){
			if(FieldValidator.exists(attachmentNames[index])){
				// Part two is attachment
				MimeBodyPart attachmentPart = new MimeBodyPart();
				String filename = attachmentNames[index];
				DataSource source = new FileDataSource(filename);
				attachmentPart.setDataHandler(new DataHandler(source));
				attachmentPart.setFileName(filename);
				mp.addBodyPart(attachmentPart);
			}
		}

		msg.setContent(mp);
		msg.setHeader("X-Mailer", mailer);
		msg.setSentDate(new Date());

		// send the thing off
		Transport transport = session.getTransport(this.isSSL() ? "smtps" : "smtp");
		transport.connect();
		msg.saveChanges();
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();
	}

	public boolean passwordRequired() {
		return FieldValidator.exists(this.getUsername()) && FieldValidator.exists(this.getPassword());
	}

	private void setAddresses(String[] to, Message msg, RecipientType rtype) throws MessagingException {
		for(String address : to){
			if(FieldValidator.isEmail(address)){
				InternetAddress addr = new InternetAddress(address);
				msg.setRecipient(rtype, addr);
			}
		}
	}

	public void parseEmailFile(LineReader fileReader, StringBuffer body,
			HashMap<String, String> emailFields) throws Exception {
		String line = null;
		boolean inBody = false;
		do{
			line = fileReader.ReadLine();
			String[] lineValues = line.split(":");
			String fieldName = lineValues[0];
			String value = null;
			if(lineValues.length > 1){
				value = line.substring(fieldName.length()+1).trim();
			}
			if(!inBody){
				if(fieldName.startsWith("----")){
					inBody = true;
				}else if(value != null){
					emailFields.put(fieldName,value);
				}
			}else{
				if(body.length() > 0){
					body.append("\n");
				}
				body.append(line);
			}
		}while(line != null);
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public InputStream getEmailData() {
		return emailData;
	}

	public void setEmailData(InputStream email) {
		this.emailData = email;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public HashMap<String,String> getEmailRecord() {
		return emailRecord;
	}

	public void setEmailRecord(HashMap<String,String> emailRecord) {
		this.emailRecord = emailRecord;
	}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSSL() {
		return ssl;
	}

	public void setSSL(boolean ssl) {
		this.ssl = ssl;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

}
