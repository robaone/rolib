package com.robaone.pool;

import java.util.Enumeration;
import java.util.Hashtable;

public abstract class ROObjectPool 
{
    private long expirationTime;
    private long lastCheckOut;

    private Hashtable<Object, Long> locked;
    private Hashtable<Object, Long> unlocked;
    
    private ROCleanUpThread cleaner;
	private boolean is_shutdown = false;
    
    protected ROObjectPool()
    {
        expirationTime = ( 1000 * 30 ); // 30 seconds   

        locked = new Hashtable<Object, Long>();         
        unlocked = new Hashtable<Object, Long>();
        
        setLastCheckOut(System.currentTimeMillis());

        cleaner = new ROCleanUpThread( this, expirationTime );
        cleaner.start();
    }
    
    abstract public Object create() throws Exception;
    
    abstract public boolean validate( Object o );
    
    abstract public void expire( Object o ) throws Exception;
    
    public void stop() throws Exception {}
    
    synchronized protected Object checkOut() throws Exception
    {
        long now = System.currentTimeMillis();      
        setLastCheckOut(now);
        Object o;              
        
        if( unlocked.size() > 0 )
        {
            Enumeration<Object> e = unlocked.keys();  
            
            while( e.hasMoreElements() )
            {
                o = e.nextElement();        

                if( validate( o ) )
                {
                    unlocked.remove( o );
                    locked.put( o, new Long( now ) );                
                    return( o );
                }
                else
                {
                    unlocked.remove( o );
                    expire( o );
                    o = null;
                }
            }
        }        
        
        o = create();        
        
        locked.put( o, new Long( now ) ); 
        return( o );
    }
    
    synchronized protected void checkIn( Object o )
    {
        if( o != null )
        {
            locked.remove( o );
            unlocked.put( o, new Long( System.currentTimeMillis() ) );
        }
    }
    
    synchronized void cleanUp() throws Exception
    {
        Object o;

        long now = System.currentTimeMillis();
        
        Enumeration<Object> e = unlocked.keys();  
        
        while( e.hasMoreElements() )
        {
            o = e.nextElement();        

            if( ( now - ( ( Long ) unlocked.get( o ) ).longValue() ) > expirationTime )
            {
                unlocked.remove( o );
                expire( o );
                o = null;
            }
        }
        
        System.gc();
    }
    
    synchronized void cleanUpAll() throws Exception{
    	Object o;

        Enumeration<Object> e = unlocked.keys();  
        
        while( e.hasMoreElements() )
        {
            o = e.nextElement();        

            unlocked.remove( o );
            expire( o );
            o = null;
        }
        
        stop();

        System.gc();
    }
    
	public void shutdown() throws Exception {
		this.setShutdown(true);
		this.cleaner.interrupt();
	}

	private void setShutdown(boolean b) {
		this.is_shutdown = b;
	}

	public long getLastCheckOut() {
		return lastCheckOut;
	}

	public void setLastCheckOut(long lastCheckOut) {
		this.lastCheckOut = lastCheckOut;
	}

	public boolean isShutdown() {
		return this.is_shutdown;
	}
}