package com.robaone.pool;

class ROCleanUpThread extends Thread
{
    private ROObjectPool pool;
    private long sleepTime;
    
    ROCleanUpThread( ROObjectPool pool, long sleepTime )
    {
        this.pool = pool;
        this.sleepTime = sleepTime;
    }
    
    public void run()
    {
        while( !this.pool.isShutdown() )
        {
            try
            {
                sleep( sleepTime );
            }
            catch( InterruptedException e )
            {
                if(this.pool.isShutdown()){
                	try {
						pool.cleanUpAll();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
                	break;
                }
            }         
            try {
				pool.cleanUp();
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
    }
}