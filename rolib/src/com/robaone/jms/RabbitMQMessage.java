package com.robaone.jms;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP.BasicProperties;

public class RabbitMQMessage {
	private String jmsType;
	private String jmsCorrelationId;
	private String jmsContentType;
	private String jmsReplyTo;
	private java.util.Date jmsTimeStamp;
	private ROJsonMessageMarshaller marshaller;
	private byte[] body;

	public BasicProperties getProperties() throws Exception {
		JSONObject msg = new JSONObject();
		msg.put("jmstype", this.getJmsType());
		msg.put("jmscorrelationid", this.getJmsCorrelationId());
		msg.put("jmscontent_type", this.getJmsContentType());
		msg.put("jmstimestamp", this.getJmsTimeStamp());
		msg.put("jmsreplyto", this.getJmsReplyTo());
		BasicProperties properties = this.getMarshaller().getProperties(msg);
		return properties;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	public String getJmsCorrelationId() {
		return jmsCorrelationId;
	}

	public void setJmsCorrelationId(String jmsCorrelationId) {
		this.jmsCorrelationId = jmsCorrelationId;
	}

	public String getJmsType() {
		return jmsType;
	}

	public void setJmsType(String jmsType) {
		this.jmsType = jmsType;
	}

	public java.util.Date getJmsTimeStamp() {
		return jmsTimeStamp;
	}

	public void setJmsTimeStamp(java.util.Date jmsTimeStamp) {
		this.jmsTimeStamp = jmsTimeStamp;
	}

	public String getJmsContentType() {
		return jmsContentType;
	}

	public void setJmsContentType(String jmsContentType) {
		this.jmsContentType = jmsContentType;
	}

	public ROJsonMessageMarshaller getMarshaller() {
		if(this.marshaller == null){
			this.setMarshaller(new ROJsonMessageMarshallerImpl());
		}
		return marshaller;
	}

	public void setMarshaller(ROJsonMessageMarshaller marshaller) {
		this.marshaller = marshaller;
	}

	public String getJmsReplyTo() {
		return jmsReplyTo;
	}

	public void setJmsReplyTo(String jmsReplyTo) {
		this.jmsReplyTo = jmsReplyTo;
	}
}
