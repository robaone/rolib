package com.robaone.jms;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class AMQPRabbitMQConnectionManager implements RabbitMQConnectionManager {
	private String host;
	private String username;
	private String password;
	@Override
	public Connection getConnection() throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		String uri = "amqp://"+this.getUsername()+":"+this.getPassword()+"@"+this.getHost()+"/"+this.getUsername();
		factory.setUri(uri);
		Connection connection = factory.newConnection();
		return connection;
	}

	@Override
	public void closeConnection(Connection con) throws Exception {
		con.close();
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
