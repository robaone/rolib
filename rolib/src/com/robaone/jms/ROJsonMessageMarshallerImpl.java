package com.robaone.jms;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.BasicProperties.Builder;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.robaone.json.JSONUtils;

public class ROJsonMessageMarshallerImpl implements ROJsonMessageMarshaller {

	@Override
	public JSONObject toJson(Delivery delivery) throws Exception {
		BasicProperties properties = delivery.getProperties();
		byte[] body = delivery.getBody();
		JSONObject message_jo = new JSONObject();
		message_jo.put("jmstype", properties.getType());
		message_jo.put("jmscorrelationid", properties.getCorrelationId());
		message_jo.put("jmscontent_type", properties.getContentType());
		message_jo.put("jmstimestamp", properties.getTimestamp());
		if("application/json".equals(properties.getContentType())){
			message_jo.put("body", new String(body,"utf-8"));
			message_jo = JSONUtils.normalize(message_jo);
		}else{
			message_jo.put("body", new String(body,"utf-8"));
		}
		return message_jo;
	}

	@Override
	public BasicProperties getProperties(JSONObject response) {
		Builder builder = new BasicProperties().builder();
		try{builder = builder.correlationId(response.getString("jmscorrelationid"));}catch(Exception e){}
		try{builder = builder.type(response.getString("jmstype"));}catch(Exception e){}
		try{builder = builder.contentType(response.getString("jmscontent_type"));}catch(Exception e){}
		try{builder = builder.replyTo(response.getString("jmsreplyto"));}catch(Exception e){}
		return builder.build();
	}

	@Override
	public byte[] getBody(JSONObject response) throws Exception {
		byte[] retval = null;
		String content_type = null;
		try{content_type = response.getString("jmscontent_type");}catch(Exception e){}
		if("application/json".equals(content_type)){
			JSONObject body = response.getJSONObject("body");
			retval = body.toString().getBytes();
		}else if("text/plain".equals(content_type)){
			retval = response.getString("body").getBytes();
		}else{
			retval = response.getString("body").getBytes();
		}
		return retval;
	}

}
