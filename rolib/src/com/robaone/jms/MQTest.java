package com.robaone.jms;

public class MQTest {
	private AMQPRabbitMQConnectionManager connectionManager;

	public AMQPRabbitMQConnectionManager getConnectionManager() {
		if(this.connectionManager == null){
			this.setConnectionManager(new AMQPRabbitMQConnectionManager());
		}
		return connectionManager;
	}

	public void setConnectionManager(AMQPRabbitMQConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}
}
