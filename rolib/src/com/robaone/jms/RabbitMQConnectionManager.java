package com.robaone.jms;

import com.rabbitmq.client.Connection;

public interface RabbitMQConnectionManager {
	public Connection getConnection() throws Exception;
	public void closeConnection(Connection con) throws Exception;
}
