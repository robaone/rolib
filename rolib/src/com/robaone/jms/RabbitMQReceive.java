package com.robaone.jms;

import java.io.File;
import java.io.PrintStream;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import com.robaone.jms.ROJsonMessageHandler;
import com.robaone.jms.ROJsonMessageMarshaller;
import com.robaone.jms.ROJsonMessageMarshallerImpl;
import com.robaone.jms.RabbitMQConnectionBlock;

public class RabbitMQReceive extends RabbitMQConnectionBlock {
	private String queue;
	private Channel channel;
	private PrintStream logWriter;
	private ROJsonMessageMarshaller marshaller;
	private ROJsonMessageHandler handler;
	@Override
	public void execute() throws Exception {
		QueueingConsumer consumer = newQueueingConsumer();
		this.writeLog(" [x] Awaiting RPC requests");
		while(true){
			try{
				handleRequest(consumer);
			}catch(com.rabbitmq.client.ShutdownSignalException e){
				this.setChannel(null);
				consumer = this.newQueueingConsumer();
			}
		}
	}
	protected void handleRequest(QueueingConsumer consumer)
			throws Exception {
		QueueingConsumer.Delivery delivery = consumer.nextDelivery();
		BasicProperties props = delivery.getProperties();
		JSONObject message_jo = this.getMarshaller().toJson(delivery);
		this.writeLog(" [x] Received: "+message_jo);
		this.getHandler().setRequest(message_jo);
		this.getHandler().run(this.getConnectionManager());
		JSONObject response_jo = this.getHandler().getResponse();
		JSONObject msg_jo = new JSONObject();
		msg_jo.put("jmscorrelationid", props.getCorrelationId());
		msg_jo.put("jmscontent_type", "application/json");
		msg_jo.put("jmstype", "response");
		msg_jo.put("jmsreplyto", props.getReplyTo());
		msg_jo.put("body", response_jo);
		if(props.getReplyTo() != null){
			if(response_jo.has("type") && "BytesMessage".equals(response_jo.getString("type"))){
				byte[] bytes = FileUtils.readFileToByteArray(new File(response_jo.getString("body")));
				msg_jo.put("jmscontent_type", "application/octet-stream");
				channel.basicPublish("", props.getReplyTo(), this.getMarshaller().getProperties(msg_jo), bytes);
			}else{
				channel.basicPublish("", props.getReplyTo(), this.getMarshaller().getProperties(msg_jo), this.getMarshaller().getBody(msg_jo));
			}
		}
		channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
	}
	protected void writeLog(String string) {
		if(this.getLogWriter() != null){
			this.getLogWriter().println(string);
		}
	}
	protected QueueingConsumer newQueueingConsumer() throws Exception {
		QueueingConsumer consumer = new QueueingConsumer(this.getChannel());
		channel.basicConsume(this.getQueue(), true, consumer);
		return consumer;
	}
	private Channel openChannel() throws Exception {
		Channel channel = this.getConnection().createChannel();
		channel.queueDeclare(this.getQueue(),false,false,false,null);
		channel.basicQos(1);
		return channel;
	}
	public Channel getChannel() throws Exception {
		if(this.channel == null){
			this.setChannel(this.openChannel());
		}
		return channel;
	}
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	public ROJsonMessageMarshaller getMarshaller() {
		if(this.marshaller == null){
			this.setMarshaller(new ROJsonMessageMarshallerImpl());
		}
		return marshaller;
	}
	public void setMarshaller(ROJsonMessageMarshaller marshaller) {
		this.marshaller = marshaller;
	}
	public PrintStream getLogWriter() {
		return logWriter;
	}
	public void setLogWriter(PrintStream logWriter) {
		this.logWriter = logWriter;
	}
	public ROJsonMessageHandler getHandler() {
		return handler;
	}
	public void setHandler(ROJsonMessageHandler handler) {
		this.handler = handler;
	}
	public void setQueue(String string) {
		this.queue = string;
	}
	public String getQueue() {
		return this.queue;
	}

}
