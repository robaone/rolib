package com.robaone.jms;

public interface MQConnectionBlock extends Runnable {
	public void run(RabbitMQConnectionManager connectionManager) throws Exception;
	public Exception getException();
}
