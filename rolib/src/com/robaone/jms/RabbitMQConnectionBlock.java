package com.robaone.jms;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.QueueingConsumer;

abstract public class RabbitMQConnectionBlock implements MQConnectionBlock {
	private Exception exception;
	private RabbitMQConnectionManager connectionManager;
	private Connection connection;
	private Channel channel;
	private String destinationQueue;
	private int timeout = 5000;
	private QueueingConsumer consumer;
	private String replyQueueName;
	private String contentType;
	private byte[] bytes;
	@Override
	public void run() {
		try{
			execute();
		}catch(Exception e){
			this.setException(e);
		}finally{
			try{
				this.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected String createReplyToQueue() throws IOException, Exception {
		if(this.replyQueueName == null){
			this.setReplyQueueName(this.getChannel().queueDeclare().getQueue());
		}
		return replyQueueName;
	}
	
	protected String createCorrelationId() {
		String corrId = java.util.UUID.randomUUID().toString();
		return corrId;
	}
	
	protected void send(RabbitMQMessage mq_message) throws IOException, Exception {
		this.getChannel().basicPublish("",this.getDestinationQueue(), mq_message.getProperties(), mq_message.getBody());
	}
	
	protected void close() throws Exception {
		this.getChannel().close();
		this.getConnectionManager().closeConnection(getConnection());
	}

	abstract public void execute() throws Exception;

	@Override
	public void run(RabbitMQConnectionManager connectionManager)
			throws Exception {
		this.setConnectionManager(connectionManager);
		this.setConnection(this.getConnectionManager().getConnection());
		this.run();
	}

	@Override
	public Exception getException() {
		return this.exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public RabbitMQConnectionManager getConnectionManager() {
		return connectionManager;
	}

	public void setConnectionManager(RabbitMQConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	public Connection getConnection() throws Exception {
		if(this.connection == null){
			this.setConnection(this.getConnectionManager().getConnection());
		}
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Channel getChannel() throws Exception {
		if(this.channel == null){
			this.setChannel(this.getConnection().createChannel());
		}
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public String getDestinationQueue() {
		return destinationQueue;
	}

	public void setDestinationQueue(String destinationQueue) {
		this.destinationQueue = destinationQueue;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public QueueingConsumer getConsumer() throws Exception {
		if(this.consumer == null){
			setConsumer(new QueueingConsumer(this.getChannel()));
			this.getChannel().basicConsume(this.getReplyQueueName(), true, getConsumer());
		}
		return consumer;
	}

	public void setConsumer(QueueingConsumer consumer) {
		this.consumer = consumer;
	}

	public String getReplyQueueName() {
		return replyQueueName;
	}

	public void setReplyQueueName(String replyQueueName) {
		this.replyQueueName = replyQueueName;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
