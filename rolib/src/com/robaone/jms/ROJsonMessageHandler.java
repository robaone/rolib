package com.robaone.jms;

import org.json.JSONObject;

public interface ROJsonMessageHandler {

	public void setRequest(JSONObject message_jo);

	public void run(RabbitMQConnectionManager connectionManager);

	public JSONObject getResponse();

}
