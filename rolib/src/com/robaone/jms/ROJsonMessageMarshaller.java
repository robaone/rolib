package com.robaone.jms;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.QueueingConsumer.Delivery;

public interface ROJsonMessageMarshaller {

	JSONObject toJson(Delivery delivery) throws Exception;

	BasicProperties getProperties(JSONObject response);

	byte[] getBody(JSONObject response) throws Exception;

}
