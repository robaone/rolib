package com.robaone.jms;

import java.io.PrintStream;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import com.robaone.jms.ROJsonMessageMarshaller;
import com.robaone.jms.ROJsonMessageMarshallerImpl;
import com.robaone.jms.RabbitMQConnectionBlock;

public final class RabbitMQSend extends RabbitMQConnectionBlock {
	private String queue;
	private JSONObject message;
	private JSONObject response;
	private ROJsonMessageMarshaller marshaller;
	private PrintStream logWriter;
	@Override
	public void execute() throws Exception {
		Channel channel = this.getChannel();
		String replyQueueName = channel.queueDeclare().getQueue();
		QueueingConsumer consumer = new QueueingConsumer(channel);
		channel.basicConsume(replyQueueName, true, consumer);
		
		String corrId = java.util.UUID.randomUUID().toString();
		BasicProperties props = new BasicProperties.Builder().correlationId(corrId).contentType("application/json").replyTo(replyQueueName).build();
		
		String message = this.getMessage().toString();
		channel.basicPublish("", this.getQueue(), props, message.getBytes());
		this.writeLog(" [x] Send '" + message + "'");
		
		while (true) {
			QueueingConsumer.Delivery delivery = null;
			if(this.getTimeout() > 0){
				delivery = consumer.nextDelivery(this.getTimeout());
			}else{
				delivery = consumer.nextDelivery();
			}
			BasicProperties properties = delivery.getProperties();
			String correlationId = properties.getCorrelationId();
			if(correlationId.equals(corrId)){
				String response = this.getMarshaller().toJson(delivery).toString();
				this.writeLog(" [x] Received: "+response);
				break;
			}
		}
	}
	protected void writeLog(String string) {
		if(this.getLogWriter() != null){
			this.getLogWriter().println(string);
		}
	}
	
	public JSONObject getMessage() {
		return message;
	}
	public void setMessage(JSONObject message) {
		this.message = message;
	}
	public JSONObject getResponse() {
		return response;
	}
	public void setResponse(JSONObject response) {
		this.response = response;
	}
	public ROJsonMessageMarshaller getMarshaller() {
		if(this.marshaller == null){
			this.setMarshaller(new ROJsonMessageMarshallerImpl());
		}
		return marshaller;
	}
	public void setMarshaller(ROJsonMessageMarshaller marshaller) {
		this.marshaller = marshaller;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public PrintStream getLogWriter() {
		return logWriter;
	}
	public void setLogWriter(PrintStream logWriter) {
		this.logWriter = logWriter;
	}
}