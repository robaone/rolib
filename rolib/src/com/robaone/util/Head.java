package com.robaone.util;

import java.io.File;

/**
 * <pre>   Copyright 2013 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class Head {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		if(args.length > 0){
			File file = new File(args[0]);
			LineReader lineReader = new LineReader(file);
			for(int i = 0; i < 10;i++){
				String line = lineReader.ReadLine();
				System.out.println(line);
			}
		}else{
			showUsage();
		}
	}

	private static void showUsage() {
		System.out.println("Usage: [filename]");
	}

}
