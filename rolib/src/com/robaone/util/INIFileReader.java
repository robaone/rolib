package com.robaone.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class INIFileReader {
	protected Reader m_file;
	protected StringBuffer stringBuffer = new StringBuffer();
	protected Hashtable<String,String> m_inientries = new Hashtable<String,String>();
	public INIFileReader(String filename) throws Exception {
		this.m_file = new FileReader(filename);
		this.ParseFile();
		this.m_file.close();
	}
	public INIFileReader(InputStream resourceAsStream) throws Exception {
		this.m_file = new InputStreamReader(resourceAsStream);
		this.ParseFile();
		this.m_file.close();
	}
	public INIFileReader(File file) throws Exception {
		this.m_file = new FileReader(file);
		this.ParseFile();
		this.m_file.close();
	}
	public String getValue(String key) throws Exception {
		return this.m_inientries.get(key).toString().trim();
	}
	
	public void addProperties(Properties properties){
		Enumeration<Object> keys = properties.keys();
		while(keys.hasMoreElements()){
			Object key = keys.nextElement();
			this.m_inientries.put(key.toString(), properties.getProperty(key.toString()));
		}
	}
	
	protected StringBuffer getBuffer(){
		return this.stringBuffer;
	}
	protected void ParseFile() throws Exception {
		for(int i = this.m_file.read();i != -1;i = this.m_file.read()){
			char c = (char)i;
			if(isEndOfLine(c)){
				this.ProccessString();
				this.clearBuffer();
			}else{
				concatenate(c);
			}
		}
		if(stringBuffer.length() > 0){
			try{
				this.ProccessString();
			}catch(Exception e){}
		}
	}
	private void clearBuffer() {
		stringBuffer = new StringBuffer();
	}
	private void concatenate(char c) {
		if(c == '\n' && (stringBuffer.toString().endsWith("\\") || stringBuffer.toString().endsWith("\r"))){
			stringBuffer.replace(stringBuffer.toString().length()-1, stringBuffer.toString().length(), "\n");
		}else{
			stringBuffer.append(c);
		}
	}
	private boolean isEndOfLine(char c) {
		if(c == '\n' && (stringBuffer.toString().trim().endsWith("\\"))){
			String temp = stringBuffer.toString().trim();
			stringBuffer = new StringBuffer();
			stringBuffer.append(temp.substring(0, temp.length()-1));
			return false;
		}else{
			return c == '\n';
		}
	}
	protected void ProccessString() throws Exception {
		String key = stringBuffer.toString().split("=")[0];
		String value = stringBuffer.substring(stringBuffer.toString().indexOf('=')+1);
		this.m_inientries.put(key, value);
	}
	protected class INIValue {
		private String m_key;
		private String m_value;
		public INIValue(String key,String value){
			this.m_key = key;
			this.m_value = value;
		}
		public String getKey(){
			return this.m_key;
		}
		public String getValue(){
			return this.m_value;
		}
		public void setKey(String key){
			this.m_key = key;
		}
		public void setValue(String value){
			this.m_value = value;
		}
	}
	public String[] getKeys() {
		return this.m_inientries.keySet().toArray(new String[0]);
	}
}