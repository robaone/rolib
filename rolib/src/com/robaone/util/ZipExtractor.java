package com.robaone.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipExtractor extends ZipIterator {
	private String destinationFolder;
	public ZipExtractor(File file) throws FileNotFoundException {
		super(file);
	}
	public ZipExtractor(FileInputStream fin) {
		super(fin);
	}
	public ZipExtractor(ZipInputStream zin) {
		super(zin);
	}

	@Override
	protected void process(ZipEntry entry) throws Exception {
		String entryFolder = getEntryFolder(entry);
		String entryFileName = getEntryFileName(entry);
		this.createDestinationFolder(entryFolder);
		String destinationFileName = entryFolder+System.getProperty("file.separator")+entryFileName;
		writeToFile(destinationFileName);
	}
	protected void writeToFile(String entryFileName)
			throws FileNotFoundException, IOException {
		InputStream in = null;
		BufferedOutputStream fout = null;
		try{
			in = this.m_zip;
			fout = new BufferedOutputStream(new FileOutputStream(new File(entryFileName)));
			byte[] buffer = new byte[256];
			for(int read = in.read(buffer);read > -1;read = in.read(buffer)){
				fout.write(buffer, 0, read);
			}
		}finally{
			if(fout != null){
				fout.flush();
				fout.close();
			}
		}
	}

	protected String getEntryFolder(ZipEntry entry) throws Exception {
		final String name = entry.getName();
		String entryFolder = new File(name).getParent();
		String folder = this.getDestinationFolder()+System.getProperty("file.separator")+entryFolder;
		return folder;
	}
	protected String getEntryFileName(ZipEntry entry) throws Exception {
		String fileName = entry.getName();
		File file = new File(fileName);
		return file.getName();
	}
	protected boolean createDestinationFolder(String entryFolder) {
		File folder = new File(entryFolder);
		return folder.mkdirs();
	}
	@Override
	protected boolean excluded(ZipEntry entry) throws Exception {
		return false;
	}

	public String getDestinationFolder() {
		return destinationFolder;
	}

	public void setDestinationFolder(String destinationFolder) {
		this.destinationFolder = destinationFolder;
	}
}