package com.robaone.util;

import java.io.EOFException;

import org.apache.commons.lang3.StringEscapeUtils;

public class EscapeString implements Runnable {
	private Exception exception;
	private String string;
	private String output;
	public static void main(String[] args) {
		try{
			EscapeString parser = new EscapeString();
			parser.setString(parser.getInput());
			parser.run();
			if(parser.getException() != null){
				throw parser.getException();
			}
			System.out.println(parser.getOutput());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	protected String getInput() throws Exception {
		LineReader lr = null;
		StringBuffer buffer = new StringBuffer();
		try{
			lr = new LineReader(System.in);
			do{
				String line = lr.ReadLine();
				if(line.equals(".")){
					break;
				}else{
					if(buffer.length() > 0){
						buffer.append("\n");
					}
					buffer.append(line);
				}
			}while(true);
		}catch(EOFException eof){
		}
		return buffer.toString();
	}

	@Override
	public void run() {
		try{
			String str = StringEscapeUtils.escapeJava(this.getString());
			str = str.replace("'", "\\'");
			//str = str.replaceAll("\\\\", "\\\\\\\\");
			this.setOutput(str);
		}catch(Exception e){
			this.setException(e);
		}
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public String getString() {
		return string;
	}

	public void setString(String xml) {
		this.string = xml;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

}