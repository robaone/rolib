package com.robaone.util;

import java.io.*;
import java.util.UUID;

public class Zip {
	static final int BUFFER = 2048;
	private File outputFolder;
	
	public File zipFolder(File folder) throws Exception {
		File zipfile = null;
		ByteArrayOutputStream berr = new ByteArrayOutputStream();
		ProcessRunner zip_process = new ProcessRunner();
		zip_process.setErrorWriter(new PrintWriter(berr));
		String[] command = {
				"/usr/bin/zip",
				"-r",
				getOutputZipFilename(),
				"."
		};
		zip_process.setWorkingFolder(folder.toString());
		zip_process.setCommand(command);
		zip_process.run();
		if(zip_process.getException() != null){
			throw zip_process.getException();
		}else if(zip_process.getExitCode() != 0){
			throw new Exception(berr.toString());
		}
		zipfile = new File(command[2]);
		return zipfile;
	}

	protected String getOutputZipFilename() {
		StringBuffer filename = new StringBuffer();
		if(this.getOutputFolder() != null){
			filename.append(this.getOutputFolder().toString());
			filename.append("/");
		}
		filename.append(UUID.randomUUID().toString()+".zip");
		return filename.toString();
	}

	public File getOutputFolder() {
		return outputFolder;
	}

	public void setOutputFolder(File outputFolder) {
		this.outputFolder = outputFolder;
	}
} 