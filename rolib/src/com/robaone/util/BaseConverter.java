package com.robaone.util;

import java.lang.Math;

public class BaseConverter {

    private char symbols[] = new char[] 
    		{ '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E',
    	      'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T',
    	      'U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i',
    	      'j','k','l','m','n','o','p','q','r','s','t','u','v','w','x',
    	      'y','z'};

    public static void main ( String args[] )
    {
                 BaseConverter converter = new BaseConverter ();
        System.out.println( converter.convert ( 23, converter.symbols.length ));
    }

    public String convert ( int number, int base )
    {
        return convert(number, base, 0, "" );
    }

    private String convert ( int number, int base, int position, String result )
    {
    	
        if ( number < Math.pow(base, position + 1) )
        {
            return symbols[(number / (int)Math.pow(base, position))] + result;
        }
        else
        {
            int remainder = (number % (int)Math.pow(base, position + 1));
            return convert (  number - remainder, base, position + 1, symbols[remainder / (int)( Math.pow(base, position) )] + result );
        }
    }
    
    public void setSymbols(char[] symbols){
    	this.symbols = symbols;
    }

	public String convert(int i) {
		return this.convert(i,symbols.length);
	}

	public int revert(String string) {
		int reverted = this.revert(string,string.length()-1,0);
		return reverted;
	}

	protected int revert(String string, int position,int value) {
		if(position < 0){
			return value;
		}else{
			value += getCharValue(string.charAt(position))*Math.pow(symbols.length,string.length()-position-1);
			value = this.revert(string,--position,value);
		}
		return value;
	}

	private int getCharValue(char charAt) {
		for(int i = 0; i < this.symbols.length;i++){
			if(this.symbols[i] == charAt){
				return i;
			}
		}
		return 0;
	}
}