package com.robaone.util;


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.EOFException;
import java.text.*;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class Strmanip {

	public Strmanip() {
	}

	// Main entry point for command line use
	public static void main(String[] args) {
		Strmanip main = new Strmanip();
		Pattern p = null;
		Integer group = null;
		Matcher m = null;
		try{
			if(args.length < 2){
				throw new Exception("Not enough arguments");
			}else{
				int index = 0;
				for(int i = 0; i < args.length;i+=2){
					switch(args[i]){
					case "-regex":
						p = Pattern.compile(args[i+1]);
						break;
					case "-group":
						group = new Integer(args[i+1]);
						break;
					default:
						throw new Exception("Invalid option, '"+args[i]+"'");
					}
				}
				if(p == null){
					throw new Exception("-regex not set");
				}else{
					LineReader reader = new LineReader(System.in);
					try{
						do{
							String line = reader.ReadLine();
							m = p.matcher(line);
							if(m.find()){
								if(group == null){
									System.out.println("true");
									System.exit(0);
								}else{
									String str = m.group(group);
									System.out.println(str);
								}
							}else if(group == null){
								System.out.println("false");
							}else{
								System.exit(1);
							}
						}while(true);
					}catch(EOFException eof){

					}
				}
			}
		}catch(Exception e){
			System.out.println("Usage: [options]");
			System.out.print  ("  -regex [regular expression] (Optional -group [group number]): The regular expression that you want to match");
			System.out.println("                                                                The group text that you want to return.  If ");
			System.out.println("                                                                this is not included, the response will be ");
			System.out.println("                                                                'true' of 'false'.");
			e.printStackTrace();
			System.exit(1);
		}
	}
	// Split string by delimitor and store in Vector.  Remove all white space
	public static int split(char delim, String val,Vector<String> list){
		return Strmanip.split(delim,val,list,true);
	}
	// Split string by delimitor and store in Vector.  Trimming optional
	public static int split(char delim, String val,Vector<String> list,boolean trim){
		char[] word = val.toCharArray();
		String token = "";
		list.clear();
		for(int i = 0;i < val.length();i++){
			if(word[i] == delim){
				if(trim == false || token.length() > 0){
					list.add(token.trim());
					token = "";
				}
			}else{
				token += word[i];
			}
		}
		if(trim == false){
			list.add(token.trim());
		}else if(token.length() > 0){
			list.add(token.trim());
		}
		return list.size();
	}

	// Split 2 No vector required
	public static Vector<String> split(char delim,String val){
		Vector<String> list = new Vector<String>();
		Strmanip.split(delim,val,list);
		return list;
	}
	// Parse date fields
	public static long parseDate(String str) throws Exception{
		SimpleDateFormat df = new SimpleDateFormat("M/d/y");
		return df.parse(str).getTime();
	}
	// Format date fields
	public static String formatDate(long date) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy");
		return df.format(new java.util.Date(date));
	}
	// Format date time fields
	public static String formatDateTime(long date) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy hh:mm a");
		return df.format(new java.util.Date(date));
	}
	// Remove white space from strings
	public static String removewhitespace(String str) throws Exception {
		String retval = "";
		for(int i = 0; i < str.length();i++){
			if((int)str.charAt(i) <= 32 && (int)str.charAt(i) != 10){
				continue;
			}else{
				retval += str.charAt(i);
			}
		}
		return retval;
	}
	// Replace a character from a string
	public static String replacechar(String str,char c,char r) throws Exception {
		String retval = "";
		for(int i = 0; i < str.length();i++){
			if(str.charAt(i) == c){
				retval += r;
			}else{
				retval += str.charAt(i);
			}
		}
		return retval;
	}
	// Replace a subsection of a string
	public static String replaceAll(String str,String pattern,String replacement) throws Exception {
		StringBuffer retval = new StringBuffer();
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		int cursor = 0;
		while(m.find()){
			int start = m.start();
			int end = m.end();
			retval.append(str.substring(cursor,start));
			retval.append(replacement);
			cursor = end;
		}
		if(cursor < str.length()){
			retval.append(str.substring(cursor,str.length()));
		}
		return retval.toString();
	}
	public static String limit(String string, int maxlength) {
		if(string != null){
			if(string.length() <= maxlength){
				return string;
			}else{
				return string.substring(0,maxlength);
			}
		}else{
			return null;
		}
	}
	public static String word(String str,char delim,int index) {
		try{
			return split(delim,str).get(index);
		}catch(Exception e){
			return "";
		}
	}
}