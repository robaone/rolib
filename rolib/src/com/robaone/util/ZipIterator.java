package com.robaone.util;
import java.io.ByteArrayOutputStream; 
import java.io.File; 
import java.io.FileInputStream; 
import java.io.FileNotFoundException; 
import java.io.IOException; 
import java.io.InputStream; 
import java.util.ArrayList; 
import java.util.zip.ZipEntry; 
import java.util.zip.ZipInputStream; 
  
/** 
 * Zip File Iterator 
 */
public abstract class ZipIterator { 
    ZipInputStream m_zip; 
    public ZipIterator(ZipInputStream in){ 
        this.m_zip = in; 
    } 
    public ZipIterator(InputStream in){ 
        this.m_zip = new ZipInputStream(in); 
    } 
      
    public ZipIterator(File file) throws FileNotFoundException{ 
        this.m_zip = new ZipInputStream(new FileInputStream(file)); 
    } 
    public static int countFiles(File file) throws Exception { 
        return ZipIterator.countFiles(new ZipInputStream(new FileInputStream(file))); 
    } 
    public static int countFiles(InputStream in) throws Exception { 
        return ZipIterator.countFiles(new ZipInputStream(in)); 
    } 
    public static int countFiles(ZipInputStream in) throws Exception { 
        final ArrayList<Integer> retval = new ArrayList<Integer>(); 
        new ZipIterator(in){ 
            int total = 0; 
            @Override
            protected void process(ZipEntry entry) throws Exception { 
                total++; 
            } 
  
            @Override
            protected boolean excluded(ZipEntry entry) throws Exception { 
                return entry.isDirectory(); 
            } 
              
            @Override
            protected void after(){ 
                retval.add(total); 
            } 
              
        }.run(); 
        return retval.size() > 0 ? retval.get(0) : 0; 
    } 
    public void run() throws Exception { 
        try{ 
            before(); 
            for(ZipEntry entry = m_zip.getNextEntry();entry != null;entry = m_zip.getNextEntry()){ 
                if(excluded(entry)){ 
                    continue; 
                } 
                process(entry); 
            } 
        }finally{ 
            try{after();}catch(Exception e){} 
            m_zip.close(); 
        } 
    } 
    protected InputStream getEntryInputStream() throws Exception { 
        EntryInputStream cp = new EntryInputStream(m_zip); 
        return cp; 
    } 
    protected void before() throws Exception {} 
    protected void after() throws Exception {} 
    abstract protected void process(ZipEntry entry) throws Exception; 
    abstract protected boolean excluded(ZipEntry entry) throws Exception; 
    public class EntryInputStream extends InputStream { 
  
        /* (non-Javadoc) 
         * @see java.io.InputStream#read() 
         */
        private ByteArrayOutputStream m_copy = new ByteArrayOutputStream(); 
        private InputStream m_is; 
        public EntryInputStream(InputStream is){ 
            this.m_is = is; 
        } 
        public int read() throws IOException { 
            int got = this.m_is.read(); 
            if(got > -1){ 
                this.m_copy.write((byte)got); 
            } 
            return got; 
        } 
        public int read(byte[] buff,int off, int len) throws IOException{ 
            return this.m_is.read(buff,off,len); 
        } 
        public void close(){ 
              
        } 
    } 
}