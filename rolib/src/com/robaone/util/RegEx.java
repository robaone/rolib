package com.robaone.util;

import java.io.EOFException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

public class RegEx implements Runnable {
	private Exception exception;
	private String pattern;
	private String string;
	private String output;
	public static void main(String[] args) {
		try{
			RegEx parser = new RegEx();
			if(args.length < 1){
				throw new Exception("Not enough arguments");
			}
			parser.setString(parser.getInput());
			parser.setPattern(args[0]);
			parser.run();
			if(parser.getException() != null){
				throw parser.getException();
			}
			System.out.println(parser.getOutput());
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Usage: [Java Regex pattern]");
			System.exit(1);
		}
	}

	protected String getInput() throws Exception {
		LineReader lr = null;
		StringBuffer buffer = new StringBuffer();
		try{
			lr = new LineReader(System.in);
			do{
				String line = lr.ReadLine();
				if(line.equals(".")){
					break;
				}else{
					if(buffer.length() > 0){
						buffer.append("\n");
					}
					buffer.append(line);
				}
			}while(true);
		}catch(EOFException eof){
		}
		return buffer.toString();
	}

	@Override
	public void run() {
		try{
			Pattern p = Pattern.compile(getPattern());
			Matcher m = p.matcher(this.getString());
			boolean matches = m.matches();
			JSONObject result = new JSONObject();
			result.put("matches", matches);
			JSONArray groups = new JSONArray();
			result.put("groups", groups);
			if(matches){
				for(int i = 0; i <= m.groupCount();i++){
					JSONObject group_info = new JSONObject();
					group_info.put("group", i);
					group_info.put("start", m.start(i));
					group_info.put("end",m.end(i));
					group_info.put("value", m.group(i));
					groups.put(group_info);
				}
			}
			this.setOutput(result.toString());
		}catch(Exception e){
			this.setException(e);
		}
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public String getString() {
		return string;
	}

	public void setString(String xml) {
		this.string = xml;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
