package com.robaone.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONArray;

import com.robaone.api.business.FieldValidator;

public class ProcessRunner implements Runnable {
	private int exitCode;
	private String[] command;
	private String workingFolder;
	private Exception exception;
	private PrintWriter logWriter;
	private PrintWriter printWriter;
	private PrintWriter errorWriter;
	private InputStream stdin;
	private Process process;
	protected void runProcess() throws Exception {
		this.writeDebug("runProcess('"+new JSONArray(command).toString()+"')");
		String line;
		InputStream stderr = null;
		InputStream stdout = null;

		// launch EXE and grab stdin/stdout and stderr
		String[] cmd = this.getCommand();
		File pathToExecutable = new File( cmd[0] );
		cmd[0] = pathToExecutable.getAbsolutePath();
		ProcessBuilder builder = new ProcessBuilder( cmd );
		if(FieldValidator.exists(getWorkingFolder())){
			builder.directory( new File(this.getWorkingFolder()).getAbsoluteFile() ); // this is where you set the root folder for the executable to run with
		}
		process =  builder.start();
		stderr = process.getErrorStream ();
		stdout = process.getInputStream ();
		
		if(this.getStdIn() != null){
			IOUtils.copy(this.getStdIn(), process.getOutputStream());
			process.getOutputStream().flush();
			process.getOutputStream().close();
		}

		// clean up if any output in stdout
		BufferedReader brCleanUp =
				new BufferedReader (new InputStreamReader (stdout));

		while ((line = brCleanUp.readLine ()) != null) {
			this.writeOut(line);
		}

		brCleanUp.close();

		// clean up if any output in stderr
		brCleanUp =
				new BufferedReader (new InputStreamReader (stderr));
		while ((line = brCleanUp.readLine ()) != null) {
			this.writeErr(line);
		}
		brCleanUp.close();
		int exitCode = process.waitFor();
		this.setExitCode(exitCode);
	}

	public OutputStream getProcessOutputStream() {
		if(this.process != null){
			return this.process.getOutputStream();
		}else{
			return null;
		}
	}
	private void writeOut(String line) {
		try{
			this.getPrintWriter().println(line);
			this.getPrintWriter().flush();
		}catch(Exception e){}
	}
	
	private void writeErr(String line) {
		try{
			this.getErrorWriter().println(line);
			this.getErrorWriter().flush();
		}catch(Exception e){}
	}

	protected void writeDebug(String string) {
		try{
			this.getLogWriter().println(string);
			this.getLogWriter().flush();
		}catch(Exception e){}
	}

	@Override
	public void run() {
		try {
			this.runProcess();
		} catch (Exception e) {
			this.setException(e);
		}
	}

	public int getExitCode() {
		return exitCode;
	}

	public void setExitCode(int exitCode) {
		this.exitCode = exitCode;
	}

	public String[] getCommand() {
		return command;
	}

	public void setCommand(String[] command) {
		this.command = command;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public PrintWriter getPrintWriter() {
		return printWriter;
	}

	public void setPrintWriter(PrintWriter printWriter) {
		this.printWriter = printWriter;
	}

	public PrintWriter getErrorWriter() {
		return errorWriter;
	}

	public void setErrorWriter(PrintWriter errorWriter) {
		this.errorWriter = errorWriter;
	}

	public PrintWriter getLogWriter() {
		return logWriter;
	}

	public void setLogWriter(PrintWriter logWriter) {
		this.logWriter = logWriter;
	}

	public void setStdIn(ByteArrayInputStream bin) {
		this.stdin = bin;
	}
	
	protected InputStream getStdIn() {
		return this.stdin;
	}

	public String getWorkingFolder() {
		return workingFolder;
	}

	public void setWorkingFolder(String workingFolder) {
		this.workingFolder = workingFolder;
	}
}
