package com.robaone.util;

import java.sql.Connection;
import java.util.HashMap;

import com.robaone.api.data.AppDatabase;
import com.robaone.dbase.HDBConnectionManager;

/**
 * <pre>   Copyright 2013 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
abstract public class BaseProgram {
	public class Output{
		private int exitCode;
		private String status;
		public Output(){

		}
		public int getExitCode() {
			return exitCode;
		}
		public void setExitCode(int exitCode) {
			this.exitCode = exitCode;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		protected void setAsError() {
			if(this.getExitCode() == 0){
				this.setExitCode(1);
			}
		}

	}
	private Output output;
	private INIFileReader ini;
	private HDBConnectionManager connectionManager;
	protected java.util.HashMap<String,String> m_mnemonics = new HashMap<String,String>();
	
	public BaseProgram(){
		this.setOutput(new Output());
	}
	public Output run(String args[]){
		try {
			this.processArguments(args);
			this.execute();
		} catch (Exception e) {
			e.printStackTrace();
			this.writeLog(e.getMessage());
			showUsage();
			getOutput().setStatus(e.getMessage());
			getOutput().setAsError();
		}
		this.writeLog("Exit code ("+this.getOutput().getExitCode()+")");
		return getOutput();
	}
	public void writeLog(String message) {
		System.out.println(message);
	}
	private void showUsage() {
		System.out.println("Usage: " + this.getClass().getName()+" [config file]");
		System.out.println("\tconfig file  : The full path of the configuration file");
	}
	abstract public void execute() throws Exception;
	private void processArguments(String[] args) throws Exception {
		if(args.length > 0){
			this.setIni(new INIFileReader(args[0]));
			try{
				for(int i = 0; i < 20;i++){
					String str = "JAVA-PARM"+(i+1);
					try{
						String val = System.getProperty(str);
						if(val != null){
							this.m_mnemonics.put(str, val);
						}
					}catch(Exception e){}
				}
			}catch(Exception e){}

			String driver = this.getConfigValue("driver");
			String url = this.getConfigValue("url");
			String username = this.getConfigValue("username");
			String password = null;
			try{
				password = this.getConfigValue("password");
			}catch(Exception e){
				INIFileReader passwordFile = new INIFileReader(this.getConfigValue("password.file"));
				password = this.replaceMnemonics(passwordFile.getValue(this.getConfigValue("password.ref")));
			}
			
			this.createConnection(driver,url,username,password);
		}else{
			throw new Exception("Not enough arguments");
		}
	}
	protected void createConnection(final String driver,final String url,final String username,final String password) throws Exception {
		this.setConnectionManager(new HDBConnectionManager(){

			@Override
			public Connection getConnection() throws Exception {
				return com.robaone.dbase.DBManager.getConnection(driver, url, username, password);
			}

			@Override
			public void closeConnection(Connection m_con) throws Exception {
				m_con.close();
			}
			
		});
	}
	protected String replaceMnemonics(String value) {
		try{
			Object[] keys = this.m_mnemonics.keySet().toArray();
			for(int i = 0; i < keys.length;i++){
				Object mnemonic = keys[i];
				String mnemonic_value = (String)this.m_mnemonics.get(mnemonic);
				value = value.replaceAll("%"+mnemonic+"%", mnemonic_value);
			}
			return value;
		}catch(Exception e){
			return value;
		}
	}
	protected void setConnectionManager(HDBConnectionManager man){
		this.connectionManager = man;
	}
	public HDBConnectionManager getConnectionManager(){
		return this.connectionManager;
	}
	public Output getOutput() {
		return output;
	}
	private void setOutput(Output output) {
		this.output = output;
	}
	public INIFileReader getIni() {
		return ini;
	}
	private void setIni(INIFileReader ini) {
		this.ini = ini;
	}
	public String getConfigValue(String parameter) throws Exception {
		try{
			return this.replaceMnemonics(this.getIni().getValue(parameter));
		}catch(Exception e){
			throw new Exception("Cannot find config value, "+parameter);
		}
	}
}
