package com.robaone.script;

public interface ScriptVariableFactory {

	String getValue(String variable) throws Exception;

}
