package com.robaone.script;

import java.text.SimpleDateFormat;

public class BaseVariableFactory implements ScriptVariableFactory {

	@Override
	public String getValue(String variable) throws Exception {
		String retval = null;
		switch(variable){
		case "currenttime":
			retval = getCurrentTime("kk-mm-ss");
			break;
		case "currentdate":
			retval = getCurrentDate("yyyy-MM-dd");
			break;
		}
		return retval;
	}

	protected String getCurrentTime(String format) {
		String retval;
		SimpleDateFormat df = new SimpleDateFormat(format);
		retval = df.format(new java.util.Date());
		return retval;
	}
	
	protected String getCurrentDate(String format){
		return this.getCurrentTime(format);
	}

}
