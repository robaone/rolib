package com.robaone.script;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.robaone.dbase.types.StringType;

public class ScriptBootstrap {
	private Properties config;
	private File configFile;
	private PrintStream printStream;
	private HashMap<Integer,String> mnemonics;
	private Pattern parmPattern;
	private Pattern varPattern;
	private ScriptVariableFactory varFactory;
	private ArgumentParser argumentParser;
	public Properties getConfig() {
		return config;
	}
	public void setConfig(Properties config) {
		this.config = config;
	}
	public File getConfigFile() {
		return configFile;
	}
	public void setConfigFile(File configFile) {
		this.configFile = configFile;
	}
	public void setConfigFile(String[] args, int index) throws Exception {
		try{
			String fileName = args[index];
			File propertiesFile = new File(fileName);
			this.setConfigFile(propertiesFile);
		}catch(Exception e){
			Exception arg_exception = new Exception("Config file file not found in argument "+(index+1));
			arg_exception.initCause(e);
			throw arg_exception;
		}
	}
	public void setConfigFile(String fileName) throws IOException {
		File propertiesFile = new File(fileName);
		this.setConfigFile(propertiesFile);
	}
	public void loadConfig(File propertiesFile) throws IOException {
		setConfig(new Properties());
		InputStream inStream = getFileInputStream(propertiesFile);
		getConfig().load(inStream);
	}
	protected void saveProperties() throws IOException {
		FileWriter fileWriter = null;
		try{
			fileWriter = new FileWriter(this.getConfigFile());
			this.getConfig().store(fileWriter, "Script Config File");
		}finally{
			fileWriter.flush();
			fileWriter.close();
		}
	}

	public void loadMnemonics() throws Exception {
		Properties props = this.getConfig();
		String[] keys = props.keySet().toArray(new String[0]);
		Pattern parmPattern = getParmPattern();
		this.setMnemonics(new HashMap<Integer,String>());
		for(String key : keys){
			String line = props.getProperty(key);
			Matcher m = parmPattern.matcher(line);
			if(m.find()){
				String index_str = m.group(1);
				int index = Integer.parseInt(index_str);
				this.getMnemonics().put(index, System.getProperty("JAVA-PARM"+index));
			}
		}
	}
	protected Pattern getParmPattern() {
		if(this.parmPattern == null){
			this.setParmPattern(Pattern.compile("%JAVA-PARM([0-9]+)%"));
		}
		return this.parmPattern;
	}
	protected void setParmPattern(Pattern p){
		this.parmPattern = p;
	}
	protected InputStream getFileInputStream(File propertiesFile) throws IOException {
		byte[] fileBytes = FileUtils.readFileToByteArray(propertiesFile);
		ByteArrayInputStream bin = new ByteArrayInputStream(fileBytes);
		return bin;
	}

	public Properties getConfigProperties() throws Exception {
		Properties retval = new Properties();
		if(this.getConfig() != null){
			String[] keys = this.getConfig().keySet().toArray(new String[0]);
			for(String key : keys){
				retval.setProperty(key, this.getProperty(key));
			}
		}
		return retval;
	}
	public String getProperty(String propertyName) throws Exception{
		try{
			String retval = null;
			retval = this.getConfig().getProperty(propertyName);
			if(this.getMnemonics() != null){
				Matcher matcher = this.getParmPattern().matcher(retval);
				while(matcher.find()){
					int index = Integer.parseInt(matcher.group(1));
					String value = this.getMnemonics().get(index);
					StringBuffer buff = new StringBuffer();
					buff.append(retval.substring(0,matcher.start()));
					buff.append(new StringType(value).toString());
					buff.append(retval.substring(matcher.end()));
					retval = buff.toString();
					matcher = this.getParmPattern().matcher(retval);
				}
			}
			if(this.getVarPattern() != null){
				Matcher matcher = this.getVarPattern().matcher(retval);
				while(matcher.find()){
					String variable = matcher.group(1);
					String value = this.getVariable(variable);
					StringBuffer buff = new StringBuffer();
					buff.append(retval.substring(0,matcher.start()));
					buff.append(value);
					buff.append(retval.substring(matcher.end()));
					retval = buff.toString();
					matcher = this.getVarPattern().matcher(retval);
				}
			}
			return retval;
		}catch(Exception e){
			Exception propertyException = new Exception("Could not read property, "+propertyName);
			propertyException.initCause(e);
			throw propertyException;
		}
	}

	protected String getVariable(String variable) throws Exception {
		ScriptVariableFactory factory = this.getVarFactory();
		String value = factory.getValue(variable);
		return value;
	}
	public void writeLog(String string) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S a");
		PrintStream outputStream = getOutputStream();
		outputStream.print(df.format(new java.util.Date()));
		outputStream.print(": ");
		outputStream.println(string);
	}
	public void writeDebug(String string) {
		if("true".equals(System.getProperty("debug"))){
			this.writeLog(string);
		}
	}
	protected PrintStream getOutputStream() {
		if(this.printStream == null){
			this.setOutputStream(System.out);
		}
		return printStream;
	}
	public void setOutputStream(PrintStream out){
		this.printStream = out;
	}
	public HashMap<Integer, String> getMnemonics() {
		return mnemonics;
	}
	public void setMnemonics(HashMap<Integer, String> mnemonics) {
		this.mnemonics = mnemonics;
	}
	public Pattern getVarPattern() {
		if(varPattern == null){
			this.setVarPattern(Pattern.compile("[$][{]([a-zA-Z_]+)[}]"));
		}
		return varPattern;
	}
	public void setVarPattern(Pattern varPattern) {
		this.varPattern = varPattern;
	}
	public ScriptVariableFactory getVarFactory() {
		if(this.varFactory == null){
			this.setVarFactory(new BaseVariableFactory());
		}
		return varFactory;
	}
	public void setVarFactory(ScriptVariableFactory varFactory) {
		this.varFactory = varFactory;
	}
	public ArgumentParser getArgumentParser(String programName) {
		if(argumentParser == null){
			this.setArgumentParser(new BasicArgumentParser(programName));
		}
		return argumentParser;
	}
	public void setArgumentParser(ArgumentParser argumentParser) {
		this.argumentParser = argumentParser;
	}
	public void importProperties(Properties properties) {
		Properties existing = this.getConfig();
		Object[] keys = properties.keySet().toArray();
		for(Object key : keys){
			existing.put(key, properties.get(key));
		}
	}
}
