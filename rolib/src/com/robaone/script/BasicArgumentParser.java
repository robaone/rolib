package com.robaone.script;

import java.util.Properties;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.json.JSONArray;
import org.json.JSONObject;

public class BasicArgumentParser implements ArgumentParser {
	private Options options;
	private HelpFormatter formatter;
	private String programName;
	public BasicArgumentParser(String program){
		this.setProgramName(program);
	}

	@Override
	public Properties parseParameters(String[] args,JSONObject options_jo) throws Exception {
		try{
			JSONArray options_ja = options_jo.getJSONArray("options");
			for(int i = 0; i < options_ja.length();i++){
				Option opt = null;
				JSONObject option_jo = options_ja.getJSONObject(i);
				if(hasAllProperties(option_jo)){
					opt = new Option(option_jo.getString("opt"),option_jo.getString("longOpt") ,option_jo.getBoolean("hasArg"), option_jo.getString("description"));
				}else if(isOptional(option_jo)){
					opt = new Option(option_jo.has("opt") ? option_jo.getString("opt") : option_jo.getString("longOpt"),option_jo.getBoolean("hasArg"), option_jo.getString("description"));
				}else{
					opt = new Option(option_jo.has("opt") ? option_jo.getString("opt") : option_jo.getString("longOpt"),option_jo.getString("description"));
				}
				this.getOptions().addOption(opt);
			}
			CommandLineParser parser = new BasicParser();
			CommandLine cmd = parser.parse(options, args);
			Properties props = new Properties();
			for(Option option : cmd.getOptions()){
				props.setProperty(option.getArgName(), option.getValue());
			}
			return props;
		}catch(Exception e){
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(this.getProgramName(), options,true);
			throw e;
		}
	}

	private boolean isOptional(JSONObject option_jo) {
		return option_jo.has("hasArg");
	}

	public boolean hasAllProperties(JSONObject option_jo) {
		return option_jo.has("opt") && option_jo.has("longOpt") && option_jo.has("hasArg") && option_jo.has("description");
	}
 
	public Options getOptions() {
		if(this.options == null){
			this.setOptions(new Options());
		}
		return options;
	}
	public void setOptions(Options options) {
		this.options = options;
	}
	public HelpFormatter getFormatter() {
		if(this.formatter == null){
			this.setFormatter(new HelpFormatter());
		}
		return formatter;
	}
	public void setFormatter(HelpFormatter formatter) {
		this.formatter = formatter;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}
	
}
