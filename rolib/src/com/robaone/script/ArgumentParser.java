package com.robaone.script;

import java.util.Properties;

import org.json.JSONObject;


public interface ArgumentParser {

	public Properties parseParameters(String[] args,JSONObject options_jo) throws Exception;

}
