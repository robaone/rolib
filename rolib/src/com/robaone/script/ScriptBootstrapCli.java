package com.robaone.script;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.json.JSONArray;
import org.json.JSONException;

import com.robaone.script.ScriptBootstrap;

abstract public class ScriptBootstrapCli extends ScriptBootstrap implements Runnable {
	public static final String OPT_SCRIPT = "script";
	public static final String OPT_PARAMETERS = "parameters";
	public static final String OPT_CONFIG = "config";
	private Options options;
	private CommandLineParser commandLineParser;
	private InputStream scriptInputStream;
	private HelpFormatter helpFormatter;
	private Exception exception;
	private String[] args;
	public ScriptBootstrapCli(){

	}
	protected void execute(String[] args) {
		int exitcode = 1;
		boolean showUsage = true;
		try{
			this.parseParameters(args);
			this.writeDebug("Running...");
			showUsage = false;
			this.run();
			if(this.getException() != null){
				throw this.getException();
			}else{
				this.writeDebug("Success...");
				System.exit(0);
			}
		}catch(Exception e){
			if(showUsage){
				this.showUsage();
			}
			e.printStackTrace();
			System.exit(exitcode);
		}
	}
	protected void parseParameters(String[] args) throws Exception {
		CommandLineParser parser = this.getCommandLineParser();
		CommandLine cmd = parser.parse(getOptions(), args);
		processParameterOptions(cmd);
	}
	protected void processParameterOptions(CommandLine cmd) throws IOException,
			Exception, JSONException, FileNotFoundException {
		if(cmd.hasOption(OPT_CONFIG)){
			String config = cmd.getOptionValue(OPT_CONFIG);
			this.setConfigFile(config);
			this.loadConfig(this.getConfigFile());
			this.loadMnemonics();
		}
		if(cmd.hasOption(OPT_PARAMETERS)){
			String parameters = cmd.getOptionValue(OPT_PARAMETERS);
			JSONArray param_ja = new JSONArray(parameters);
			String[] params = new String[param_ja.length()];
			for(int i = 0; i < param_ja.length();i++){
				params[i] = param_ja.getString(i);
			}
			this.setArgs(params);
		}
		if(cmd.hasOption(OPT_SCRIPT)){
			String script = cmd.getOptionValue(OPT_SCRIPT);
			File file = new File(script);
			if(file.exists()){
				InputStream scriptInput = new FileInputStream(file);
				this.setScriptInputStream(scriptInput);
			}else{
				throw new Exception("Script file does not exist");
			}
		}
	}
	

	protected void showUsage() {
		HelpFormatter formatter = this.getHelpFormatter();
		formatter.printHelp(this.getClass().getName(), this.getOptions(),true);
	}
	public Options getOptions() {
		if(options == null){
			this.setOptions(new Options());
			setCliOptions();
		}
		return options;
	}
	protected void setCliOptions() {
		this.getOptions().addOption(OPT_SCRIPT,true,"The xml script file");
		this.getOptions().addOption(OPT_CONFIG,true,"The configuration file");
		this.getOptions().addOption(OPT_PARAMETERS,true,"An array of strings");
	}
	public void setOptions(Options options) {
		this.options = options;
	}
	public CommandLineParser getCommandLineParser() {
		if(this.commandLineParser == null){
			this.setCommandLineParser(new BasicParser());
		}
		return commandLineParser;
	}
	public void setCommandLineParser(CommandLineParser commandLineParser) {
		this.commandLineParser = commandLineParser;
	}
	public InputStream getScriptInputStream() {
		return scriptInputStream;
	}
	public void setScriptInputStream(InputStream scriptInputStream) {
		this.scriptInputStream = scriptInputStream;
	}
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public String[] getArgs() {
		return args;
	}
	public void setArgs(String[] args) {
		this.args = args;
	}
	public HelpFormatter getHelpFormatter() {
		if(this.helpFormatter == null){
			this.setHelpFormatter(new HelpFormatter());
		}
		return helpFormatter;
	}
	public void setHelpFormatter(HelpFormatter helpFormatter) {
		this.helpFormatter = helpFormatter;
	}

}
