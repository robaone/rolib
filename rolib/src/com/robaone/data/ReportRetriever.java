package com.robaone.data;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;

import org.w3c.dom.Document;

import com.robaone.api.business.XMLDocumentReader;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
abstract public class ReportRetriever {
	public Document getReport(String name,Connection con) throws Exception {
		String sql = this.getSQL();
		java.sql.PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1,name);
		ResultSet rs = ps.executeQuery();
		try{
			if(rs.next()){
				String xml = rs.getString(1);
				StringReader sr = new StringReader(xml);
				XMLDocumentReader reader = new XMLDocumentReader();
				reader.read(xml);
				return reader.getDocument();
			}
		}finally{
			try{ps.close();}catch(Exception e){}
			try{rs.close();}catch(Exception e){}
		}
		return null;
	}
	abstract public String getSQL();
}
