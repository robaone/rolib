package com.robaone.test;

import com.robaone.dbase.DatabaseConnectionManager;

public class DBTest extends BaseTest {
	private DatabaseConnectionManager connectionManager;

	public DatabaseConnectionManager getConnectionManager() {
		if(this.connectionManager == null){
			this.setConnectionManager(new DatabaseConnectionManager());
		}
		return connectionManager;
	}

	public void setConnectionManager(DatabaseConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}
}
