package com.robaone.test;

import java.io.InputStream;

import com.robaone.net.PingWebService;

public class PingTest extends BaseTest {
	public class PingWebServiceImpl extends PingWebService{
		private InputStream XMLScript;
		@Override
		public InputStream getXMLScript() {
			return this.XMLScript;
		}
		public void setXMLScript(InputStream xMLScript) {
			XMLScript = xMLScript;
		}
		
	}
	private PingWebServiceImpl ping;

	public PingWebServiceImpl getPing() throws Exception {
		if(this.ping == null){
			this.setPing(new PingWebServiceImpl());
		}
		return ping;
	}

	public void setPing(PingWebServiceImpl ping) {
		this.ping = ping;
	}
}
