package com.robaone.security;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
/**
Aes encryption
 */
public class EncryptDecryptStringWithAES512 implements EncryptDecryptString
{

	private SecretKeySpec secretKey ;
	private byte[] key ;
	private Vector<EncryptDecryptStringWithAES512> additional = new Vector<EncryptDecryptStringWithAES512>();

	private String decryptedString;
	private String encryptedString;
	public EncryptDecryptStringWithAES512(String password) throws Exception {
		if(password.length() < 64){
			throw new Exception("Passphrase is too short.  Please enter a passphrase that is at least 64 characters.");
		}
		this.setKey(password,0);
	}
	public EncryptDecryptStringWithAES512(String password, int offset) throws Exception {
		password = password.substring(offset);
		this.setKey(password,offset);
	} 
	public void setKey(String myKey,int offset) throws Exception{


		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16); // use only first 128 bit
			secretKey = new SecretKeySpec(key, "AES");

			if(offset == 0){
				myKey = myKey.substring(0,64);
			}
			// Set next 128 bits
			if(myKey.length() >= 32){
				EncryptDecryptStringWithAES512 next = new EncryptDecryptStringWithAES512(myKey,16);
				additional.add(next);
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}



	}

	public String getDecryptedString() {
		return decryptedString;
	}
	public  void setDecryptedString(String decryptedString) {
		this.decryptedString = decryptedString;
	}
	public String getEncryptedString() {
		return encryptedString;
	}
	public void setEncryptedString(String encryptedString) {
		this.encryptedString = encryptedString;
	}
	public String encrypt(String strToEncrypt) throws Exception
	{

		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

		cipher.init(Cipher.ENCRYPT_MODE, secretKey);

		String encodeBase64String = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		for(EncryptDecryptStringWithAES512 next : this.additional){
			encodeBase64String = next.encrypt(encodeBase64String);
		}

		setEncryptedString(encodeBase64String);

		return this.getEncryptedString();

	}
	public String decrypt(String strToDecrypt) throws Exception
	{

		for(EncryptDecryptStringWithAES512 next : this.additional){
			strToDecrypt = next.decrypt(strToDecrypt);
		}
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");

		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		String mydecryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
		setDecryptedString(mydecryptedString);
		return this.getDecryptedString();

	}
	public static void main(String args[]) throws Exception
	{	
		final String strToEncrypt = "My text to encrypt";
		final String strPssword = "encryptor key";
		EncryptDecryptStringWithAES512 encryptor = new EncryptDecryptStringWithAES512(strPssword); 


		encryptor.encrypt(strToEncrypt.trim());

		System.out.println("String to Encrypt: " + strToEncrypt); 
		System.out.println("Encrypted: " + encryptor.getEncryptedString());

		final String strToDecrypt =  encryptor.getEncryptedString();
		encryptor.decrypt(strToDecrypt.trim());

		System.out.println("String To Decrypt : " + strToDecrypt);
		System.out.println("Decrypted : " + encryptor.getDecryptedString());

	}

}