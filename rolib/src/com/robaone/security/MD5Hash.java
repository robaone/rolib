package com.robaone.security;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;

public class MD5Hash {
	public String getHash(byte[] data) throws Exception{
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] thedigest = md.digest(data);
		String retval = Base64.encodeBase64String(thedigest);
		return retval;
	}
	
	public String getCheckSum(byte[] data) throws Exception {
		String retval = org.apache.commons.codec.digest.DigestUtils.md5Hex(data);
		return retval;
	}
}
