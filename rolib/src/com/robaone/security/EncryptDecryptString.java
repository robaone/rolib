package com.robaone.security;

public interface EncryptDecryptString {
	public String encrypt(String str) throws Exception;
	public String decrypt(String str) throws Exception;
}
