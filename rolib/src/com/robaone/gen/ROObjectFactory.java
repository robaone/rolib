package com.robaone.gen;

import java.util.ArrayList;

abstract public class ROObjectFactory<T> {
	@SuppressWarnings("unchecked")
	public T getInstance(Class<?>... clazz) {
		ArrayList<T> inst = new ArrayList<T>();
		if(clazz != null){
			try{
				for(Class<?> claz: clazz){
					inst.add((T) claz.newInstance());
				}
				initialize(inst.toArray());
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return inst.get(0);
	}

	abstract protected void initialize(Object... inst) throws Exception;
	
}
