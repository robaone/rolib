package com.robaone.gen;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.robaone.api.business.XMLDocumentReader;
import com.robaone.json.JSONToXML;
import com.robaone.script.ScriptBootstrapCli;
import com.robaone.xml.ROTransform;

public class MicroserviceGenerator extends ScriptBootstrapCli {
	public MicroserviceGenerator(){
	}
	@Override
	public void run() {
		try {
			String folder = this.getProperty("output.folder");
			String swagger_json = IOUtils.toString(this.getScriptInputStream());
			JSONObject jo = new JSONObject(swagger_json);
			String package_name = jo.getString("x-package");
			String package_folder = folder+"/"+package_name.replaceAll("[.]", "/");
			File package_folder_f = new File(package_folder);
			package_folder_f.mkdirs();
			generateServiceClass(folder, swagger_json, jo, package_folder);
			generateServiceHandlerFactoryInterface(folder, swagger_json, jo, package_folder);
			//generateServiceHandlerAbstractClass(folder, swagger_json, jo, package_folder);
			generateServiceHandlerFactoryClass(folder, swagger_json, jo, package_folder);
			//generateInvalidJmsTypeHandlerClass(folder, swagger_json, jo, package_folder+"/handlers");
			generateFunctionHandlerClasses(folder, swagger_json, jo, package_folder+"/handlers");
			generateDTOInterfaces(folder,swagger_json,jo,package_folder+"/dto");
		} catch (Exception e) {
			this.setException(e);
		}
	}
	private void generateServiceHandlerFactoryInterface(String folder,
			String swagger_json, JSONObject jo, String package_folder) throws Exception{
		this.writeLog("Creating the Service Handler Factory Interface in "+folder);
		InputStream stylesheet = this.getClass().getResourceAsStream("handlerfactoryinterface.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		JSONToXML toxml = new JSONToXML();
		toxml.setJson(swagger_json);
		toxml.setAttributeMode(true);
		toxml.run();
		String xml = toxml.getXml();
		String source = trn.transformXML(xml);
		File source_file = new File(package_folder+"/"+jo.getJSONObject("info").getString("title")+"HandlerFactory.java");
		this.writeLog("Saving "+source_file);
		FileUtils.writeStringToFile(source_file, source);
	}
	private void generateServiceHandlerFactoryClass(String folder,
			String swagger_json, JSONObject jo, String package_folder) throws Exception {
		this.writeLog("Creating the Service Handler Factory Class in "+folder);
		InputStream stylesheet = this.getClass().getResourceAsStream("handlerfactory.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		JSONToXML toxml = new JSONToXML();
		toxml.setJson(swagger_json);
		toxml.setAttributeMode(true);
		toxml.run();
		String xml = toxml.getXml();
		String factory_source = trn.transformXML(xml);
		File factory_source_file = new File(package_folder+"/"+jo.getJSONObject("info").getString("title")+"HandlerFactoryImpl.java");
		this.writeLog("Saving "+factory_source_file);
		FileUtils.writeStringToFile(factory_source_file, factory_source);
	}
	public void generateServiceClass(String folder, String swagger_json,
			JSONObject jo, String package_folder) throws Exception,
			JSONException, IOException {
		this.writeLog("Creating the Service Class in "+folder);
		InputStream stylesheet = this.getClass().getResourceAsStream("microservice.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		JSONToXML toxml = new JSONToXML();
		toxml.setJson(swagger_json);
		toxml.setAttributeMode(true);
		toxml.run();
		String xml = toxml.getXml();
		String microservice_source = trn.transformXML(xml);
		File microservice_source_file = new File(package_folder+"/"+jo.getJSONObject("info").getString("title")+".java");
		this.writeLog("Saving "+microservice_source_file);
		FileUtils.writeStringToFile(microservice_source_file, microservice_source);
	}
	public void generateFunctionHandlerClasses(String folder, String swagger_json,
			JSONObject jo, String package_folder) throws Exception {
		XMLDocumentReader reader = new XMLDocumentReader();
		this.writeLog("Creating the Function Handler Classes in "+folder);
		InputStream stylesheet = this.getClass().getResourceAsStream("functionhandler.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		JSONToXML toxml = new JSONToXML();
		toxml.setJson(swagger_json);
		toxml.setAttributeMode(true);
		toxml.run();
		String xml = toxml.getXml();
		reader.read(xml);
		NodeList path_nodes = reader.findXPathNode("/node[@name='json']/node[@name='paths']/node");
		for(int i = 0; i < path_nodes.getLength();i++){
			Node path_node = path_nodes.item(i);
			String path = reader.findXPathString(path_node, "@name");
			trn.getParameters().put("path", path);
			String source = trn.transformXML(xml);
			File source_file = new File(package_folder+"/"+jo.getJSONObject("info").getString("title")+path.substring(1, 2).toUpperCase()+path.substring(2)+"Handler.java");
			this.writeLog("Saving "+source_file);
			FileUtils.writeStringToFile(source_file, source);
		}
	}
	
	public void generateDTOInterfaces(String folder, String swagger_json,
			JSONObject jo, String package_folder) throws Exception {
		XMLDocumentReader reader = new XMLDocumentReader();
		this.writeLog("Creating the DTO Interfaces in "+folder);
		new File(folder).mkdirs();
		InputStream stylesheet = this.getClass().getResourceAsStream("dtointerface.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		JSONToXML toxml = new JSONToXML();
		toxml.setJson(swagger_json);
		toxml.setAttributeMode(true);
		toxml.run();
		String xml = toxml.getXml();
		reader.read(xml);
		NodeList path_nodes = reader.findXPathNode("/node[@name='json']/node[@name='definitions']/node");
		for(int i = 0; i < path_nodes.getLength();i++){
			Node path_node = path_nodes.item(i);
			String dto_name = reader.findXPathString(path_node, "@name");
			trn.getParameters().put("dto_name", dto_name);
			String source = trn.transformXML(xml);
			File source_file = new File(package_folder+"/"+jo.getJSONObject("info").getString("title")+dto_name+".java");
			this.writeLog("Saving "+source_file);
			FileUtils.writeStringToFile(source_file, source);
		}
	}

	public static void main(String[] args) {
		MicroserviceGenerator gen = new MicroserviceGenerator();
		gen.execute(args);
	}

}
