package com.robaone.gen;

import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import com.robaone.script.ScriptBootstrap;

public class CrudToSwaggerGenerator implements Runnable {
	private ScriptBootstrap script;
	private Exception exception;
	public static void main(String[] args) {
		CrudToSwaggerGenerator generator = null;
		try{
			generator = new CrudToSwaggerGenerator();
			JSONObject config = generator.loadConfig();
			Properties properties = generator.getScript().getArgumentParser(generator.getClass().getName()).parseParameters(args, config);
			generator.getScript().importProperties(properties);
			if(generator.getException() != null){
				throw generator.getException();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	protected JSONObject loadConfig() throws Exception {
		InputStream in = this.getClass().getResourceAsStream("crudgeneratorconfig.json");
		String string = IOUtils.toString(in);
		JSONObject jsonObject = new JSONObject(string);
		return jsonObject;
	}
	public ScriptBootstrap getScript() {
		if(this.script == null){
			this.setScript(new ScriptBootstrap());
		}
		return script;
	}
	public void setScript(ScriptBootstrap script) {
		this.script = script;
	}
	@Override
	public void run() {
		try{
			
		}catch(Exception e){
			this.setException(e);
		}
	}
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}

}
