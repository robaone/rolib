package com.robaone.gen;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.robaone.script.ScriptBootstrapCli;
import com.robaone.xml.ROTransform;

public class RabbitMQMicroserviceGenerator extends ScriptBootstrapCli {

	public static void main(String[] args) {
		RabbitMQMicroserviceGenerator gen = new RabbitMQMicroserviceGenerator();
		gen.execute(args);
	}

	@Override
	public void run() {
		try{
			String folder = this.getArgs()[0];
			String json = IOUtils.toString(this.getScriptInputStream());
			JSONObject jo = new JSONObject(json);
			String package_name = jo.getString("package");
			String package_folder = folder+"/"+package_name.replaceAll("[.]", "/");
			File package_folder_f = new File(package_folder);
			package_folder_f.mkdirs();
			generateServiceClass(folder,jo,package_folder);
			generateServiceHandlerClass(folder,jo,package_folder);
		}catch(Exception e){
			this.setException(e);
		}
	}

	public void generateServiceClass(String folder, 
			JSONObject jo, String package_folder) throws Exception,
			JSONException, IOException {
		this.writeLog("Creating the Service Class in "+folder);
		InputStream stylesheet = this.getClass().getResourceAsStream("RabbitMQService.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		String xml = XML.toString(jo, "service");
		String microservice_source = trn.transformXML(xml);
		File microservice_source_file = new File(package_folder+"/"+jo.getString("name")+".java");
		this.writeLog("Saving "+microservice_source_file);
		FileUtils.writeStringToFile(microservice_source_file, microservice_source);
	}
	
	public void generateServiceHandlerClass(String folder, 
			JSONObject jo, String package_folder) throws Exception,
			JSONException, IOException {
		this.writeLog("Creating the Service Handler Class in "+folder);
		InputStream stylesheet = this.getClass().getResourceAsStream("RabbitMQServiceHandler.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		String xml = XML.toString(jo, "service");
		String microservice_source = trn.transformXML(xml);
		File microservice_source_file = new File(package_folder+"/"+jo.getString("name")+"Handler.java");
		this.writeLog("Saving "+microservice_source_file);
		FileUtils.writeStringToFile(microservice_source_file, microservice_source);
	}
}
