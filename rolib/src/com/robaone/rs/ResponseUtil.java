package com.robaone.rs;

import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.robaone.api.business.FieldValidator;

public class ResponseUtil {
	public static Response buildResponse(String callback, JSONObject jo) {
		if(FieldValidator.exists(callback)){
			return Response.ok(callback+"("+jo.toString()+");", "text/javascript").build();
		}else{
			return Response.ok(jo.toString(), "application/json").build();
		}
	}
}
