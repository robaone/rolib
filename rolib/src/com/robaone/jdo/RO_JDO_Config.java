package com.robaone.jdo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Properties;

import org.json.JSONObject;
import org.json.XML;

import com.robaone.dbase.DatabaseConnectionManager;
import com.robaone.dbase.HDBConnectionManager;
import com.robaone.xml.ROTransform;

public class RO_JDO_Config {
	private TableListQuery tableListQuery;
	private PrintStream outputWriter;
	private Properties properties;
	private HDBConnectionManager connectionManager;
	public static void main(String[] args) {
		RO_JDO_Config config = new RO_JDO_Config();
		config.execute(args);
	}
	public void execute(String[] args) {
		try{
			this.loadProperties(args);
			String[] list = this.getTableList();
			String config_file = this.createConfigFile(list);
			this.getOutputWriter().println(config_file);
			this.exit(0);
		}catch(Exception e){
			e.printStackTrace(this.getOutputWriter());
			this.showUsage();
			this.exit(1);
		}
	}
	public String[] getTableList() throws Exception {
		ArrayList<String> list = null;
		this.getTableListQuery().run(this.getConnectionManager());
		list = this.getTableListQuery().getList();
		return list.toArray(new String[0]);
	}
	public String createConfigFile(java.lang.String[] list) throws Exception {
		JSONObject jo = new JSONObject();
		for(String item : list){
			jo.put(item, "");
		}
		String xml = XML.toString(jo,"json");
		InputStream stylesheet = this.getClass().getResourceAsStream("jdbc_config.xsl");
		ROTransform trn = new ROTransform(stylesheet);
		String output = trn.transformXML(xml);
		return output;
	}
	public void loadProperties(String[] args)
			throws FileNotFoundException, IOException {
		Properties props = new Properties();
		if(args.length > 0){
			File file = new File(args[0]);
			FileReader file_reader = new FileReader(file);
			props.load(file_reader);
			file_reader.close();
		}
		props.putAll(System.getProperties());
		this.setProperties(props);
	}
	protected void exit(int i){
		System.exit(i);
	}
	protected void showUsage() {
		this.getOutputWriter().println("Usage: [(Optional) config file]");
	}
	public PrintStream getOutputWriter() {
		if(this.outputWriter == null){
			this.setOutputWriter(System.out);
		}
		return outputWriter;
	}
	public void setOutputWriter(PrintStream outputWriter) {
		this.outputWriter = outputWriter;
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	public HDBConnectionManager getConnectionManager() {
		if(this.connectionManager == null){
			DatabaseConnectionManager man = new DatabaseConnectionManager();
			man.setUsername(this.getProperties().getProperty("db.username"));
			String password = this.getProperties().getProperty("db.password");
			man.setPassword(password);
			man.setUrl(this.getProperties().getProperty("db.url"));
			this.setConnectionManager(man);
		}
		return connectionManager;
	}
	public void setConnectionManager(HDBConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}
	public TableListQuery getTableListQuery() {
		if(this.tableListQuery == null){
			this.setTableListQuery(new TableListQuery());
		}
		return tableListQuery;
	}
	public void setTableListQuery(TableListQuery tableListQuery) {
		this.tableListQuery = tableListQuery;
	}

}
