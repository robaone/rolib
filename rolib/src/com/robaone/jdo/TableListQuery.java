package com.robaone.jdo;

import java.util.ArrayList;

import com.robaone.dbase.ConnectionBlock;

public class TableListQuery extends ConnectionBlock {
	private ArrayList<String> list;

	public TableListQuery(ArrayList<String> list) {
		this.list = list;
	}

	public TableListQuery() {
		this.list = new ArrayList<String>();
	}

	@Override
	protected void run() throws Exception {
		String sql = "show tables";
		this.prepareStatement(sql);
		this.executeQuery();
		while(next()){
			list.add(this.getResultSet().getString(1));
		}
	}

	public ArrayList<String> getList() {
		return this.list;
	}
}