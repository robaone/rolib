package com.robaone.jdo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.csvreader.CsvReader;
import com.robaone.api.business.FieldValidator;
import com.robaone.api.business.ROTransformer;
import com.robaone.api.business.XMLDocumentReader;
import com.robaone.api.data.AppDatabase;

public class RO_JSO_Generator implements Runnable {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			RO_JSO_Generator main = new RO_JSO_Generator();
			String url = args[0]; //Data
			String destination = args[1]; //Code destination
			URL u = new URL(url);
			main.setSource(u);
			main.run();
			
			/**
			 * Append package to destination folder
			 */
			
			String package_folder = main.getPackage().replaceAll("[.]", System.getProperty("file.separator"));
			destination = destination + System.getProperty("file.separator") + package_folder;
			
			File folder = new File(destination);
			folder.mkdirs();
					
			destination += System.getProperty("file.separator") + main.getClassName() + ".java";
			
			File f = new File(destination);
			FileOutputStream fout = null;
			try{
				fout = new FileOutputStream(f);
				OutputStreamWriter fr = new OutputStreamWriter(fout);
				fr.write(main.getSourceCode());
				fr.flush();
			}finally{
				try{fout.flush();fout.close();}catch(Exception e){}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private String m_classname;

	public String getClassName() {
		return this.m_classname;
	}

	private void setClassName(String class_name){
		this.m_classname = class_name;
	}
	private InputStream m_inputstream;
	private int m_exitcode = -1;
	private Exception m_exception;
	private String m_source;
	private String m_package;

	public void setSource(URL u) throws IOException {
		InputStream in = u.openConnection().getInputStream();
		this.m_inputstream = in;
	}

	public void setSource(File f) throws IOException {
		InputStream in = new FileInputStream(f);
		this.m_inputstream = in;
	}

	@Override
	public void run() {
		try{
			// Read the input stream as a csv file and generate the JavaScriptObject class.
			CsvReader reader = new CsvReader(new InputStreamReader(this.m_inputstream));
			XMLDocumentReader xml_reader = new XMLDocumentReader();
			xml_reader.read("<data></data>");

			for(int i = 0;reader.readRecord();i++){
				if(i == 0){
					/**
					 * This is the first line cell A2
					 * package | [package name]
					 */
					String package_name = reader.get(1);
					setPackage(package_name);
					Attr attr = xml_reader.getDocument().createAttribute("package");
					attr.setValue(package_name);
					xml_reader.getDocument().getFirstChild().getAttributes().setNamedItem(attr);
				}else if(i == 1){
					/**
					 * This is the second line cell B2
					 * class name | [class name]
					 */
					String class_name = reader.get(1);
					this.setClassName(class_name);
					Element n = xml_reader.getDocument().createElement("class");
					if(FieldValidator.exists(reader.get(2))){
						Attr attr = xml_reader.getDocument().createAttribute("template");
						attr.setValue("<"+reader.get(2)+">");
						n.setAttributeNode(attr);
					}
					n.setTextContent(class_name);
					xml_reader.getDocument().getFirstChild().appendChild(n);
				}else if(i > 2){
					/**
					 * This is where the field descriptions begin
					 */
					String fieldname = reader.get(0);
					String type = reader.get(1);
					if(FieldValidator.exists(fieldname)){
						Element n = xml_reader.getDocument().createElement("field");
						Attr atr = xml_reader.getDocument().createAttribute("type");
						atr.setValue(type);
						n.getAttributes().setNamedItem(atr);
						n.setTextContent(fieldname);
						xml_reader.getDocument().getFirstChild().appendChild(n);
					}
				}
			}

			AppDatabase.writeLog(xml_reader.toString());
			
			InputStream in = this.getClass().getResourceAsStream("Create_JSO.xsl");
			ROTransformer trn = new ROTransformer(in);
			setSourceCode(trn.transform(xml_reader.toString()));
			this.setExitCode(0);
		}catch(Exception e){
			this.setException(e);
			this.setExitCode(1);
		}finally{
			try {
				this.m_inputstream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void setPackage(String package_name) {
		this.m_package = package_name;
	}
	
	public String getPackage(){
		return this.m_package;
	}

	private void setSourceCode(String transform) {
		this.m_source = transform;
	}
	
	public String getSourceCode(){
		return this.m_source;
	}

	private void setExitCode(int i) {
		this.m_exitcode = i;
	}

	private void setException(Exception e) {
		this.m_exception = e;
	}

}
