package com.robaone.jdo;

import java.sql.Connection;
import com.robaone.dbase.ConnectionBlock;
import com.robaone.dbase.HDBConnectionManager;

public class RO_JDO_MySQL implements RO_JDO_IdentityManager<java.lang.Long> {

	private final class CopyConnectionManager implements HDBConnectionManager {
		private final Connection con;

		private CopyConnectionManager(Connection con) {
			this.con = con;
		}

		@Override
		public Connection getConnection() throws Exception {
			return con;
		}

		@Override
		public void closeConnection(Connection m_con) throws Exception {
			
		}
	}

	private final class GetLastInsertedIdQuery extends ConnectionBlock {
		private long retval;

		private GetLastInsertedIdQuery() {
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void run() throws Exception {
			String str = "select last_insert_id()";
			this.setPreparedStatement(this.getConnection().prepareStatement(str));
			this.setResultSet(this.getPreparedStatement().executeQuery());
			if(this.getResultSet().next()){
				retval = this.getResultSet().getLong(1);
			}else{
				throw new Exception("Unable to retrieve last_insert_id");
			}
		}
		
		public long getValue(){
			return retval;
		}
	}

	@Override
	public Long getIdentity(String table,final Connection con) throws Exception {
		GetLastInsertedIdQuery block = new GetLastInsertedIdQuery();
		block.run(getConnectionManager(con));
		return block.getValue();
	}

	private HDBConnectionManager getConnectionManager(final Connection con) {
		return new CopyConnectionManager(con);
	}


}
