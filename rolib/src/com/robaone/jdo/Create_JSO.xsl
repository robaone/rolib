<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="text" encoding="utf-8" indent="no" />

	<xsl:template match="/">
		<xsl:text>package </xsl:text>
		<xsl:value-of select="/data/@package" />
		<xsl:text>;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

</xsl:text>
		<xsl:text>public class </xsl:text>
		<xsl:value-of select="/data/class" />
		<xsl:value-of select="/data/class/@template"/>
		<xsl:text> extends JavaScriptObject {
</xsl:text>
		<xsl:text>    protected </xsl:text>
		<xsl:value-of select="/data/class" />
		<xsl:text>(){
</xsl:text>
		<xsl:text>    }
</xsl:text>
		<xsl:for-each select="/data/field">
			<xsl:text>    public final native </xsl:text>
			<xsl:value-of select="@type" />
			<xsl:text> get</xsl:text>
			<xsl:value-of select="." />
			<xsl:text>()/*-{
        return this.</xsl:text>
			<xsl:value-of select="." />
			<xsl:text>;
</xsl:text>
			<xsl:text>    }-*/;
</xsl:text>
			<xsl:text>    public native void </xsl:text>
			<xsl:text> set</xsl:text>
			<xsl:value-of select="." />
			<xsl:text>(</xsl:text>
			<xsl:value-of select="@type" />
			<xsl:text> value)/*-{
        this.</xsl:text>
			<xsl:value-of select="." />
			<xsl:text> = value;
</xsl:text>
			<xsl:text>    }-*/;
</xsl:text>
		</xsl:for-each>
		<xsl:text>}</xsl:text>
	</xsl:template>

</xsl:stylesheet>
						