package com.robaone.jdo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

public class RO_JDO_HSQL implements RO_JDO_IdentityManager<Integer> {

	@Override
	public Integer getIdentity(String table, Connection con) throws Exception {
		CallableStatement call = con.prepareCall("CALL IDENTITY()");
		ResultSet rs = call.executeQuery();
		if(rs.next()){
			Integer id = rs.getInt(1);
			rs.close();
			call.close();
			return id;
		}
		return null;
	}

}
