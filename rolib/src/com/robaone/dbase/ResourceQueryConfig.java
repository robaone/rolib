package com.robaone.dbase;

import java.io.InputStream;

public class ResourceQueryConfig implements QueryConfig {
	private String resourcePath;
	public ResourceQueryConfig(String path) {
		this.setResourcePath(path);
	}
	@Override
	public InputStream getResourceAsStream() throws Exception {
		InputStream in = this.getClass().getResourceAsStream(this.getResourcePath());
		return in;
	}
	public String getResourcePath() {
		return resourcePath;
	}
	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

}
