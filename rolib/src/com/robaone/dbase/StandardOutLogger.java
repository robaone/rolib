package com.robaone.dbase;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.robaone.api.data.AppDatabase;

public class StandardOutLogger extends Logger {

	public StandardOutLogger(){
		super("name",null);
	}
	protected StandardOutLogger(String name, String resourceBundleName) {
		super(name, resourceBundleName);
	}
	
	@Override
	public void info(String message){
		System.out.println(AppDatabase.getTimestamp()+": "+message);
	}
	
	@Override
	public void log(Level level, String message) {
		System.out.println(AppDatabase.getTimestamp()+": "+level+": " + message);
	}

}
