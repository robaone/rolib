package com.robaone.dbase.hierarchial;

import java.sql.Connection;
import java.util.Vector;

import com.robaone.dbase.ConnectionBlock;
import com.robaone.dbase.HDBConnectionManager;
import com.robaone.jdo.RO_JDO_IdentityManager;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class HistoryIDManager implements RO_JDO_IdentityManager<Integer> {
	Integer val = null;
	@Override
	public Integer getIdentity(final String table,final Connection con) throws Exception {

		final Vector<Integer> retval = new Vector<Integer>();
		new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				String str = "exec get_next_id "+table;
				this.setCallableStatement(this.getConnection().prepareCall(str));
				this.setResultSet(this.getCall().executeQuery());
				if(next()){
					Integer id = this.getResultSet().getInt(1);
					retval.add(id);
				}
			}

		}.run(new HDBConnectionManager(){

			@Override
			public Connection getConnection() throws Exception {
				return con;
			}

			@Override
			public void closeConnection(Connection m_con) throws Exception {

			}

		});
		return retval.size() > 0 ? retval.get(0) : null;

	}
}
