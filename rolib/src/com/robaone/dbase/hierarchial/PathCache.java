package com.robaone.dbase.hierarchial;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.robaone.api.data.AppDatabase;

/**
 * <pre>  Copyright 2013 Microdynamics Group, Inc.
        http://www.microdg.com

   All right reserved.</pre>

   @author - Ansel Robateau: arobateau@microdg.com
 */
public class PathCache {
	private List<String> m_cached_paths =  Collections.synchronizedList(new LinkedList<String>());
	private HashMap<String, BigDecimal> m_cached_paths_map = new HashMap<String, BigDecimal>();
	public PathCache(){
		AppDatabase.writeTrace("Creating new PathCache object");
	}
	public BigDecimal get(String parent_path) {

		BigDecimal retval = m_cached_paths_map.get(parent_path);
		return retval;
	}
	public void add(String parent_path,BigDecimal id) {
		m_cached_paths.add(parent_path);
		m_cached_paths_map.put(parent_path, id);
		
		/***********
		 * Remove item that is oldest if list is larger than 250  
		 ***********/
		if(m_cached_paths.size() > 250){
			for(int i = 250;i < m_cached_paths.size();){
				String path_to_remove = m_cached_paths.get(i);
				if(path_to_remove != null){
					m_cached_paths_map.remove(path_to_remove);
				}
				m_cached_paths.remove(i);
			}
		}
	}
}
