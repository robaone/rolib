package com.robaone.dbase;

import java.sql.Connection;
import java.sql.DriverManager;
import com.robaone.dbase.HDBConnectionManager;

public class DatabaseConnectionManager implements HDBConnectionManager {

	private String url = null;
	private boolean close = true;
	private Connection con = null;
	private String username;
	private String password;

	public Connection getConnection() throws Exception {
		if(this.con == null){
			con = DriverManager.getConnection(
					this.getUrl(), 
					this.getUsername(), 
					this.getPassword());
		}
		return con;
	}

	public void closeConnection(Connection m_con) throws Exception {
		if(this.autoClose()){
			m_con.close();
			con = null;
		}
	}
	
	public void autoClose(boolean flag){
		this.close = flag;
	}
	
	protected boolean autoClose(){
		return this.close;
	}

	public void setUrl(String jdbcurl) {
		this.url = jdbcurl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

}
