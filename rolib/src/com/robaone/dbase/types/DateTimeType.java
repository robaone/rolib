package com.robaone.dbase.types;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.robaone.dbase.DBSQLValidationException;

/**
 * <pre>   Copyright 2013 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class DateTimeType extends DBType{

	public DateTimeType(java.util.Date dt) {
		this.m_val = new java.sql.Timestamp(dt.getTime());
	}
	
	public DateTimeType(String date) throws Exception {
		this.m_val = this.getNewObject(date);
	}
	
	public java.sql.Timestamp getTimestamp(){
		return (java.sql.Timestamp)this.m_val;
	}

	@Override
	public Object getNewObject(String val) throws DBSQLValidationException {
		SimpleDateFormat df1 = new SimpleDateFormat("MM/dd/yy hh:mma");
		SimpleDateFormat df2 = new SimpleDateFormat("MM/dd/yy hh:mm a");
		SimpleDateFormat df3 = new SimpleDateFormat("MM/dd/yy");
		java.util.Date dt = null;
		try{
			dt = df1.parse(val);
		}catch(Exception e){
			try{
				dt = df2.parse(val);
			}catch(Exception e1){
				try{
					dt = df3.parse(val);
				}catch(Exception e2){
					e2.printStackTrace();
				}
			}
		}
		return new java.sql.Timestamp(dt.getTime());
	}

}
