package com.robaone.dbase;

import java.io.InputStream;

public interface QueryConfig {

	public InputStream getResourceAsStream() throws Exception ;

}
