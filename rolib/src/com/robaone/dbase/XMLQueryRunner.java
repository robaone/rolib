package com.robaone.dbase;

import java.io.InputStream;
import java.sql.ResultSetMetaData;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.json.JSONObject;

import com.robaone.api.json.JSONResponse;
import com.robaone.dbase.XMLConnectionBlock;

public class XMLQueryRunner extends XMLConnectionBlock<JSONObject> implements QueryRunner {
	private	QueryConfig queryConfig;
	public XMLQueryRunner(String queryName,QueryConfig config)
			throws ParserConfigurationException, JSONException {
		super(queryName, 0, 100);
		this.setQueryConfig(config);
	}

	public JSONResponse<JSONObject> getResults() {
		JSONResponse<JSONObject> response = new JSONResponse<JSONObject>();
		for(int i = 0; i < this.getRecordCount();i++){
			response.addData(this.getRecord(i));
		}
		response.setTotalRows(this.getTotalRows());
		response.setEndRow(this.getEndRow());
		response.setStartRow(this.getStartRow());
		response.setPage(this.getPage());

		return response;
	}

	@Override
	protected JSONObject bindRecord() throws Exception {
		ResultSetMetaData metadata = this.getResultSet().getMetaData();
		JSONObject jo = new JSONObject();
		for(int i = 0; i < metadata.getColumnCount();i++){
			String name = metadata.getColumnName(i+1);
			Object value = this.getResultSet().getObject(i+1);
			jo.put(name, value);
		}
		return jo;
	}

	@Override
	protected InputStream getQueryFile() {
		InputStream in;
		try {
			in = this.getQueryConfig().getResourceAsStream();
			return in;
		} catch (Exception e) {
			return null;
		}
	}

	public QueryConfig getQueryConfig() {
		return queryConfig;
	}

	public void setQueryConfig(QueryConfig queryConfig) {
		this.queryConfig = queryConfig;
	}

}
