package com.robaone.dbase;

import org.json.JSONException;
import org.json.JSONObject;

import com.robaone.api.json.JSONResponse;

public interface QueryRunner {

	public void setParameter(String name, Object value) throws JSONException;

	public void run(HDBConnectionManager dbConnectionManager) throws Exception;

	public JSONResponse<JSONObject> getResults();

	public void setPage(int parseInt) throws JSONException;

	public void setLimit(int parseInt) throws JSONException;

}
