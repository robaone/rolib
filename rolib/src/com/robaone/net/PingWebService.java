package com.robaone.net;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.auth.params.AuthPNames;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.params.AuthPolicy;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.PrettyXmlSerializer;
import org.htmlcleaner.TagNode;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.robaone.api.business.FieldValidator;
import com.robaone.api.business.XMLDocumentReader;
import com.robaone.dbase.types.StringType;
import com.robaone.json.JSONToXML;
import com.robaone.script.ScriptBootstrap;
import com.robaone.util.Strmanip;
import com.robaone.xml.ROTransform;

/**
 * The class is designed to perform http and https requests
 * to web services in order to perform actions defined in the script xml file.<br/>
 * These actions will allow this program to test web site functionality.
 * <br/><br/>
 * Actions are determined by an XML script file.<br/>
 * <br/>
 * The script file uses the schema <a href="http://mdgtmfsrv1:8080/tfs/web/UI/Pages/Scc/ViewSource.aspx?path=%24%2FeStatements%2Ftrunk%2Fweb_services%2Fsrc%2Fcom%2Fmicrodg%2Fdata%2Fxsd%2FWebServiceScript.xsd">WebServiceScript.xsd</a><br/>
 * <br/>
 * This class supports the following actions
 * <ul>
 * <li>end_success</li>
 * <li>end_failure</li>
 * <li>content_compare</li>
 * <li>find_content</li>
 * <li>goto</li>
 * <li>sleep</li>
 * <li>query_response</li>
 * <li>query_content</li>
 * <li>test_response</li>
 * <li>test_content</li>
 * <li>test_all_links</li>
 * <li>clear_cookies</li>
 * <li>echo</li>
 * </ul>
 * <h2>Action: end_sucess</h2>
 * <p>End the current step as a success.</br>
 * This end the program with an exit code of zero and the log entry is created as a success.</p>
 * <h2>Action: end_failure</h2>
 * <p>End the current step as a failure.</br>
 * This ends the program with an error exit code and a log entry is created as a failure.</p>
 * <h2>Action: content_compare</h2>
 * <p>Compare the html / text content that was received against an expected result.</br>
 * If the content does not match, end the program as a failure.</p>
 * <h2>Action: find_content</h2>
 * <p>Look for text content within the html using an XPath expression.<br/>
 * The html content is first converted to xml and stored in a file.  Then the XPath expression is run against that xml document allowing you to look for specific information.<br/>
 * That text will be stored in a variable with a specified 'name'.<br/>
 * That variable can then be used in subsequent steps or actions.</p>
 * <h2>Action: goto</h2>
 * <p>Skip to the step number specified<br/>
 * This command allows you to go to any step number in the xml script.<br/>
 * It should be used at the end of a set of actions to go to the next step or possibly skip a step.</p>
 * <h2>Action: sleep</h2>
 * <p>Sleep for x milliseconds<br/>
 * This command puts the script to sleep for a number of milliseconds before it goes to the next action.<br/>
 * This is useful when you want to poll a web service and you don't want to create too many requests and use of your loop too quickly.</p>
 * <h2>Action: query_response</h2>
 * <p>Run an XQuery statement against the Http Response parameters and save to a variable.</p>
 * <h2>Action: query_content</h2>
 * <p>Run an XQuery statement against the html or xml content that is returned and save to a variable.</p>
 * <h2>Action: test_response</h2>
 * <p>Run an XQuery statement that resolves to true or false against the Http Response parameters.  Fail if the answer is false.</p>
 * <h2>Action: test_content</h2>
 * <p>Run an XQuery statement that resolves to true or false against the html or xml content that is returned.  Fail if the answer is false.</p>
 * <h2>Action: test_all_links</h2>
 * <p>Test all links in the html document.  Fail if any link does not return status code 200.</p>
 * <h2>An Example</h2>
 * <p>Let's say you want to use this program to test the functionality of a document hosting web site.  You will first need to know how to log
 * into the site as well as what information you should be looking for.<br/>
 * The following is an example of that.
 * The script below will perform the following actions.
 * <ul>
 * <li>Ping https://www.pdmidata.com/cica and expect an text/html response to make sure the site is up</li>
 * <li>Send a POST request to https://www.pdmidata.com/cica/login.asp to log into the site</li>
 * <li>Send a POST request to https://www.pdmidata.com/cica/search.asp to look for a set of documents</li>
 * <li>Retrieve one of the document links</li>
 * <li>Download the pdf document and expect the data type application/pdf</li>
 * </ul></p>
 *     <pre>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;
 * &lt;Ping xmlns=&quot;http://www.microdg.com/WebServiceScript&quot;
 * xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
 * xsi:schemaLocation=&quot;http://www.microdg.com/WebServiceScript ../xsd/WebServiceScript.xsd &quot;&gt;
 * 	&lt;Step number=&quot;1&quot;&gt;
 *		&lt;request method=&quot;GET&quot;&gt;
 * 			&lt;url&gt;https://www.pdmidata.com/cica&lt;/url&gt;
 * 		&lt;/request&gt;
 * 		&lt;response content_type=&quot;text/html&quot;&gt;
 * 			&lt;content&gt;&lt;/content&gt;
 * 		&lt;/response&gt;
 * 		&lt;onsuccess&gt;
 * 			&lt;action type=&quot;goto&quot;&gt;2&lt;/action&gt;
 * 		&lt;/onsuccess&gt;
 * 		&lt;onfailure&gt;
 * 			&lt;action type=&quot;end_failure&quot;&gt;&lt;/action&gt;
 * 		&lt;/onfailure&gt;
 * 	&lt;/Step&gt;
 * 	&lt;Step number=&quot;2&quot;&gt;
 * 		&lt;request method=&quot;POST&quot;&gt;
 * 			&lt;url&gt;https://www.pdmidata.com/cica/login.asp&lt;/url&gt;
 * 			&lt;parameter name=&quot;usernm&quot;&gt;xxxxxxxxxx&lt;/parameter&gt;
 * 			&lt;parameter name=&quot;pass&quot;&gt;xxxxxxxxxxx&lt;/parameter&gt;
 * 			&lt;parameter name=&quot;B1&quot;&gt;Login&lt;/parameter&gt;
 * 		&lt;/request&gt;
 * 		&lt;response content_type=&quot;text/html&quot;&gt;
 * 			&lt;content&gt;&lt;/content&gt;
 * 		&lt;/response&gt;
 * 		&lt;onsuccess&gt;
 * 			&lt;action type=&quot;goto&quot;&gt;3&lt;/action&gt;
 * 		&lt;/onsuccess&gt;
 * 		&lt;onfailure&gt;
 * 			&lt;action type=&quot;end_failure&quot;&gt;&lt;/action&gt;
 * 		&lt;/onfailure&gt;
 * 	&lt;/Step&gt;
 * 	&lt;Step number=&quot;3&quot;&gt;
 * 		&lt;request method=&quot;POST&quot;&gt;
 * 			&lt;url&gt;https://www.pdmidata.com/cica/search.asp&lt;/url&gt;
 * 			&lt;parameter name=&quot;StartRow&quot;&gt;0&lt;/parameter&gt;
 * 			&lt;parameter name=&quot;EndRow&quot;&gt;99&lt;/parameter&gt;
 * 			&lt;parameter name=&quot;PageDir&quot;&gt;none&lt;/parameter&gt;
 * 			&lt;parameter name=&quot;SearchRptDate&quot;&gt;01/20/2012&lt;/parameter&gt;
 * 			&lt;parameter name=&quot;SearchJobName&quot;&gt;&lt;/parameter&gt;
 * 			&lt;parameter name=&quot;SearchRptName&quot;/&gt;
 * 			&lt;parameter name=&quot;SearchDropLoc&quot;/&gt;
 * 			&lt;parameter name=&quot;SearchJobNum&quot;/&gt;
 * 			&lt;parameter name=&quot;SearchRptDesc&quot;/&gt;
 * 			&lt;parameter name=&quot;Submit1&quot;&gt;Submit&lt;/parameter&gt;
 * 		&lt;/request&gt; 
 *		&lt;response content_type=&quot;text/html&quot;&gt;
 * 			&lt;content&gt;&lt;/content&gt;
 * 		&lt;/response&gt;
 * 		&lt;onsuccess&gt;
 * 			&lt;action type=&quot;find_content&quot; name=&quot;link&quot;&gt;(//table[@id = &#39;ResultsTable&#39;]//a[starts-with(@href,&#39;showimage.asp?&#39;)]/@href)[1]&lt;/action&gt;
 * 			&lt;action type=&quot;goto&quot;&gt;4&lt;/action&gt;
 * 		&lt;/onsuccess&gt;
 * 		&lt;onfailure&gt;
 * 			&lt;action type=&quot;end_failure&quot;&gt;&lt;/action&gt;
 * 		&lt;/onfailure&gt;
 * 	&lt;/Step&gt;
 * 	&lt;Step number=&quot;4&quot;&gt;
 * 		&lt;request method=&quot;GET&quot;&gt;
 * 			&lt;url variable=&quot;__base_uri&quot; step=&quot;3&quot;&gt;&lt;/url&gt;
 * 			&lt;url variable=&quot;link&quot; step=&quot;3&quot;&gt;&lt;/url&gt;
 * 		&lt;/request&gt;
 * 		&lt;response content_type=&quot;text/html&quot;&gt;
 * 			&lt;content&gt;&lt;/content&gt;
 * 		&lt;/response&gt;
 * 		&lt;onsuccess&gt;
 * 			&lt;action type=&quot;find_content&quot; name=&quot;link&quot;&gt;//frameset/frame/@src[1]&lt;/action&gt;
 * 			&lt;action type=&quot;goto&quot;&gt;5&lt;/action&gt;
 * 		&lt;/onsuccess&gt;
 * 		&lt;onfailure&gt;
 * 			&lt;action type=&quot;end_failure&quot;/&gt;
 * 		&lt;/onfailure&gt;
 *	&lt;/Step&gt;
 * 	&lt;Step number=&quot;5&quot;&gt;
 * 		&lt;request method=&quot;GET&quot;&gt;
 * 			&lt;url variable=&quot;__base_uri&quot; step=&quot;3&quot;&gt;&lt;/url&gt;
 * 			&lt;url variable=&quot;link&quot; step=&quot;4&quot;&gt;&lt;/url&gt;
 * 		&lt;/request&gt;
 * 		&lt;response content_type=&quot;application/pdf&quot;&gt;
 * 			&lt;content&gt;&lt;/content&gt;
 * 		&lt;/response&gt;
 * 		&lt;onsuccess&gt;
 * 			&lt;action type=&quot;end_success&quot;&gt;&lt;/action&gt;
 * 		&lt;/onsuccess&gt;
 * 		&lt;onfailure&gt;
 * 			&lt;action type=&quot;end_failure&quot;&gt;&lt;/action&gt;
 * 		&lt;/onfailure&gt;
 * 	&lt;/Step&gt;
 * &lt;/Ping&gt;</pre>
 * @author Ansel Robateau
 * @version 1.1
 */
abstract public class PingWebService extends ScriptBootstrap implements Runnable {
	public class RequestTimeoutTimer extends TimerTask {
		private boolean closed = false;
		@Override
		public void run() {
			if(!isClosed()){
				System.err.print("Timeout reached");
				System.exit(1);
			}
		}

		public synchronized void close() {
			this.closed = true;
		}
		
		public boolean isClosed() {
			return this.closed;
		}
	}
	public class XPathAction {
		private XMLDocumentReader reader;

		public String getStyleSheet(String xpath) {
			String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
					"<xsl:stylesheet version=\"1.0\"\n"+
					"\txmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n"+
					"\txmlns:xalan=\"http://xml.apache.org/xalan\" \n"+
					"\txmlns:java=\"http://xml.apache.org/xslt/java\" \n"+
					"\texclude-result-prefixes=\"java\">\n"+
					"<xsl:output method=\"text\" omit-xml-declaration=\"yes\"/>\n"+
					"<xsl:template match=\"/\">\n"+
					"        <xsl:variable name=\"result\" select=\""+this.escapeXPath(xpath)+"\"/>\n"+
					"\t<xsl:value-of select=\"$result\"/>\n"+
					"</xsl:template>\n"+
					"</xsl:stylesheet>";
			return str;
		}

		protected String escapeXPath(String xpath) {
			String retval = null;
			try{
				if(xpath != null){
					retval = xpath.replaceAll("\"", "&quot;");
					retval = retval.replaceAll("<", "&lt;");
					retval = retval.replaceAll(">", "&gt;");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			return retval;
		}

		protected String runQuery(String xml,String xpath) throws ParserConfigurationException,
		SAXException, IOException, Exception {
			XMLDocumentReader reader = this.getReader();
			reader.read(xml);
			String result = runQuery(reader.getDocument(),xpath);
			return result;
		}
		protected String runQuery(Document doc, String xpath) throws Exception {
			String stylesheet = getStyleSheet(xpath);
			ROTransform trn = new ROTransform(stylesheet);
			String result = trn.transformXML(doc);
			return result;
		}

		public XMLDocumentReader getReader() throws ParserConfigurationException {
			if(this.reader == null){
				this.setReader(new XMLDocumentReader());
			}
			return reader;
		}

		public void setReader(XMLDocumentReader reader) {
			this.reader = reader;
		}
	}

	private static final String CONTENT_XML = "__content.xml";
	private static final String BASE_URI = "__base_uri";
	private static final String URI = "__uri";
	private static final String STEP_COUNTER = "__step_counter";
	private static final String OVER_STEP_LIMIT_ERROR = "This step has been run too many times.  It has passed the step limit";
	private static final String GET = "get";
	private static final String POST = "post";
	private static final String SLEEP = "sleep";
	private static final String FIND_CONTENT = "find_content";
	private static final String CONTENT_MATCH_ERROR = "Content does not match what was expected";
	private static final String CONTENT_COMPARE = "content_compare";
	private static final String END_FAILURE_MESSAGE = "This script is ending in a failure";
	private static final String END_FAILURE = "end_failure";
	private static final String END_SUCCESS = "end_success";
	private static final String GOTO = "goto";
	private static final String QUERY_RESPONSE = "query_response";
	private static final String RESPONSE = "__response";
	private static final String TEST_RESPONSE = "test_response";
	private static final String TEST_CONTENT = "test_content";
	private static final String TEST_ALL_LINKS = "test_all_links";
	private static final String QUERY_CONTENT = "query_content";
	private static final String STEP_LIMIT = "step.limit";
	private static final String STEP_RESPONSE = "step.response";
	private static final String CLEAR_COOKIES = "clear_cookies";
	private static final String PUT = "put";
	private static final String DELETE = "delete";
	private static final String ECHO = "echo";
	private DocumentBuilderFactory factory;
	protected DocumentBuilder builder;
	private XPathFactory xfactory;
	private XPath xpath;
	private Document m_parse_script;
	private String m_script_name = null;
	private int m_next_step = 1;
	DefaultHttpClient client  = null;
	CredentialsProvider credsProvider = new BasicCredentialsProvider();
	private String ctype;
	private InputStream in;
	private int m_step_limit = 5;
	private Exception exception;
	private int exitCode;
	private boolean threaded = false;
	private String[] args = {};
	protected X509TrustManager tm = new X509TrustManager() {

		@Override
		public void checkClientTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {

		}

		@Override
		public void checkServerTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {

		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

	};
	HashMap<String,HashMap<String,Object>> m_steps = new HashMap<String,HashMap<String,Object>>();
	private int m_current_step;
	private BasicHttpContext localContext;
	private XMLDocumentReader m_doc_reader;
	private String m_message;
	private long m_content_length;
	private boolean m_end;
	private Timer timer;
	public PingWebService(){
		try{
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			xfactory = XPathFactory.newInstance();
			xpath = xfactory.newXPath();
			this.m_doc_reader = new XMLDocumentReader();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Override
	public void run() {
		try{
			this.m_end = false;
			setupStepLimit();
			setupAuthenticationMethods();
			String output_path = null;
			output_path = getOutputPath();
			this.setExitCode(99);
			NodeList steps = findXPathNode("/ping:Ping/ping:Step");
			while(this.m_next_step > 0 && (this.getExitCode() == 0 || this.getExitCode() == 99) && steps.getLength() > 0){
				runStep(output_path);
			}
			client.getConnectionManager().shutdown();
		}catch(Exception e){
			this.setException(e);
		}finally{
			this.getTimer().cancel();
		}
	}
	public void run(String[] args) throws Exception {
		this.setArgs(args);
		this.initialize();
		this.run();
	}
	public void runStep(String output_path) throws Exception, SAXException,
	IOException, XPathExpressionException,
	UnsupportedEncodingException, FileNotFoundException,
	UnknownHostException, ClientProtocolException {
		this.setCurrentStep(this.m_next_step);
		resetStepPointer();
		String base_path = getBaseXPathForStep();
		String description = getDescription(base_path);
		if(FieldValidator.exists(description)){
			this.writeLog("### "+ description + " ###");
		}
		NodeList step = findXPathNode(base_path);
		if(step.getLength() > 0){
			/**
			 * Build the request string
			 */
			String url_str = buildRequestString(base_path);
			String method = getRequestMethod(base_path);
			List<NameValuePair> headers = buildRequestHeaders(base_path);
			String content_type = getExpectedContentType(base_path);
			String expected_content = "";
			try{
				expected_content = getExpectedContent(base_path);
			}catch(Exception e){}
			method = getDefaultRequestMethod(method);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			InputStream data_stream = null;
			String data_content_type = null;
			long data_length = 0;
			if(method.equalsIgnoreCase(GET) || method.equalsIgnoreCase(DELETE)){
				/**
				 * Append Get parameters
				 */
				url_str = appendGetParameters(base_path, url_str);
			}else if(method.equalsIgnoreCase(POST) || method.equalsIgnoreCase(PUT)){
				/**
				 * Handle the data object if it exists
				 */
				NodeList data_nodes = findXPathNode(base_path+"/ping:request/ping:data");
				if(data_nodes != null && data_nodes.getLength() > 0){
					url_str = appendGetParameters(base_path, url_str);
					Node data_node = data_nodes.item(0);
					String type = findXPathText(data_node,"@type");
					String data = this.replaceParameters(findXPathText(data_node,"text()"));
					if(FieldValidator.exists(type)){
						if(type.equals("file")){
							/**
							 * Read a file as an input stream
							 */
							File file = new File(data);
							this.writeLog(" - Loading file: "+file.toString());
							data_length = file.length();
							data_stream = new FileInputStream(file);
							if(data.toLowerCase().endsWith(".xml")){
								data_content_type = "text/xml";
							}else if(data.toLowerCase().endsWith(".sxml")){
								data_content_type = "application/soap+xml";
							}else if(data.toLowerCase().endsWith(".htm") || data.toLowerCase().endsWith(".html")){
								data_content_type = "text/html";
							}else if(data.toLowerCase().endsWith(".txt")){
								data_content_type = "text/plain";
							}else if(data.toLowerCase().endsWith(".json")){
								data_content_type = "application/json";
							}else{
								data_content_type = "application/byte-octet";
							}
						}else if(type.equals("url")){
							/**
							 * Read a url as an input stream
							 */
							//TODO: Implement
						}
					}else{
						byte[] bytes = data.getBytes();
						data_stream = new ByteArrayInputStream(bytes);
						this.writeLog(" - Request Data: "+data);
						data_length = bytes.length;
					}
				}else{
					preparePostParameters(base_path, nvps);
				}
			}else{
				throw new Exception("Invalid request method, "+method);
			}
			ctype = null;
			in = null;
			java.util.Date date = new java.util.Date();

			writeLog(method+" Request: " + url_str);
			boolean success = true;

			/**
			 * If credentials are included in this get request,
			 * setup the credentials with the client
			 */
			String username = findXPathText(base_path+"/ping:request/@username");
			String password = findXPathText(base_path+"/ping:request/@password");
			String domain = findXPathText(base_path+"/ping:request/@domain");
			String inmemory = findXPathText(base_path+"/ping:response/@inmemory");
			String filename = findXPathText(base_path+"/ping:response/@filename");
			if(FieldValidator.exists(username) && FieldValidator.exists(password)){
				if(FieldValidator.exists(domain)){
					java.net.InetAddress i = java.net.InetAddress.getLocalHost();
					NTCredentials creds = new NTCredentials(username,password,i.getHostName(),domain);
					this.client.getCredentialsProvider().setCredentials(new AuthScope("mcp.microdg.corp", AuthScope.ANY_PORT),creds);
				}else{
					UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
					this.client.getCredentialsProvider().setCredentials(new AuthScope(url_str, AuthScope.ANY_PORT),creds);
				}
				this.localContext = new BasicHttpContext();
			}
			RequestTimeoutTimer timer = startTimerThread();
			this.log(headers);
			if(method.equalsIgnoreCase(POST)){
				success = performPost(output_path, url_str,headers, nvps, data_stream,data_length,data_content_type,!"false".equalsIgnoreCase(inmemory),filename);
			}else if(method.equalsIgnoreCase(GET)){
				success = performGet(output_path, url_str,headers,!"false".equalsIgnoreCase(inmemory),filename);
			}else if(method.equalsIgnoreCase(PUT)){
				success = performPut(output_path, url_str,headers, nvps, data_stream, data_length, data_content_type,!"false".equalsIgnoreCase(inmemory),filename);
			}else if(method.equalsIgnoreCase(DELETE)){
				success = performDelete(output_path, url_str,headers, nvps, data_stream, data_length, data_content_type,!"false".equalsIgnoreCase(inmemory),filename);
			}
			timer.close();
			if(FieldValidator.exists(content_type) && FieldValidator.exists(ctype) && !content_type.equalsIgnoreCase(ctype.split(";")[0])){
				this.writeLog("Content type, "+ctype+", does not match "+content_type+".");
				success = false;
			}else if(FieldValidator.exists(content_type) && !FieldValidator.exists(ctype)){
				this.writeLog("Content type, "+ctype+", does not match "+content_type+".");
				success = false;
			}
			long time = new java.util.Date().getTime() - date.getTime();
			writeLog("Response time: "+time+" ms");
			resetStepPointer();
			if(success){
				/**
				 * Perform action on success
				 */
				NodeList actions = this.findXPathNode(base_path+"/ping:onsuccess/ping:action");
				for(int j = 0; j < actions.getLength();j++){
					try{
						performAction(actions.item(j),expected_content);
						if(this.m_end == true){
							break;
						}
					}catch(Exception e){
						if(e instanceof FailureException){
							break;
						}else{
							failureActions(base_path, expected_content);
							break;
						}
					}
				}
			}else{
				/**
				 * Perform action on failure
				 */
				failureActions(base_path, expected_content);
			}
		}else{
			throw new Exception("Step "+this.getCurrentStep()+" does not exist");
		}
	}
	protected void log(List<NameValuePair> headers) {
		for(NameValuePair header : headers) {
			this.writeLog(" - Header: "+header.getName() + ": "+header.getValue());
		}
	}
	protected RequestTimeoutTimer startTimerThread() {
		RequestTimeoutTimer timeout = new RequestTimeoutTimer();
		try{
			this.getTimer().schedule(timeout, this.getTimeout());
		}catch(java.lang.IllegalStateException e){
			this.setTimer(null);
			this.getTimer().schedule(timeout, this.getTimeout());
		}
		return timeout;
	}
	public long getTimeout() {
		long timeout = 60000;
		try{
			timeout = Long.parseLong(this.getProperty("timeout"));
		}catch(Exception e){}
		return timeout;
	}
	private String getDescription(String base_path) throws Exception {
		String description_path = base_path+"/@description";
		String description = this.findXPathText(description_path);
		return description;
	}
	public String getDefaultRequestMethod(String method) {
		if(!FieldValidator.exists(method)){
			method = GET;
		}
		return method;
	}
	public String getExpectedContent(String base_path) throws SAXException,
	IOException, XPathExpressionException {
		return findXPathText(base_path+"/ping:response/ping:content/text()");
	}
	public String getExpectedContentType(String base_path) throws SAXException,
	IOException, XPathExpressionException {
		return findXPathText(base_path+"/ping:response/@content_type");
	}
	public String getRequestMethod(String base_path) throws SAXException,
	IOException, XPathExpressionException {
		return findXPathText(base_path+"/ping:request/@method");
	}
	public String buildRequestString(String base_path) throws Exception {
		String url_str = "";
		NodeList urls = findXPathNode(base_path+"/ping:request/ping:url");
		for(int i = 0; i < urls.getLength();i++){
			/**
			 * Text content is processed first.
			 */
			url_str += this.replaceParameters(urls.item(i).getTextContent());
			if(urls.item(i).hasAttributes()){
				/**
				 * There are variables to retrieve
				 */
				int variable_step = Integer.parseInt(urls.item(i).getAttributes().getNamedItem("step").getTextContent());
				String variable_name = urls.item(i).getAttributes().getNamedItem("variable").getTextContent();
				url_str += this.getStepVariable(variable_step, variable_name);
			}
		}
		return url_str;
	}
	public List<NameValuePair> buildRequestHeaders(String base_path) throws Exception {
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		NodeList header_elements = findXPathNode(base_path+"/ping:request/ping:header");
		for(int i = 0; i < header_elements.getLength();i++){
			/**
			 * Text content is processed first.
			 */
			Node header_node = header_elements.item(i);
			String name = header_node.getAttributes().getNamedItem("name").getTextContent();
			String value = header_node.getTextContent();
			value = this.replaceParameters(value);
			NameValuePair pair = new BasicNameValuePair(name,value);
			headers.add(pair);

		}
		return headers;
	}
	public String getBaseXPathForStep() {
		return "/ping:Ping/ping:Step[@number = '"+(this.getCurrentStep())+"']";
	}
	public void resetStepPointer() {
		this.m_next_step = 0;
	}
	public void setupAuthenticationMethods() {
		List<String> authpref = new ArrayList<String>();
		authpref.add(AuthPolicy.BASIC);
		authpref.add(AuthPolicy.DIGEST);
		client.getParams().setParameter(AuthPNames.PROXY_AUTH_PREF, authpref);
	}
	public void setupStepLimit() {
		try{
			int new_limit = Integer.parseInt(this.getProperty(STEP_LIMIT));
			this.m_step_limit = new_limit;
		}catch(Exception e){}
	}

	protected String appendGetParameters(String base_path, String url_str)
			throws Exception, SAXException, IOException,
			XPathExpressionException, UnsupportedEncodingException {
		NodeList parameters = findXPathNode(base_path+"/ping:request/ping:parameter");
		if(parameters.getLength() > 0){
			url_str += "?";
		}
		for(int j = 0;j < parameters.getLength();j++){
			String param_name = findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/@name");
			String param_value = this.replaceParameters(findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/text()"));
			String param_variable = findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/@variable");
			String param_step = findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/@step");
			if(!FieldValidator.exists(param_step)){
				param_step = ""+this.m_current_step;
			}
			if(FieldValidator.exists(param_variable) && FieldValidator.isNumber(param_step)){
				HashMap<String,Object> map = this.m_steps.get(param_step);
				Object value = map.get(param_variable);
				param_value = value.toString();
			}
			url_str += (j > 0 ? "&" : "") + param_name + "=" + (param_value == null ? "" : URLEncoder.encode(param_value, "UTF-8"));
		}
		return url_str;
	}

	protected void preparePostParameters(String base_path,
			List<NameValuePair> nvps) throws Exception, SAXException,
			IOException, XPathExpressionException {
		NodeList parameters = findXPathNode(base_path+"/ping:request/ping:parameter");
		for(int j = 0;j < parameters.getLength();j++){
			String param_name = findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/@name");
			String param_value = this.replaceParameters(findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/text()"));
			String param_variable = findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/@variable");
			String param_step = findXPathText(base_path+"/ping:request/ping:parameter["+(j+1)+"]/@step");
			if(!FieldValidator.exists(param_step)){
				param_step = ""+this.m_current_step;
			}
			if(FieldValidator.exists(param_variable) && FieldValidator.isNumber(param_step)){
				HashMap<String,Object> map = this.m_steps.get(param_step);
				Object value = map.get(param_variable);
				param_value = value.toString();
			}
			nvps.add(new BasicNameValuePair(param_name,param_value));
		}
	}

	protected void failureActions(String base_path, String expected_content)
			throws Exception {
		NodeList actions = this.findXPathNode(base_path+"/ping:onfailure/ping:action");
		for(int j = 0; j < actions.getLength();j++){
			performAction(actions.item(j),expected_content);
			if(this.m_end == true){
				break;
			}
		}
	}

	protected boolean performPost(String output_path, String url_str, List<NameValuePair> headers,
			List<NameValuePair> nvps, InputStream data_stream,long length, String content_type,boolean inmemory, String filename) throws UnsupportedEncodingException,
			IOException, ClientProtocolException, Exception,
			FileNotFoundException {
		boolean retval = true;
		this.setStepVariable(BASE_URI, this.getBasePath(url_str));
		this.setStepVariable(URI,url_str);
		HttpPost httppost = new HttpPost(url_str);
		for(int i = 0; i < headers.size();i++){
			NameValuePair header = headers.get(i);
			String name = header.getName();
			String value = header.getValue();
			httppost.addHeader(name, value);
		}
		try{
			if(data_stream != null){
				sendDataStream(data_stream, length, content_type, httppost);
			}else{
				sendFormEntities(nvps, httppost);
			}
		}catch(Exception e){
			writeLog(e.getClass().getName()+": "+e.getMessage());
			return false;
		}
		HttpResponse response = null;
		if(this.localContext == null){
			response = client.execute(httppost);
		}else{
			response = client.execute(httppost, localContext);
		}
		HttpEntity entity = response.getEntity();
		writeLog("Response Status: " + response.getStatusLine());
		if(response.getStatusLine().toString().contains("302")){
			retval = followRedirect(output_path, httppost.getURI().toString(),headers, response,inmemory,filename);
		}else{
			/**
			 * Store information about response for later reference
			 */
			retval = storeResponseInfo(output_path, inmemory, filename, retval,
					response, entity);
		}
		return retval;
	}
	protected boolean performPut(String output_path, String url_str, List<NameValuePair> headers,
			List<NameValuePair> nvps, InputStream data_stream,long length, String content_type,boolean inmemory, String filename) throws UnsupportedEncodingException,
			IOException, ClientProtocolException, Exception,
			FileNotFoundException {
		boolean retval = true;
		this.setStepVariable(BASE_URI, this.getBasePath(url_str));
		this.setStepVariable(URI,url_str);
		HttpPut httpput = new HttpPut(url_str);
		for(int i = 0; i < headers.size();i++){
			NameValuePair header = headers.get(i);
			String name = header.getName();
			String value = header.getValue();
			httpput.addHeader(name, value);
		}
		try{
			if(data_stream != null){
				sendDataStream(data_stream, length, content_type, httpput);
			}else{
				sendFormEntities(nvps, httpput);
			}
		}catch(Exception e){
			writeLog(e.getClass().getName()+": "+e.getMessage());
			return false;
		}
		HttpResponse response = null;
		if(this.localContext == null){
			response = client.execute(httpput);
		}else{
			response = client.execute(httpput, localContext);
		}
		HttpEntity entity = response.getEntity();
		writeLog("Response Status: " + response.getStatusLine());
		if(response.getStatusLine().toString().contains("302")){
			retval = followRedirect(output_path, httpput.getURI().toString(),headers, response,inmemory,filename);
		}else{
			/**
			 * Store information about response for later reference
			 */
			retval = storeResponseInfo(output_path, inmemory, filename, retval,
					response, entity);
		}
		return retval;
	}
	protected boolean performDelete(String output_path, String url_str, List<NameValuePair> headers,
			List<NameValuePair> nvps, InputStream data_stream,long length, String content_type,boolean inmemory, String filename) throws UnsupportedEncodingException,
			IOException, ClientProtocolException, Exception,
			FileNotFoundException {
		boolean retval = true;
		this.setStepVariable(BASE_URI, this.getBasePath(url_str));
		this.setStepVariable(URI,url_str);
		HttpDelete httpdelete = new HttpDelete(url_str);
		for(int i = 0; i < headers.size();i++){
			NameValuePair header = headers.get(i);
			String name = header.getName();
			String value = header.getValue();
			httpdelete.addHeader(name, value);
		}

		HttpResponse response = null;
		if(this.localContext == null){
			response = client.execute(httpdelete);
		}else{
			response = client.execute(httpdelete, localContext);
		}
		HttpEntity entity = response.getEntity();
		writeLog("Response Status: " + response.getStatusLine());
		if(response.getStatusLine().toString().contains("302")){
			retval = followRedirect(output_path, httpdelete.getURI().toString(),headers, response,inmemory,filename);
		}else{
			/**
			 * Store information about response for later reference
			 */
			retval = storeResponseInfo(output_path, inmemory, filename, retval,
					response, entity);
		}
		return retval;
	}
	public boolean storeResponseInfo(String output_path, boolean inmemory,
			String filename, boolean retval, HttpResponse response,
			HttpEntity entity) throws JSONException, Exception, IOException,
			FileNotFoundException {
		JSONObject response_json_data = new JSONObject();
		response_json_data.put("response",new JSONObject(response));
		try{
			if(inmemory){
				response_json_data.getJSONObject("response").put("content_length", entity.getContentLength());
			}
		}catch(Exception e){}
		listCookies();
		in = entity.getContent();
		ctype = null;
		if(entity.getContentType() != null){
			ctype = entity.getContentType().getValue();
		}
		storeResponse(output_path, in,inmemory,filename);
		if(!response.getStatusLine().toString().contains("200")){
			retval = false;
		}
		this.setStepVariable(RESPONSE, XML.toString(response_json_data));
		return retval;
	}
	public void sendFormEntities(List<NameValuePair> nvps, HttpPost httppost)
			throws UnsupportedEncodingException {
		httppost.setEntity(new UrlEncodedFormEntity(nvps,HTTP.UTF_8));
	}
	public void sendFormEntities(List<NameValuePair> nvps, HttpPut httpput)
			throws UnsupportedEncodingException {
		httpput.setEntity(new UrlEncodedFormEntity(nvps,HTTP.UTF_8));
	}
	public void sendDataStream(InputStream data_stream, long length,
			String content_type, HttpPost httppost) {
		InputStreamEntity ise = new InputStreamEntity(data_stream,length);
		if(FieldValidator.exists(content_type)){
			ise.setContentType(content_type);
		}
		httppost.setEntity(ise);
	}
	public void sendDataStream(InputStream data_stream, long length,
			String content_type, HttpPut httpput) {
		InputStreamEntity ise = new InputStreamEntity(data_stream,length);
		ise.setContentType(content_type);
		httpput.setEntity(ise);
	}
	protected boolean followRedirect(String output_path, String uri,List<NameValuePair> requestHeaders,
			HttpResponse response,boolean inmemory,String filename) throws Exception, IOException,
			ClientProtocolException, FileNotFoundException {
		boolean retval = true;
		Header[] headers = response.getHeaders("Location");
		writeLog("Headers:");
		writeLog(" - " + headers[0].getName()+": "+headers[0].getValue());
		String location = headers[0].getValue();
		if(!location.toLowerCase().startsWith("http")){
			location = getBasePath(uri) + location;
		}
		writeLog(" - Redirecting to "+location);
		EntityUtils.consume(response.getEntity());
		retval = performGet(output_path, location,requestHeaders,inmemory,filename);
		return retval;
	}
	protected boolean performGet(String output_path, String url_str,List<NameValuePair> headers,boolean inmemory, String filename)
			throws IOException, ClientProtocolException, Exception,
			FileNotFoundException {
		boolean retval = true;
		HttpGet httpget = new HttpGet(url_str);
		for(int i = 0; i < headers.size();i++){
			NameValuePair header = headers.get(i);
			String name = header.getName();
			String value = header.getValue();
			httpget.addHeader(name, value);
		}
		this.setStepVariable(BASE_URI, this.getBasePath(url_str));
		HttpResponse response = null;
		try{
			if(this.localContext == null){
				response = client.execute(httpget);
			}else{
				response = client.execute(httpget, localContext);
			}
		}catch(Exception e){
			writeLog(e.getClass().getName()+": "+e.getMessage());
			return false;
		}
		if(response.getStatusLine().toString().contains("302")){
			/**
			 * Follow redirects
			 */
			followRedirect(output_path, httpget.getURI().toString(),headers, response,inmemory,filename);
		}else{
			/**
			 * Store information about response for later reference
			 */
			JSONObject response_json_data = new JSONObject();
			response_json_data.put("response",new JSONObject(response));
			HttpEntity entity = response.getEntity();
			writeLog("Response Status: " + response.getStatusLine());
			try{
				if(inmemory){
					response_json_data.getJSONObject("response").put("content_length", entity.getContentLength());
				}
			}catch(Exception e){}
			writeLog("Initial set of cookies:");
			listCookies();
			writeLog("Get Http InputStream for "+url_str);
			in = entity.getContent();
			ctype = null;
			try{
				ctype = entity.getContentType().getValue();
			}catch(java.lang.NullPointerException e){}
			storeResponse(output_path, in,inmemory,filename);
			if(!response.getStatusLine().toString().contains("200")){
				retval = false;
			}
			this.setStepVariable(RESPONSE, response_json_data.toString(3));
		}
		return retval;
	}
	private String getDomain(String string) throws MalformedURLException {
		URL u = new URL(string);
		return u.getProtocol()+"://"+u.getHost()+(u.getPort() != 80 ? ":"+u.getPort() : "");
	}
	private String getBasePath(String string) {
		String retval = string;
		for(int i = string.length()-1;i >= 0;i--){
			if(string.charAt(i) == '/'){
				retval = string.substring(0,i+1);
				if(retval.equalsIgnoreCase("http://") || retval.equalsIgnoreCase("https://")){
					retval = string;
				}
				break;
			}
		}
		return retval;
	}
	protected void storeResponse(String output_path, InputStream in,boolean inmemory, String filename)
			throws FileNotFoundException, IOException, Exception {
		OutputStream output = null;
		File step_content_file = null;
		if(output_path != null){
			step_content_file = new File(output_path+buildFileName(filename));
		}
		try{
			/**
			 * Store the contents in memory
			 */
			if(inmemory || step_content_file == null){
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				IOUtils.copy(in, bout);
				//Store in step variables
				this.storeResponseInMemory(bout.toByteArray());
				if(step_content_file != null){
					output = storeResponseInFile(new ByteArrayInputStream(bout.toByteArray()), step_content_file);
				}
			}else{
				output = storeResponseInFile(in, step_content_file);
			}
		}finally{
			try{output.close();}catch(Exception e){}
			in.close();
		}
	}
	public OutputStream storeResponseInFile(InputStream in,
			File step_content_file) throws FileNotFoundException, IOException,
			Exception {
		OutputStream output;
		output = new FileOutputStream(step_content_file);
		IOUtils.copy(in, output);
		writeLog("Storing Step "+(this.getCurrentStep())+" output to "+step_content_file.getAbsolutePath());
		output.flush();
		output.close();
		this.setContentLength(step_content_file.length());
		return output;
	}
	private String buildFileName(String filename) throws Exception {
		String retval = "step"+(this.getCurrentStep())+"output "+this.getCurrentStepRunCount();
		if(FieldValidator.exists(filename)){
			filename = this.replaceParameters(filename);
			return filename;
		}else{
			return retval;
		}
	}
	private void storeResponseInMemory(byte[] byteArray) throws Exception {
		this.setStepVariable(STEP_RESPONSE, byteArray);
	}
	protected byte[] retrieveResponseFromMemory(int step) throws Exception {
		return (byte[])this.getStepVariable(step, STEP_RESPONSE);
	}
	private int getCurrentStepRunCount() {
		Integer counter = (Integer)this.getStepVariable(this.m_current_step, STEP_COUNTER);
		return counter;
	}
	protected void listCookies() throws Exception {
		List<Cookie> cookies = client.getCookieStore().getCookies();
		if(cookies.isEmpty()){
			writeLog("None");
		}else{
			for(int i = 0; i < cookies.size();i++){
				writeLog("- " + cookies.get(i).toString());
			}
		}
	}

	private void performAction(Node item,String expected_content) throws Exception {
		String type = item.getAttributes().getNamedItem("type").getTextContent();
		writeLog("Performing Action: "+type + ": "+replaceParameters(item.getTextContent()));
		if(type.equalsIgnoreCase(GOTO)){
			this.m_next_step = Integer.parseInt(item.getTextContent());
		}else if(type.equalsIgnoreCase(END_SUCCESS)){
			this.m_next_step = -1;
			this.m_end = true;
			this.setExitCode(0);
			this.setMessage(replaceParameters(item.getTextContent()));
			return;
		}else if(type.equalsIgnoreCase(END_FAILURE)){
			this.m_next_step = -1;
			/**
			 * Added this to allow for custom error messages
			 */
			String message = END_FAILURE_MESSAGE;
			try{
				if(FieldValidator.exists(item.getAttributes().getNamedItem("variable").getTextContent())){
					String value = this.m_steps.get(item.getAttributes().getNamedItem("step")).get(item.getAttributes().getNamedItem("variable")).toString();
					if(FieldValidator.exists(value)){
						message = value;
					}
				}
			}catch(Exception e){
				try{
					if(FieldValidator.exists(item.getTextContent())){
						message = this.replaceParameters(item.getTextContent());
					}
				}catch(Exception e2){}
			}
			this.setExitCode(1);
			throw new FailureException(message);
		}else if(type.equalsIgnoreCase(CONTENT_COMPARE)){
			InputStream in = new ByteArrayInputStream(this.retrieveResponseFromMemory(this.m_current_step));
			String str = IOUtils.toString(in);
			in.close();
			if(!expected_content.equals(str) && !item.getTextContent().equals(str)){
				throw new Exception(CONTENT_MATCH_ERROR);
			}
		}else if(type.equalsIgnoreCase(FIND_CONTENT) || type.equalsIgnoreCase(QUERY_CONTENT)){
			String xpath = this.replaceParameters(item.getTextContent());
			/**
			 * Convert html to document
			 */
			
			Document content_doc = storeContentasXML();
			XPathAction xpath_exp = new XPathAction();
			String value = xpath_exp.runQuery(content_doc, xpath);
			String variable_name = item.getAttributes().getNamedItem("name").getTextContent();
			this.setStepVariable(variable_name, value);
		}else if(type.equalsIgnoreCase(SLEEP)){
			Thread.sleep(Long.parseLong(item.getTextContent()));
		}else if(type.equalsIgnoreCase(QUERY_RESPONSE)){
			String rsp_value = (String) this.getStepVariable(getCurrentStep(), RESPONSE);
			getDocumentReader().read(XML.toString(new JSONObject(rsp_value)));
			XPathAction xpath_exp = new XPathAction();
			String value = xpath_exp.runQuery(getDocumentReader().getDocument(), this.replaceParameters(item.getTextContent()));
			this.setStepVariable(item.getAttributes().getNamedItem("name").getTextContent(), value);
		}else if(type.equalsIgnoreCase(TEST_RESPONSE)){
			String rsp_value = (String) this.getStepVariable(getCurrentStep(), RESPONSE);
			try{
				getDocumentReader().read(XML.toString(new JSONObject(rsp_value)));
			}catch(Exception e){
				getDocumentReader().read(rsp_value);
			}
			XPathAction xpath_exp = new XPathAction();
			String value = xpath_exp.runQuery(getDocumentReader().getDocument(), this.replaceParameters(item.getTextContent()));
			if(!value.equals("true")){
				throw new Exception("test query, "+item.getTextContent()+", did not return a true value");
			}
		}else if(type.equalsIgnoreCase(TEST_CONTENT)){
			Document content_doc = storeContentasXML();
			getDocumentReader().read(content_doc);
			String xquery = this.replaceParameters(item.getTextContent());
			XPathAction xpath_exp = new XPathAction();
			String value = xpath_exp.runQuery(getDocumentReader().getDocument(),xquery);
			//String value = getDocumentReader().findXPathString(xquery);
			this.writeLog("- Result: "+value);
			if(!value.equals("true")){
				throw new Exception("test query, "+item.getTextContent()+", did not return a true value");
			}
		}else if(type.equalsIgnoreCase(TEST_ALL_LINKS)){
			writeLog("Testing all links in current page");
			Document content_doc = storeContentasXML();
			getDocumentReader().read(content_doc);
			String path = "//a[@href]/@href";
			if(FieldValidator.exists(item.getTextContent())){
				path = item.getTextContent();
			}
			NodeList links = getDocumentReader().findXPathNode(path);
			for(int i = 0; i < links.getLength();i++){
				boolean forwarded = false;
				String location = links.item(i).getTextContent();
				do{
					HttpResponse response = null;
					if(!location.startsWith("http")){
						if(location.startsWith("/")){
							location = getDomain((String)this.getStepVariable(getCurrentStep(), BASE_URI))+location;
						}else{
							location = this.getStepVariable(getCurrentStep(), BASE_URI)+location;
						}
					}
					writeLog(" - Testing "+location);
					HttpGet httpget = new HttpGet(location);
					if(this.localContext == null){
						response = client.execute(httpget);
					}else{
						response = client.execute(httpget, localContext);
					}	
					if(response.getStatusLine().toString().contains("302")){
						Header[] headers = response.getHeaders("Location");
						writeLog("Headers:");
						writeLog(" - " + headers[0].getName()+": "+headers[0].getValue());
						location = headers[0].getValue();
						if(!location.toLowerCase().startsWith("http")){
							location = getBasePath(httpget.getURI().toString()) + location;
						}
						writeLog(" - Redirecting to "+location);
						EntityUtils.consume(response.getEntity());
						forwarded = true;
					}else{
						forwarded = false;
						EntityUtils.consume(response.getEntity());
						if(response.getStatusLine().getStatusCode() == 200){
							writeLog("   - Result: "+response.getStatusLine().toString());
						}else{
							throw new Exception(response.getStatusLine().toString());
						}
					}
				}while(forwarded);
			}
		}else if(type.equalsIgnoreCase(CLEAR_COOKIES)){
			this.client = null;
			this.initializeClient();
			writeLog(" - Cookies cleared");
		}else if(type.equalsIgnoreCase(ECHO)){
			String echo = replaceParameters(item.getTextContent());
			writeLog(echo);
		}
	}

	protected void setMessage(String replaceParameters) {
		this.m_message = replaceParameters;
	}
	public String getMessage(){
		return new StringType(this.m_message).toString();
	}
	private String replaceParameters(String textContent) throws Exception {
		return replaceParameters(textContent,false);
	}
	private String replaceParameters(String textContent,boolean urlescape) throws Exception {
		String retval = replaceInputParameters(textContent);
		retval = replaceStepVariables(retval);
		retval = replaceConfigValues(retval);
		retval = replaceContentLength(retval);

		return retval;
	}
	protected String replaceContentLength(String originalString) throws Exception {
		String pattern;
		pattern = "[$][{]content_length[}]";
		long content_length = this.getContentLength();
		String returnString = originalString.replaceAll(pattern, new Long(content_length).toString());
		return returnString;
	}
	protected long getContentLength() {
		return this.m_content_length;
	}
	private void setContentLength(long length){
		this.m_content_length = length;
	}
	protected String replaceConfigValues(String originalString) throws Exception {
		boolean more;
		String pattern;
		Pattern p;
		pattern = "[$][{]config[.]";
		String[] keys = this.getConfig().keySet().toArray(new String[0]);
		for(int i = 0; i < keys.length;i++){
			String newPattern = pattern+escapePatterns(keys,i)+"[}]";
			p = Pattern.compile(newPattern);
			more = true;
			do{
				Matcher m = p.matcher(originalString);
				if(m.find()){
					String temp = originalString.substring(0,m.start());
					temp += this.getProperty(keys[i]);
					temp += originalString.substring(m.end());
					originalString = temp;
				}else{
					more = false;
				}
			}while(more);
		}
		return originalString;
	}
	protected String escapePatterns(String[] keys, int i) {
		return keys[i];  //TODO: Implement
	}

	protected String replaceStepVariables(String retval) {
		boolean more = true;
		String pattern = "[$][{]step([0-9]+)[.]([a-zA-Z_0-9.]+)[}]";
		Pattern p = Pattern.compile(pattern);
		do{
			Matcher m = p.matcher(retval);
			if(m.find()){
				String step_number = m.group(1);
				String variable_name = m.group(2);
				Object value = this.getStepVariable(Integer.parseInt(step_number), variable_name);
				String temp = retval.substring(0,m.start());
				temp += value;
				temp += retval.substring(m.end());

				retval = temp;
			}else{
				more = false;
			}
		}while(more);
		return retval;
	}
	protected String replaceInputParameters(String textContent)
			throws Exception {
		String retval = textContent;
		if(this.getArgs() != null){
			for(int i = 0; i < this.getArgs().length;i++){
				String pattern = "[$][{]parm["+i+"][}]";
				retval = Strmanip.replaceAll(retval, pattern, this.getArgs()[i]);
			}
		}

		/**
		 * Remove any unused params with blanks
		 * e.g. $parm8 is unused.  It will be replaced with an empty string.
		 */
		{
			String pattern = "[$][{]parm[0-9]+[}]";
			retval = Strmanip.replaceAll(retval, pattern, "");
		}
		return retval;
	}
	private Document storeContentasXML() throws IOException,
	Exception, SAXException {
		String output_file_name = this.getOutputPath()+"step"+m_current_step+"output "+this.getCurrentStepRunCount()+".xml";
		try{
			// Check to see if the file is already an xml document
			XMLDocumentReader reader = new XMLDocumentReader();
			reader.read(new ByteArrayInputStream(cleanXML(output_file_name).getBytes()));
			return reader.getDocument();
		}catch(Exception e){
			Document content_doc = (Document) this.getStepVariable(this.getCurrentStep(),CONTENT_XML);
			/**
			 * Attempt to convert json data
			 */
			try{
				InputStream input = null;
				try{
					input = new ByteArrayInputStream(this.retrieveResponseFromMemory(m_current_step));
					String jsonString = IOUtils.toString(input).trim();
					if(jsonString.startsWith("{") && jsonString.endsWith("}")){
						JSONObject jo = new JSONObject();
						XMLDocumentReader reader = new XMLDocumentReader();
						reader.read(XML.toString(jo));
						File f = new File(output_file_name);
						writeLog("   - Stored in "+f.getAbsolutePath());
						FileOutputStream out = new FileOutputStream(f);
						IOUtils.write(reader.toString(),out);
						out.flush();
						out.close();
						content_doc = reader.getDocument();
						this.setStepVariable(CONTENT_XML,content_doc);
					}else{
						throw new Exception("Not JSON");
					}
				}finally{
					try{input.close();}catch(Exception e1){}
				}
			}catch(Exception je){
				String xml = cleanXML(output_file_name);
				writeLog(" - XML stored to "+output_file_name);
				content_doc = builder.parse(xml);
				this.setStepVariable(CONTENT_XML,content_doc);
			}
			return content_doc;
		}
	}
	public String cleanXML(String output_file_name)
			throws IOException, Exception {
		String str = IOUtils.toString(new ByteArrayInputStream(this.retrieveResponseFromMemory(m_current_step))).trim();
		XMLDocumentReader reader = new XMLDocumentReader();
		if((str.startsWith("{") && str.endsWith("}")) || (str.startsWith("[") && str.endsWith("]"))){
			JSONToXML converter = new JSONToXML();
			converter.setJson(str);
			converter.run();
			return converter.getXml();
		}else{
			HtmlCleaner cleaner = new HtmlCleaner();
			CleanerProperties props = cleaner.getProperties();
			TagNode node = cleaner.clean(str);
			new PrettyXmlSerializer(props).writeToFile(node,output_file_name,"utf-8");
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			new PrettyXmlSerializer(props).writeToStream(node, bout,"utf-8");
			reader.read(bout.toString());
			this.setStepVariable(CONTENT_XML,reader.getDocument());
			return bout.toString();
		}
	}
	private XMLDocumentReader getDocumentReader(){
		return this.m_doc_reader;
	}
	private Object getStepVariable(int step,String variable) {
		try{
			return this.m_steps.get(""+step).get(variable);
		}catch(Exception e){
			return null;
		}
	}
	private void setStepVariable(String string, Object content_doc) throws Exception {

		if(this.m_steps.get(""+this.getCurrentStep()) == null){
			this.m_steps.put(""+this.getCurrentStep(),new HashMap<String,Object>());
		}
		this.m_steps.get(""+this.getCurrentStep()).put(string,content_doc);
		if(string.equals(RESPONSE)){
			writeLog("- Setting Variable: "+string);
		}else{
			writeLog("- Setting Variable: "+string+": " + content_doc.toString());
		}
		if(string.equals(RESPONSE) && this.getOutputPath() != null){
			File response_file = new File(this.getOutputPath()+"step"+this.getCurrentStep()+"response "+this.getCurrentStepRunCount()+".txt");
			FileOutputStream fout = null;
			try{
				fout = new FileOutputStream(response_file);
				IOUtils.write(content_doc.toString(), fout);
				writeLog("  - Stored in "+response_file.getAbsolutePath());
			}finally{
				fout.close();
			}
		}

	}
	private void setCurrentStep(int step) throws Exception{
		writeLog("** Step "+ step + " **");
		this.m_current_step = step;
		/**
		 * Set the counter for this step
		 */
		Integer counter = (Integer)this.getStepVariable(step, STEP_COUNTER);
		if(counter == null){
			counter = new Integer(1);
		}else{
			counter = new Integer(counter.intValue()+1);
		}
		this.setStepVariable(STEP_COUNTER, counter);
		if(counter.intValue() > this.m_step_limit){
			throw new Exception(OVER_STEP_LIMIT_ERROR);
		}
	}
	private int getCurrentStep(){
		return this.m_current_step;
	}
	protected String getThreadFolder() throws Exception {
		String processName = ManagementFactory.getRuntimeMXBean().getName() + "-" + Thread.currentThread().getId();
		return processName;
	}
	protected String getOutputPath() throws Exception {
		String retval = "";
		String path = null;
		try{
			try{
				path = this.getProperty("output.path");
			}catch(Exception e){
				if(FieldValidator.exists(e.getMessage()) && e.getMessage().contains("Could not read property")){
					path = this.getProperty("output.folder");
				}else{
					throw e;
				}
			}
			if(FieldValidator.exists(path) && !path.endsWith("/")){
				path += "/";
			}
			retval = path;
		}catch(Exception e){
			String system_path = path;
			if(!FieldValidator.exists(system_path)){
				system_path = "";
			}
			if(FieldValidator.exists(system_path) && !system_path.endsWith("/")){
				system_path += "/";
			}
			retval = system_path;
		}
		if(this.isThreaded()){
			String threadFolder = retval + this.getThreadFolder() + "/";
			new File(threadFolder).mkdirs();
			return threadFolder;
		}else{
			return retval;
		}
	}
	public String findXPathText(Node node,String path) throws Exception{
		XMLDocumentReader reader = new XMLDocumentReader();
		reader.read(this.m_parse_script);
		return reader.findXPathString(node, path);
	}
	public String findXPathText(String path) throws SAXException, IOException, XPathExpressionException{
		Document doc = this.m_parse_script;
		XPathExpression expr = xpath.compile(path);
		return (String)expr.evaluate(doc, XPathConstants.STRING);
	}
	public NodeList findXPathNode(String path) throws Exception {
		Document doc = this.m_parse_script;
		xpath.setNamespaceContext(new NamespaceContext(){

			@Override
			public String getNamespaceURI(String arg0) {
				return "http://www.robaone.com/WebServiceScript";
			}

			@Override
			public String getPrefix(String arg0) {
				return "ping";
			}

			@SuppressWarnings("rawtypes")
			@Override
			public Iterator getPrefixes(String arg0) {
				HashMap<String,String> m = new HashMap<String,String>();
				m.put("ping", "");
				return m.keySet().iterator();
			}

		});
		XPathExpression expr = xpath.compile(path);
		return (NodeList)expr.evaluate(doc, XPathConstants.NODESET);
	}

	public void initialize() throws Exception {
		InputStream in = getXMLScript();
		this.setScript(builder.parse(in));
		initializeClient();
		this.m_next_step = 1;
		this.m_current_step = 1;
		this.m_steps = new HashMap<String,HashMap<String,Object>>();

	}
	abstract public InputStream getXMLScript();
	@SuppressWarnings("deprecation")
	protected void initializeClient() throws NoSuchAlgorithmException,
	KeyManagementException {
		client = new DefaultHttpClient();
		// Get an SSLContext
		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(null, new TrustManager[]{tm},null);
		org.apache.http.conn.ssl.SSLSocketFactory ssf = new org.apache.http.conn.ssl.SSLSocketFactory(ctx);
		ssf.setHostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		ClientConnectionManager ccm = client.getConnectionManager();
		SchemeRegistry sr = ccm.getSchemeRegistry();
		sr.register(new Scheme("https",ssf,443));
		client = new DefaultHttpClient(ccm,client.getParams());
	}
	protected void setScript(Document parse) {
		this.m_parse_script = parse;
	}
	public void setStepLimit(int limit){
		this.m_step_limit = limit;
	}
	protected String getScriptName() {
		try{
			return this.getProperty("script");
		}catch(Exception e){
			return this.m_script_name;
		}
	}
	public void setScriptName(String name){
		this.m_script_name  = name;
	}
	/**
	 * @param args
	 * @throws Exception 
	 */
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public int getExitCode() {
		return exitCode;
	}
	public void setExitCode(int exitCode) {
		this.exitCode = exitCode;
	}
	public String[] getArgs() {
		return args;
	}
	public void setArgs(String[] args) {
		this.args = args;
	}
	public boolean isThreaded() {
		return threaded;
	}
	public void setThreaded(boolean threaded) {
		this.threaded = threaded;
	}
	public Timer getTimer() {
		if (this.timer == null){
			this.setTimer(new Timer());
		}
		return this.timer;
	}
	public void setTimer(Timer t){
		this.timer = t;
	}

}