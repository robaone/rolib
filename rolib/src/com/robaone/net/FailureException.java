package com.robaone.net;

public class FailureException extends Exception {

	public FailureException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4475448139615007995L;

}
