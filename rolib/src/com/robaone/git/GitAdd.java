package com.robaone.git;

import java.io.File;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.dircache.DirCache;
import org.eclipse.jgit.storage.file.FileRepository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.robaone.util.BaseProgram;
import com.robaone.util.INIFileReader;

/**
 * 
 */
public class GitAdd extends BaseProgram {

	private static final String USERNAME = "username";
	private static final String PASSWORDFILE = "password.file";
	private static final String REPOSITORY = "repository";
	private static final String MESSAGE = "message";

	private CredentialsProvider m_cp;
	private File m_dir;


	protected void initialize() throws Exception {
		String name = getConfigValue(USERNAME);
		INIFileReader passwords = new INIFileReader(getConfigValue(PASSWORDFILE));
		String password = passwords.getValue("git."+name);
		m_cp = new UsernamePasswordCredentialsProvider(name,password);

		File dir = new File(getConfigValue(REPOSITORY));
		m_dir = dir;
	}

	@Override
	public void execute() throws Exception {
		this.initialize();
		Git git = new Git(new FileRepository(m_dir));
		DirCache dc = git.add().addFilepattern(".").call();
		for(int i = 0; i < dc.getEntryCount();i++){
			writeLog(dc.getEntry(i).toString());
		}
		RevCommit rc = git.commit().setMessage(getConfigValue(MESSAGE)).call();
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GitAdd main = new GitAdd();
		main.run(args);
	}

}
