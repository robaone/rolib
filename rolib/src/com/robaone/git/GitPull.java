package com.robaone.git;

import java.io.File;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.storage.file.FileRepository;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.robaone.util.BaseProgram;
import com.robaone.util.INIFileReader;

/**
 * 
 */
public class GitPull extends BaseProgram {

	private static final String USERNAME = "username";
	private static final String PASSWORDFILE = "password.file";
	private static final String REPOSITORY = "repository";
	private CredentialsProvider m_cp;
	private File m_dir;


	protected void initialize() throws Exception {
		String name = getConfigValue(USERNAME);
		INIFileReader passwords = new INIFileReader(getConfigValue(PASSWORDFILE));
		String password = passwords.getValue("git."+name);
		m_cp = new UsernamePasswordCredentialsProvider(name,password);

		File dir = new File(getConfigValue(REPOSITORY));
		m_dir = dir;
	}

	@Override
	public void execute() throws Exception {
		this.initialize();
		Git git = new Git(new FileRepository(m_dir));
		PullCommand pull = git.pull();
		PullResult pr = pull.setCredentialsProvider(m_cp).call();
		if(pr.isSuccessful()){
			String messages = pr.getFetchResult().getMessages();
			writeLog(messages);
			writeLog("Pull is successful");
		}else{
			writeLog("Pull has failed");
			throw new Exception("Pull has failed");
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GitPull main = new GitPull();
		main.run(args);
	}

}
