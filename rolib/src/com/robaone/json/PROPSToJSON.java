package com.robaone.json;

import java.io.EOFException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Properties;

import org.json.JSONObject;

import com.robaone.util.LineReader;

public class PROPSToJSON implements Runnable {
	private Exception exception;
	private String json;
	private Properties properties;
	private InputStream inputStream;
	public static void main(String[] args) {
		try{
			PROPSToJSON converter = new PROPSToJSON();
			Properties props = new Properties();
			props.load(new StringReader(converter.getInput()));
			converter.setProperties(props);
			converter.run();
			if(converter.getException() != null){
				throw converter.getException();
			}
			System.out.println(converter.getJson());
			System.exit(0);
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	@Override
	public void run() {
		try{
			JSONObject json = new JSONObject(this.getProperties());
			this.setJson(json.toString());
		}catch(Exception e){
			this.setException(e);
		}
	}
	
	protected String getInput() throws Exception {
		LineReader lr = null;
		StringBuffer buffer = new StringBuffer();
		try{
			lr = new LineReader(this.getInputStream());
			do{
				String line = lr.ReadLine();
				if(buffer.length() > 0){
					buffer.append("\n");
				}
				if(line.equals(".")){
					break;
				}
				buffer.append(line);
			}while(true);
		}catch(EOFException eof){}
		return buffer.toString();
	}

	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public InputStream getInputStream() {
		if(this.inputStream == null){
			this.setInputStream(System.in);
		}
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

}
