package com.robaone.json;

import org.json.JSONArray;
import org.json.JSONObject;

public interface JSONSanitizer {
	public JSONObject sanitize(JSONObject jo) throws Exception;
	public JSONArray sanitize(JSONArray ja) throws Exception;
}
