package com.robaone.json;

import java.io.EOFException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.json.JSONObject;
import org.json.XML;

import com.robaone.util.LineReader;

public class JSONToXML implements Runnable {
	private Exception exception;
	private Options options;
	private String json;
	private String xml;
	private boolean attributeMode;
	public JSONToXML() {
		this.options = new Options();
		this.options.addOption("a","attribute", false, "Attribute mode");
	}
	public static void main(String[] args) {
		try{
			JSONToXML converter = new JSONToXML();
			CommandLineParser parser = new BasicParser();
			CommandLine cmd = parser.parse(converter.options, args);
			if(cmd.hasOption("a")){
				converter.setAttributeMode(true);
			}
			converter.setJson(converter.getInput());
			converter.run();
			if(converter.getException() != null){
				throw converter.getException();
			}
			System.out.println(converter.getXml());
			System.exit(0);
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	@Override
	public void run() {
		try{
			if(this.isAttributeMode()){
				String xml = JSONUtils.toXMLbyAttribute(new JSONObject(this.getJson()),"json");
				this.setXml(xml);
			}else{
				String xml = XML.toString(new JSONObject(this.getJson()),"json");
				this.setXml(xml);
			}
		}catch(Exception e){
			this.setException(e);
		}
	}

	protected String getInput() throws Exception {
		LineReader lr = null;
		StringBuffer buffer = new StringBuffer();
		try{
			lr = new LineReader(System.in);
			do{
				if(buffer.length() > 0){
					buffer.append("\n");
				}
				String line = lr.ReadLine();
				if(line.equals(".")){
					break;
				}
				buffer.append(line);
			}while(true);
		}catch(EOFException eof){}
		return buffer.toString();
	}

	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
	public boolean isAttributeMode() {
		return attributeMode;
	}
	public void setAttributeMode(boolean attributeMode) {
		this.attributeMode = attributeMode;
	}

}
