package com.robaone.json;

import java.io.EOFException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import org.json.JSONObject;

import com.robaone.util.LineReader;

public class JSONToPROPS implements Runnable {
	private Exception exception;
	private String json;
	private Properties properties;
	private InputStream inputStream;
	public static void main(String[] args) {
		try{
			JSONToPROPS converter = new JSONToPROPS();
			converter.setJson(converter.getInput());
			converter.run();
			if(converter.getException() != null){
				throw converter.getException();
			}
			converter.getProperties().store(System.out, "Created by JSONToPROPS.java");
			System.exit(0);
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	@Override
	public void run() {
		try{
			JSONObject json = new JSONObject(this.getJson());
			Properties props = new Properties();
			@SuppressWarnings("unchecked")
			Iterator<String> keys = json.keys();
			while(keys.hasNext()){
				String key = keys.next();
				String value = json.getString(key);
				props.setProperty(key, value);
			}
			this.setProperties(props);
		}catch(Exception e){
			this.setException(e);
		}
	}
	
	protected String getInput() throws Exception {
		LineReader lr = null;
		StringBuffer buffer = new StringBuffer();
		try{
			lr = new LineReader(this.getInputStream());
			do{
				String line = lr.ReadLine();
				if(buffer.length() > 0){
					buffer.append("\n");
				}
				if(line.equals(".")){
					break;
				}
				buffer.append(line);
			}while(true);
		}catch(EOFException eof){}
		return buffer.toString();
	}

	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public InputStream getInputStream() {
		if(this.inputStream == null){
			this.setInputStream(System.in);
		}
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

}
