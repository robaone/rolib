package com.robaone.json;

import com.robaone.api.json.DSResponse;

/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
@SuppressWarnings({ "rawtypes" })
public class DSException extends Exception {
	private DSResponse m_response;
	private static final long serialVersionUID = 1687021754163783884L;
	public DSException(int status){
		this.m_response = new DSResponse();
		this.m_response.getResponse().setStatus(status);
	}
	public void addError(String field,String message){
		this.m_response.getResponse().addError(field, message);
	}
	public int getStatus(){
		return this.m_response.getResponse().getStatus();
	}
	public DSResponse getResponse(){
		return this.m_response;
	}
}
