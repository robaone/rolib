package com.robaone.json;

import java.io.EOFException;
import java.io.InputStream;
import java.io.StringReader;

import org.json.JSONArray;
import org.json.JSONObject;

import com.csvreader.CsvReader;
import com.robaone.util.LineReader;

public class CSVToJSON implements Runnable {
	private Exception exception;
	private String json;
	private String csv;
	private InputStream inputStream;
	public static void main(String[] args) {
		try{
			CSVToJSON converter = new CSVToJSON();
			converter.setCsv(converter.getInput());
			converter.run();
			if(converter.getException() != null){
				throw converter.getException();
			}
			System.out.println(converter.getJson());
			System.exit(0);
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	@Override
	public void run() {
		try{
			
			JSONObject json = null;
			StringReader sr = new StringReader(this.getCsv());
			CsvReader reader = new CsvReader(sr);
			json = new JSONObject();
			json.put("csv",new JSONArray());
			
			while(reader.readRecord()){
				String[] values = reader.getValues();
				json.getJSONArray("csv").put(new JSONArray(values));
			}
			String str = json.toString();
			this.setJson(str);
		}catch(Exception e){
			this.setException(e);
		}
	}
	
	protected String getInput() throws Exception {
		LineReader lr = null;
		StringBuffer buffer = new StringBuffer();
		try{
			lr = new LineReader(this.getInputStream());
			do{
				String line = lr.ReadLine();
				if(buffer.length() > 0){
					buffer.append("\n");
				}
				if(line.equals(".")){
					break;
				}
				buffer.append(line);
			}while(true);
		}catch(EOFException eof){}
		return buffer.toString();
	}

	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public String getCsv() {
		return csv;
	}
	public void setCsv(String xml) {
		this.csv = xml;
	}
	public InputStream getInputStream() {
		if(this.inputStream == null){
			this.setInputStream(System.in);
		}
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
