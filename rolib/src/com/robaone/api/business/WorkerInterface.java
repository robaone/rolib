package com.robaone.api.business;

import java.io.IOException;
import java.io.InputStream;

import com.robaone.api.data.SessionDataInterface;

public interface WorkerInterface {
	public String getKey();
	public String getMessage();
	public int getExitCode();
	public String getName();
	public void acceptPayload(InputStream input) throws IOException;
	public SessionDataInterface getSessionData();
}
