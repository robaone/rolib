package com.robaone.api.business;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MindtouchAPI {
	private String m_base_url;
	private DocumentBuilderFactory m_dbf;

	public MindtouchAPI(String url){
		this.m_base_url = url;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		this.m_dbf = dbf;
	}
	public static String streamToString(InputStream in) throws IOException{
		String retval = null;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		for(int i = in.read(buffer);i > -1;i = in.read(buffer)){
			bout.write(buffer, 0, i);
		}
		retval = bout.toString();
		return retval;
	}
	public Document getPageInfo(int page_number) throws IOException, ParserConfigurationException, SAXException{
		DocumentBuilder db = m_dbf.newDocumentBuilder();
		Document dom = db.parse(this.m_base_url+"/@api/deki/pages/"+page_number);
		return dom;
	}
	public Document getPageContents(int page_number) throws SAXException, IOException, ParserConfigurationException{
		DocumentBuilder db = m_dbf.newDocumentBuilder();
		Document dom = db.parse(this.m_base_url+"/@api/deki/pages/"+page_number+"/contents?format=xhtml");
		return dom;
	}
	public static void main(String[] args) throws ParserConfigurationException, SAXException{
		System.out.println("Testing...");
		MindtouchAPI api = new MindtouchAPI("http://wiki");
		try {
			Document doc = api.getPageInfo(1230);
			String title = doc.getElementsByTagName("title").item(0).getFirstChild().getNodeValue();
			String uri = doc.getElementsByTagName("uri.ui").item(0).getFirstChild().getNodeValue();
			System.out.println("Title = "+title);
			System.out.println("uri = "+uri);
			Document content = api.getPageContents(1230);
			Node req_by = findElement(content,"td","requested_by");
			if(req_by != null){
				System.out.println("Requested by = "+getNodeText(req_by));
			}
			System.out.println("date of request = "+getNodeText(findElement(content,"td","date_of_request")));
			Node desc_span = findElement(content,"span","Description");
			if(desc_span != null){
				Node description = findElement(desc_span.getParentNode(),"p",null);
				System.out.println("description = "+getNodeText(description));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private static Node findElement(Node parentNode, String tag,
			String id) {
		Node retval = null;
		if(parentNode.getNodeName().equals(tag) && 
				((				id != null && 
								parentNode.getAttributes().getNamedItem("id") != null &&
								parentNode.getAttributes().getNamedItem("id").equals(id)
				) || 
				(
						id == null))){
			retval = parentNode;
		}else{
			if(parentNode.hasChildNodes()){
				for(int i = 0; i < parentNode.getChildNodes().getLength();i++){
					Node child = parentNode.getChildNodes().item(i);
					retval = findElement(child,tag,id);
					if(retval != null) break;
				}
			}
		}
		return retval;
	}
	private static String getNodeText(Node req_by) {
		if(req_by != null && req_by.hasChildNodes()){
			return req_by.getTextContent();
		}
		return null;
	}
	private static Node findElement(Document content, String string,
			String string2) {
		NodeList list = content.getElementsByTagName(string);
		for(int i = 0; i < list.getLength();i++){
			Node node = list.item(i);
			if(node.getAttributes().getNamedItem("id") != null){
				String id = node.getAttributes().getNamedItem("id").getNodeValue();
				if(id != null && id.equals(string2)){
					return node;
				}
			}
		}
		return null;
	}
}
