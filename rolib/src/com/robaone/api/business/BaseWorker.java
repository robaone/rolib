package com.robaone.api.business;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;

import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.SessionDataInterface;

abstract public class BaseWorker implements WorkerInterface, Runnable {
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	protected static String TEMPFOLDER = "temp.folder";
	private String m_key;
	private String m_message;
	private int m_exitcode = -1;
	private SessionDataInterface m_sessiondata;
	private EventManagerInterface m_eventmanager;
	public BaseWorker(SessionDataInterface sdata,EventManagerInterface eman){
		this.setMessage("Initializing");
		this.m_sessiondata = sdata;
		this.setKey();
		this.setEventManager(eman);
		this.setMessage("Initialized");
	}
	
	@Override
	public void acceptPayload(InputStream in) throws IOException {
		try{
			openPayloadOutputStream();
			this.setMessage("Accepting Payload File");
			OutputStream fout = getPayloadOutputStream();
			IOUtils.copy(in, fout);
			this.setMessage("Payload File Accepted");
		}catch(Exception e){
			throw new IOException(e);
		}finally{
			closePayloadOutputStream();
		}
	}
	abstract protected void openPayloadOutputStream() throws IOException ;
	abstract protected void closePayloadOutputStream() throws IOException ;
	abstract protected OutputStream getPayloadOutputStream() throws IOException ;
	
	private void setEventManager(EventManagerInterface eman) {
		this.m_eventmanager = eman;
	}
	protected EventManagerInterface getEventManager(){
		return this.m_eventmanager;
	}
	private void setKey(){
		if(this.m_key == null){
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.m_key = new java.util.Date().getTime()+"-"+m_sessiondata.getUser().getUserId();
		}
	}
	@Override
	public String getKey() {
		return this.m_key;
	}

	public void setMessage(String message){
		this.m_message = message;
	}
	@Override
	public String getMessage() {
		return this.m_message;
	}

	public void setExitCode(int code){
		this.m_exitcode = code;
	}
	@Override
	public int getExitCode() {
		return this.m_exitcode;
	}
	
	public SessionDataInterface getSessionData(){
		return this.m_sessiondata;
	}

	@Override
	public void run() {
		try{
			this.setMessage("Starting");
			this.writeInfoEvent("Start");
			this.execute();
			this.writeInfoEvent("End");
			this.setMessage("Ending");
			this.setExitCode(0);
			this.setMessage("Ended Successfully");
		}catch(Exception e){
			this.writeErrorEvent("End with Error",e);
			if(this.getExitCode() <= 0){
				this.setExitCode(1);
			}
		}finally{
			this._base_cleanup();
		}
	}
		
	private void _base_cleanup() {
		try{
			deletePayload();
		}catch(Exception e){
			this.writeErrorEvent("Error deleting payload file",e);
		}
	}
	
	abstract protected void deletePayload() throws Exception;
	
	abstract protected InputStream getPayloadInputStream() throws IOException;
	public void writeErrorEvent(String string, Throwable e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		try{
			e.printStackTrace(pw);
		}catch(Exception e2){}
		AppDatabase.writeTrace(sw.toString());
		this.m_eventmanager.writeError(string+"\n"+sw.toString(),this.getName(), this.m_sessiondata);
	}
	public void writeInfoEvent(String string) {
		this.m_eventmanager.writeInfo(string,this.getName(),this.m_sessiondata);
	}
	public void writeWarningEvent(String string){
		this.m_eventmanager.writeWarning(string,this.getName(),this.m_sessiondata);
	}
	public void writeErrorEvent(String string){
		AppDatabase.writeTrace(string);
		this.writeErrorEvent(string,null);
	}
	public void writeTraceEvent(String string){
		this.m_eventmanager.writeTrace(string,this.getName(),this.m_sessiondata);
	}

	
	
	abstract public void execute() throws Exception;

}
