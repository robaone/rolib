package com.robaone.api.business;

import com.robaone.api.data.SessionDataInterface;

public class Authorization {
	public static boolean requiresLogin(String page,SessionDataInterface sessiondata){
		boolean retval = false;
		String[] pages = {"dashboard"};
		for(String p : pages){
			if(p.equalsIgnoreCase(page)){
				if(sessiondata == null || sessiondata.getUser() == null){
					return true;
				}
			}
		}
		return retval;
	}
}
