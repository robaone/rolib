package com.robaone.api.business;

import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.DatabaseImpl;
import com.robaone.api.data.UserManagerInterface;
import com.robaone.api.data.jdo.User_jdo;
import com.robaone.api.data.jdo.User_jdoManager;
import com.robaone.dbase.ConnectionBlock;

public class DefaultUserManager implements UserManagerInterface {
	User_jdo user = null;
	@Override
	public void setUserByID(final String id) throws Exception {
		new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				User_jdoManager uman = new User_jdoManager(this.getConnection());
				this.setPreparedStatement(uman.prepareStatement(User_jdo.USERNAME + " = ?"));
				this.getPS().setString(1, id);
				this.executeQuery();
				while(next()){
					user = uman.bindUser(getResultSet());
				}
			}

		}.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));

	}
	@Override
	public Integer getIduser() {
		return user.getIduser();
	}
	@Override
	public String getUserId() {
		return user.getUsername();
	}
	@Override
	public boolean checkPassword(String password) {
		try {
			return user.getPassword().equals(StringEncrypter.encryptString(password));
		} catch (Exception e) {
			return false;
		}
	}
	@Override
	public void setUserByID(final Integer id) throws Exception {
		new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				User_jdoManager man = new User_jdoManager(this.getConnection());
				User_jdo user = man.getUser(id);
				DefaultUserManager.this.user = user;
			}
			
		}.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));
	}
}
