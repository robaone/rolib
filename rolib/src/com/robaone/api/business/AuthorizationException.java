package com.robaone.api.business;

@SuppressWarnings("serial")
public class AuthorizationException extends Exception {
	public AuthorizationException(Throwable e){
		super(e);
	}

	public AuthorizationException(String string) {
		super(string);
	}
}
