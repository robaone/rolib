package com.robaone.api.business;

import com.robaone.api.data.SessionDataInterface;

public interface EventManagerInterface {

	void writeError(String string, String name, SessionDataInterface m_sessiondata);

	void writeInfo(String string, String name, SessionDataInterface m_sessiondata);

	void writeWarning(String string, String name, SessionDataInterface m_sessiondata);

	void writeTrace(String string, String name, SessionDataInterface m_sessiondata);

}
