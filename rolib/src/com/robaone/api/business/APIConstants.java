package com.robaone.api.business;

public interface APIConstants {
	public static final String TOKEN = "oauth_token";
	public static final String USERNAME = "username";
	public static final String TITLE = "title";
	public static final String BASE_CONFIG_PATH = "c:\\usr\\var\\";
	public static final String XSL_FOLDER = "xsl.folder";
	public static final String FORM_FOLDER = "form.folder";
	public static final String GENERAL_ERROR = "general.error";
	public static final String ERROR_DUMP = "error.dump";
	public static final String ANONYMOUS_PAGES = "anonymous.pages";
}
