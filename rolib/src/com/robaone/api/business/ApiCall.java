package com.robaone.api.business;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import net.oauth.OAuthMessage;

import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.SessionData;
import com.robaone.api.data.SessionDataInterface;
import com.robaone.api.json.DSResponse;
import com.robaone.api.json.JSONResponse;

public class ApiCall {
	public static String clean(String url) throws UnsupportedEncodingException{
		if(url.toLowerCase().startsWith("http://")){
			url = url.substring(7);
		}else if(url.toLowerCase().startsWith("https://")){
			url = url.substring(8);
		}
		url = url.replaceAll("[/&:]", "_");
		url = URLDecoder.decode(url, "UTF-8");
		return url;
	}

	public static String toXML(org.w3c.dom.Node doc) throws Exception {
		DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
		DOMImplementationLS impl = 
				(DOMImplementationLS)registry.getDOMImplementation("LS");
		LSSerializer writer = impl.createLSSerializer();
		String xml = writer.writeToString(doc);
		return xml;
	}
	public static JSONObject toJSONObject(org.w3c.dom.Node doc) throws Exception{
		return XML.toJSONObject(toXML(doc));
	}
	public static String call_function(String instanceid,String function,String data,String style_sheet) {
		return call_function(instanceid,function,data,null,style_sheet);
	}
	public static String call_url(String instanceid,String url,String data,String style_sheet) {
		return call_url(instanceid,url,data,null,style_sheet);
	}
	public static String transform(org.w3c.dom.Node inherited_doc,String style_sheet){
		AppDatabase.writeLog("00084: ApiCall.transform('"+inherited_doc+"','"+style_sheet+"')");
		String retval = "";
		try{
			retval = transform(XML.toString(toJSONObject(inherited_doc)),style_sheet);
		}catch(Exception e){
			e.printStackTrace();
			retval = e.getMessage();
		}
		return retval;
	}
	public static String call_url(String instanceid,String url,String data,org.w3c.dom.Node inherited_doc,String style_sheet){
		String retval = "";
		JSONObject session_jo = null;
		AppDatabase.writeLog("00092: ApiCall.call_url('"+instanceid+"','"+url+"','"+data+"','"+inherited_doc+"','"+style_sheet+"')");
		try{
			SessionDataInterface session = getSessionData(instanceid);
			session_jo = AppDatabase.getSession(instanceid);
			ApiCall.authorize(instanceid);
			String access_token = null;
			try{ 
				access_token = session_jo.getString("accessToken");
			}catch(Exception e){
				e.printStackTrace();
			}

			String parent_element = clean(url);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			if(access_token != null){
				/* Create document based on url data */
				Document doc = session.callURL(url, data);
				/* Create document based on data that is being passed to this funtion */
				Document data_doc = builder.parse(new ByteArrayInputStream(XML.toString(new JSONObject(data),"input").getBytes()));
				/* This is the document that will contain the combined data */
				Document combined = builder.newDocument();
				/* This root represents the response to the url that is being called */
				Node root = combined.createElement(parent_element);
				combined.appendChild(root);
				/***
				 * <? xml version="1.0" ?>
				 *   <url>
				 *   </url>
				 */
				root.appendChild(combined.createElement("response"));
				/**
				 * <? xml version="1.0" ?>
				 *   <url>
				 *     <response>
				 *     </response>
				 *   </url>
				 */
				root.getFirstChild().appendChild(combined.importNode(doc.getFirstChild(),true));
				/**
				 * <? xml version="1.0" ?>
				 *   <url>
				 *     <response>
				 *     	 <... doc contents />
				 *     </response>
				 *   </url>
				 */
				root.appendChild(combined.importNode(data_doc.getFirstChild(),true));
				/**
				 * <? xml version="1.0" ?>
				 *   <url>
				 *     <response>
				 *       <... doc contents />
				 *     </response>
				 *     <input>
				 *       <... data contents />
				 *     </input>
				 *   </url>
				 */
				Node instance = combined.createElement("instanceid");
				instance.setTextContent(instanceid);
				root.appendChild(instance);
				/**
				 * <? xml version="1.0" ?>
				 *   <url>
				 *     <response>
				 *       <... doc contents />
				 *     </response>
				 *     <input>
				 *       <... data contents />
				 *     </input>
				 *     <instanceid>[instance]</instanceid>
				 *   </url>
				 */
				if(inherited_doc != null){
					Node n = inherited_doc.getFirstChild();
					Document pdoc = builder.newDocument();
					pdoc.appendChild(pdoc.importNode(n, true));
					pdoc.getFirstChild().appendChild(pdoc.importNode(combined.getFirstChild(), true));
					/**
					 * <? xml version="1.0" ?>
					 *   <parentdoc>
					 *     <url>
					 *       <.. contents />
					 *     </url>
					 *     <.. parent contents />
					 *   </parentdoc>
					 */
					combined = pdoc;
				}
				File debug_output_file = new File(AppDatabase.getProperty("temp.folder") + System.getProperty("file.separator") +
						"ApiCall.call_url_" +ApiCall.clean(url) + "_" + new java.util.Date().getTime() + ".xml");
				FileOutputStream fout = null;
				try{
					fout = new FileOutputStream(debug_output_file);
					String xml = ApiCall.toXML(combined);
					IOUtils.write(xml, fout);
					AppDatabase.writeLog("00095: Writing xml return value to "+debug_output_file.getAbsolutePath());
					retval = transform(xml,style_sheet);
				}finally{
					try{fout.close();}catch(Exception e){}
				}

			}else{
				session_jo.put("request_login", true);
				AppDatabase.putSession(instanceid, session_jo);
			}
		}catch(AuthorizationException ae){
			try{
				session_jo.put("request_login", true);
				AppDatabase.putSession(instanceid, session_jo);
			}catch(Exception e){
				e.printStackTrace();
				retval = e.getMessage();
			}
		}catch(Exception e){
			e.printStackTrace();
			retval = e.getMessage();
		}
		return retval;
	}
	public static String call_function(String instanceid,String function,String data,org.w3c.dom.Node inherited_doc,String style_sheet){
		String retval = "";
		JSONObject session_jo = null;
		AppDatabase.writeLog("00076: ApiCall.call_function('"+instanceid+"','"+function+"','"+data+"','"+inherited_doc+"','"+style_sheet+"')");
		try{
			SessionDataInterface session = getSessionData(instanceid);
			boolean debug_mode = false;
			try{
				debug_mode = AppDatabase.getProperty("debug").equals("true");
			}catch(Exception e){}
			session_jo = AppDatabase.getSession(instanceid);
			ApiCall.authorize(instanceid);
			String access_token = null;
			try{ 
				access_token = session_jo.getString("accessToken");
			}catch(Exception e){
				e.printStackTrace();
			}
			if(access_token != null){
				OAuthMessage message = session.executeAPI(function, data);
				JSONObject retval_json = new JSONObject(StreamtoString(message.getBodyAsStream()));
				JSONObject data_json = new JSONObject(data);
				JSONObject combined_data = new JSONObject();
				combined_data.put("retval", retval_json);
				combined_data.put("input", data_json);
				combined_data.put("instanceid", instanceid);
				if(inherited_doc != null){
					JSONObject parent = toJSONObject(inherited_doc);
					JSONObject jretval = null;
					try{jretval = parent.getJSONObject(JSONObject.getNames(parent)[0]).getJSONObject("retval");}catch(org.json.JSONException je){
						jretval = new JSONObject();
						parent.getJSONObject(JSONObject.getNames(parent)[0]).put("retval", jretval);
					}
					JSONObject jresponse = null;
					try{
						jresponse = jretval.getJSONObject("response");
					}catch(org.json.JSONException je){
						jresponse = new JSONObject();
						jretval.put("response", jresponse);
					}
					jresponse.put(function, combined_data);
					combined_data = parent;
				}
				String xml = null;
				if(inherited_doc != null){
					xml = XML.toString(combined_data);
				}else{
					xml = XML.toString(combined_data,function);
				}
				String status = findXPathString(XML.toString(retval_json),"//response/status");

				if("3".equals(status)){
					/* Request a login */
					session_jo.put("request_login", true);
					AppDatabase.putSession(instanceid, session_jo);
				}else if("1".equals(status)){
					String error = findXPathString(XML.toString(retval_json),"/response/error");
					if(error != null && (error.endsWith("permission_denied") || error.endsWith("token_expired"))){
						session_jo.put("request_login", true);
						AppDatabase.putSession(instanceid, session_jo);
					}
				}
				if(debug_mode){
					retval = ApiCall.getXML(xml);
				}
				retval += transform(xml,style_sheet);
			}else{
				session_jo.put("request_login", true);
				AppDatabase.putSession(instanceid, session_jo);
			}
		}catch(AuthorizationException ae){
			try{
				session_jo.put("request_login", true);
				AppDatabase.putSession(instanceid, session_jo);
			}catch(Exception e){
				e.printStackTrace();
				retval = e.getMessage();
			}
		}catch(Exception e){
			e.printStackTrace();
			DSResponse<String> response = new DSResponse<String>();
			response.getResponse().setStatus(JSONResponse.GENERAL_ERROR);
			response.getResponse().setError(e.getMessage());
			try{
				return transform(XML.toString(response),style_sheet);
			}catch(Exception e2){
				return "<pre class=\"error\">"+e2.getMessage()+"</pre>";
			}
		}
		return retval;
	}
	public static String format(String unformattedXml) throws ParserConfigurationException, SAXException {
        try {
        	XMLDocumentReader reader = new XMLDocumentReader();
        	reader.read(unformattedXml);
            final Document document = reader.getDocument();

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
	private static String getXML(String xml) throws ParserConfigurationException, SAXException {
		String str = "<pre class=\"brush: xml;\">";
		str += ApiCall.format(xml);
		str += "</pre>";
		return str;
	}

	protected static void authorize(String instanceid) throws Exception {
		SessionDataInterface data = getSessionData(instanceid);
		authorize(data);
	}
	private static SessionDataInterface getSessionData(String instanceid) throws Exception {
		SessionDataInterface data = new SessionData();
		JSONObject session_jo = AppDatabase.getSession(instanceid);
		String accesstoken = null;
		try{ 
			accesstoken = session_jo.getString("accessToken");
		}catch(Exception e){}
		if(accesstoken != null && !accesstoken.equals("null")){
			data.setAccessToken(session_jo.getString("accessToken"));
		}
		try{
			data.setCallbackUrl(session_jo.getString("callbackUrl"));
		}catch(Exception e){}
		String remote_host = null;
		try{
			remote_host = session_jo.getString("remoteHost");
		}catch(Exception e){}
		if(remote_host != null && !remote_host.equals("null")){
			data.setRemoteHost(session_jo.getString("remoteHost"));
		}
		try{
			data.setRequestToken(session_jo.getString("requestToken"));
		}catch(Exception e){}
		try{
			data.setTokenSecret(session_jo.getString("tokenSecret"));
		}catch(Exception e){}
		String user = null;
		try{
			user = session_jo.getString("user");
		}catch(Exception e){}
		if(user != null && !user.equals("null")){
			data.setUserId(session_jo.getString("user"));
		}
		boolean debugging = false;
		try{
			debugging = session_jo.getBoolean("debugging");
		}catch(Exception e){}
		data.setDebugging(debugging);
		return data;
	}
	public static void authorize(SessionDataInterface data) throws Exception {
		if(!data.isAuthorized()){
			data.executeRequest();
			data.executeAuthorize();
			throw new AuthorizationException(data.getAuthenticateUrl());
		}
	}
	public static String findXPathString(String xml,String path) throws Exception {
		DocumentBuilderFactory factory;
		DocumentBuilder builder;
		XPathFactory xfactory;
		XPath xpath;
		factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true); // never forget this!
		builder = factory.newDocumentBuilder();
		xfactory = XPathFactory.newInstance();
		xpath = xfactory.newXPath();
		ByteArrayInputStream bin = new ByteArrayInputStream(xml.getBytes());
		Document doc = builder.parse(bin);
		XPathExpression expr = xpath.compile(path);
		return (String)expr.evaluate(doc, XPathConstants.STRING);
	}
	public static String StreamtoString(InputStream in) throws Exception {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		for(int i = in.read(buffer);i > -1;i = in.read(buffer)){
			bout.write(buffer, 0, i);
		}
		return bout.toString();
	}
	public static String transform(String xml,String xsl) throws TransformerException{
		String xsl_folder = AppDatabase.getProperty("xsl.folder");
		StreamSource source = new StreamSource(new StringReader(xml));
		StreamSource stylesource = new StreamSource(xsl_folder+System.getProperty("file.separator")+xsl+".xsl");
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(stylesource);
		StringWriter out = new StringWriter();
		StreamResult result = new StreamResult(out);
		transformer.transform(source, result);
		String retval = out.toString();
		return retval;
	}
}
