/**
 * 
 */
package com.robaone.api.business;

import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.robaone.api.data.AppDatabase;

/**
 * @author ansel
 *
 */
public class WorkerThreadPoolExecutor extends ThreadPoolExecutor {
	protected HashMap<String,WorkerInterface> m_eventManager = new HashMap<String,WorkerInterface>();
	protected HashMap<String,WorkerInterface> m_queue = new HashMap<String,WorkerInterface>();
	public WorkerThreadPoolExecutor(int corePoolSize, int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}
	
	public WorkerThreadPoolExecutor(int corePoolSize){
		super(corePoolSize, corePoolSize, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}
	
	@Override
	public void execute(Runnable r){
		try{
			WorkerInterface w = (WorkerInterface)r;
			m_queue.put(w.getKey(), w);
		}catch(Exception e){
			AppDatabase.writeLog(e);
		}
		super.execute(r);
	}
	
	public HashMap<String,WorkerInterface> getWorkerInfo(){
		return this.m_eventManager;
	}
	
	public HashMap<String,WorkerInterface> getQueuedWorkerInfo(){
		return this.m_queue;
	}
	
	@Override
	protected void beforeExecute(Thread t, Runnable r){
		if(this.m_eventManager != null){
			try{
				WorkerInterface w = (WorkerInterface)r;
				this.m_queue.remove(w.getKey());
				this.m_eventManager.put(w.getKey(), w);
			}catch(Exception e){
				AppDatabase.writeLog(e);
			}
		}
		super.beforeExecute(t,r);
	}
	
	@Override
	protected void afterExecute(Runnable r, Throwable t){
		if(this.m_eventManager != null){
			try{
				WorkerInterface w = (WorkerInterface)r;
				this.m_eventManager.remove(w.getKey());
			}catch(Exception e){
				AppDatabase.writeLog(e);
			}
		}
		super.afterExecute(r, t);
	}
	
	@Override
	protected void terminated(){
		super.terminated();
	}
}
