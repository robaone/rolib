package com.robaone.api.business;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.SessionData;
import com.robaone.api.data.SessionDataInterface;
import com.robaone.api.data.StylesheetFactory;
import com.robaone.api.data.StylesheetManagerInterface;
import com.robaone.xml.ROTransform;

public class ActionDispatcher {
	private SessionDataInterface session;
	private HttpServletRequest request;
	private HttpServletResponse response;
	public static final String ACTION_PACKAGE_NAME = "com.robaone.api.action_package";
	private String ACTION_PACKAGE;
	private String m_stylesheet;
	private String m_contenttype;
	private ByteArrayOutputStream m_temp_out;
	private OutputStream m_out;
	private String m_included_data;
	public ActionDispatcher(SessionDataInterface session,HttpServletRequest request,HttpServletResponse response){
		ACTION_PACKAGE = AppDatabase.getProperty(ACTION_PACKAGE_NAME);
		this.session = session;
		this.request = request;
		this.setResponse(response);
	}
	public void setPackage(String action_package){
		ACTION_PACKAGE = action_package;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void runAction(String action,Map parameters,OutputStream out) throws Exception {
		try{
			String data = IOUtils.toString(this.request.getInputStream());
			if(!FieldValidator.exists(data)){
				data = parameters.get("data") == null ? "{}" : ((String[])parameters.get("data"))[0];
			}
			if(!FieldValidator.exists(action)){
				try{
					action = new JSONObject(data).getString("_action");
				}catch(Exception e){
					
				}
			}
			setOutputFormatting(parameters);
			if(FieldValidator.exists(getStyleSheet())){
				setFinalOut(out);
				out = getTempOut();
			}
			AppDatabase.writeLog("00007: Running Action, "+action);
			if(action == null || action.trim().length() == 0){
				throw new Exception("No action specified");
			}
			if(data.length() == 0) data = "{}";
			AppDatabase.writeLog("00008: Data = "+data);
			Object o = null;
			if(FieldValidator.exists(action)){
				Class[] parameterTypes = new Class[]{JSONObject.class};
				Class myClass = Class.forName(ACTION_PACKAGE+"."+action.split("[.]")[0]);
				Method meth = myClass.getMethod(action.split("[.]")[1], parameterTypes);
				Class[] constrpartypes = new Class[]{OutputStream.class,SessionData.class,HttpServletRequest.class};
				Constructor constr = myClass.getConstructor(constrpartypes);
				o = constr.newInstance(new Object[]{out,session,request});
				JSONObject jo = new JSONObject(data);
				Object[] arglist = new Object[]{jo};
				meth.invoke(o, arglist);
			}
			if(o instanceof BaseAction){
				BaseAction base = (BaseAction)o;
				writeResponse(out, base);
			}else{
				writeResponse(out, null);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	private ByteArrayOutputStream getTempOut() {
		if(this.m_temp_out == null){
			this.m_temp_out = new ByteArrayOutputStream();
		}
		return this.m_temp_out;
	}
	private void setFinalOut(OutputStream out) {
		this.m_out = out;
	}
	private OutputStream getFinalOut(){
		return this.m_out;
	}
	@SuppressWarnings("rawtypes")
	private void setOutputFormatting(Map parameters) {
		try{
			this.setContentType(((String[])parameters.get("_content_type"))[0]);
		}catch(Exception e){
			this.setContentType((String)parameters.get("_content_type"));
		}
		try{
			this.setStyleSheet(((String[])parameters.get("_stylesheet"))[0]);
		}catch(Exception e){
			this.setStyleSheet((String)parameters.get("_stylesheet"));
		}
		try{
			this.setIncludedData(((String[])parameters.get("_data"))[0]);
		}catch(Exception e){
			this.setIncludedData((String)parameters.get("_data"));
		}
	}
	private void setIncludedData(String string) {
		this.m_included_data = string;
	}
	private String getIncludedData(){
		return this.m_included_data;
	}
	private void setStyleSheet(String string) {
		this.m_stylesheet = string;
	}
	private void setContentType(String string) {
		this.m_contenttype = string;
	}
	private String getStyleSheet(){
		return this.m_stylesheet;
	}
	private String getContentType(){
		return this.m_contenttype;
	}
	private void writeResponse(OutputStream out, BaseAction<?> base)
			throws Exception, JSONException {
		if(base != null){
			// Process the api call and possibly the included data
			base.prepareHeader(this.response);
			if(base.getSQLStream() != null){
				base.getSQLStream().CopyStream(out);
			}else{
				base.writeResponse();
				if(FieldValidator.exists(getStyleSheet())){
					StylesheetManagerInterface man = StylesheetFactory.getStylesheetManager();
					String xsl = man.getStyleSheet(getStyleSheet());
					JSONObject jo = null;
					jo = new JSONObject(new String(getTempOut().toByteArray(),"UTF-8"));
					try{
						jo.getJSONObject("response").put("included_data", new JSONObject(this.getIncludedData()));
					}catch(Exception e){}
					String xml = XML.toString(jo);
					ROTransform trn = new ROTransform(xsl);
					String result = trn.transformXML(xml);
					if(FieldValidator.exists(getContentType())){
						this.response.setContentType(getContentType());
					}
					this.getFinalOut().write(result.getBytes());
				}
			}
		}else{
			// Possibly process only the included data
			StylesheetManagerInterface man = StylesheetFactory.getStylesheetManager();
			String xsl = man.getStyleSheet(getStyleSheet());
			JSONObject jo = new JSONObject();
			jo.put("response", new JSONObject());
			try{
				jo.getJSONObject("response").put("included_data", new JSONObject(this.getIncludedData()));
			}catch(Exception e){}
			String xml = XML.toString(jo);
			ROTransform trn = new ROTransform(xsl);
			String result = trn.transformXML(xml);
			if(FieldValidator.exists(getContentType())){
				this.response.setContentType(getContentType());
			}
			this.getFinalOut().write(result.getBytes());
		}
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String runFormAction(Map parameterMap,
			OutputStream out) throws Exception {
		try{
			String action = this.getParameter(parameterMap,"_action");
			setOutputFormatting(parameterMap);
			if(FieldValidator.exists(getStyleSheet())){
				setFinalOut(out);
				out = getTempOut();
			}
			AppDatabase.writeLog("00009: Running Action, "+action);
			JSONObject data = new JSONObject(parameterMap);
			AppDatabase.writeLog("00010: Data = "+data.toString());
			Object o = null;
			if(FieldValidator.exists(action)){
				Class[] parameterTypes = new Class[]{JSONObject.class};
				Class myClass = Class.forName(ACTION_PACKAGE+"."+action.split("[.]")[0]);
				Method meth = myClass.getMethod(action.split("[.]")[1], parameterTypes);
				Class[] constrpartypes = new Class[]{OutputStream.class,SessionData.class,HttpServletRequest.class};
				Constructor constr = myClass.getConstructor(constrpartypes);
				o = constr.newInstance(new Object[]{out,session,request});
				Object[] arglist = new Object[]{data};
				meth.invoke(o, arglist);
			}
			if(o instanceof BaseAction){
				BaseAction base = (BaseAction)o;
				writeResponse(out, base);
			}else{
				writeResponse(out, null);
			}
			return null;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	@SuppressWarnings("rawtypes")
	private String getParameter(Map parameterMap, String string) {
		try{
			return ((String[])parameterMap.get(string))[0];
		}catch(Exception e){
			return "";
		}
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void runAction(List<FileItem> items, OutputStream out) throws Exception {
		try{
			// Process the uploaded items
			Iterator<FileItem> iter = items.iterator();
			String action = null;
			HashMap<String,String> parametermap = new HashMap<String,String>();
			while (iter.hasNext()) {
			    FileItem item = iter.next();

			    if (item.isFormField()) {
			        continue;
			    } else {
			        String field = item.getFieldName();
			        if(field.equalsIgnoreCase("action")){
			        	action = item.getString();
			        	break;
			        }else if(field.equalsIgnoreCase("_stylesheet")){
			        	parametermap.put("_stylesheet", item.getString());
			        }else if(field.equalsIgnoreCase("_content_typ")){
			        	parametermap.put("_content_type",item.getString());
			        }
			    }
			}
			setOutputFormatting(parametermap);
			if(FieldValidator.exists(getStyleSheet())){
				setFinalOut(out);
				out = getTempOut();
			}
			if(action == null || action.trim().length() == 0){
				throw new Exception("No action specified");
			}
			AppDatabase.writeLog("00007: Running Action, "+action);
			Object o = null;
			if(FieldValidator.exists(action)){
				Class[] parameterTypes = new Class[]{JSONObject.class};
				Class myClass = Class.forName(ACTION_PACKAGE+"."+action.split("[.]")[0]);
				Method meth = myClass.getMethod(action.split("[.]")[1], parameterTypes);
				Class[] constrpartypes = new Class[]{OutputStream.class,SessionData.class,HttpServletRequest.class};
				Constructor constr = myClass.getConstructor(constrpartypes);
				o = constr.newInstance(new Object[]{out,session,request});
				Object[] arglist = new Object[]{items};
				meth.invoke(o, arglist);
			}
			if(o instanceof BaseAction){
				BaseAction base = (BaseAction)o;
				writeResponse(out, base);
			}else{
				writeResponse(out, null);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
}
