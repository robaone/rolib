package com.robaone.api.business.worker;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import org.json.JSONObject;

import com.robaone.api.business.BaseWorker;
import com.robaone.api.business.EventManagerInterface;
import com.robaone.api.business.WorkerThreadPoolExecutor;
import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.SessionData;
import com.robaone.api.data.SessionDataInterface;
import com.robaone.api.data.UserManagerInterface;

public class TestWorker extends BaseWorker {

	public TestWorker(SessionDataInterface sdata, EventManagerInterface eman) {
		super(sdata, eman);
	}

	@Override
	public String getName() {
		return "Test Worker";
	}

	@Override
	public void execute() throws Exception {
		Thread.sleep(AppDatabase.getRandomInt(1000, 5000));
		this.writeInfoEvent("Hello world");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SessionDataInterface sdata = null;
		try {
			sdata = new SessionData();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		sdata.setUser(new UserManagerInterface(){

			@Override
			public void setUserByID(String id) throws Exception {
				
			}

			@Override
			public void setUserByID(Integer id) throws Exception {
				
			}

			@Override
			public Integer getIduser() {
				return 0;
			}

			@Override
			public String getUserId() {
				return "test@user.com";
			}

			@Override
			public boolean checkPassword(String password) {
				return false;
			}
			
		});
		EventManagerInterface eman = new EventManagerInterface(){

			@Override
			public void writeError(String string, String name,
					SessionDataInterface m_sessiondata) {
				AppDatabase.writeLog("Error: "+string);
			}

			@Override
			public void writeInfo(String string, String name,
					SessionDataInterface m_sessiondata) {
				AppDatabase.writeLog("Info: "+string);
			}

			@Override
			public void writeWarning(String string, String name,
					SessionDataInterface m_sessiondata) {
				AppDatabase.writeLog("Warning: "+string);
			}

			@Override
			public void writeTrace(String string, String name,
					SessionDataInterface m_sessiondata) {
				AppDatabase.writeLog("Trace: "+string);
			}
			
		};

		WorkerThreadPoolExecutor service = new WorkerThreadPoolExecutor(3);
		TestWorker main = new TestWorker(sdata,eman);
		TestWorker main2 = new TestWorker(sdata,eman);
		TestWorker main3 = new TestWorker(sdata,eman);
		service.execute(main);
		service.execute(main2);
		service.execute(main3);
		service.shutdown();
		while(!service.isTerminated()){
			try {
				AppDatabase.writeLog("*** QUEUE ****");
				AppDatabase.writeLog(new JSONObject(service.getQueuedWorkerInfo()).toString());
				AppDatabase.writeLog("*** RUNNING ****");
				AppDatabase.writeLog(new JSONObject(service.getWorkerInfo()).toString());
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		AppDatabase.writeLog(new JSONObject(service.getWorkerInfo()).toString());
		AppDatabase.writeLog("Exit Code ("+main.getExitCode()+")");
	}

	@Override
	public void openPayloadOutputStream() throws IOException {
		
	}

	@Override
	public void closePayloadOutputStream() throws IOException {
		
	}

	@Override
	public OutputStream getPayloadOutputStream() throws IOException {
		return null;
	}

	@Override
	public void deletePayload() throws Exception {
		
	}

	@Override
	public InputStream getPayloadInputStream() throws IOException {
		return null;
	}

}
