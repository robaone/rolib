package com.robaone.api.business;

public class InputParamsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7653260257024990691L;

	public InputParamsException(String string) {
		super(string);
	}

	public InputParamsException(Exception e) {
		super(e);
	}

}
