package com.robaone.api.security;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;

public class Gravatar {
	private String email;
	private int imageSize;
	public enum IMGFORMAT {PNG,JPEG,GIF,TIFF};
	private IMGFORMAT imageFormat;
	
	public Gravatar(){
		this.setImageSize(60);
		this.setImageFormat(IMGFORMAT.PNG);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getProfileHash() {
		String email = this.getEmail();
		String hash = MD5Util.md5Hex(email);
		return hash;
	}
	
	public byte[] getProfileImage() throws IOException{
		URL url = new URL(this.getProfileImageUrl());
		URLConnection connection = url.openConnection();
		byte[] bytes = IOUtils.toByteArray(connection.getInputStream());
		return bytes;
	}
	
	public String getProfileImageUrl(){
		return "http://www.gravatar.com/avatar/"+this.getProfileHash()+"."+this.getImageFormat()+"?s="+this.getImageSize();
	}

	public int getImageSize() {
		return imageSize;
	}

	public void setImageSize(int imageSize) {
		this.imageSize = imageSize;
	}

	public IMGFORMAT getImageFormat() {
		return imageFormat;
	}
	
	public String getContentType(){
		return "image/"+this.getImageFormat().toString().toLowerCase();
	}

	public void setImageFormat(IMGFORMAT imageFormat) {
		this.imageFormat = imageFormat;
	}
}
