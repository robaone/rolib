package com.robaone.api.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.XML;

import com.robaone.api.business.APIConstants;
import com.robaone.api.business.ROTransformer;
import com.robaone.api.data.APISessionData;

/**
 * Servlet implementation class XMLServiceServlet
 */
public class HTMLServiceServlet extends HttpServlet implements APIConstants {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HTMLServiceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		FileInputStream fin = null;
		try {
			APISessionData data = new APISessionData(request.getSession(),request.getContextPath());
			String json_str = request.getParameter("json");
			String xml = request.getParameter("xml");
			String xsl_file = request.getParameter("xsl");
			File f = new File(data.getXSLFolder()+xsl_file+".xsl");
			if(xml == null && json_str != null){
				xml = XML.toString(new JSONObject(json_str),"request");
			}else if(xml == null){
				throw new Exception("Data source not specified");
			}
			xml = "<?xml version=\"1.0\" ?>"+xml;
			ROTransformer trn = new ROTransformer();
			fin = new FileInputStream(f);
			trn.setXSLDocument(fin);
			response.setContentType("text/html");
			trn.transformToStream(xml, response.getOutputStream());
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String msg = e.getMessage() == null ? "Unknown Error" : e.getMessage();
			request.setAttribute(GENERAL_ERROR, msg);
			request.setAttribute(ERROR_DUMP, sw.toString());
			request.getRequestDispatcher("jsp/error.jsp").forward(request, response);
		} finally {
			try{ fin.close(); } catch(Exception e){}
		}
	}

}
