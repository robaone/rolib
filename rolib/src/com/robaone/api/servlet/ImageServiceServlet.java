package com.robaone.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.robaone.api.business.APIConstants;
import com.robaone.api.data.APISessionData;


/**
 * Servlet implementation class ImageServiceServlet
 */
//@WebServlet("/ImageService")
public class ImageServiceServlet extends HttpServlet implements APIConstants {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageServiceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			APISessionData data = new APISessionData(request.getSession(),request.getContextPath());
			
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String msg = e.getMessage() == null ? "Unknown Error" : e.getMessage();
			request.setAttribute(GENERAL_ERROR, msg);
			request.setAttribute(ERROR_DUMP, sw.toString());
			request.getRequestDispatcher("jsp/error.jsp").forward(request, response);
		}
	}
}
