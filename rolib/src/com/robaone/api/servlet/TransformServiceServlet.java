package com.robaone.api.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.robaone.api.business.APIConstants;
import com.robaone.api.business.FieldValidator;
import com.robaone.api.business.XMLDocumentReader;
import com.robaone.xml.ROTransform;

/**
 * Servlet implementation class XMLServiceServlet
 */
public class TransformServiceServlet extends HttpServlet implements APIConstants {
	private static final long serialVersionUID = 1L;
	private static final String JSON = "json";
	private static final String XML = "xml";
	private static final String COMMANDS = "commands";
	private static final String XSL = "xsl";
	private static final String JSON_URL = "json_url";
	private static final String XML_URL = "xml_url";
	private static final String COMMANDS_URL = "commands_url";
	private static final String XSL_URL = "xsl_url";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransformServiceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		FileInputStream fin = null;
		try {
			String json = request.getParameter(JSON);
			String json_url = request.getParameter(JSON_URL);
			String xml = request.getParameter(XML);
			String xml_url = request.getParameter(XML_URL);
			String commands = request.getParameter(COMMANDS);
			String commands_url = request.getParameter(COMMANDS_URL);
			String xsl = request.getParameter(XSL);
			String xsl_url = request.getParameter(XSL_URL);
			
			/**
			 * Resolve in order of precedence
			 * 
			 * Source: json -> json_url -> xml -> xml_url -> commands -> commands_url
			 * Stylesheet: xsl -> xsl_url
			 */
			String source = this.getSourceData(json,json_url,xml,xml_url,commands,commands_url);
			String stylesheet = this.getStyleSheet(xsl,xsl_url);
			
			ROTransform trn = new ROTransform(stylesheet);
			String output = trn.transformXML(source);
			
			response.getOutputStream().print(output);
			
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String msg = e.getMessage() == null ? "Unknown Error" : e.getMessage();
			msg = cleanXMLContent(msg);
			String dump = cleanXMLContent(sw.toString());
			response.getOutputStream().print("<?xml version=\"1.0\" ?><error><msg>"+msg+"</msg><dump>"+dump+"</dump></error>");
		} finally {
			try{ fin.close(); } catch(Exception e){}
		}
	}

	private String getStyleSheet(String xsl2, String xsl_url2) throws IOException {
		if(FieldValidator.exists(xsl2)){
			return xsl2;
		}else{
			URL url = new URL(xsl_url2);
			String retval = IOUtils.toString(url.openStream());
			return retval;
		}
	}

	private String getSourceData(String json2, String json_url2, String xml2,
			String xml_url2, String commands2, String commands_url2) throws DOMException, Exception {
		XMLDocumentReader retval = new XMLDocumentReader();
		retval.read("<request></request>");
		if(FieldValidator.exists(json2)){
			return org.json.XML.toString(new JSONObject(json2), "request");
		}else if(FieldValidator.exists(json_url2)){
			URL url = new URL(json_url2);
			String url_content = IOUtils.toString(url.openStream());
			return org.json.XML.toString(new JSONObject(url_content),"request");
		}else if(FieldValidator.exists(xml2)){
			return xml2;
		}else if(FieldValidator.exists(xml_url2)){
			return getStyleSheet(null,xml_url2);
		}else if(FieldValidator.exists(commands2)){
			executeCommands(commands2, retval);
			return retval.toString();
		}else if(FieldValidator.exists(commands_url2)){
			URL url = new URL(commands_url2);
			String url_content = IOUtils.toString(url.openStream());
			executeCommands(url_content, retval);
			return retval.toString();
		}
		return null;
	}

	protected void executeCommands(String commands2, XMLDocumentReader retval)
			throws JSONException, Exception, ParserConfigurationException,
			SAXException, IOException {
		if(commands2.startsWith("[")){
			JSONArray commands_jo = new JSONArray(commands2);
			for(int i = 0; i < commands_jo.length();i++){
				JSONObject command_jo = commands_jo.getJSONObject(i);
				insertDocument(retval,command_jo);
			}
		}else if(commands2.startsWith("{")){
			JSONObject command_jo = new JSONObject(commands2);
			insertDocument(retval, command_jo);
		}
	}

	protected void insertDocument(XMLDocumentReader retval,
			JSONObject command_jo) throws JSONException, Exception,
			ParserConfigurationException, SAXException, IOException {
		String name = command_jo.getString("name");
		String json_url = null;
		try{json_url = command_jo.getString("json_url");}catch(Exception e){}
		String xml_url = null;
		try{xml_url = command_jo.getString("xml_url");}catch(Exception e){}
		String source = null;
		XMLDocumentReader reader = new XMLDocumentReader();
		if(FieldValidator.exists(json_url)){
			URL url = new URL(json_url);
			source = IOUtils.toString(url.openStream());
			source = org.json.XML.toString(new JSONObject(source),name);
			reader.read(source);
			Node n = retval.getDocument().importNode(reader.getDocument().getFirstChild(), true);
			retval.getDocument().getFirstChild().appendChild(n);
		}else if(FieldValidator.exists(xml_url)){
			URL url = new URL(xml_url);
			source = IOUtils.toString(url.openStream());
			reader.read(source);
			Node n = retval.getDocument().importNode(reader.getDocument().getFirstChild(), true);
			retval.getDocument().getFirstChild().appendChild(retval.getDocument().createElement(name));
			retval.findXPathNode("/request/"+name).item(0).appendChild(n);
		}
	}

	private String cleanXMLContent(String msg) {
		msg = msg.replaceAll("[<]", "&lt;");
		msg = msg.replaceAll("[>]", "&gt;");
		msg = msg.replaceAll("[&]", "&amp;");
		return msg;
	}

}
