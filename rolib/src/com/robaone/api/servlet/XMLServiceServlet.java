package com.robaone.api.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.XML;

import com.robaone.api.business.APIConstants;
import com.robaone.api.business.ROTransformer;
import com.robaone.api.data.APISessionData;

/**
 * Servlet implementation class XMLServiceServlet
 */
public class XMLServiceServlet extends HttpServlet implements APIConstants {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public XMLServiceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		FileInputStream fin = null;
		try {
			response.setContentType("text/xml");
			APISessionData data = new APISessionData(request.getSession(),request.getContextPath());
			String json_str = request.getParameter("json");
			String xsl_file = request.getParameter("xsl");
			String source = request.getParameter("source");
			File f = null;
			if(source != null){
				if("forms".equalsIgnoreCase(source)){
					f = new File(data.getFormFolder()+xsl_file+".xsl");
				}else{
					f = new File(data.getXSLFolder()+xsl_file+".xsl");
				}
			}else{
				f = new File(data.getXSLFolder()+xsl_file+".xsl");
			}
			String xml = XML.toString(new JSONObject(json_str),"request");
			xml = "<?xml version=\"1.0\" ?>"+xml;
			ROTransformer trn = new ROTransformer();
			fin = new FileInputStream(f);
			trn.setXSLDocument(fin);
			trn.transformToStream(xml, response.getOutputStream());
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String msg = e.getMessage() == null ? "Unknown Error" : e.getMessage();
			msg = cleanXMLContent(msg);
			String dump = cleanXMLContent(sw.toString());
			response.getOutputStream().print("<?xml version=\"1.0\" ?><error><msg>"+msg+"</msg><dump>"+dump+"</dump></error>");
		} finally {
			try{ fin.close(); } catch(Exception e){}
		}
	}

	private String cleanXMLContent(String msg) {
		msg = msg.replaceAll("[<]", "&lt;");
		msg = msg.replaceAll("[>]", "&gt;");
		msg = msg.replaceAll("[&]", "&amp;");
		return msg;
	}

}
