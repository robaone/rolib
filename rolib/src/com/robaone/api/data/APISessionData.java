package com.robaone.api.data;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import com.robaone.api.business.APIConstants;


public class APISessionData implements APIConstants {
	private HttpSession m_map;
	private Properties m_properties;
	private ArrayList<String> m_history = new ArrayList<String>();
	public APISessionData(HttpSession session,String context) throws Exception{
		this.m_map = session;
		this.m_properties = new Properties();
		FileInputStream in = null;
		try{
			File f = new File(BASE_CONFIG_PATH+context+"_config.ini");
			in = new FileInputStream(f);
			this.m_properties.load(in);
		}catch(Exception e){
			throw e;
		}finally{
			try{in.close();}catch(Exception e){}
		}
	}
	public String getUsername(){
		try{
			return this.m_map.getAttribute(USERNAME).toString();
		}catch(Exception e){
			return null;
		}
	}
	public String getTitle(){
		return m_properties.getProperty(TITLE);
	}
	public String getXSLFolder() {
		return m_properties.getProperty(XSL_FOLDER);
	}
	public String getFormFolder() {
		return m_properties.getProperty(FORM_FOLDER);
	}
	public boolean requiresAuthentication(String page_str){
		boolean retval = true;
		/**
		 * Check to see if the page exists
		 */
		File xsl_file = new File(getXSLFolder()+page_str+".xsl");
		if(!xsl_file.exists()){
			return false;
		}
		String anonymous_pages = m_properties.getProperty(ANONYMOUS_PAGES);
		try{
			String[] pages = anonymous_pages.split("[,]");
			for(String str : pages){
				if(str.equalsIgnoreCase(page_str)){
					return false;
				}
			}
		}catch(Exception e){}
		return retval;
	}
	public void addHistory(String url){
		this.m_history.add(url);
	}
	public ArrayList<String> getHistory(){
		return this.m_history;
	}
}
