package com.robaone.api.data;

import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.ThreadContext.ContextStack;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.ThrowableProxy;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.SimpleMessage;

public class DBLogEvent {
		private final Appender appender;
		private final String name;
		private final String string;
		private Throwable exception;
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public DBLogEvent(Appender appender, String name, String string) {
			this.appender = appender;
			this.name = name;
			this.string = string;
		}

		public DBLogEvent(Appender appender, String name, Throwable e) {
			this.appender = appender;
			this.name = name;
			this.exception = e;
			this.string = "";
		}

		public Map<String, String> getContextMap() {
			return null;
		}

		public ContextStack getContextStack() {
			return null;
		}

		public Level getLevel() {
			return Level.INFO;
		}

		public String getLoggerName() {
			return appender.getName();
		}

		public Marker getMarker() {
			return null;
		}

		public Message getMessage() {
			Message msg = new SimpleMessage(string); 
			return msg;
		}

		public StackTraceElement getSource() {
			return null;
		}

		public String getThreadName() {
			return null;
		}

		public Throwable getThrown() {
			return this.exception;
		}

		public boolean isEndOfBatch() {
			return false;
		}

		public boolean isIncludeLocation() {
			return false;
		}

		public void setEndOfBatch(boolean endOfBatch) {
			// TODO Auto-generated method stub
			
		}

		public void setIncludeLocation(boolean locationRequired) {
			// TODO Auto-generated method stub
			
		}

		public String getFQCN() {
			// TODO Auto-generated method stub
			return null;
		}

		public long getMillis() {
			// TODO Auto-generated method stub
			return 0;
		}
	}