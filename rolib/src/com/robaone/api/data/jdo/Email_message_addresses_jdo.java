/*
* Created on Jan 13, 2012
* Author Ansel Robateau
* http://www.robaone.com
*
*/
package com.robaone.api.data.jdo;

import java.math.BigDecimal;
import java.util.Date;
import com.robaone.jdo.RO_JDO;


public class Email_message_addresses_jdo extends RO_JDO{
  public final static String ID = "ID";
  public final static String FIELD = "FIELD";
  public final static String NAME = "NAME";
  public final static String EMAIL_ADDRESS = "EMAIL_ADDRESS";
  public final static String MESSAGEID = "MESSAGEID";
  protected Email_message_addresses_jdo(){
    
  }
  protected void setId(Integer id){
    this.setField(ID,id);
  }
  public final Integer getId(){
    Object[] val = this.getField(ID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setField(String field){
    this.setField(FIELD,field);
  }
  public String getField(){
    Object[] val = this.getField(FIELD);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setName(String name){
    this.setField(NAME,name);
  }
  public String getName(){
    Object[] val = this.getField(NAME);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setEmail_address(String email_address){
    this.setField(EMAIL_ADDRESS,email_address);
  }
  public String getEmail_address(){
    Object[] val = this.getField(EMAIL_ADDRESS);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setMessageid(Integer messageid){
    this.setField(MESSAGEID,messageid);
  }
  public Integer getMessageid(){
    Object[] val = this.getField(MESSAGEID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public String getIdentityName() {
    return "ID";
  }
}