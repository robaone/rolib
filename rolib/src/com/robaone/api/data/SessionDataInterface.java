package com.robaone.api.data;

import javax.naming.NamingException;

import net.oauth.OAuthMessage;

import org.w3c.dom.Document;

/**
 * <pre>   Copyright 2013 Ansel Robateau
 http://www.robaone.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.</pre>
 * @author Ansel
 *
 */
public interface SessionDataInterface {

	public static final String ACCESSURL = "accessUrl";

	public abstract void setUser(UserManagerInterface user_object);

	public abstract UserManagerInterface getUser();

	public abstract String getRemoteHost();

	public abstract void setRemoteHost(String host);

	public abstract void setCredentials(AppCredentialsInterface cred);

	public abstract AppCredentialsInterface getCredentials();

	public abstract String getSession_token();

	public abstract void setSession_token(String session_token);

	public abstract Document callURL(String url, String data) throws Exception;

	public abstract OAuthMessage executeAPI(String function, String data)
			throws NamingException, Exception;

	public abstract void setUserId(String parameter) throws NamingException,
			Exception;

	public abstract String getAccessToken();

	public abstract String getTokenSecret();

	public abstract String getAccessUrl();

	public abstract String getRequestToken();

	public abstract void setAccessToken(String string);

	public abstract void setCallbackUrl(String string);

	public abstract void setRequestToken(String string);

	public abstract void setTokenSecret(String string);

	public abstract boolean isAuthorized() throws Exception;

	public abstract void executeRequest() throws NamingException, Exception;

	public abstract void executeAuthorize() throws NamingException, Exception;

	public abstract String getAuthenticateUrl();

	public abstract boolean isDebugging();

	public abstract void setDebugging(boolean debug);

}