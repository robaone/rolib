package com.robaone.api.data;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.robaone.dbase.HDBConnectionManager;



public class DatabaseImpl{
	private static final class ConnMan implements HDBConnectionManager {
		private final DataSource ds;

		private ConnMan(DataSource ds) {
			this.ds = ds;
		}

		@Override
		public Connection getConnection() throws Exception {
			// Allocate and use a connection from the pool
			Connection conn = ds.getConnection();
			m_allocated++;
			writeTrace("     # "+m_allocated+" connections open");
			return conn;
		}

		@Override
		public void closeConnection(Connection m_con) throws Exception {
			m_con.close();
			m_allocated--;
			writeTrace("     # "+m_allocated+" connections open");
		}
	}


	public static final String INSUFFICIENT_RIGHTS_MSG = "You do not have access rights to perform this action";
	public static final String NOT_IMPLEMENTED_MSG = "Not Yet Implemented";
	private static int m_allocated = 0;
	public DatabaseImpl(){
		
	}

	public static void writeLog(String string) {
		AppDatabase.writeLog(string);
	}
	
	public static void writeTrace(String string) {
		if("true".equalsIgnoreCase(System.getProperty("debug")) ||
				"y".equalsIgnoreCase(System.getProperty("debug"))){
			AppDatabase.writeLog(string);
		}
	}

	
	public static HDBConnectionManager getConnectionManager(String connection_name) throws NamingException {
		// Obtain our environment naming context
		writeTrace(" -- getConnectionManager('"+connection_name+"')");
		Context initCtx = new InitialContext();
		Context envCtx = (Context) initCtx.lookup("java:comp/env");
		initCtx.list("java:comp/env");

		// Look up our data source
		final DataSource ds = (DataSource)envCtx.lookup("jdbc/"+connection_name);
		return new ConnMan(ds);
	}

}
