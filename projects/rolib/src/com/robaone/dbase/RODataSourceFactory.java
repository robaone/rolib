package com.robaone.dbase;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.apache.tomcat.dbcp.dbcp.BasicDataSourceFactory;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
abstract public class RODataSourceFactory extends BasicDataSourceFactory {
	@Override
	public Object getObjectInstance(Object obj, Name name, Context nameCtx,
			@SuppressWarnings("rawtypes") Hashtable environment) throws Exception {
		Object o = super.getObjectInstance(obj, name, nameCtx, environment);
		if(o != null){
			BasicDataSource ds = (BasicDataSource) o;
			if (ds.getPassword() != null && ds.getPassword().length() > 0){
				String pwd = getPasswordStore().getPassword(ds.getPassword());
				ds.setPassword(pwd);
			}
			return ds;
		} else {
			return null;
		}
	}
	abstract public ROPasswordStoreInterface getPasswordStore();
}
