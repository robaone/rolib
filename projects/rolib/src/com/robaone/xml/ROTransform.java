package com.robaone.xml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class ROTransform {
	protected Transformer m_transformer;
	public ROTransform(String xslt) throws Exception {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		StreamSource xsltSrc = new StreamSource(new ByteArrayInputStream(xslt.getBytes("UTF-8")));
		Transformer transformer = tFactory.newTransformer(xsltSrc);
		this.m_transformer = transformer;
	}
	public String transformXML(String xml) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(out);
		ByteArrayInputStream in = new ByteArrayInputStream(xml.getBytes("UTF-8"));
		this.m_transformer.transform(new StreamSource(in), result);
		String retval = new String(out.toByteArray());
		return retval;
	}
}
