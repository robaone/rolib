/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.1.2.1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.robaone.xml.report;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Record.
 * 
 * @version $Revision$ $Date$
 */
public class Record implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _fieldList.
     */
    private java.util.Vector _fieldList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Record() {
        super();
        this._fieldList = new java.util.Vector();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vField
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addField(
            final com.robaone.xml.report.Field vField)
    throws java.lang.IndexOutOfBoundsException {
        this._fieldList.addElement(vField);
    }

    /**
     * 
     * 
     * @param index
     * @param vField
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addField(
            final int index,
            final com.robaone.xml.report.Field vField)
    throws java.lang.IndexOutOfBoundsException {
        this._fieldList.add(index, vField);
    }

    /**
     * Method enumerateField.
     * 
     * @return an Enumeration over all com.robaone.xml.report.Field
     * elements
     */
    public java.util.Enumeration enumerateField(
    ) {
        return this._fieldList.elements();
    }

    /**
     * Method getField.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the com.robaone.xml.report.Field at the
     * given index
     */
    public com.robaone.xml.report.Field getField(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._fieldList.size()) {
            throw new IndexOutOfBoundsException("getField: Index value '" + index + "' not in range [0.." + (this._fieldList.size() - 1) + "]");
        }
        
        return (com.robaone.xml.report.Field) _fieldList.get(index);
    }

    /**
     * Method getField.Returns the contents of the collection in an
     * Array.  <p>Note:  Just in case the collection contents are
     * changing in another thread, we pass a 0-length Array of the
     * correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public com.robaone.xml.report.Field[] getField(
    ) {
        com.robaone.xml.report.Field[] array = new com.robaone.xml.report.Field[0];
        return (com.robaone.xml.report.Field[]) this._fieldList.toArray(array);
    }

    /**
     * Method getFieldCount.
     * 
     * @return the size of this collection
     */
    public int getFieldCount(
    ) {
        return this._fieldList.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllField(
    ) {
        this._fieldList.clear();
    }

    /**
     * Method removeField.
     * 
     * @param vField
     * @return true if the object was removed from the collection.
     */
    public boolean removeField(
            final com.robaone.xml.report.Field vField) {
        boolean removed = _fieldList.remove(vField);
        return removed;
    }

    /**
     * Method removeFieldAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public com.robaone.xml.report.Field removeFieldAt(
            final int index) {
        java.lang.Object obj = this._fieldList.remove(index);
        return (com.robaone.xml.report.Field) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vField
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setField(
            final int index,
            final com.robaone.xml.report.Field vField)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._fieldList.size()) {
            throw new IndexOutOfBoundsException("setField: Index value '" + index + "' not in range [0.." + (this._fieldList.size() - 1) + "]");
        }
        
        this._fieldList.set(index, vField);
    }

    /**
     * 
     * 
     * @param vFieldArray
     */
    public void setField(
            final com.robaone.xml.report.Field[] vFieldArray) {
        //-- copy array
        _fieldList.clear();
        
        for (int i = 0; i < vFieldArray.length; i++) {
                this._fieldList.add(vFieldArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled com.robaone.xml.report.Record
     */
    public static com.robaone.xml.report.Record unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.robaone.xml.report.Record) Unmarshaller.unmarshal(com.robaone.xml.report.Record.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
