/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.1.2.1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.robaone.xml.report;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Report.
 * 
 * @version $Revision$ $Date$
 */
public class Report implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _date.
     */
    private java.util.Date _date;

    /**
     * Field _name.
     */
    private java.lang.String _name;

    /**
     * Field _author.
     */
    private java.lang.String _author;

    /**
     * Field _group.
     */
    private com.robaone.xml.report.Group _group;

    /**
     * Field _variableList.
     */
    private java.util.Vector _variableList;

    /**
     * Field _resultSet.
     */
    private com.robaone.xml.report.ResultSet _resultSet;


      //----------------/
     //- Constructors -/
    //----------------/

    public Report() {
        super();
        this._variableList = new java.util.Vector();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vVariable
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addVariable(
            final com.robaone.xml.report.Variable vVariable)
    throws java.lang.IndexOutOfBoundsException {
        this._variableList.addElement(vVariable);
    }

    /**
     * 
     * 
     * @param index
     * @param vVariable
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addVariable(
            final int index,
            final com.robaone.xml.report.Variable vVariable)
    throws java.lang.IndexOutOfBoundsException {
        this._variableList.add(index, vVariable);
    }

    /**
     * Method enumerateVariable.
     * 
     * @return an Enumeration over all
     * com.robaone.xml.report.Variable elements
     */
    public java.util.Enumeration enumerateVariable(
    ) {
        return this._variableList.elements();
    }

    /**
     * Returns the value of field 'author'.
     * 
     * @return the value of field 'Author'.
     */
    public java.lang.String getAuthor(
    ) {
        return this._author;
    }

    /**
     * Returns the value of field 'date'.
     * 
     * @return the value of field 'Date'.
     */
    public java.util.Date getDate(
    ) {
        return this._date;
    }

    /**
     * Returns the value of field 'group'.
     * 
     * @return the value of field 'Group'.
     */
    public com.robaone.xml.report.Group getGroup(
    ) {
        return this._group;
    }

    /**
     * Returns the value of field 'name'.
     * 
     * @return the value of field 'Name'.
     */
    public java.lang.String getName(
    ) {
        return this._name;
    }

    /**
     * Returns the value of field 'resultSet'.
     * 
     * @return the value of field 'ResultSet'.
     */
    public com.robaone.xml.report.ResultSet getResultSet(
    ) {
        return this._resultSet;
    }

    /**
     * Method getVariable.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the com.robaone.xml.report.Variable at
     * the given index
     */
    public com.robaone.xml.report.Variable getVariable(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._variableList.size()) {
            throw new IndexOutOfBoundsException("getVariable: Index value '" + index + "' not in range [0.." + (this._variableList.size() - 1) + "]");
        }
        
        return (com.robaone.xml.report.Variable) _variableList.get(index);
    }

    /**
     * Method getVariable.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public com.robaone.xml.report.Variable[] getVariable(
    ) {
        com.robaone.xml.report.Variable[] array = new com.robaone.xml.report.Variable[0];
        return (com.robaone.xml.report.Variable[]) this._variableList.toArray(array);
    }

    /**
     * Method getVariableCount.
     * 
     * @return the size of this collection
     */
    public int getVariableCount(
    ) {
        return this._variableList.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllVariable(
    ) {
        this._variableList.clear();
    }

    /**
     * Method removeVariable.
     * 
     * @param vVariable
     * @return true if the object was removed from the collection.
     */
    public boolean removeVariable(
            final com.robaone.xml.report.Variable vVariable) {
        boolean removed = _variableList.remove(vVariable);
        return removed;
    }

    /**
     * Method removeVariableAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public com.robaone.xml.report.Variable removeVariableAt(
            final int index) {
        java.lang.Object obj = this._variableList.remove(index);
        return (com.robaone.xml.report.Variable) obj;
    }

    /**
     * Sets the value of field 'author'.
     * 
     * @param author the value of field 'author'.
     */
    public void setAuthor(
            final java.lang.String author) {
        this._author = author;
    }

    /**
     * Sets the value of field 'date'.
     * 
     * @param date the value of field 'date'.
     */
    public void setDate(
            final java.util.Date date) {
        this._date = date;
    }

    /**
     * Sets the value of field 'group'.
     * 
     * @param group the value of field 'group'.
     */
    public void setGroup(
            final com.robaone.xml.report.Group group) {
        this._group = group;
    }

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(
            final java.lang.String name) {
        this._name = name;
    }

    /**
     * Sets the value of field 'resultSet'.
     * 
     * @param resultSet the value of field 'resultSet'.
     */
    public void setResultSet(
            final com.robaone.xml.report.ResultSet resultSet) {
        this._resultSet = resultSet;
    }

    /**
     * 
     * 
     * @param index
     * @param vVariable
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setVariable(
            final int index,
            final com.robaone.xml.report.Variable vVariable)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._variableList.size()) {
            throw new IndexOutOfBoundsException("setVariable: Index value '" + index + "' not in range [0.." + (this._variableList.size() - 1) + "]");
        }
        
        this._variableList.set(index, vVariable);
    }

    /**
     * 
     * 
     * @param vVariableArray
     */
    public void setVariable(
            final com.robaone.xml.report.Variable[] vVariableArray) {
        //-- copy array
        _variableList.clear();
        
        for (int i = 0; i < vVariableArray.length; i++) {
                this._variableList.add(vVariableArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled com.robaone.xml.report.Report
     */
    public static com.robaone.xml.report.Report unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.robaone.xml.report.Report) Unmarshaller.unmarshal(com.robaone.xml.report.Report.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
