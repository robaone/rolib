/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.1.2.1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.robaone.xml.report.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.util.Hashtable;

/**
 * Class ParameterTypeType.
 * 
 * @version $Revision$ $Date$
 */
public class ParameterTypeType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The date type
     */
    public static final int DATE_TYPE = 0;

    /**
     * The instance of the date type
     */
    public static final ParameterTypeType DATE = new ParameterTypeType(DATE_TYPE, "date");

    /**
     * The string type
     */
    public static final int STRING_TYPE = 1;

    /**
     * The instance of the string type
     */
    public static final ParameterTypeType STRING = new ParameterTypeType(STRING_TYPE, "string");

    /**
     * The int type
     */
    public static final int INT_TYPE = 2;

    /**
     * The instance of the int type
     */
    public static final ParameterTypeType INT = new ParameterTypeType(INT_TYPE, "int");

    /**
     * The long type
     */
    public static final int LONG_TYPE = 3;

    /**
     * The instance of the long type
     */
    public static final ParameterTypeType LONG = new ParameterTypeType(LONG_TYPE, "long");

    /**
     * The double type
     */
    public static final int DOUBLE_TYPE = 4;

    /**
     * The instance of the double type
     */
    public static final ParameterTypeType DOUBLE = new ParameterTypeType(DOUBLE_TYPE, "double");

    /**
     * The bigdecimal type
     */
    public static final int BIGDECIMAL_TYPE = 5;

    /**
     * The instance of the bigdecimal type
     */
    public static final ParameterTypeType BIGDECIMAL = new ParameterTypeType(BIGDECIMAL_TYPE, "bigdecimal");

    /**
     * The char type
     */
    public static final int CHAR_TYPE = 6;

    /**
     * The instance of the char type
     */
    public static final ParameterTypeType CHAR = new ParameterTypeType(CHAR_TYPE, "char");

    /**
     * The float type
     */
    public static final int FLOAT_TYPE = 7;

    /**
     * The instance of the float type
     */
    public static final ParameterTypeType FLOAT = new ParameterTypeType(FLOAT_TYPE, "float");

    /**
     * The time type
     */
    public static final int TIME_TYPE = 8;

    /**
     * The instance of the time type
     */
    public static final ParameterTypeType TIME = new ParameterTypeType(TIME_TYPE, "time");

    /**
     * The timestamp type
     */
    public static final int TIMESTAMP_TYPE = 9;

    /**
     * The instance of the timestamp type
     */
    public static final ParameterTypeType TIMESTAMP = new ParameterTypeType(TIMESTAMP_TYPE, "timestamp");

    /**
     * Field _memberTable.
     */
    private static java.util.Hashtable _memberTable = init();

    /**
     * Field type.
     */
    private final int type;

    /**
     * Field stringValue.
     */
    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private ParameterTypeType(final int type, final java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method enumerate.Returns an enumeration of all possible
     * instances of ParameterTypeType
     * 
     * @return an Enumeration over all possible instances of
     * ParameterTypeType
     */
    public static java.util.Enumeration enumerate(
    ) {
        return _memberTable.elements();
    }

    /**
     * Method getType.Returns the type of this ParameterTypeType
     * 
     * @return the type of this ParameterTypeType
     */
    public int getType(
    ) {
        return this.type;
    }

    /**
     * Method init.
     * 
     * @return the initialized Hashtable for the member table
     */
    private static java.util.Hashtable init(
    ) {
        Hashtable members = new Hashtable();
        members.put("date", DATE);
        members.put("string", STRING);
        members.put("int", INT);
        members.put("long", LONG);
        members.put("double", DOUBLE);
        members.put("bigdecimal", BIGDECIMAL);
        members.put("char", CHAR);
        members.put("float", FLOAT);
        members.put("time", TIME);
        members.put("timestamp", TIMESTAMP);
        return members;
    }

    /**
     * Method readResolve. will be called during deserialization to
     * replace the deserialized object with the correct constant
     * instance.
     * 
     * @return this deserialized object
     */
    private java.lang.Object readResolve(
    ) {
        return valueOf(this.stringValue);
    }

    /**
     * Method toString.Returns the String representation of this
     * ParameterTypeType
     * 
     * @return the String representation of this ParameterTypeType
     */
    public java.lang.String toString(
    ) {
        return this.stringValue;
    }

    /**
     * Method valueOf.Returns a new ParameterTypeType based on the
     * given String value.
     * 
     * @param string
     * @return the ParameterTypeType value of parameter 'string'
     */
    public static com.robaone.xml.report.types.ParameterTypeType valueOf(
            final java.lang.String string) {
        java.lang.Object obj = null;
        if (string != null) {
            obj = _memberTable.get(string);
        }
        if (obj == null) {
            String err = "" + string + " is not a valid ParameterTypeType";
            throw new IllegalArgumentException(err);
        }
        return (ParameterTypeType) obj;
    }

}
