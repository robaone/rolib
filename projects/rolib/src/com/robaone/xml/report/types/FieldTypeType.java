/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.1.2.1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.robaone.xml.report.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.util.Hashtable;

/**
 * Class FieldTypeType.
 * 
 * @version $Revision$ $Date$
 */
public class FieldTypeType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The date type
     */
    public static final int DATE_TYPE = 0;

    /**
     * The instance of the date type
     */
    public static final FieldTypeType DATE = new FieldTypeType(DATE_TYPE, "date");

    /**
     * The string type
     */
    public static final int STRING_TYPE = 1;

    /**
     * The instance of the string type
     */
    public static final FieldTypeType STRING = new FieldTypeType(STRING_TYPE, "string");

    /**
     * The int type
     */
    public static final int INT_TYPE = 2;

    /**
     * The instance of the int type
     */
    public static final FieldTypeType INT = new FieldTypeType(INT_TYPE, "int");

    /**
     * The long type
     */
    public static final int LONG_TYPE = 3;

    /**
     * The instance of the long type
     */
    public static final FieldTypeType LONG = new FieldTypeType(LONG_TYPE, "long");

    /**
     * The double type
     */
    public static final int DOUBLE_TYPE = 4;

    /**
     * The instance of the double type
     */
    public static final FieldTypeType DOUBLE = new FieldTypeType(DOUBLE_TYPE, "double");

    /**
     * The bigdecimal type
     */
    public static final int BIGDECIMAL_TYPE = 5;

    /**
     * The instance of the bigdecimal type
     */
    public static final FieldTypeType BIGDECIMAL = new FieldTypeType(BIGDECIMAL_TYPE, "bigdecimal");

    /**
     * The byte[] type
     */
    public static final int BYTE_TYPE = 6;

    /**
     * The instance of the byte[] type
     */
    public static final FieldTypeType BYTE = new FieldTypeType(BYTE_TYPE, "byte[]");

    /**
     * The char type
     */
    public static final int CHAR_TYPE = 7;

    /**
     * The instance of the char type
     */
    public static final FieldTypeType CHAR = new FieldTypeType(CHAR_TYPE, "char");

    /**
     * The float type
     */
    public static final int FLOAT_TYPE = 8;

    /**
     * The instance of the float type
     */
    public static final FieldTypeType FLOAT = new FieldTypeType(FLOAT_TYPE, "float");

    /**
     * The time type
     */
    public static final int TIME_TYPE = 9;

    /**
     * The instance of the time type
     */
    public static final FieldTypeType TIME = new FieldTypeType(TIME_TYPE, "time");

    /**
     * The timestamp type
     */
    public static final int TIMESTAMP_TYPE = 10;

    /**
     * The instance of the timestamp type
     */
    public static final FieldTypeType TIMESTAMP = new FieldTypeType(TIMESTAMP_TYPE, "timestamp");

    /**
     * Field _memberTable.
     */
    private static java.util.Hashtable _memberTable = init();

    /**
     * Field type.
     */
    private final int type;

    /**
     * Field stringValue.
     */
    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private FieldTypeType(final int type, final java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method enumerate.Returns an enumeration of all possible
     * instances of FieldTypeType
     * 
     * @return an Enumeration over all possible instances of
     * FieldTypeType
     */
    public static java.util.Enumeration enumerate(
    ) {
        return _memberTable.elements();
    }

    /**
     * Method getType.Returns the type of this FieldTypeType
     * 
     * @return the type of this FieldTypeType
     */
    public int getType(
    ) {
        return this.type;
    }

    /**
     * Method init.
     * 
     * @return the initialized Hashtable for the member table
     */
    private static java.util.Hashtable init(
    ) {
        Hashtable members = new Hashtable();
        members.put("date", DATE);
        members.put("string", STRING);
        members.put("int", INT);
        members.put("long", LONG);
        members.put("double", DOUBLE);
        members.put("bigdecimal", BIGDECIMAL);
        members.put("byte[]", BYTE);
        members.put("char", CHAR);
        members.put("float", FLOAT);
        members.put("time", TIME);
        members.put("timestamp", TIMESTAMP);
        return members;
    }

    /**
     * Method readResolve. will be called during deserialization to
     * replace the deserialized object with the correct constant
     * instance.
     * 
     * @return this deserialized object
     */
    private java.lang.Object readResolve(
    ) {
        return valueOf(this.stringValue);
    }

    /**
     * Method toString.Returns the String representation of this
     * FieldTypeType
     * 
     * @return the String representation of this FieldTypeType
     */
    public java.lang.String toString(
    ) {
        return this.stringValue;
    }

    /**
     * Method valueOf.Returns a new FieldTypeType based on the
     * given String value.
     * 
     * @param string
     * @return the FieldTypeType value of parameter 'string'
     */
    public static com.robaone.xml.report.types.FieldTypeType valueOf(
            final java.lang.String string) {
        java.lang.Object obj = null;
        if (string != null) {
            obj = _memberTable.get(string);
        }
        if (obj == null) {
            String err = "" + string + " is not a valid FieldTypeType";
            throw new IllegalArgumentException(err);
        }
        return (FieldTypeType) obj;
    }

}
