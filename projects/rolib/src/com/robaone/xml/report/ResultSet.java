/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.1.2.1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.robaone.xml.report;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ResultSet.
 * 
 * @version $Revision$ $Date$
 */
public class ResultSet implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _query.
     */
    private com.robaone.xml.report.Query _query;

    /**
     * Field _resultGroupList.
     */
    private java.util.Vector _resultGroupList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ResultSet() {
        super();
        this._resultGroupList = new java.util.Vector();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vResultGroup
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addResultGroup(
            final com.robaone.xml.report.ResultGroup vResultGroup)
    throws java.lang.IndexOutOfBoundsException {
        this._resultGroupList.addElement(vResultGroup);
    }

    /**
     * 
     * 
     * @param index
     * @param vResultGroup
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addResultGroup(
            final int index,
            final com.robaone.xml.report.ResultGroup vResultGroup)
    throws java.lang.IndexOutOfBoundsException {
        this._resultGroupList.add(index, vResultGroup);
    }

    /**
     * Method enumerateResultGroup.
     * 
     * @return an Enumeration over all
     * com.robaone.xml.report.ResultGroup elements
     */
    public java.util.Enumeration enumerateResultGroup(
    ) {
        return this._resultGroupList.elements();
    }

    /**
     * Returns the value of field 'query'.
     * 
     * @return the value of field 'Query'.
     */
    public com.robaone.xml.report.Query getQuery(
    ) {
        return this._query;
    }

    /**
     * Method getResultGroup.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the com.robaone.xml.report.ResultGroup
     * at the given index
     */
    public com.robaone.xml.report.ResultGroup getResultGroup(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._resultGroupList.size()) {
            throw new IndexOutOfBoundsException("getResultGroup: Index value '" + index + "' not in range [0.." + (this._resultGroupList.size() - 1) + "]");
        }
        
        return (com.robaone.xml.report.ResultGroup) _resultGroupList.get(index);
    }

    /**
     * Method getResultGroup.Returns the contents of the collection
     * in an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public com.robaone.xml.report.ResultGroup[] getResultGroup(
    ) {
        com.robaone.xml.report.ResultGroup[] array = new com.robaone.xml.report.ResultGroup[0];
        return (com.robaone.xml.report.ResultGroup[]) this._resultGroupList.toArray(array);
    }

    /**
     * Method getResultGroupCount.
     * 
     * @return the size of this collection
     */
    public int getResultGroupCount(
    ) {
        return this._resultGroupList.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllResultGroup(
    ) {
        this._resultGroupList.clear();
    }

    /**
     * Method removeResultGroup.
     * 
     * @param vResultGroup
     * @return true if the object was removed from the collection.
     */
    public boolean removeResultGroup(
            final com.robaone.xml.report.ResultGroup vResultGroup) {
        boolean removed = _resultGroupList.remove(vResultGroup);
        return removed;
    }

    /**
     * Method removeResultGroupAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public com.robaone.xml.report.ResultGroup removeResultGroupAt(
            final int index) {
        java.lang.Object obj = this._resultGroupList.remove(index);
        return (com.robaone.xml.report.ResultGroup) obj;
    }

    /**
     * Sets the value of field 'query'.
     * 
     * @param query the value of field 'query'.
     */
    public void setQuery(
            final com.robaone.xml.report.Query query) {
        this._query = query;
    }

    /**
     * 
     * 
     * @param index
     * @param vResultGroup
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setResultGroup(
            final int index,
            final com.robaone.xml.report.ResultGroup vResultGroup)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._resultGroupList.size()) {
            throw new IndexOutOfBoundsException("setResultGroup: Index value '" + index + "' not in range [0.." + (this._resultGroupList.size() - 1) + "]");
        }
        
        this._resultGroupList.set(index, vResultGroup);
    }

    /**
     * 
     * 
     * @param vResultGroupArray
     */
    public void setResultGroup(
            final com.robaone.xml.report.ResultGroup[] vResultGroupArray) {
        //-- copy array
        _resultGroupList.clear();
        
        for (int i = 0; i < vResultGroupArray.length; i++) {
                this._resultGroupList.add(vResultGroupArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled com.robaone.xml.report.ResultSet
     */
    public static com.robaone.xml.report.ResultSet unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.robaone.xml.report.ResultSet) Unmarshaller.unmarshal(com.robaone.xml.report.ResultSet.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
