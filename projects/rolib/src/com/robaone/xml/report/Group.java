/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.1.2.1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.robaone.xml.report;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Group.
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class Group implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _headerList.
     */
    @SuppressWarnings("rawtypes")
	private java.util.Vector _headerList;

    /**
     * Field _data.
     */
    private com.robaone.xml.report.Data _data;


      //----------------/
     //- Constructors -/
    //----------------/

    @SuppressWarnings("rawtypes")
	public Group() {
        super();
        this._headerList = new java.util.Vector();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vHeader
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addHeader(
            final com.robaone.xml.report.Header vHeader)
    throws java.lang.IndexOutOfBoundsException {
        this._headerList.addElement(vHeader);
    }

    /**
     * 
     * 
     * @param index
     * @param vHeader
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addHeader(
            final int index,
            final com.robaone.xml.report.Header vHeader)
    throws java.lang.IndexOutOfBoundsException {
        this._headerList.add(index, vHeader);
    }

    /**
     * Method enumerateHeader.
     * 
     * @return an Enumeration over all
     * com.robaone.xml.report.Header elements
     */
    public java.util.Enumeration enumerateHeader(
    ) {
        return this._headerList.elements();
    }

    /**
     * Returns the value of field 'data'.
     * 
     * @return the value of field 'Data'.
     */
    public com.robaone.xml.report.Data getData(
    ) {
        return this._data;
    }

    /**
     * Method getHeader.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the com.robaone.xml.report.Header at
     * the given index
     */
    public com.robaone.xml.report.Header getHeader(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._headerList.size()) {
            throw new IndexOutOfBoundsException("getHeader: Index value '" + index + "' not in range [0.." + (this._headerList.size() - 1) + "]");
        }
        
        return (com.robaone.xml.report.Header) _headerList.get(index);
    }

    /**
     * Method getHeader.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public com.robaone.xml.report.Header[] getHeader(
    ) {
        com.robaone.xml.report.Header[] array = new com.robaone.xml.report.Header[0];
        return (com.robaone.xml.report.Header[]) this._headerList.toArray(array);
    }

    /**
     * Method getHeaderCount.
     * 
     * @return the size of this collection
     */
    public int getHeaderCount(
    ) {
        return this._headerList.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllHeader(
    ) {
        this._headerList.clear();
    }

    /**
     * Method removeHeader.
     * 
     * @param vHeader
     * @return true if the object was removed from the collection.
     */
    public boolean removeHeader(
            final com.robaone.xml.report.Header vHeader) {
        boolean removed = _headerList.remove(vHeader);
        return removed;
    }

    /**
     * Method removeHeaderAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public com.robaone.xml.report.Header removeHeaderAt(
            final int index) {
        java.lang.Object obj = this._headerList.remove(index);
        return (com.robaone.xml.report.Header) obj;
    }

    /**
     * Sets the value of field 'data'.
     * 
     * @param data the value of field 'data'.
     */
    public void setData(
            final com.robaone.xml.report.Data data) {
        this._data = data;
    }

    /**
     * 
     * 
     * @param index
     * @param vHeader
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setHeader(
            final int index,
            final com.robaone.xml.report.Header vHeader)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._headerList.size()) {
            throw new IndexOutOfBoundsException("setHeader: Index value '" + index + "' not in range [0.." + (this._headerList.size() - 1) + "]");
        }
        
        this._headerList.set(index, vHeader);
    }

    /**
     * 
     * 
     * @param vHeaderArray
     */
    public void setHeader(
            final com.robaone.xml.report.Header[] vHeaderArray) {
        //-- copy array
        _headerList.clear();
        
        for (int i = 0; i < vHeaderArray.length; i++) {
                this._headerList.add(vHeaderArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled com.robaone.xml.report.Group
     */
    public static com.robaone.xml.report.Group unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.robaone.xml.report.Group) Unmarshaller.unmarshal(com.robaone.xml.report.Group.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
