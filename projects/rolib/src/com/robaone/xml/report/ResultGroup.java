/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.1.2.1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.robaone.xml.report;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ResultGroup.
 * 
 * @version $Revision$ $Date$
 */
public class ResultGroup implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _recordList.
     */
    private java.util.Vector _recordList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ResultGroup() {
        super();
        this._recordList = new java.util.Vector();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vRecord
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addRecord(
            final com.robaone.xml.report.Record vRecord)
    throws java.lang.IndexOutOfBoundsException {
        this._recordList.addElement(vRecord);
    }

    /**
     * 
     * 
     * @param index
     * @param vRecord
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addRecord(
            final int index,
            final com.robaone.xml.report.Record vRecord)
    throws java.lang.IndexOutOfBoundsException {
        this._recordList.add(index, vRecord);
    }

    /**
     * Method enumerateRecord.
     * 
     * @return an Enumeration over all
     * com.robaone.xml.report.Record elements
     */
    public java.util.Enumeration enumerateRecord(
    ) {
        return this._recordList.elements();
    }

    /**
     * Method getRecord.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the com.robaone.xml.report.Record at
     * the given index
     */
    public com.robaone.xml.report.Record getRecord(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._recordList.size()) {
            throw new IndexOutOfBoundsException("getRecord: Index value '" + index + "' not in range [0.." + (this._recordList.size() - 1) + "]");
        }
        
        return (com.robaone.xml.report.Record) _recordList.get(index);
    }

    /**
     * Method getRecord.Returns the contents of the collection in
     * an Array.  <p>Note:  Just in case the collection contents
     * are changing in another thread, we pass a 0-length Array of
     * the correct type into the API call.  This way we <i>know</i>
     * that the Array returned is of exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public com.robaone.xml.report.Record[] getRecord(
    ) {
        com.robaone.xml.report.Record[] array = new com.robaone.xml.report.Record[0];
        return (com.robaone.xml.report.Record[]) this._recordList.toArray(array);
    }

    /**
     * Method getRecordCount.
     * 
     * @return the size of this collection
     */
    public int getRecordCount(
    ) {
        return this._recordList.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllRecord(
    ) {
        this._recordList.clear();
    }

    /**
     * Method removeRecord.
     * 
     * @param vRecord
     * @return true if the object was removed from the collection.
     */
    public boolean removeRecord(
            final com.robaone.xml.report.Record vRecord) {
        boolean removed = _recordList.remove(vRecord);
        return removed;
    }

    /**
     * Method removeRecordAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public com.robaone.xml.report.Record removeRecordAt(
            final int index) {
        java.lang.Object obj = this._recordList.remove(index);
        return (com.robaone.xml.report.Record) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vRecord
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setRecord(
            final int index,
            final com.robaone.xml.report.Record vRecord)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._recordList.size()) {
            throw new IndexOutOfBoundsException("setRecord: Index value '" + index + "' not in range [0.." + (this._recordList.size() - 1) + "]");
        }
        
        this._recordList.set(index, vRecord);
    }

    /**
     * 
     * 
     * @param vRecordArray
     */
    public void setRecord(
            final com.robaone.xml.report.Record[] vRecordArray) {
        //-- copy array
        _recordList.clear();
        
        for (int i = 0; i < vRecordArray.length; i++) {
                this._recordList.add(vRecordArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled com.robaone.xml.report.ResultGroup
     */
    public static com.robaone.xml.report.ResultGroup unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.robaone.xml.report.ResultGroup) Unmarshaller.unmarshal(com.robaone.xml.report.ResultGroup.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
