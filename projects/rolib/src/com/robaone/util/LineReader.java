package com.robaone.util;

import java.io.FileReader;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class LineReader {
  protected FileReader m_file;
  public LineReader(String filename) throws Exception {
    this.m_file = new FileReader(filename);
  }
  public LineReader(FileReader file) throws Exception {
    this.m_file = file;
  }
  public String ReadLine() throws Exception {
    String buffer = "";
    int i = 0;
    for(i = this.m_file.read(); i != -1;i = this.m_file.read()){
      char c = (char)i;
      if(c == '\n') break;
      buffer += c;
    }
    if(i != -1){
      return buffer;
    }else if(i == -1 && buffer.length() > 0){
      return buffer;
    }else{
      throw new java.io.EOFException("End of File Reached");
    }
  }
}