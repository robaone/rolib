package com.robaone.util;


import java.util.*;
import java.text.*;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class Strmanip {

  public Strmanip() {
  }
  // Split string by delimitor and store in Vector.  Remove all white space
  public static int split(char delim, String val,Vector<String> list){
    return Strmanip.split(delim,val,list,true);
  }
  // Split string by delimitor and store in Vector.  Trimming optional
  public static int split(char delim, String val,Vector<String> list,boolean trim){
    char[] word = val.toCharArray();
    String token = "";
    list.clear();
    for(int i = 0;i < val.length();i++){
	if(word[i] == delim){
		if(trim == false || token.length() > 0){
                        list.add(token.trim());
                        token = "";
		}
	}else{
		token += word[i];
	}
    }
    if(trim == false){
      list.add(token.trim());
    }else if(token.length() > 0){
      list.add(token.trim());
    }
    return list.size();
  }

  // Split 2 No vector required
  public static Vector<String> split(char delim,String val){
    Vector<String> list = new Vector<String>();
    Strmanip.split(delim,val,list);
    return list;
  }
  // Parse date fields
  public static long parseDate(String str) throws Exception{
    SimpleDateFormat df = new SimpleDateFormat("M/d/y");
    return df.parse(str).getTime();
  }
  // Format date fields
  public static String formatDate(long date) throws Exception {
    SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy");
    return df.format(new java.util.Date(date));
  }
  // Remove white space from strings
  public static String removewhitespace(String str) throws Exception {
    String retval = "";
    for(int i = 0; i < str.length();i++){
      if((int)str.charAt(i) <= 32 && (int)str.charAt(i) != 10){
        continue;
      }else{
        retval += str.charAt(i);
      }
    }
    return retval;
  }
  // Replace a character from a string
  public static String replacechar(String str,char c,char r) throws Exception {
    String retval = "";
    for(int i = 0; i < str.length();i++){
      if(str.charAt(i) == c){
        retval += r;
      }else{
        retval += str.charAt(i);
      }
    }
    return retval;
  }
}