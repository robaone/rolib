package com.robaone.util;

import java.util.*;
import java.io.FileReader;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class TabParser {
  protected Vector<Vector<String>> m_records;
  protected TabParser(){
    this.m_records = new Vector<Vector<String>>();
  }
  public static Vector<Vector<String>> parseFile(FileReader file) throws Exception{
    TabParser parser = new TabParser();
    LineReader lr = new LineReader(file);
    try{
      for(String line = lr.ReadLine();;line = lr.ReadLine()){

        //Split by tab space
        Vector<String> fields = new Vector<String>();
        Strmanip.split('\t',line,fields,false);
        //Save into records
        parser.m_records.add(fields);
      }
    }catch(java.io.EOFException eof){
    }
    return parser.m_records;
  }
}