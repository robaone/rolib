package com.robaone.presentation.servlet;

import java.sql.Connection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.robaone.business.ReportFactory;
import com.robaone.business.StyleFactory;
import com.robaone.data.ReportRetriever;
import com.robaone.data.StyleRetriever;
import com.robaone.presentation.ROHTMLReport;
import com.robaone.presentation.ROPDFReport;
import com.robaone.xml.report.Query;
import com.robaone.xml.report.Report;

/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
 public class ReportServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;
   static final String URL = "jdbc:mysql://mysql.sacorrections.org/ccc";
   static final String DRIVER = "org.gjt.mm.mysql.Driver";//"com.mysql.jdbc.Driver";
   static final String USERNAME = "research";
   static final String PASSWORD = "oracle.fnd";
    /* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public ReportServlet() {
		super();
	}   	
	
	protected void doHTML(HttpServletRequest request, HttpServletResponse response,
			ReportRetriever retriever,StyleRetriever styleretriever,Connection con) throws Exception {
		String name = request.getParameter("name");
		String style = request.getParameter("style");
		try{
			String html_str = this.getHTML(name,style,request.getParameterMap(),
					retriever,styleretriever,con);
			response.setContentType("text/html");
			response.getOutputStream().print(html_str);
		}catch(Exception e){
		}finally{
		}
	}

	protected String getHTML(String name,String style,Map<String,String[]> parameters,
			ReportRetriever retriever,StyleRetriever styleretriever,Connection con) throws Exception {
		Report report = ReportFactory.getReport(name,con,retriever);
		Query q = report.getResultSet().getQuery();
		for(int i = 0; i < q.getParameterCount();i++){
			String param_name = q.getParameter(i).getName();
			Object value = parameters.get(param_name);
			if(value != null){
				String[] val = (String[])value;
				q.getParameter(i).setContent(val[0].toString());
			}
		}
		report = ReportFactory.createReport(report, con);
		String xsl = StyleFactory.getStyle(style,con,styleretriever);
		ROHTMLReport html = new ROHTMLReport(report,xsl);
		String html_str = html.getHTML();
		return html_str;
	}
	protected void doPDF(HttpServletRequest request, HttpServletResponse response,
			ReportRetriever retriever,StyleRetriever styleretriever,Connection con) {
		String name = request.getParameter("name");
		String style = request.getParameter("style");
		try{
			Map<String,String[]> parameters = request.getParameterMap();
			byte[] bytes = this.getPDF(name,style,parameters,retriever,styleretriever,con);
			response.setContentType("application/pdf");
			response.getOutputStream().write(bytes);
			response.getOutputStream().flush();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{con.close();}catch(Exception e){}
		}
	}
	protected byte[] getPDF(String name,String style,Map<String,String[]> parameters,
			ReportRetriever retriever,StyleRetriever styleretriever,Connection con) throws Exception {
		Report report = ReportFactory.getReport(name,con,retriever);
		Query q = report.getResultSet().getQuery();
		for(int i = 0; i < q.getParameterCount();i++){
			String param_name = q.getParameter(i).getName();
			Object value = parameters.get(param_name);
			if(value != null){
				String[] val = (String[])value;
				q.getParameter(i).setContent(val[0].toString());
			}
		}
		report = ReportFactory.createReport(report, con);
		String xsl = StyleFactory.getStyle(style,con,styleretriever);
		ROPDFReport pdf = new ROPDFReport(report,xsl);
		byte[] bytes = pdf.getPDF();
		return bytes;
	}
}