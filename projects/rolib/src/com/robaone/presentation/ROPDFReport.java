package com.robaone.presentation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.StringWriter;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import com.robaone.xml.ROTransform;
import com.robaone.xml.report.Report;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 * @version 1.0
 *
 */
public class ROPDFReport {
	protected String m_xmlReport;
	protected String m_xslReportStyle;
	private FopFactory fopFactory = FopFactory.newInstance();
	private TransformerFactory tFactory = TransformerFactory.newInstance();
	
	public ROPDFReport(String xmlReport,String xslReportStyle){
		this.m_xmlReport = xmlReport;
		this.m_xslReportStyle = xslReportStyle;
	}
	public ROPDFReport(Report report,String xslReportStyle){
		StringWriter sw = new StringWriter();
		try{
			report.marshal(sw);
		}catch(Exception e){
			System.out.println("Error in ROPDFReport(Report,String): "+e.getClass().getName()+": "+e.getMessage());
		}
		this.m_xmlReport = sw.toString();
		this.m_xslReportStyle = xslReportStyle;
	}
	public byte[] getPDF() throws Exception {
		ROTransform trn = new ROTransform(this.m_xslReportStyle);
		String xml_fo = trn.transformXML(this.m_xmlReport);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
		Transformer transformer = tFactory.newTransformer();
		Source src = new StreamSource(new ByteArrayInputStream(xml_fo.getBytes()));
		Result res = new SAXResult(fop.getDefaultHandler());
		transformer.transform(src, res);
		return out.toByteArray();
	}
}
