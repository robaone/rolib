package com.robaone.presentation;

import java.io.StringWriter;

import com.robaone.xml.ROTransform;
import com.robaone.xml.report.Report;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class ROHTMLReport {
	private String m_xmlReport;
	private String m_xslReportStyle;

	public ROHTMLReport(String xmlReport,String xslReportStyle){
		this.m_xmlReport = xmlReport;
		this.m_xslReportStyle = xslReportStyle;
	}
	public ROHTMLReport(Report report,String xslReportStyle){
		StringWriter sw = new StringWriter();
		try{
			report.marshal(sw);
		}catch(Exception e){}
		this.m_xmlReport = sw.toString();
		this.m_xslReportStyle = xslReportStyle;
		
	}
	public String getHTML() throws Exception {
		ROTransform trn = new ROTransform(this.m_xslReportStyle);
		return trn.transformXML(this.m_xmlReport);
	}
}
