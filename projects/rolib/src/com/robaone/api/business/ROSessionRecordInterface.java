package com.robaone.api.business;

public interface ROSessionRecordInterface {
	public Integer getCreatedby();
	public void setCreatedby(Integer createdby);
	public Boolean get_void();
	public void set_void(Boolean _void);
	public java.sql.Timestamp getCreateddate();
	public void setCreateddate(java.sql.Timestamp creationdate);
	public Integer getModifiedby();
	public void setModifiedby(Integer modifiedby);
	public java.sql.Timestamp getModifieddate();
	public void setModifieddate(java.sql.Timestamp modificationdate);
	public String getCreationhost();
	public void setCreationhost(String creationhost);
	public String getModificationhost();
	public void setModificationhost(String modificationhost);
	public java.sql.Timestamp get_lock();
	public void set_lock(java.sql.Timestamp _lock);
	public Integer get_lockowner();
	public void set_lockowner(Integer _lockowner);
}
