package com.robaone.api.business;

public class RecordLockedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5815098984161952239L;
	public RecordLockedException(Exception e){
		super(e);
	}
	public RecordLockedException(String msg){
		super(msg);
	}
}
