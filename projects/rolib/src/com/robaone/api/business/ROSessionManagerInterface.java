package com.robaone.api.business;

import org.json.JSONObject;

public interface ROSessionManagerInterface {

	void prepareRecord(ROSessionRecordInterface record);

	void cleanJSON(JSONObject jo);
	
	public boolean lockRecord(ROSessionRecordInterface record);
	
	public boolean isLocked(ROSessionRecordInterface record);

}
