package com.robaone.api.business;

public class InputParamsException extends Exception {

	public InputParamsException(String string) {
		super(string);
	}

	public InputParamsException(Exception e) {
		super(e);
	}

}
