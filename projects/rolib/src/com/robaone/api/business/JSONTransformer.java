package com.robaone.api.business;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.json.XML;

public class JSONTransformer {
	public static void main(String[] args){
		try{
			if(args.length < 2){
				throw new Exception("not enough arguments");
			}
			ROTransformer trn = new ROTransformer(IOUtils.toString(new FileInputStream(new File(args[1]))));
			OutputStream out = System.out;
			if(args.length > 2){
				FileOutputStream fout = new FileOutputStream(new File(args[2]));
				out = fout;
			}
			FileInputStream fin = new FileInputStream(new File(args[0]));
			String json = IOUtils.toString(fin);
			String xml = XML.toString(new JSONObject(json));
			trn.transformToStream(xml, out);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
