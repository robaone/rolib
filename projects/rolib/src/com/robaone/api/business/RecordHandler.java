package com.robaone.api.business;

import java.io.OutputStream;
import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONObject;

import com.robaone.api.business.BaseAction;
import com.robaone.api.business.FieldValidator;
import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.SessionData;
import com.robaone.api.data.jdo.Projects_jdo;
import com.robaone.api.data.jdo.Projects_jdoManager;
import com.robaone.api.json.DSResponse;
import com.robaone.dbase.ConnectionBlock;
import com.robaone.dbase.HDBConnectionManager;
import com.robaone.jdo.RO_JDO;

abstract public class RecordHandler extends BaseAction<JSONObject> {

	private String QUERIES;
	protected static final String LIST = "list";
	protected static final String LIST_COUNT = "count";
	protected static final String FILTERED_LIST = "list_filtered";
	protected static final String FILTERED_COUNT = "count_filtered";
	protected static final String GET = "get";
	protected static final String GET_COUNT = "get_count";
	protected static final String DELETE = "delete";
	public static final String NOT_SUPPORTED = "Not Supported";
	public static final String NOT_FOUND_ERROR = "Record not found";
	protected String ID_ERROR = "You must enter a valid id";
	protected static String ID;
	public RecordHandler(OutputStream o, SessionData d, HttpServletRequest request)
	throws ParserConfigurationException {
		super(o, d, request);
		QUERIES = getQueries();
		ID = getIdentity();
	}
	abstract protected String getQueries();
	abstract protected String getIdentity();
	abstract protected void put_record(final JSONObject jo,
			final String id,Connection con) throws Exception;
	abstract protected void create_record(final JSONObject jo,Connection con) throws Exception;
	abstract protected HDBConnectionManager getConnectionManager() throws Exception;
	public void list(JSONObject jo){
		this.getFunctionCallHandler(LIST, jo).run(this,jo);
	}
	abstract public PagedFunctionCall getFunctionCallHandler(String name,JSONObject jo);
	public void get(JSONObject jo){
		this.getFunctionCallHandler(GET, jo).run(this,jo);
	}
	public void put(JSONObject jo){
		new FunctionCall(){

			@Override
			protected void run(final JSONObject jo) throws Exception {
				final String id = this.findXPathString("//"+ID);
				if(!FieldValidator.isNumber(id)){
					fieldError(ID,ID_ERROR);
				}else{
					new ConnectionBlock(){

						@Override
						protected void run() throws Exception {
							jo.remove(ID);
							removeReservedFields(jo);
							setModificationFields(jo);
							put_record(jo, id,this.getConnection());
						}

						

					}.run(getConnectionManager());
				}
			}

		}.run(this, jo);
	}
	public void create(JSONObject jo){
		new FunctionCall(){

			@Override
			protected void run(final JSONObject jo) throws Exception {
				new ConnectionBlock(){

					@Override
					protected void run() throws Exception {
						jo.remove(ID);
						removeReservedFields(jo);
						setCreationFields(jo);
						create_record(jo,this.getConnection());
					}

				}.run(getConnectionManager());
			}


		}.run(this, jo);
	}
	public void delete(JSONObject jo){
		this.getFunctionCallHandler(DELETE, jo).run(this,jo);	}

	@Override
	public DSResponse<JSONObject> newDSResponse() {
		return new DSResponse<JSONObject>();
	}

}
