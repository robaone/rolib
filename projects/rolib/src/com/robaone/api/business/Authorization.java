package com.robaone.api.business;

import com.robaone.api.data.SessionData;

public class Authorization {
	public static boolean requiresLogin(String page,SessionData sessiondata){
		boolean retval = false;
		String[] pages = {"dashboard"};
		for(String p : pages){
			if(p.equalsIgnoreCase(page)){
				if(sessiondata == null || sessiondata.getUser() == null){
					return true;
				}
			}
		}
		return retval;
	}
}
