package com.robaone.api.business;


public interface ROSessionRecordManagerInterface {
	public void lock(ROSessionRecordInterface record) throws RecordLockedException;
}
