package com.robaone.api.business;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FieldValidator {
	public static final String PASSWORD_REQUIREMENT = "You password must be at least 6 characters";

	public static boolean isEmail(String str){
		boolean retval = false;
		if(str != null){
			String[] t = str.split(" ");
			if(t.length == 1){
				t = str.split("[@]");
				if(t.length == 2){
					t = t[1].split("[.]");
					if(t.length > 1){
						retval = true;
					}
				}
			}
		}
		return retval;
	}

	public static boolean isZipCode(String string) {
		boolean retval = false;
		if(string != null){
			String[] t = string.split("[-]");
			if(t.length <= 2){
				String fivedigit = t[0];
				if(t.length > 1){
					String fourdigit = t[1];
					if(fivedigit.trim().length() == 5 && fourdigit.trim().length() == 4){
						try{
							Integer.parseInt(fivedigit);
							Integer.parseInt(fourdigit);
							retval = true;
						}catch(Exception e){}
					}
				}else{
					if(fivedigit.trim().length() == 5){
						try{
							Integer.parseInt(fivedigit);
							retval = true;
						}catch(Exception e){}
					}
				}
			}
		}
		return retval;
	}

	public static boolean exists(String username) {
		if(username == null || username.trim().length() == 0)
			return false;
		else
			return true;
	}

	public static boolean validPassword(String password) {
		try{
			if(password.length() >= 6){
				return true;
			}
		}catch(Exception e){}
		return false;
	}

	public static String getPasswordRequirement() {
		return FieldValidator.PASSWORD_REQUIREMENT;
	}

	public static boolean isNumber(String limit) {
		if(limit == null) return false;
		try{
			limit = limit.trim();
			if(limit.length() > 0){
				String chars = "0123456789";
				for(int i = 0; i < limit.length();i++){
					char c = limit.charAt(i);
					boolean found = false;
					for(int j = 0; j < chars.length(); j++){
						char nc = chars.charAt(j);
						if(nc == c){
							found = true;
							break;
						}
					}
					if(!found){
						return false;
					}
				}
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
	}
	public static java.util.Date parseDate(String str) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
		SimpleDateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");
		try{
			Date d = df.parse(str);
			return d;
		}catch(Exception e){
			try{
				Date d = df2.parse(str);
				return d;
			}catch(Exception e2){
				throw e2;
			}
		}
	}
	public static boolean isDate(String workdate) {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
		SimpleDateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");
		try{
			Date d = df.parse(workdate);
			if(d.getTime() > 0) return true;
		}catch(Exception e){
			try{
				Date d = df2.parse(workdate);
				if(d.getTime() > 0) return true;
			}catch(Exception e2){
				return false;
			}
		}
		return false;
	}

	public static java.util.Date getDate(String date) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
		SimpleDateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");
		try{
			Date d = df.parse(date);
			return d;
		}catch(Exception e){
			try{
				Date d = df2.parse(date);
				return d;
			}catch(Exception e2){
				throw e2;
			}
		}
	}
}
