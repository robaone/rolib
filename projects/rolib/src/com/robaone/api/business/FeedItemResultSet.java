package com.robaone.api.business;

public interface FeedItemResultSet {

	boolean next() throws Exception;

	java.util.Date getTime() throws Exception;

	String getContenttype() throws Exception;

	String getDescription() throws Exception;

	String getTitle() throws Exception;

	String getMainUrl() throws Exception;

	String getUrl() throws Exception;

	String getCategory() throws Exception;

	String getId() throws Exception;

}
