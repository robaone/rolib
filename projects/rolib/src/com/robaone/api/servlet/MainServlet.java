package com.robaone.api.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.robaone.api.business.ApiCall;
import com.robaone.api.business.AuthorizationException;
import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.SessionData;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/index")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try{
			AppDatabase.setFilename(request.getContextPath().substring(1)+"-config.ini");
			SessionData data = (SessionData)request.getSession().getAttribute("appsessiondata");
			if(data == null){
				data = new SessionData();
				request.getSession().setAttribute("appsessiondata", data);
			}
			try{
				ApiCall.authorize(data);
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}catch(AuthorizationException e){
				String url = e.getMessage();
				response.sendRedirect(url);
			}
		}catch(Exception e){
			throw new ServletException(e);
		}
	}

}
