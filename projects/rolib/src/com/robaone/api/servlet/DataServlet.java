package com.robaone.api.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.robaone.api.business.ActionDispatcher;
import com.robaone.api.data.AppDatabase;
import com.robaone.api.data.Error;
import com.robaone.api.data.SessionData;
import com.robaone.api.json.DSResponse;
import com.robaone.api.json.JSONResponse;

/**
 * Servlet implementation class DataServlet
 */
public class DataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DataServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		AppDatabase.writeLog("00030: Start Server Side Code:");
		SessionData sdata = (SessionData)request.getSession().getAttribute("sessiondata");
		String type = "json";
		try{
			if(sdata == null){
				sdata = new SessionData();
				request.getSession().setAttribute("sessiondata", sdata);
			}
			sdata.setRemoteHost(request.getRemoteHost());
			ActionDispatcher dsp = new ActionDispatcher(sdata,request,response);
			try{
				String t = request.getParameter("type");
				if(t.equals("xml")){
					type = "xml";
				}
			}catch(Exception e){}
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if(!isMultipart)
				if(type.equals("json")){
					dsp.runAction(request.getParameter("action"), request.getParameterMap(),response.getOutputStream());
				}else{
					ByteArrayOutputStream bout = new ByteArrayOutputStream();
					dsp.runAction(request.getParameter("action"), request.getParameterMap(),bout);
					String xml = XML.toString(new JSONObject(bout.toString()));
					response.getOutputStream().print("<?xml version=\"1.0\" ?>\n"+xml);
				}
			else{
				// Create a factory for disk-based file items
				DiskFileItemFactory factory = new DiskFileItemFactory();

				// Set factory constraints
				factory.setSizeThreshold(0);
				factory.setRepository(new File(AppDatabase.getProperty("temp.folder")));

				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);

				// Set overall request size constraint
				//upload.setSizeMax(yourMaxRequestSize);

				// Parse the request
				List /* FileItem */ items = upload.parseRequest(request);

				dsp.runAction(items, response.getOutputStream());
			}
		}catch(Exception e){
			DSResponse<Error> dsr = new DSResponse<Error>();
			dsr.getResponse().setStatus(JSONResponse.GENERAL_ERROR);
			dsr.getResponse().setError(e.getClass().getName().split("[.]")[e.getClass().getName().split("[.]").length-1]+": "+e.getMessage());
			PrintWriter pw = null;
			if(type.equals("json")){
				pw = new PrintWriter(response.getOutputStream());
				JSONObject jo = new JSONObject(dsr);
				try {
					pw.print(jo.toString(4));
				} catch (JSONException e1) {
					throw new ServletException(e1);
				}
			}else{
				pw = new PrintWriter(response.getOutputStream());
				JSONObject jo = new JSONObject(dsr);
				pw.print("<?xml version=\"1.0\" ?>\n");
				try {
					pw.print(XML.toString(jo));
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
			pw.flush();
			pw.close();
		}
	}

}
