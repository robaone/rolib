package com.robaone.api.data;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Vector;

import javax.security.auth.login.LoginException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.json.XML;

import com.robaone.api.business.ROTransformer;
import com.robaone.api.business.StringEncrypter;
import com.robaone.api.data.blocks.FindCredentialsBlock;
import com.robaone.api.data.blocks.UserRolesBlock;
import com.robaone.api.data.jdo.Credentials_jdo;
import com.robaone.api.data.jdo.Roles_jdo;
import com.robaone.api.data.jdo.Roles_jdoManager;
import com.robaone.api.data.jdo.User_jdo;
import com.robaone.api.data.jdo.User_jdoManager;
import com.robaone.dbase.ConnectionBlock;
import com.robaone.dbase.types.StringType;
import com.robaone.util.INIFileReader;

public class AppDatabase {
	public static final int ROLE_ROOT = 0;
	public static final int ROLE_ADMINISTRATOR = 1;
	public static final int ROLE_CUSTOMERSERVICE = 2;
	public static final int ROLE_SERVICEPROVIDER = 3;
	public static final int ROLE_VENDOR = 4;
	public static enum SEQUENCEID { ACCOUNTID, AD_TRACKING, BATCHID, ENROLL_EMAIL_BATCH, KEYID, MEMBERID, STATEMENTID };
	private static final String PAGE_NOT_FOUND_ERROR = "<h1>Page Not Found</h1>";
	private static INIFileReader inifile;
	public static final String PACKAGE_VARIABLE = "com.robaone.api.data.AppDatabase.config";
	public static final String DEFAULT_DB_CONTEXT = "default.db.context";
	private static String path = System.getProperty("resource.path");
	private static String inifilename = System.getProperty(PACKAGE_VARIABLE) == null ? "config.ini" : System.getProperty(PACKAGE_VARIABLE);
	public static void setFilename(String filename){
		inifilename = filename;
	}
	public static void setResourcePath(String path){
		AppDatabase.path = path;
	}
	public static String getProperty(String prop,String file){
		if(inifile == null){
			try {
				String file_path = AppDatabase.path+System.getProperty("file.separator")+file;
				AppDatabase.writeLog(" -- Reading config file, "+file_path);
				inifile = new INIFileReader(file_path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			return inifile.getValue(prop);
		} catch (Exception e) {
			AppDatabase.writeLog(" -- Could not retrieve parameter, '"+prop+"', from config file.");
		}
		return null;
	}
	public static String getProperty(String prop){
		return getProperty(prop,inifilename);
	}
	public static String[] getParameterValues(Map<String,String[]> params,String key){
		try{
			String[] retval = null;
			Object val = params.get(key);
			if(val instanceof String[]){
				retval = (String[])val;
			}else{
				retval = new String[1];
				retval[0] = val.toString(); 
			}
			return retval;
		}catch(Exception e){
			return new String[0];
		}
	}
	public static String getParameterString(Map<String,String[]> params,String key){
		try{
			Object val = params.get(key);
			if(val instanceof String[]){
				String[] vals = (String[])val;
				return new StringType(vals[0]).toString();
			}else{
				return new StringType(val.toString()).toString();
			}
		}catch(Exception e){
			return "";
		}
	}
	public static JSONObject getSession(String sessionid) throws Exception {
		String temp_folder = getProperty("temp.folder");
		String json_str = FileUtils.readFileToString(new File(temp_folder+System.getProperty("file.separator")+sessionid));
		JSONObject jo = new JSONObject(json_str);
		return jo;
	}
	public static void putSession(String sessionid, JSONObject jo) throws Exception {
		String temp_folder = getProperty("temp.folder");
		FileUtils.writeStringToFile(new File(temp_folder+System.getProperty("file.separator")+sessionid), jo.toString(4));
	}
	public static void removeSession(String sessionid) throws Exception {
		String temp_folder = getProperty("temp.folder");
		File f = new File(temp_folder+System.getProperty("file.separator")+sessionid);
		f.delete();
	}
	public static String getPage(String page){
		String page_folder = getProperty("page.folder");
		String retval = null;
		try{
			retval = org.apache.commons.io.FileUtils.readFileToString(new File(page_folder+System.getProperty("file.separator")+page+".html"));
		}catch(Exception e){
			retval = AppDatabase.PAGE_NOT_FOUND_ERROR;
		}
		return retval;
	}
	public static String getPage(String page,String section){
		String page_folder = getProperty("page.folder");
		String retval = null;
		try{
			retval = org.apache.commons.io.FileUtils.readFileToString(new File(page_folder+System.getProperty("file.separator")+section+System.getProperty("file.separator")+page+".html"));
		}catch(Exception e){
			retval = AppDatabase.PAGE_NOT_FOUND_ERROR;
		}
		return retval;
	}
	public static String storeSessionData(JSONObject session) throws Exception {
		String instanceid = ""+new java.util.Date().getTime();
		AppDatabase.putSession(instanceid, session);
		return instanceid;
	}
	public static String generatePage(String page,Map<String,String[]> parameters,JSONObject session) throws Exception {
		return generatePage(page,null,parameters,session);
	}
	public static String generatePage(String page,String section,Map<String,String[]> parameters,JSONObject session) throws Exception{
		String retval = null;
		String instanceid = storeSessionData(session);
		try{
			String xsl_folder = getProperty("xsl.folder");
			if(section != null){
				xsl_folder = xsl_folder + System.getProperty("file.separator") + section;
			}
			JSONObject jo = new JSONObject();
			jo.put("sessiondata", session);
			jo.put("parameters", parameters);
			jo.put("page", page);
			jo.put("instanceid", instanceid);
			String xml = XML.toString(jo, "data");
			StreamSource source = new StreamSource(new StringReader(xml));
			StreamSource stylesource = new StreamSource(xsl_folder+System.getProperty("file.separator")+page+".xsl");

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(stylesource);
			StringWriter out = new StringWriter();
			StreamResult result = new StreamResult(out);
			transformer.transform(source, result);
			retval = out.toString();
			JSONObject session_jo = AppDatabase.getSession(instanceid);
			Boolean b = new Boolean(false);
			try{ b = session_jo.getBoolean("request_login");}catch(Exception e){}
			if(b){
				throw new LoginException("login requested"); 
			}
		}finally{
			try {
				AppDatabase.removeSession(instanceid);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return retval;
	}
	public static String filterPageParameter(String page){
		if(page == null || page.trim().length() == 0){
			page = "home";
		}
		return page;
	}
	public static void copyStream(FileInputStream fin, OutputStream out) throws IOException {
		byte[] buffer= new byte[1024];
		for(int i = fin.read(buffer);i > -1;i = fin.read(buffer)){
			out.write(buffer,0,i);
		}
	}
	public static String getStylesheet(String string) throws Exception {
		String path = AppDatabase.getProperty("xsl.folder");
		String filename = path + System.getProperty("file.separator")+string+".xsl";
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		FileInputStream fin = null;
		try{
			fin = new FileInputStream(new File(filename));
			copyStream(fin,bout);
			return new String(bout.toByteArray());
		}catch(Exception e){
			throw e;
		}finally{
			fin.close();
		}
	}
	public static String transform(String stylesheet, String xml) throws Exception {
		ROTransformer trn = new ROTransformer(stylesheet);
		return trn.transform(xml);
	}
	public static void writeLog(String string) {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		System.out.println(df.format(new java.util.Date())+": "+string);
	}
	public static String generateToken() throws Exception {
		return StringEncrypter.encryptString(new java.util.Date().getTime()+"");
	}
	public static Timestamp getTimestamp() {
		return new java.sql.Timestamp(new java.util.Date().getTime());
	}
	public static Credentials_jdo getCredentials(String string, String token) throws Exception {
		final Vector<Credentials_jdo> retval = new Vector<Credentials_jdo>();
		ConnectionBlock block = new FindCredentialsBlock(retval,string,token);
		block.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));
		return retval.size() > 0 ? retval.get(0) : null;
	}
	public static Roles_jdo addUserRole(final Integer iduser,final int role) throws Exception {
		final Vector<Roles_jdo> retval = new Vector<Roles_jdo>();
		new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				Roles_jdoManager man = new Roles_jdoManager(this.getConnection());
				Roles_jdo role_record = man.newRoles();
				role_record.setIduser(iduser);
				role_record.setRole(role);
				man.save(role_record);
				retval.add(role_record);
			}

		}.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));
		return retval.size() > 0 ? retval.get(0) : null;
	}
	public static void removeUserRole(final Integer iduser,final int role) throws Exception {
		new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				Roles_jdoManager man = new Roles_jdoManager(this.getConnection());
				this.setPreparedStatement(man.prepareStatement(Roles_jdo.ROLE + " = ? and "+Roles_jdo.IDUSER + " = ?"));
				this.getPreparedStatement().setInt(1, role);
				this.getPreparedStatement().setInt(2, iduser.intValue());
				this.setResultSet(this.getPreparedStatement().executeQuery());
				if(this.getResultSet().next()){
					Roles_jdo record = man.bindRoles(this.getResultSet());
					man.delete(record);
				}
			}

		}.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));
	}
	public static User_jdo getUser(final Integer iduser) throws Exception {
		final Vector<User_jdo> retval = new Vector<User_jdo>();
		new ConnectionBlock(){

			@Override
			public void run() throws Exception {
				User_jdoManager man = new User_jdoManager(this.getConnection());
				User_jdo user = man.getUser(iduser);
				retval.add(user);
			}

		}.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));
		return retval.size() > 0 ? retval.get(0) : null;
	}
	public static Roles_jdo[] getUserRoles(Integer iduser) throws Exception {
		Vector<Roles_jdo> retval = new Vector<Roles_jdo>();
		UserRolesBlock block = new UserRolesBlock(retval,iduser);
		block.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));
		return retval.toArray(new Roles_jdo[0]);
	}
	public static boolean hasRole(final Integer iduser,final int... roles) throws Exception {
		final Vector<Boolean> retval = new Vector<Boolean>();
		new ConnectionBlock(){

			@Override
			public void run() throws Exception {
				String str = "select * from roles where iduser = ? and role in (";
				for(int i = 0; i < roles.length;i++){
					str += (i > 0 ? "," : "") + "?";
				}
				str += ")";
				this.setPreparedStatement(this.getConnection().prepareStatement(str));
				this.getPreparedStatement().setInt(1, iduser.intValue());
				for(int i = 0; i < roles.length;i++){
					this.getPreparedStatement().setInt(i+2, roles[i]);
				}
				this.setResultSet(this.getPreparedStatement().executeQuery());
				if(this.getResultSet().next()){
					retval.add(new Boolean(true));
				}
			}

		}.run(DatabaseImpl.getConnectionManager(AppDatabase.getProperty(AppDatabase.DEFAULT_DB_CONTEXT)));
		return retval.size() > 0 ? retval.get(0).booleanValue() : false;
	}
	public static String base48Encode(long no) {
		BigDecimal num = new BigDecimal(no);
		String charSet = "0123456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";
		int length = charSet.length();
		String encodeString = new String();
		while (num.doubleValue() > length) {
			encodeString = charSet.charAt(num.intValue() % length) + encodeString;
			num = num.divide(new BigDecimal(length), 0);//new Double(Math.ceil((new Double(num.doubleValue() / length)).doubleValue() - 1));
		}
		encodeString = charSet.charAt(num.intValue()) + encodeString;
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
		}
		return encodeString;
	}
	public static BigDecimal getNextSequence(final SEQUENCEID id,final String db) throws Exception {
		final Vector<BigDecimal> retval = new Vector<BigDecimal>();
		new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				String str = "DECLARE @RC int\n"+
				"\n"+
				"DECLARE @curval numeric(38,0)\n"+
				"EXECUTE @RC = [MICROD].[dbo].[$SSMA_sp_get_nextval_SEQ_"+id+"] \n"+
				"   @curval OUTPUT\n"+
				"select @curval";
				this.setPreparedStatement(this.getConnection().prepareStatement(str));
				boolean result = this.getPS().execute();
				do{
					if(result){
						this.setResultSet(getPS().getResultSet());
						if(next()){
							retval.add(this.getResultSet().getBigDecimal(1));
						}
					}
					result = this.getPS().getMoreResults();
				}while(!(result == false && this.getPS().getUpdateCount() == -1));

			}

		}.run(DatabaseImpl.getConnectionManager(db));
		return retval.size() > 0 ? retval.get(0) : null;
	}
}
