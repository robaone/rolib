package com.robaone.api.data;

import java.io.InputStream;
import java.io.OutputStream;

import com.robaone.dbase.ConnectionBlock;
import com.robaone.dbase.HDBConnectionManager;


abstract public class SQLInputStream extends ConnectionBlock {
	protected OutputStream m_out = null;
	private HDBConnectionManager m_con;
	public SQLInputStream(HDBConnectionManager con){
		this.m_con = con;
	}
	public void CopyStream(OutputStream out) throws Exception {
		this.m_out = out;
		run(m_con);
	}

}
