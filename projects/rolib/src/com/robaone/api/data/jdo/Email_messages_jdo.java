/*
* Created on Jan 13, 2012
* Author Ansel Robateau
* http://www.robaone.com
*
*/
package com.robaone.api.data.jdo;

import java.math.BigDecimal;
import java.util.Date;
import com.robaone.jdo.RO_JDO;


public class Email_messages_jdo extends RO_JDO{
  public final static String MESSAGEID = "MESSAGEID";
  public final static String SUBJECT = "SUBJECT";
  public final static String FROM_ADDRESS = "FROM_ADDRESS";
  public final static String FROM_NAME = "FROM_NAME";
  public final static String CONTENT_TYPE = "CONTENT_TYPE";
  public final static String BODY = "BODY";
  public final static String SENT_DATE = "SENT_DATE";
  public final static String LOCK = "LOCK";
  public final static String SENT_BY = "SENT_BY";
  public final static String BATCHID = "BATCHID";
  public final static String REPLY_ADDRESS = "REPLY_ADDRESS";
  public final static String REPLY_NAME = "REPLY_NAME";
  public final static String RETURNED_MSG = "RETURNED_MSG";
  public final static String RETURNED_DATE = "RETURNED_DATE";
  protected Email_messages_jdo(){
    
  }
  protected void setMessageid(Integer messageid){
    this.setField(MESSAGEID,messageid);
  }
  public final Integer getMessageid(){
    Object[] val = this.getField(MESSAGEID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setSubject(String subject){
    this.setField(SUBJECT,subject);
  }
  public String getSubject(){
    Object[] val = this.getField(SUBJECT);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setFrom_address(String from_address){
    this.setField(FROM_ADDRESS,from_address);
  }
  public String getFrom_address(){
    Object[] val = this.getField(FROM_ADDRESS);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setFrom_name(String from_name){
    this.setField(FROM_NAME,from_name);
  }
  public String getFrom_name(){
    Object[] val = this.getField(FROM_NAME);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setContent_type(String content_type){
    this.setField(CONTENT_TYPE,content_type);
  }
  public String getContent_type(){
    Object[] val = this.getField(CONTENT_TYPE);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setBody(String body){
    this.setField(BODY,body);
  }
  public String getBody(){
    Object[] val = this.getField(BODY);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setSent_date(java.sql.Timestamp sent_date){
    this.setField(SENT_DATE,sent_date);
  }
  public java.sql.Timestamp getSent_date(){
    Object[] val = this.getField(SENT_DATE);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public void setLock(Integer lock){
    this.setField(LOCK,lock);
  }
  public Integer getLock(){
    Object[] val = this.getField(LOCK);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setSent_by(String sent_by){
    this.setField(SENT_BY,sent_by);
  }
  public String getSent_by(){
    Object[] val = this.getField(SENT_BY);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setBatchid(Integer batchid){
    this.setField(BATCHID,batchid);
  }
  public Integer getBatchid(){
    Object[] val = this.getField(BATCHID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setReply_address(String reply_address){
    this.setField(REPLY_ADDRESS,reply_address);
  }
  public String getReply_address(){
    Object[] val = this.getField(REPLY_ADDRESS);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setReply_name(String reply_name){
    this.setField(REPLY_NAME,reply_name);
  }
  public String getReply_name(){
    Object[] val = this.getField(REPLY_NAME);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setReturned_msg(String returned_msg){
    this.setField(RETURNED_MSG,returned_msg);
  }
  public String getReturned_msg(){
    Object[] val = this.getField(RETURNED_MSG);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setReturned_date(java.sql.Timestamp returned_date){
    this.setField(RETURNED_DATE,returned_date);
  }
  public java.sql.Timestamp getReturned_date(){
    Object[] val = this.getField(RETURNED_DATE);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public String getIdentityName() {
    return "MESSAGEID";
  }
}