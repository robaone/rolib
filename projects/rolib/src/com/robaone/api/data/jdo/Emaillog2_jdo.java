/*
* Created on Jan 13, 2012
* Author Ansel Robateau
* http://www.robaone.com
*
*/
package com.robaone.api.data.jdo;

import java.math.BigDecimal;
import java.util.Date;
import com.robaone.jdo.RO_JDO;


public class Emaillog2_jdo extends RO_JDO{
  public final static String EMAILID = "EMAILID";
  public final static String FILENAME = "FILENAME";
  public final static String DOMAIN = "DOMAIN";
  public final static String STATUS = "STATUS";
  public final static String SENDTIME = "SENDTIME";
  public final static String CU_ID = "CU_ID";
  public final static String OBFUSCATED = "OBFUSCATED";
  public final static String EMAIL_ADDRESSES = "EMAIL_ADDRESSES";
  public final static String SUBJECT = "SUBJECT";
  public final static String RETURNED_MSG = "RETURNED_MSG";
  public final static String RETURNED_DATE = "RETURNED_DATE";
  protected Emaillog2_jdo(){
    
  }
  protected void setEmailid(Integer emailid){
    this.setField(EMAILID,emailid);
  }
  public final Integer getEmailid(){
    Object[] val = this.getField(EMAILID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setFilename(String filename){
    this.setField(FILENAME,filename);
  }
  public String getFilename(){
    Object[] val = this.getField(FILENAME);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setDomain(String domain){
    this.setField(DOMAIN,domain);
  }
  public String getDomain(){
    Object[] val = this.getField(DOMAIN);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setStatus(String status){
    this.setField(STATUS,status);
  }
  public String getStatus(){
    Object[] val = this.getField(STATUS);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setSendtime(java.sql.Timestamp sendtime){
    this.setField(SENDTIME,sendtime);
  }
  public java.sql.Timestamp getSendtime(){
    Object[] val = this.getField(SENDTIME);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public void setCu_id(String cu_id){
    this.setField(CU_ID,cu_id);
  }
  public String getCu_id(){
    Object[] val = this.getField(CU_ID);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setObfuscated(String obfuscated){
    this.setField(OBFUSCATED,obfuscated);
  }
  public String getObfuscated(){
    Object[] val = this.getField(OBFUSCATED);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setEmail_addresses(String email_addresses){
    this.setField(EMAIL_ADDRESSES,email_addresses);
  }
  public String getEmail_addresses(){
    Object[] val = this.getField(EMAIL_ADDRESSES);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setSubject(String subject){
    this.setField(SUBJECT,subject);
  }
  public String getSubject(){
    Object[] val = this.getField(SUBJECT);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setReturned_msg(String returned_msg){
    this.setField(RETURNED_MSG,returned_msg);
  }
  public String getReturned_msg(){
    Object[] val = this.getField(RETURNED_MSG);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setReturned_date(java.sql.Timestamp returned_date){
    this.setField(RETURNED_DATE,returned_date);
  }
  public java.sql.Timestamp getReturned_date(){
    Object[] val = this.getField(RETURNED_DATE);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public String getIdentityName() {
    return "EMAILID";
  }
}