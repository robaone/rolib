/*
* Created on Jan 13, 2012
*
* Author: Ansel Robateau
*         http://www.robaone.com
*/
package com.robaone.api.data.jdo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import com.robaone.jdo.*;
import org.json.*;
import java.util.HashMap;
import java.util.Iterator;

public class Emaillog2_jdoManager {
  private Connection m_con;
  private String m_options = "";
  private final static String SELECT = "select ~ from #TABLE# #OPTION#  where emailid = ?";
  private final static String INSERT = "insert into #TABLE# ";
  private final static String QUERY = "select ~ from #TABLE# #OPTION#  where ";
  private final static String UPDATE = "update #TABLE# set ";
  private final static String SEARCH = "select COUNT(1) from #TABLE# #OPTION# where #TABLE#.EMAILID = ?";
  private final static String DELETE = "delete from #TABLE# where #TABLE#.EMAILID = ?";
  private final static String IDENTITY = "EMAILID";
  private RO_JDO_IdentityManager<BigDecimal> NEXT_SQL;
  public final static String FIELDS = "#TABLE#.EMAILID,#TABLE#.FILENAME,#TABLE#.DOMAIN,#TABLE#.STATUS,#TABLE#.SENDTIME,#TABLE#.CU_ID,#TABLE#.OBFUSCATED,#TABLE#.EMAIL_ADDRESSES,#TABLE#.SUBJECT,#TABLE#.RETURNED_MSG,#TABLE#.RETURNED_DATE";
  private String TABLE = "EMAILLOG2";
  protected boolean debug = false;
  public Emaillog2_jdoManager(Connection con){
    this.m_con = con;
    this.setIdentityClass();
    try{
    	if(System.getProperty("debug").equals("Y")){
    		debug = true;
    	}
    }catch(Exception e){}
  }
  protected void setIdentityClass(){
     this.NEXT_SQL = new RO_JDO_SQLServer<BigDecimal>();
  }
  protected Connection getConnection(){
    return this.m_con;
  }
  public String getTableName(){
    return TABLE;
  }
  public void setTableName(String tablename){
    TABLE = tablename;
  }
  public Emaillog2_jdo bindEmaillog2(ResultSet rs) throws SQLException{
Emaillog2_jdo retval = null;
    retval = Emaillog2_jdoManager.createObject(rs);
    return retval;
  }

  public Emaillog2_jdo getEmaillog2(Integer emailid){
    PreparedStatement ps = null;
    ResultSet rs = null;
    Emaillog2_jdo retval = null;
    try{
      String sql = this.getSQL(SELECT.split("[~]")[0]+FIELDS+SELECT.split("[~]")[1]);
      ps = this.getConnection().prepareStatement(sql);
      ps.setObject(1,emailid);
      rs = ps.executeQuery();
      if(rs.next()){
        retval = Emaillog2_jdoManager.createObject(rs);
      }
    }catch(Exception e){

    }finally{
      try{rs.close();}catch(Exception e){}
      try{ps.close();}catch(Exception e){}
    }
    return retval;
  }
/**
* @param rs
* @return
* @throws SQLException
*/
  private static Emaillog2_jdo createObject(ResultSet rs) throws SQLException {
    Emaillog2_jdo retval = null;
    retval = new Emaillog2_jdo();
    /*
     *
     * Insert values from Result Set into object
     */
    ResultSetMetaData mdata = rs.getMetaData();
    for(int i = 0;i< mdata.getColumnCount();i++){
      String fieldname = mdata.getColumnName(i+1).toUpperCase();
      Object[] val = new Object[2];
      val[1] = new Boolean(false);
      // Set the value
      		val[0] = rs.getObject(i+1);
		if(val[0] instanceof java.sql.Date){
			val[0] = new java.util.Date(rs.getTimestamp(i+1).getTime());
		}
      retval.bindField(fieldname,val);
    }

    return retval;
  }
  private void setTableOptions(String str){
    this.m_options = str;
  }
  private String getTableOptions(){
    return this.m_options;
  }
  public void save(Emaillog2_jdo record) throws Exception {
    this.save(record,false);
  }
  public void save(Emaillog2_jdo record,boolean dirty) throws Exception {
    Connection con = this.getConnection();
    if(dirty){
      this.setTableOptions("with (nolock)");
    }
    boolean finished = false;
    if(record.getDirtyFieldCount() == 0){
      return;
    }
    try{
      String search = this.getSQL(SEARCH);
      int count = 0;
      if(record.getField(record.getIdentityName()) != null  && record.getField(record.getIdentityName())[0] != null){
        PreparedStatement ps = con.prepareStatement(search);
        ps.setObject(1,record.getField(record.getIdentityName())[0]);
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
          count = rs.getInt(1);
        }
        rs.close();
        ps.close();
      }
      if(count > 0){
      // Update
        if(debug) System.out.println("Updating...");
        this.handleBeforeUpdate(record);
        String update_sql = this.getSQL(UPDATE);
        int dirtyfieldcount = 0;
        for(int i = 0; i < record.getDirtyFieldCount();i++){
          String fieldname = record.getDirtyField(i);
          if(i > 0){
            update_sql += ", ";
          }
          update_sql += fieldname +" = ?";
          dirtyfieldcount ++;
        }
        update_sql += " where "+record.getIdentityName()+" = ?";
        PreparedStatement update_ps = con.prepareStatement(update_sql);

        for(int i = 0; i < record.getDirtyFieldCount();i++){
          Object value = record.getField(record.getDirtyField(i))[0];
          if(value instanceof java.sql.Timestamp){
            update_ps.setObject(i+1,value);
          }else if(value instanceof java.sql.Date){
            update_ps.setObject(i+1,new java.sql.Timestamp(((java.sql.Date)value).getTime()));
          }else if(value instanceof java.util.Date){
            value = new java.sql.Timestamp(((java.util.Date)value).getTime());
            update_ps.setObject(i+1,value);
          }else{
            update_ps.setObject(i+1,value);
          }
        }
        update_ps.setObject(dirtyfieldcount+1,record.getField(record.getIdentityName())[0]);
        int updated = update_ps.executeUpdate();
        finished = true;
        if(updated == 0){
          throw new Exception("No rows updated.");
        }
        if(debug) System.out.println(updated +" rows updated.");
        this.handleAfterUpdate(record);
        /**
         * Mark all fields as clean
         */
        this.setAllClean(record);
        update_ps.close();
      }else{
        // Insert
        if(debug) System.out.println("Inserting...");
        String insert_sql = this.getSQL(INSERT);
        String insert_pre,insert_post;
        insert_pre = "("; insert_post = "(";
        for(int i = 0;i < record.getFieldCount();i++){
          String fieldname = record.getField(i);
          if(fieldname.equalsIgnoreCase(record.getIdentityName())){
            continue;
          }
          if(i > 0 && insert_pre.length() > 1 && insert_post.length() > 1){
            insert_pre += ",";
            insert_post += ",";
          }
          insert_pre += fieldname;
          insert_post += "?";
        }
        insert_pre += ") values ";
        insert_post += ")";
        insert_sql += insert_pre + insert_post;
        PreparedStatement insert_ps = con.prepareStatement(insert_sql);
        int field_index = 1;
        for(int i = 0; i < record.getFieldCount();i++){
          String fieldname = record.getField(i);
          if(fieldname.equalsIgnoreCase(record.getIdentityName())){
            continue;
          }
          Object[] val = record.getField(fieldname);
          if(val[0] instanceof java.sql.Timestamp){
            insert_ps.setObject(field_index,val[0]);
          }else if(val[0] instanceof java.sql.Date){
            insert_ps.setObject(field_index,new java.sql.Timestamp(((java.sql.Date)val[0]).getTime()));
          }else
            if(val[0] instanceof java.util.Date){
              val[0] = new java.sql.Date(((java.util.Date)val[0]).getTime());
              insert_ps.setObject(field_index,val[0]);
          }else{
            insert_ps.setObject(field_index,val[0]);
          }
          field_index ++;
        }
        int updated = insert_ps.executeUpdate();
        record.setEmailid(new Integer(NEXT_SQL.getIdentity(this.getTableName(),this.m_con).toString()));
        finished = true;
        if(updated == 0){
          throw new Exception("No rows added.");
        }else{
          this.handleAfterInsert(record);
          this.setAllClean(record);
        }
        if(debug) System.out.println(updated+" rows added.");
        insert_ps.close();
      }
    }finally{}
  }
	protected void handleAfterInsert(Emaillog2_jdo record) {}
	protected void handleAfterUpdate(Emaillog2_jdo record) {}
	protected void handleBeforeUpdate(Emaillog2_jdo record) {}
  private void setAllClean(Emaillog2_jdo record){
    try{
      for(int i = 0; i < record.getFieldCount();i++){
         String fieldname = record.getField(i);
         Object[] val = record.getField(fieldname);
         if(val != null)
           val[1] = new Boolean(false);
      }
    }catch(Exception e){}
  }
  public void delete(Emaillog2_jdo record) throws Exception {
    Connection con = this.getConnection();
    String sql_delete = this.getSQL(DELETE);
    PreparedStatement ps = con.prepareStatement(sql_delete);
    ps.setObject(1, record.getField(IDENTITY)[0]);
    int updated = ps.executeUpdate();
    if(debug) System.out.println(updated +" records deleted.");
  }
  public PreparedStatement prepareStatement(String query) throws SQLException{
    String sql = this.getSQL(QUERY.split("[~]")[0]+FIELDS+QUERY.split("[~]")[1] + query);
    if(debug) System.out.println(sql);
    PreparedStatement ps = this.getConnection().prepareStatement(sql);
    return ps;
  }
public Emaillog2_jdo newEmaillog2() {
Emaillog2_jdo retval = new Emaillog2_jdo();
 retval.setEmailid(null);
 return retval;
}
  public String getSQL(String sql){
    String retval = "";
    retval = sql.replaceAll("#TABLE#",TABLE);
    retval = retval.replaceAll("#OPTION#",this.getTableOptions());
    return retval;
  }
  public JSONObject toJSONObject(Emaillog2_jdo record) throws Exception {
    JSONObject retval = null;
    if(record != null){
      JSONObject object = new JSONObject();
      for(int i = 0; i < record.getFieldCount(); i++){
        String fieldname = record.getField(i);
        Object value = record.getField(fieldname)[0];
        object.put(fieldname.toLowerCase(), value);
      }
      retval = object;
    }
    return retval;
  }
  public void bindEmaillog2JSON(Emaillog2_jdo record,String jsondata) throws Exception {
    JSONObject jo = new JSONObject(jsondata);
    bindEmaillog2JSON(record,jo);
  }
  public void bindEmaillog2JSON(Emaillog2_jdo record, JSONObject jo) throws Exception {
    Iterator keys = jo.keys();
    HashMap keymap = new HashMap();
    while(keys.hasNext()){
      String key = keys.next().toString();
      String lc_key = key.toUpperCase();
      keymap.put(lc_key, key);
    }
    if(record != null && jo != null){
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.EMAILID))){
          if(keymap.get(Emaillog2_jdo.EMAILID) != null)
            record.setEmailid(null);
        }else{
             record.setEmailid(new Integer(jo.getInt((String)keymap.get(Emaillog2_jdo.EMAILID))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.FILENAME))){
          if(keymap.get(Emaillog2_jdo.FILENAME) != null)
            record.setFilename(null);
        }else{
             record.setFilename(new String(jo.getString((String)keymap.get(Emaillog2_jdo.FILENAME))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.DOMAIN))){
          if(keymap.get(Emaillog2_jdo.DOMAIN) != null)
            record.setDomain(null);
        }else{
             record.setDomain(new String(jo.getString((String)keymap.get(Emaillog2_jdo.DOMAIN))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.STATUS))){
          if(keymap.get(Emaillog2_jdo.STATUS) != null)
            record.setStatus(null);
        }else{
             record.setStatus(new String(jo.getString((String)keymap.get(Emaillog2_jdo.STATUS))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.SENDTIME))){
          if(keymap.get(Emaillog2_jdo.SENDTIME) != null)
            record.setSendtime(null);
        }else{
          try{
             record.setSendtime(new java.sql.Timestamp(jo.getLong((String)keymap.get(Emaillog2_jdo.SENDTIME))));
          }catch(JSONException e){
            java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd kk:mm:ss.S");
            long time = df.parse(jo.getString((String)keymap.get(Emaillog2_jdo.SENDTIME))).getTime();
            record.setSendtime(new java.sql.Timestamp(time));
          }
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.CU_ID))){
          if(keymap.get(Emaillog2_jdo.CU_ID) != null)
            record.setCu_id(null);
        }else{
             record.setCu_id(new String(jo.getString((String)keymap.get(Emaillog2_jdo.CU_ID))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.OBFUSCATED))){
          if(keymap.get(Emaillog2_jdo.OBFUSCATED) != null)
            record.setObfuscated(null);
        }else{
             record.setObfuscated(new String(jo.getString((String)keymap.get(Emaillog2_jdo.OBFUSCATED))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.EMAIL_ADDRESSES))){
          if(keymap.get(Emaillog2_jdo.EMAIL_ADDRESSES) != null)
            record.setEmail_addresses(null);
        }else{
             record.setEmail_addresses(new String(jo.getString((String)keymap.get(Emaillog2_jdo.EMAIL_ADDRESSES))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.SUBJECT))){
          if(keymap.get(Emaillog2_jdo.SUBJECT) != null)
            record.setSubject(null);
        }else{
             record.setSubject(new String(jo.getString((String)keymap.get(Emaillog2_jdo.SUBJECT))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.RETURNED_MSG))){
          if(keymap.get(Emaillog2_jdo.RETURNED_MSG) != null)
            record.setReturned_msg(null);
        }else{
             record.setReturned_msg(new String(jo.getString((String)keymap.get(Emaillog2_jdo.RETURNED_MSG))));
        }
      }catch(org.json.JSONException e){
      }
      try{
        if(jo.isNull((String)keymap.get(Emaillog2_jdo.RETURNED_DATE))){
          if(keymap.get(Emaillog2_jdo.RETURNED_DATE) != null)
            record.setReturned_date(null);
        }else{
          try{
             record.setReturned_date(new java.sql.Timestamp(jo.getLong((String)keymap.get(Emaillog2_jdo.RETURNED_DATE))));
          }catch(JSONException e){
            java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd kk:mm:ss.S");
            long time = df.parse(jo.getString((String)keymap.get(Emaillog2_jdo.RETURNED_DATE))).getTime();
            record.setReturned_date(new java.sql.Timestamp(time));
          }
        }
      }catch(org.json.JSONException e){
      }
    }
  }
	public Emaillog2_jdo bindEmaillog2JSON(String jsondata) throws Exception {
		Emaillog2_jdo retval = null;
		JSONObject jo = new JSONObject(jsondata);
		retval = this.bindEmaillog2JSON(jo);
		return retval;
	}
	private Emaillog2_jdo bindEmaillog2JSON(JSONObject jo) throws Exception{
		Iterator keys = jo.keys();
		HashMap keymap = new HashMap();
		while(keys.hasNext()){
			String key = keys.next().toString();
			String lc_key = key.toUpperCase();
			keymap.put(lc_key, key);
		}
		Emaillog2_jdo record = null;
		try{
			if(!jo.isNull((String)keymap.get(IDENTITY))){
			record = this.getEmaillog2(new Integer(jo.getInt((String)keymap.get(IDENTITY))));
			}
		}catch(JSONException e){}
		if(record == null){
			record = this.newEmaillog2();
		}
		bindEmaillog2JSON(record, jo);
		return record;
	}
}
