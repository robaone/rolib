/*
* Created on Jan 13, 2012
* Author Ansel Robateau
* http://www.robaone.com
*
*/
package com.robaone.api.data.jdo;

import java.math.BigDecimal;
import java.util.Date;
import com.robaone.jdo.RO_JDO;


public class Email_message_batches_jdo extends RO_JDO{
  public final static String BATCHID = "BATCHID";
  public final static String DESCRIPTION = "DESCRIPTION";
  public final static String CLIENTID = "CLIENTID";
  public final static String CREATION_DATE = "CREATION_DATE";
  public final static String PROOF_RUN = "PROOF_RUN";
  public final static String PROOF_ADDRESS = "PROOF_ADDRESS";
  public final static String PROOF_SENT = "PROOF_SENT";
  public final static String BATCH_SENT = "BATCH_SENT";
  public final static String PROCESSID = "PROCESSID";
  public final static String PROCESS_START = "PROCESS_START";
  public final static String STATUS = "STATUS";
  public final static String MESSAGE = "MESSAGE";
  protected Email_message_batches_jdo(){
    
  }
  protected void setBatchid(Integer batchid){
    this.setField(BATCHID,batchid);
  }
  public final Integer getBatchid(){
    Object[] val = this.getField(BATCHID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setDescription(String description){
    this.setField(DESCRIPTION,description);
  }
  public String getDescription(){
    Object[] val = this.getField(DESCRIPTION);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setClientid(Integer clientid){
    this.setField(CLIENTID,clientid);
  }
  public Integer getClientid(){
    Object[] val = this.getField(CLIENTID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setCreation_date(java.sql.Timestamp creation_date){
    this.setField(CREATION_DATE,creation_date);
  }
  public java.sql.Timestamp getCreation_date(){
    Object[] val = this.getField(CREATION_DATE);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public void setProof_run(Integer proof_run){
    this.setField(PROOF_RUN,proof_run);
  }
  public Integer getProof_run(){
    Object[] val = this.getField(PROOF_RUN);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setProof_address(String proof_address){
    this.setField(PROOF_ADDRESS,proof_address);
  }
  public String getProof_address(){
    Object[] val = this.getField(PROOF_ADDRESS);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setProof_sent(java.sql.Timestamp proof_sent){
    this.setField(PROOF_SENT,proof_sent);
  }
  public java.sql.Timestamp getProof_sent(){
    Object[] val = this.getField(PROOF_SENT);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public void setBatch_sent(java.sql.Timestamp batch_sent){
    this.setField(BATCH_SENT,batch_sent);
  }
  public java.sql.Timestamp getBatch_sent(){
    Object[] val = this.getField(BATCH_SENT);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public void setProcessid(BigDecimal processid){
    this.setField(PROCESSID,processid);
  }
  public BigDecimal getProcessid(){
    Object[] val = this.getField(PROCESSID);
    if(val != null && val[0] != null){
      return (BigDecimal)val[0];
    }else{
      return null;
    }
  }
  public void setProcess_start(java.sql.Timestamp process_start){
    this.setField(PROCESS_START,process_start);
  }
  public java.sql.Timestamp getProcess_start(){
    Object[] val = this.getField(PROCESS_START);
    if(val != null && val[0] != null){
      return (java.sql.Timestamp)val[0];
    }else{
      return null;
    }
  }
  public void setStatus(Integer status){
    this.setField(STATUS,status);
  }
  public Integer getStatus(){
    Object[] val = this.getField(STATUS);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setMessage(String message){
    this.setField(MESSAGE,message);
  }
  public String getMessage(){
    Object[] val = this.getField(MESSAGE);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public String getIdentityName() {
    return "BATCHID";
  }
}