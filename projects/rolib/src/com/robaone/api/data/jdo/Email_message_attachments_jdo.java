/*
* Created on Jan 13, 2012
* Author Ansel Robateau
* http://www.robaone.com
*
*/
package com.robaone.api.data.jdo;

import java.math.BigDecimal;
import java.util.Date;
import com.robaone.jdo.RO_JDO;


public class Email_message_attachments_jdo extends RO_JDO{
  public final static String ID = "ID";
  public final static String CONTENTID = "CONTENTID";
  public final static String FILENAME = "FILENAME";
  public final static String FILE_CONTENT = "FILE_CONTENT";
  public final static String MESSAGEID = "MESSAGEID";
  protected Email_message_attachments_jdo(){
    
  }
  protected void setId(Integer id){
    this.setField(ID,id);
  }
  public final Integer getId(){
    Object[] val = this.getField(ID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public void setContentid(String contentid){
    this.setField(CONTENTID,contentid);
  }
  public String getContentid(){
    Object[] val = this.getField(CONTENTID);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setFilename(String filename){
    this.setField(FILENAME,filename);
  }
  public String getFilename(){
    Object[] val = this.getField(FILENAME);
    if(val != null && val[0] != null){
      return (String)val[0];
    }else{
      return null;
    }
  }
  public void setFile_content(byte[] file_content){
    this.setField(FILE_CONTENT,file_content);
  }
  public byte[] getFile_content(){
    Object[] val = this.getField(FILE_CONTENT);
    if(val != null && val[0] != null){
      return (byte[])val[0];
    }else{
      return null;
    }
  }
  public void setMessageid(Integer messageid){
    this.setField(MESSAGEID,messageid);
  }
  public Integer getMessageid(){
    Object[] val = this.getField(MESSAGEID);
    if(val != null && val[0] != null){
      if(val[0] instanceof java.lang.Short){
        return new Integer(((java.lang.Short)val[0]).toString());
      }else{
        return (Integer)val[0];
      }
    }else{
      return null;
    }
  }
  public String getIdentityName() {
    return "ID";
  }
}