package com.robaone.api.data;

public interface UserManagerInterface {
	public void setUserByID(String id) throws Exception;
	public void setUserByID(Integer id) throws Exception;
	public Integer getIduser();
	public String getUserId();
	public boolean checkPassword(String password);
}
