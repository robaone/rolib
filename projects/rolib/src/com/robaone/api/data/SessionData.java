package com.robaone.api.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.NamingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthServiceProvider;
import net.oauth.client.OAuthClient;
import net.oauth.client.URLConnectionClient;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;

import com.robaone.api.business.ApiCall;

public class SessionData {

	public static final String ACCESSURL = "accessUrl";
	private static final String USERMANAGERCLASS = "user.manager.class";
	private UserManagerInterface user;
	private String remotehost;
	private AppCredentialsInterface credentials;
	private String session_token;
	private String authenticateurl;
	private String accesstoken;
	private String tokenSecret;
	private String requesttoken;
	private boolean m_debugging;
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public SessionData() throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException{
		String class_name = AppDatabase.getProperty(USERMANAGERCLASS);
		if(class_name == null){
			class_name = "com.robaone.api.business.DefaultUserManager";
		}
		Class myClass = Class.forName(class_name);
		Class[] params = new Class[]{};
		Constructor constr = myClass.getConstructor(params);
		Object o = constr.newInstance(new Object[]{});
		user = (UserManagerInterface)o;
	}
	public SessionData(UserManagerInterface manager){
		this.user = manager;
	}
	public void setUser(UserManagerInterface user_object) {
		this.user = user_object;
	}

	public UserManagerInterface getUser() {
		return this.user;
	}

	public String getRemoteHost() {
		return this.remotehost;
	}

	public void setRemoteHost(String host){
		this.remotehost = host;
	}

	public void setCredentials(AppCredentialsInterface cred) {
		this.credentials = cred;
		this.setSession_token(cred.getAccess_token());
	}
	
	public AppCredentialsInterface getCredentials(){
		return this.credentials;
	}

	public String getSession_token() {
		return session_token;
	}

	public void setSession_token(String session_token) {
		this.session_token = session_token;
	}

	public Document callURL(String url, String data) throws Exception {
		URL u = new URL(url);
		URLConnection con = u.openConnection();
		con.getContentType();
		int content_length = con.getContentLength();
		if(content_length > 5000){
			throw new Exception("Data size is too large");
		}
		InputStream in = con.getInputStream();
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		IOUtils.copy(in, bout);
		String str = bout.toString();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		File debug_output_file = new File(AppDatabase.getProperty("temp.folder") + System.getProperty("file.separator") +
				ApiCall.clean(url) + "_" + new java.util.Date().getTime() + ".xml");
		FileOutputStream fout = null;
		try{
			fout = new FileOutputStream(debug_output_file);
			AppDatabase.writeLog("00094: Writing xml return value to "+debug_output_file.getAbsolutePath());
			if(str.startsWith("<?")){
				Document doc = builder.parse(new ByteArrayInputStream(str.getBytes()));
				IOUtils.write(ApiCall.toXML(doc), fout);
				return doc;
			}else if(str.startsWith("{")){
				String xml = XML.toString(new JSONObject(str),"url");
				Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes()));
				IOUtils.write(ApiCall.toXML(doc), fout);
				return doc;
			}else{
				/* Try html processing */
				Tidy tidy = new Tidy();
				Document doc = tidy.parseDOM(new ByteArrayInputStream(str.getBytes()), fout);
				return doc;
			}
		}finally{
			try{fout.close();}catch(Exception e){}
		}
	}

	public OAuthMessage executeAPI(String function, String data) throws NamingException, Exception {
		return this.execute(AppDatabase.getProperty("api.url"),function,data);
	}
	private OAuthMessage execute(String operation,String action,String jsondata) throws NamingException, Exception
	{
		if ("request".equals(operation)){
			OAuthAccessor accessor = createOAuthAccessor();
			OAuthClient client = new OAuthClient(new URLConnectionClient());
			client.getRequestToken(accessor);
			this.setRequestToken(accessor.requestToken);
			this.setTokenSecret(accessor.tokenSecret);

			AppDatabase.writeLog("00046: Last action: added requestToken");
		}
		else if ("access".equals(operation))
		{
			Properties paramProps = new Properties();
			paramProps.setProperty("oauth_token",
					this.getRequestToken());

			OAuthMessage response 
			= sendRequest(paramProps, this.getAccessUrl());

			this.setAccessToken(
					response.getParameter("oauth_token"));
			this.setTokenSecret( 
					response.getParameter("oauth_token_secret"));
			this.setUserId(response.getParameter("user_id"));


			AppDatabase.writeLog("00047: Last action: added accessToken");
		}
		else if ("authorize".equals(operation))
		{
			// just print the redirect
			Properties paramProps = new Properties();
			paramProps.setProperty("oauth_token",
					this.getRequestToken());
			//paramProps.setProperty("oauth_callback",this.getCallbackUrl());

			OAuthAccessor accessor = createOAuthAccessor();

			OAuthMessage response = sendGETRequest(paramProps,
					accessor.consumer.serviceProvider.userAuthorizationURL);

			AppDatabase.writeLog("00048: Paste this in a browser:");
			AppDatabase.writeLog("00049: "+response.URL);
			this.authenticateurl = response.URL;
		} else {
			// access the resource
			Properties paramProps = new Properties();
			paramProps.setProperty("oauth_token",
					this.getAccessToken());
			paramProps.setProperty("action", action);
			paramProps.setProperty("data", jsondata);
			OAuthMessage response = sendRequest(paramProps, operation);
			return response;
		}
		return null;
	}
	public void setUserId(String parameter) throws NamingException, Exception {
		this.user.setUserByID(parameter);
	}

	public String getAccessToken() {
		return this.accesstoken;
	}

	@SuppressWarnings("rawtypes")
	private OAuthMessage sendGETRequest(Map map, String url) throws IOException,
	URISyntaxException, OAuthException
	{
		List<Map.Entry> params = new ArrayList<Map.Entry>();
		Iterator it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry p = (Map.Entry) it.next();
			params.add(new OAuth.Parameter((String)p.getKey(),
					(String)p.getValue()));
		}
		OAuthAccessor accessor = createOAuthAccessor();
		accessor.tokenSecret = this.getTokenSecret();
		OAuthClient client = new OAuthClient(new URLConnectionClient());
		return client.invoke(accessor, "GET",  url, params);
	}

	public String getTokenSecret() {
		return this.tokenSecret;
	}
	@SuppressWarnings("rawtypes")
	private OAuthMessage sendRequest(Map map, String url) throws IOException,
	URISyntaxException, OAuthException
	{
		List<Map.Entry> params = new ArrayList<Map.Entry>();
		Iterator it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry p = (Map.Entry) it.next();
			params.add(new OAuth.Parameter((String)p.getKey(),
					(String)p.getValue()));
		}
		OAuthAccessor accessor = createOAuthAccessor();
		accessor.tokenSecret = this.getTokenSecret();
		OAuthClient client = new OAuthClient(new URLConnectionClient());
		return client.invoke(accessor, "POST",  url, params);
	}
	public String getAccessUrl() {
		return AppDatabase.getProperty(ACCESSURL);
	}

	public String getRequestToken() {
		return this.requesttoken;
	}

	private OAuthAccessor createOAuthAccessor() {
		String consumerKey = AppDatabase.getProperty("consumerKey");
		String callbackUrl = AppDatabase.getProperty("callbackUrl");
		String consumerSecret = AppDatabase.getProperty("consumerSecret");

		String reqUrl = AppDatabase.getProperty("requestUrl");
		String authzUrl = AppDatabase.getProperty("authorizationUrl");
		String accessUrl = AppDatabase.getProperty("accessUrl");

		OAuthServiceProvider provider
		= new OAuthServiceProvider(reqUrl, authzUrl, accessUrl);
		OAuthConsumer consumer
		= new OAuthConsumer(callbackUrl, consumerKey,
				consumerSecret, provider);
		return new OAuthAccessor(consumer);
	}

	public void setAccessToken(String string) {
		this.accesstoken = string;
	}

	public void setCallbackUrl(String string) {
	}

	public void setRequestToken(String string) {
		this.requesttoken = string;
	}

	public void setTokenSecret(String string) {
		this.tokenSecret = string;
	}

	public boolean isAuthorized() throws Exception {
		if(this.accesstoken != null){
			try {
				OAuthMessage message = this.executeAPI("Login.getProfile", "{}");
				JSONObject jo = new JSONObject(ApiCall.StreamtoString(message.getBodyAsStream()));
				int status = jo.getJSONObject("response").getInt("status");
				String error = jo.getJSONObject("response").getString("error");
				if(status == 3 || (status == 1 && (error.endsWith("permission_denied") || error.endsWith("token_expired")))){
					return false;
				}else{
					return true;
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw e;
			} catch (OAuthException e) {
				e.printStackTrace();
				throw e;
			} catch (URISyntaxException e) {
				e.printStackTrace();
				throw e;
			} catch (JSONException e) {
				e.printStackTrace();
				throw e;
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}else{
			if(this.getUser().getUserId() == null){
				return false;
			}else{
				return true;
			}
		}
	}

	public void executeRequest() throws NamingException, Exception {
		this.execute("request",null, null);
	}

	public void executeAuthorize() throws NamingException, Exception {
		this.execute("authorize",null,null);
	}

	public String getAuthenticateUrl() {
		return this.authenticateurl;
	}
	public boolean isDebugging() {
		return this.m_debugging;
	}
	public void setDebugging(boolean debug){
		this.m_debugging = debug;
	}
}
