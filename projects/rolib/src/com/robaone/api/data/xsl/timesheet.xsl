<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" indent="yes" method="html"
		standalone="no" omit-xml-declaration="yes" />
	<xsl:template match="/">
		<style>
			td{
			 vertical-align:top;
			}
		</style>
		<table width="100%">
			<col width="300px"/>
			<col width="300px"/>
			<thead>
				<tr>
					<th>Date</th>
					<th>WOID</th>
					<th>Hours</th>
					<th>Notes</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="//data/workdate">
					<xsl:variable name="previous_workdate"
						select="(./parent::data/preceding-sibling::data[1])/workdate" />
					<xsl:variable name="current_workdate" select="text()" />
					<xsl:if
						test="string-length($previous_workdate) = 0 or $current_workdate != $previous_workdate">
						<xsl:for-each select="//data[workdate = $current_workdate]/woid">
							<xsl:variable name="previous_woid"
								select="(./parent::data[workdate = $current_workdate]/preceding-sibling::data[workdate = $current_workdate][1])/woid" />
							<xsl:variable name="current_woid" select="text()" />
							<xsl:if
								test="string-length($previous_woid) = 0 or $current_woid != $previous_woid">
								<tr>
									<td>
										<xsl:value-of select="$current_workdate" />
									</td>
									<td>
										<xsl:value-of select="$current_woid" />
									</td>
									<td>
										<xsl:value-of
											select="sum(//data[workdate = $current_workdate and woid = $current_woid]/hours)" />
									</td>
									<td>
										<xsl:for-each
											select="//data[workdate = $current_workdate and woid = $current_woid]/comment">
											<xsl:value-of disable-output-escaping="yes" select="text()" />
											<br />
										</xsl:for-each>
									</td>
								</tr>
							</xsl:if>
						</xsl:for-each>
						<tr style="font-weight:bold;font-size:14pt;color:red">
							<td>Total</td>
							<td>
								<xsl:value-of select="sum(//data[workdate = $current_workdate]/hours)" />
							</td>
						</tr>
					</xsl:if>

				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>