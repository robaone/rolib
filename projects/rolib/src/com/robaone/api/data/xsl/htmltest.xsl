<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="UTF-8" indent="yes" method="html"
		standalone="no" omit-xml-declaration="yes" />
	<xsl:template match="/">
		Hello<xsl:value-of disable-output-escaping="yes" select="'&amp;nbsp;'"/>World
	</xsl:template>
</xsl:stylesheet>