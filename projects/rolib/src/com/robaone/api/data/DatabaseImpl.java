package com.robaone.api.data;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.robaone.dbase.HDBConnectionManager;



public class DatabaseImpl{
	public static final String INSUFFICIENT_RIGHTS_MSG = "You do not have access rights to perform this action";
	public static final String NOT_IMPLEMENTED_MSG = "Not Yet Implemented";
	public static final String SQLSERVERDEV = "eStmtDev";
	public static final String ORACLESERVERDEV = "MICRODEV11G";
	public static final String SQLSERVERPROD = "eStmtProd";
	public static final String ORACLESERVERPROD = "MICROPROD11G";
	public static final String MCP = "MCP";
	public static final String MICROD = "MICROD";
	public static final String SQLDBA = "master";
	public DatabaseImpl(){
		
	}

	public void writeLog(String string) {
		AppDatabase.writeLog(string);
	}


	public static HDBConnectionManager getConnectionManager(String connection_name) throws NamingException {
		// Obtain our environment naming context
		AppDatabase.writeLog(" -- getConnectionManager('"+connection_name+"')");
		Context initCtx = new InitialContext();
		Context envCtx = (Context) initCtx.lookup("java:comp/env");

		// Look up our data source
		final DataSource ds = (DataSource)envCtx.lookup("jdbc/"+connection_name);
		return new HDBConnectionManager(){

			@Override
			public Connection getConnection() throws Exception {
				// Allocate and use a connection from the pool
				Connection conn = ds.getConnection();
				return conn;
			}

			@Override
			public void closeConnection(Connection m_con) throws Exception {
				m_con.close();
			}
			
		};
	}

}
