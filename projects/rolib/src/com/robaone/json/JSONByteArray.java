package com.robaone.json;

import java.io.UnsupportedEncodingException;

import org.json.JSONArray;
import org.json.JSONException;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class JSONByteArray {
	byte[] m_bytes;
	public JSONByteArray(JSONArray array) throws JSONException{
		m_bytes = new byte[array.length()];
		for(int i = 0; i < array.length();i++){
			m_bytes[i] = (byte)array.getInt(i);
		}
	}
	public byte[] getBytes(){
		return this.m_bytes;
	}
	public String toString(){
		try {
			return new String(this.m_bytes,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			return this.toString();
		}
	}
}