package com.robaone.gwt.form.client;

import java.util.HashMap;
import java.util.Vector;

import com.robaone.gwt.form.client.table.ui.DataTableUi;
import com.robaone.gwt.form.client.ui.FormUi;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FormManager implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	/*
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	 */
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		loadForms();
		loadDataGrids();
		loadInlineFrame();
		createTestForm();
	}
	private void createTestForm() {
		if(RootPanel.get("test_form_manager_2323") != null){
			final FormUi form = new FormUi();
			form.getForm().setAction("test");
			form.getForm().setMethod("get");
			form.setOnSuccess("http://www.google.com");
			RootPanel.get().add(form);
			form.setTitle("Example Form");
			form.setDescription("This is an example form.  It is the first form that I have ever dynamically created.");
			Vector<HashMap<String,String[]>> fields = new Vector<HashMap<String,String[]>>();
			String[][] first_name = {{FormUi.TITLE,"First Name"},{FormUi.NAME,"first_name"},{FormUi.TYPE,FormUi.TYPES.Text.toString()},{FormUi.DESCRIPTION,"Type your first name"},{FormUi.REQUIRED,"true"},{FormUi.HELP,"<h3>Help</h3><p>This is help information on this field</p>"}};
			String[][] last_name = {{FormUi.TITLE,"Last Name"},{FormUi.NAME,"last_name"},{FormUi.TYPE,FormUi.TYPES.Text.toString()},{FormUi.DESCRIPTION,"Type your last name"},{FormUi.REQUIRED,"true"},{FormUi.HELP,"<h3>Help</h3><p>This is help information on this field</p>"}};
			String[][] role = {{FormUi.TITLE,"Role"},{FormUi.NAME,"role"},{FormUi.TYPE,FormUi.TYPES.List.toString()},{FormUi.DESCRIPTION,"User Role"},{FormUi.REQUIRED,"true"},{FormUi.HELP,"<h3>Help</h3><p>This is help information on this field</p>"},{FormUi.VALUE,"Option 3"},{FormUi.ITEMS,"Option 1","Option 2","Option 3"}};
			String[][] active = {{FormUi.TITLE,"Activate"},{FormUi.NAME,"active"},{FormUi.TYPE,FormUi.TYPES.Radio.toString()},{FormUi.DESCRIPTION,"Set account active"},{FormUi.REQUIRED,"false"},{FormUi.HELP,"<h3>Help</h3><p>This is help information on this field</p>"},{FormUi.VALUE,"Inactive"},{FormUi.ITEMS,"Active","Inactive"}};
			String[][] more_info = {{FormUi.TITLE,"Message"},{FormUi.NAME,"more_info"},{FormUi.TYPE,FormUi.TYPES.TextArea.toString()},{FormUi.DESCRIPTION,"Type more instructions"},{FormUi.REQUIRED,"false"},{FormUi.HELP,"<h3>Help</h3><p>This is help information on this field</p>"}};
			fields.add(this.getMapforFields(first_name));
			fields.add(this.getMapforFields(last_name));
			fields.add(this.getMapforFields(role));
			fields.add(this.getMapforFields(active));
			fields.add(this.getMapforFields(more_info));

			try{
				form.setFields(fields);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	private void loadInlineFrame() {
		if(RootPanel.get("dashboard_main") != null){
			/**
			 * Load the app controller
			 */
			DashboardAppController app = new DashboardAppController(getRequestBuilder());
			RootPanel.get("dashboard_main").add(app);
			app.load(History.getToken());
		}
	}
	public static void loadDataGrids() {
		NodeList<Element> nodes = RootPanel.get().getElement().getElementsByTagName("div");
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			if(e.getId().startsWith("form_datagrid_")){
				String src = e.getAttribute("src");
				String fields = e.getAttribute("fields");
				String onrowclick = e.getAttribute("onrowclick");
				e.removeAttribute("onrowclick");
				DataTableUi grid = new DataTableUi();
				grid.setRowClickAction(onrowclick);
				RootPanel.get(e.getId()).add(grid);
				grid.load(src,fields);
			}
		}
	}
	public static void loadForms() {
		NodeList<Element> nodes = RootPanel.get().getElement().getElementsByTagName("div");
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			if(e.getId().startsWith("form_manager_")){
				String url = e.getAttribute("url");
				if(url != null && url.length() > 0){
					e.removeAttribute("url");
					FormUi form = new FormUi();
					String class_name = null;
					try{
						class_name = e.getAttribute("styleName");
						if(class_name != null && class_name.length() > 0){
							form.setStyleName(class_name);
						}
						e.removeAttribute("class");
					}catch(Exception e2){}
					try{
						String addedClass = e.getAttribute("addStyleName");
						if(addedClass != null && addedClass.length() > 0){
							form.addStyleName(addedClass);
						}
						e.removeAttribute("addStyleName");
					}catch(Exception e2){}
					try{
						String action = e.getAttribute("action");
						if(action != null && action.length() > 0){
							form.setAction(action);
						}
					}catch(Exception e2){}
					try{
						String onsuccess = e.getAttribute("onsuccess");
						if(onsuccess != null && onsuccess.length() > 0){
							form.setOnSuccess(onsuccess);
						}
					}catch(Exception e2){}
					try{
						String authurl = e.getAttribute("authurl");
						if(authurl != null && authurl.length() > 0){
							form.setAuthenticationUrl(authurl);
						}
					}catch(Exception e2){}
					RootPanel.get(e.getId()).add(form);
					form.load(url);
				}
			}
		}
	}
	private RequestBuilder getRequestBuilder() {
		return new RequestBuilder(RequestBuilder.POST,GWT.getHostPageBaseURL()+"rpc");
	}
	private HashMap<String, String[]> getMapforFields(String[][] first_name) {
		HashMap<String,String[]> retval = new HashMap<String,String[]>();
		for(int i = 0; i < first_name.length;i++){
			String[] val = new String[first_name[i].length-1];
			for(int ii = 0; ii < first_name[i].length-1;ii ++){
				val[ii] = first_name[i][ii+1];
			}
			retval.put(first_name[i][0], val);
		}
		return retval;
	}
}
