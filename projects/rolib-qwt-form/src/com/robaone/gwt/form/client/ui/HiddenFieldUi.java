package com.robaone.gwt.form.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Widget;

/**
   Copyright Mar 26, 2012 Ansel Robateau
   http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 **/

/**
 * @author Ansel Robateau
 *
 */
public class HiddenFieldUi extends Composite implements HasText,FormField {

	private static HiddenFieldUiUiBinder uiBinder = GWT
			.create(HiddenFieldUiUiBinder.class);

	interface HiddenFieldUiUiBinder extends UiBinder<Widget, HiddenFieldUi> {
	}

	public HiddenFieldUi() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	Hidden hidden;

	public HiddenFieldUi(String name) {
		initWidget(uiBinder.createAndBindUi(this));
		hidden.setName(name);
	}
	/**
	 * Gets invoked when the default constructor is called
	 * and a string is provided in the ui.xml file.
	 */
	public String getText() {
		return hidden.getDefaultValue();
	}

	/* (non-Javadoc)
	 * @see com.robaone.gwt.form.client.ui.FormField#getName()
	 */
	@Override
	public String getName() {
		return hidden.getName();
	}

	/* (non-Javadoc)
	 * @see com.robaone.gwt.form.client.ui.FormField#setName(java.lang.String)
	 */
	@Override
	public void setName(String str) {
		hidden.setName(str);
	}

	/* (non-Javadoc)
	 * @see com.robaone.gwt.form.client.ui.FormField#getValues()
	 */
	@Override
	public String[] getValues() {
		String[] retval = {this.hidden.getDefaultValue()};
		return retval;
	}

	/* (non-Javadoc)
	 * @see com.robaone.gwt.form.client.ui.FormField#setError(boolean)
	 */
	@Override
	public void setError(boolean b) {
		
	}

	/* (non-Javadoc)
	 * @see com.robaone.gwt.form.client.ui.FormField#addKeyUpHandler(com.google.gwt.event.dom.client.KeyUpHandler)
	 */
	@Override
	public void addKeyUpHandler(KeyUpHandler handler) {
		
	}
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasText#setText(java.lang.String)
	 */
	@Override
	public void setText(String text) {
		hidden.setDefaultValue(text);
	}

}
