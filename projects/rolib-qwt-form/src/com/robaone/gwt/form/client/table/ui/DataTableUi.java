package com.robaone.gwt.form.client.table.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;

public class DataTableUi extends Composite {

	private static DataTableUiUiBinder uiBinder = GWT
			.create(DataTableUiUiBinder.class);

	interface DataTableUiUiBinder extends UiBinder<Widget, DataTableUi> {
	}

	public DataTableUi() {
		initWidget(uiBinder.createAndBindUi(this));
		initialize();
	}

	@UiField
	FlowPanel panel;
	@UiField Label error;
	CellTable<JSONObject> table;
	List<JSONObject> RECORDS = new ArrayList<JSONObject>();
	final SingleSelectionModel<JSONObject> selectionModel = new SingleSelectionModel<JSONObject>();
	private JSONObject m_keyinfo;
	private Handler m_selection_change_event;
	public DataTableUi(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		initialize();
	}

	public void load(String src, final String fields) {
		panel.add(table);
		error.setVisible(false);
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, src);
		try {
			rb.sendRequest(null, new RequestCallback(){

				@Override
				public void onResponseReceived(Request request, Response response) {
					try{
						int code = response.getStatusCode();
						if(code == 200){
							String str = response.getText();
							JSONValue data = JSONParser.parseLenient(str);
							JSONArray records = data.isObject().get("response").isObject().get("data").isArray();
							for(int record_index = 0;record_index < records.size();record_index++){
								try{
									RECORDS.add(records.get(record_index).isObject());
									/**
									 * Create columns
									 */
									if(record_index == 0){
										JSONValue fielddev = JSONParser.parseLenient(fields);
										for(int field_index = 0; field_index < fielddev.isArray().size();field_index++){
											JSONValue field = fielddev.isArray().get(field_index);
											String fieldname = field.isObject().get("name").isString().stringValue();
											String type = null;
											try{
												type = field.isObject().get("type").isString().stringValue();
											}catch(Exception e){}
											boolean iskey = false;
											boolean display = true;
											try{
												iskey = field.isObject().get("key").isBoolean().booleanValue();
												if(iskey){
													setKeyInfo(field.isObject());
												}
											}catch(Exception e){}
											try{
												display = field.isObject().get("display").isBoolean().booleanValue();
											}catch(Exception e){}
											String label = null;
											try{
												label = JSONParser.parseLenient(fields).isArray().get(field_index).isObject().get("label").isString().stringValue();
											}catch(Exception e){}
											if(display){
												addField(records.get(record_index).isObject(),fieldname,type,label);
											}
										}
									}
								}catch(Exception e2){}
							}
							// Set the total row count. This isn't strictly necessary, but it affects
							// paging calculations, so its good habit to keep the row count up to date.
							selectionModel.addSelectionChangeHandler(getSelectionChangeEvent());
							table.setRowCount((int)data.isObject().get("response").isObject().get("totalRows").isNumber().doubleValue(), true);
							table.setRowData(0, RECORDS);
						}else{
							throw new Exception(response.getStatusText());
						}
					}catch(Exception e){
						onError(request,e);
					}
				}

				@Override
				public void onError(Request request, Throwable exception) {
					setError(exception.getMessage());
				}

			});
		} catch (RequestException e) {
			this.clearErrors();
			this.setError(e.getMessage());
		}
		// Add a text column to show the name.

		//table.addColumn(newTetColumn(), "Name");
		//DataGrid grid = new DataGrid();
		//panel.add(grid);
	}

	protected void setKeyInfo(JSONObject object) {
		this.m_keyinfo = object;
	}

	private void initialize() {
		// Create a CellTable.
		table = new CellTable<JSONObject>();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		// Add a selection model to handle user selection.
		table.setSelectionModel(selectionModel);
	}
	private Handler getSelectionChangeEvent() {
		return m_selection_change_event;
	}

	private void clearErrors() {
		error.setVisible(false);
		error.setText("");
	}
	protected void setError(String msg){
		error.setText(msg);
		error.setVisible(true);
	}

	private TextColumn<JSONObject> newTextColumn(JSONObject data,final String field){
		TextColumn<JSONObject> nameColumn = new TextColumn<JSONObject>() {
			@Override
			public String getValue(JSONObject object) {
				try{
					return object.get(field).isArray().get(0).isString().stringValue();
				}catch(Exception e){
					return object.get(field).isString().stringValue();
				}
			}
		};
		return nameColumn;
	}

	public void addField(JSONObject data,String fieldname, String type,String label) {
		if("text".equalsIgnoreCase(type)){
			if(label == null){
				table.addColumn(newTextColumn(data,fieldname));
			}else{
				table.addColumn(newTextColumn(data,fieldname),label);
			}
		}
	}

	public void setRowClickAction(final String onrowclick) {
		try{
			if(onrowclick.startsWith("location=")){
				m_selection_change_event = new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						JSONObject selected = selectionModel.getSelectedObject();
						try{
							if (selected != null) {
								String keyvalue = selected.get(m_keyinfo.get("name").isString().stringValue()).toString();
								String onclick = null;
								try{onclick = m_keyinfo.get("onclick").isString().stringValue();}catch(Exception e){}
								if(onclick != null){
									onclick = onclick.replaceAll("$key", URL.encode(keyvalue));
								}else{
									onclick = onrowclick.replaceAll("$key", URL.encode(keyvalue));
									onclick = onrowclick.substring("location=".length());
								}
								Window.Location.assign(onclick);
							}
						}catch(Exception e){
							System.err.println("onSelectionChange(): Could not perform action: "+e.getMessage());
						}
					}
				};
			}
		}catch(Exception e){}
	}
}
