package com.robaone.gwt.form.server;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 *
 */
public class TestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3852351352596664242L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			printRequest(request);
			response.setContentType("text/plain");
			int status = 0;
			String error = "";
			try{
				if(request.getParameter("username").equals("arobateau@microdg.com")){
					status = 0;
				}else{
					status = 1;
				}
			}catch(Exception e){
				status = 2;
				error = e.getMessage();
			}
			String str = "{\n"+
					"  \"response\":{\n"+
					"     \"startRow\":0,\n"+
					"     \"endRow\":0,\n"+
					"     \"totalRows\":1,\n"+
					"     \"errors\":{\n"+
					"        \"username\":\"You must enter a valid username\",\n"+
					"        \"state\":\"That is not your state\"\n"+
					"     },\n"+
					"     \"error\":\""+error+"\",\n"+
					"     \"page\":0,\n"+
					"     \"status\":"+status+",\n"+
					"     \"data\":[\n"+
					"        {\"username\":\"value\"}\n"+
					"     ],\n"+
					"     \"properties\":{\n"+
					"        \"key\":\"value\"\n"+
					"     }\n"+
					"   }\n"+
					"}";
			response.getOutputStream().write(str.getBytes());
		} catch (JSONException e) {
			System.out.println("Error:"+e.getMessage());
		}
		
	}

	private void printRequest(HttpServletRequest request) throws JSONException {
		JSONObject r = new JSONObject(request.getParameterMap());
		System.out.println(r.toString(3));
	}
	

}
