package com.robaone.jdbc;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FilePasswordStoreTest {

	@Before
	public void setUp() throws Exception {
		String tempfile = "test.password=1234";
		FileWriter fw = null;
		try{
			File f = new File("test.txt");
			fw = new FileWriter(f);
			fw.write(tempfile);
			System.setProperty(FilePasswordStore.PASSWORD_FILE, f.getAbsolutePath());
		}finally{
			fw.flush();
			fw.close();
		}
	}

	@After
	public void tearDown() throws Exception {
		File temp = new File("test.txt");
		temp.delete();
	}

	@Test
	public void testGetPassword() {
		FilePasswordStore store = new FilePasswordStore();
		assertNotNull(store);
		try {
			String pwd = store.getPassword("test.password");
			assertNotNull(pwd);
			assertEquals(pwd,"1234");
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
