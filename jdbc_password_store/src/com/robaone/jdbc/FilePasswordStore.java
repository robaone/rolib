package com.robaone.jdbc;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class FilePasswordStore implements ROPasswordStoreInterface {
	public static final String PASSWORD_FILE = "password.file";
	@Override
	public String getPassword(String password) throws Exception {
		String file = System.getProperty(PASSWORD_FILE);
		Properties prop = new Properties();
		FileInputStream fin = null;
		try{
			fin = new FileInputStream(new File(file));
			prop.load(fin);
			String password_str = prop.getProperty(password);
			return password_str;
		}finally{
			if(fin != null){
				fin.close();
			}
		}
	}

}
