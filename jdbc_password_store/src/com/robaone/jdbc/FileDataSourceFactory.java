package com.robaone.jdbc;


public class FileDataSourceFactory extends RODataSourceFactory {

	@Override
	public ROPasswordStoreInterface getPasswordStore() {
		return new FilePasswordStore();
	}

}
