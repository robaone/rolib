package com.robaone.gwt.form.client;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.robaone.gwt.form.shared.FieldVerifier;

public class ContentLoadManager {
	private String m_command_variable;
	private String m_rpc_source;
	private LoaderInterface m_content;
	private JSONObject m_jo;
	private int m_completed_request_count = 0;
	private String m_command_source;
	private String m_callback;
	public ContentLoadManager(LoaderInterface loader){
		m_content = loader;
		m_jo = new JSONObject();
	}

	public void setCommandsVariable(String attribute) {
		this.m_command_variable = attribute;
	}
	public void setRPCSource(String attribute) {
		this.m_rpc_source = attribute;
	}
	public void incrementCompletedCount(){
		this.m_completed_request_count++;
	}
	public boolean allCompleted(){
		return this.m_completed_request_count >= this.getCommandCount(this.m_command_variable);
	}
	public void execute() {
		if(FieldVerifier.exists(this.m_command_source) && !FieldVerifier.exists(this.m_command_variable)){
			try{
				RequestBuilder rb = new RequestBuilder(RequestBuilder.POST,this.m_command_source);
				rb.sendRequest(null, new RequestCallback(){

					@Override
					public void onResponseReceived(Request request,
							Response response) {
						try{
							if(response.getStatusCode() == 200){
								HTML h = new HTML(response.getText());
								String json = h.getText();
								m_command_variable = json;
								execute();
							}else{
								throw new Exception(response.getStatusText());
							}
						}catch(Exception e){
							onError(request,e);
						}
					}

					@Override
					public void onError(Request request, Throwable exception) {
						showError(exception);
					}

				});
			}catch(Exception e){
				showError(e);
			}
		}else if(FieldVerifier.exists(this.m_command_variable)){
			if(this.m_command_variable.startsWith("[") || this.m_command_variable.startsWith("{")){
				JSONValue val = JSONParser.parseLenient(this.m_command_variable);
				try{
				JSONArray array = val.isArray();
				for(int i = 0; i < array.size();i++){
					try {
						sendCommand(array.get(i).isObject());
					} catch (RequestException e) {
						showError(e);
					}
				}
				}catch(Exception e){
					JSONObject jo = val.isObject();
					try {
						sendCommand(jo);
					} catch (RequestException e1) {
						showError(e1);
					}
				}
			}else{
				for(int i = 0; i < this.getCommandCount(this.m_command_variable);i++){
					try{
						JavaScriptObject obj = getCommand(this.m_command_variable,i);
						JSONObject jo = new JSONObject(obj);
						sendCommand(jo);
					}catch(Exception e){
						e.printStackTrace();
						showError(e);
					}
				}
			}
		}else{
			showError(new Exception("No commands found"));
		}
	}

	private void sendCommand(JSONObject jo) throws RequestException {
		System.out.println("ContentLoadManager.execute()");
		String url = jo.get("_url").isString().stringValue();
		final String action = jo.get("_action").isString().stringValue();
		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST,url);
		rb.sendRequest(jo.toString(), new RequestCallback(){

			@Override
			public void onResponseReceived(Request request,
					Response response) {
				try{
					if(response.getStatusCode() == 200){
						System.out.println("ContentLoadManager.execute().onResponseReceived(...)");
						String response_str = response.getText();
						HTML h = new HTML(response_str);
						response_str = h.getText();
						JSONValue jo = JSONParser.parseLenient(response_str);
						m_jo.put(action, jo);
					}else{
						throw new Exception(response.getStatusText());
					}
				}catch(Exception e){
					onError(request,e);
				}finally{
					incrementCompletedCount();
					if(allCompleted()){
						try{
							composeContent();
						}catch(Exception e){
							onError(request,e);
						}
					}
				}
			}

			@Override
			public void onError(Request request, Throwable exception) {
				JSONValue jo = JSONParser.parseLenient("{\"response\":{\"data\":{},\"status\":1}}");
				jo.isObject().get("response").isObject().put("error", new JSONString(exception.getMessage()));
				m_jo.put(action, jo);
				System.err.print(exception.getMessage());
				showError(exception);
			}

		});
	}
	private void showError(Throwable exception) {
		m_content.showError(exception);
	}

	private int getCommandCount(String variable){
		if(variable.startsWith("[")){
			JSONValue array = JSONParser.parseLenient(variable);
			return array.isArray().size();
		}else if(variable.startsWith("{")){
			return 1;
		}else{
			return getVarCount(variable);
		}
	};
	private native int getVarCount(String variable)/*-{
		return $wnd[variable].length;
	}-*/;
	private native JavaScriptObject getCommand(String variable,int index)/*-{
		return $wnd[variable][index];
	}-*/;

	public void composeContent() throws Exception {
		JSONObject jorequest = new JSONObject();
		jorequest.put("request", m_jo);
		m_content.setCallback(this.m_callback);
		m_content.load(this.m_rpc_source, jorequest);	
	}
	public Widget getContent() {
		return (Widget) m_content;
	}

	public void setCommandSource(String attribute) {
		this.m_command_source = attribute;
	}

	public void setChannels(String channels) {
		this.m_content.setChannels(channels);
	}

	public void setCallback(String attribute) {
		this.m_callback = attribute;
	}

}
