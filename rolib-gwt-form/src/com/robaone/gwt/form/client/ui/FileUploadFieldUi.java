package com.robaone.gwt.form.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.Widget;

public class FileUploadFieldUi extends Composite implements FormField {

	private static FileUploadFieldUiUiBinder uiBinder = GWT
			.create(FileUploadFieldUiUiBinder.class);

	interface FileUploadFieldUiUiBinder extends
			UiBinder<Widget, FileUploadFieldUi> {
	}

	public FileUploadFieldUi() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	FileUpload upload;

	public FileUploadFieldUi(String name) {
		initWidget(uiBinder.createAndBindUi(this));
		this.setName(name);
	}

	@Override
	public String getName() {
		return upload.getName();
	}

	@Override
	public void setStyleName(String name){
		super.setStyleName(name);
		this.upload.setStyleName(name);
	}
	
	@Override
	public void addStyleName(String name){
		super.addStyleName(name);
		this.upload.addStyleName(name);
	}
	@Override
	public void setName(String str) {
		this.upload.setName(str);
	}

	@Override
	public String[] getValues() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setError(boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addKeyUpHandler(KeyUpHandler handler) {
		// TODO Auto-generated method stub
		
	}

}
