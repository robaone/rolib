package com.robaone.gwt.form.client.ui;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.Widget;
/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 *
 */
public class PasswordFieldUi extends Composite implements HasText,FormField {
	interface Style extends CssResource {
		String error();
	}
	private static TextFieldUiUiBinder uiBinder = GWT
			.create(TextFieldUiUiBinder.class);

	interface TextFieldUiUiBinder extends UiBinder<Widget, PasswordFieldUi> {
	}
	private String name;

	public PasswordFieldUi(String name) {
		initWidget(uiBinder.createAndBindUi(this));
		this.setName(name);
	}
	@UiField PasswordTextBox field;
	@UiField Style style;
	public String[] getValues() {
		Vector<String> retval = new Vector<String>();
		retval.add(this.getText());
		return retval.toArray(new String[0]);
	}

	@Override
	public void setStyleName(String name){
		super.setStyleName(name);
		field.setStyleName(name);
	}
	@Override
	public void addStyleName(String name){
		super.addStyleName(name);
		field.addStyleName(name);
	}
	public String getName() {
		return name;
	}
	public void setName(String str){
		this.name = str;
		this.field.setName(str);
	}

	@Override
	public String getText() {
		return field.getText();
	}

	@Override
	public void setText(String text) {
		this.field.setText(text);
	}

	@Override
	public void setError(boolean b) {
		if(b){
			field.addStyleName(style.error());
		}else{
			field.removeStyleName(style.error());
		}
	}

	@Override
	public void addKeyUpHandler(KeyUpHandler handler) {
		field.addKeyUpHandler(handler);
	}

}
