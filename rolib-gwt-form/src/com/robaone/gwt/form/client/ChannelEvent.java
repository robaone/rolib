package com.robaone.gwt.form.client;

import java.util.HashMap;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.robaone.gwt.form.shared.FieldVerifier;

public class ChannelEvent extends GwtEvent<ChannelMessageHandler> {
	private String channel;
	private String message;
	private HashMap<String,String> parameters = new HashMap<String,String>();
	public ChannelEvent(String channel, String message) {
		this.setChannel(channel);
		this.setMessage(message);
	}
	public static Type<ChannelMessageHandler> TYPE = new Type<ChannelMessageHandler>();
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ChannelMessageHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ChannelMessageHandler handler) {
		if(FieldVerifier.exists(message) && FieldVerifier.exists(channel)){
			
			if(FieldVerifier.exists(getParameter("src"))){
				handler.setSource(getChannel(),getParameter("src"));
			}
			if(FieldVerifier.exists(getParameter("fields"))){
				handler.setFields(getChannel(),getParameter("fields"));
			}
			if(FieldVerifier.exists(getParameter("commands"))){
				handler.setCommands(getChannel(),getParameter("commands"));
			}
			if(FieldVerifier.exists(getParameter("command_src"))){
				handler.setCommandSource(getChannel(),getParameter("command_src"));
			}
			if(FieldVerifier.exists(getParameter("refresh"))){
				handler.refresh(getChannel(),getParameter("refresh"));
			}
			if(FieldVerifier.exists(getParameter("hide"))){
				handler.hide(getChannel(),getParameter("hide"));
			}
			if(FieldVerifier.exists(getParameter("show"))){
				handler.show(getChannel(),getParameter("show"));
			}
		}
	}
	public String getParameter(String param){
		return this.parameters.get(param);
	}
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
		try{
			String[] params = message.split("[&]");
			for(String str : params){
				try{
					String[] pair = str.split("[=]");
					String name = pair[0];
					String value = pair[1];
					this.parameters.put(name, value);
				}catch(Exception e){}
			}
		}catch(Exception e){}
	}

}
