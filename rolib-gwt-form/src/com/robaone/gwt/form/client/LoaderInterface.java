package com.robaone.gwt.form.client;

import com.google.gwt.json.client.JSONValue;

public interface LoaderInterface {

	public void load(String m_rpc_source, JSONValue jorequest) throws Exception;

	public void showError(Throwable exception);

	public void setChannels(String channels);
	
	public void setCommandSource(String source);
	
	public void setCommands(String source);
	
	public void setSource(String source);
	
	public void setRequestData(JSONValue jo);

	public void setCallback(String callback);

}
