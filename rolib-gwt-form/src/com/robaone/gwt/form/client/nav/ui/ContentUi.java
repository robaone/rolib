package com.robaone.gwt.form.client.nav.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.robaone.gwt.form.client.ChannelEvent;
import com.robaone.gwt.form.client.ChannelMessageHandler;
import com.robaone.gwt.form.client.ContentLoadManager;
import com.robaone.gwt.form.client.FormManager;
import com.robaone.gwt.form.client.LoaderInterface;
import com.robaone.gwt.form.shared.FieldVerifier;

public class ContentUi extends Composite implements LoaderInterface {

	private static ContentUiUiBinder uiBinder = GWT
			.create(ContentUiUiBinder.class);

	interface ContentUiUiBinder extends UiBinder<Widget, ContentUi> {
	}

	public ContentUi() {
		initWidget(uiBinder.createAndBindUi(this));
		bind();
	}
	ArrayList<String> channels = new ArrayList<String>();
	@UiField
	HTML html;
	@UiField Image wait_image;
	private String m_source;
	private JSONValue m_requestdata;
	private String m_command_src;
	private String m_commands;
	private String m_callback;

	public ContentUi(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		bind();
	}
	public boolean isListening(String channel){
		for(String str : this.channels){
			if(str.equals(channel)){
				return true;
			}
		}
		return false;
	}
	private void bind() {
		FormManager.EVENT_BUS.addHandler(ChannelEvent.TYPE, new ChannelMessageHandler(){

			@Override
			public void setSource(String channel, String message) {
				if(isListening(channel)){
					System.out.println("ContentUi::setSource(\""+channel+"\",\""+message+"\")");
					ContentUi.this.setSource(message);
				}
			}

			@Override
			public void setFields(String channel, String message) {
				if(isListening(channel)){
					System.out.println("ContentUi::setFields(\""+channel+"\",\""+message+"\")");
				}
			}

			@Override
			public void setCommandSource(String channel, String message) {
				if(isListening(channel)){
					System.out.println("ContentUi::setCommandSource(\""+channel+"\",\""+message+"\")");
					ContentUi.this.setCommandSource(message);
					ContentUi.this.setCommands(null);
				}
			}

			@Override
			public void refresh(String channel, String message) {
				if(isListening(channel)){
					System.out.println("ContentUi::refresh(\""+channel+"\",\""+message+"\")");
					ContentUi.this.load();
				}
			}

			@Override
			public void hide(String channel, String message) {
				if(isListening(channel)){
					System.out.println("ContentUi::hide(\""+channel+"\",\""+message+"\")");
					ContentUi.this.setVisible(false);
				}
			}

			@Override
			public void show(String channel, String message) {
				if(isListening(channel)){
					System.out.println("ContentUi::show(\""+channel+"\",\""+message+"\")");
					ContentUi.this.setVisible(true);
				}
			}

			@Override
			public void setCommands(String channel, String message) {
				if(isListening(channel)){
					System.out.println("ContentUi::setCommands(\""+channel+"\",\""+message+"\")");
					ContentUi.this.setCommands(message);
					ContentUi.this.setCommandSource(null);
				}
			}

		});
	}
	public void setCommands(String message) {
		this.m_commands = message;
	}
	public void setCommandSource(String message) {
		this.m_command_src = message;
	}
	protected void load() {
		if(FieldVerifier.exists(m_commands) || FieldVerifier.exists(m_command_src)){
			ContentLoadManager man = new ContentLoadManager(this);
			man.setCommandSource(m_command_src);
			man.setCommandsVariable(m_commands);
			man.execute();
		}else{
			this.load(this.m_source,this.m_requestdata);
		}
	}
	public void setSource(String message) {
		this.m_source = message;
	}
	public void load(String src,final JSONValue jo){
		this.setSource(src);
		this.setRequestData(jo);
		try {
			RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, src);
			rb.sendRequest(jo == null ? null : jo.toString(), new RequestCallback(){

				@Override
				public void onResponseReceived(Request request, Response response) {
					try{
						if(response.getStatusCode() == 200){
							String html_response = response.getText();
							html.setHTML(html_response);
							executeCallback();
						}else{
							throw new Exception(response.getStatusText());
						}
					}catch(Exception e){
						onError(request,e);
					}finally{
						wait_image.setVisible(false);
					}
				}

				@Override
				public void onError(Request request, Throwable exception) {
					showError(exception);
				}

			});
		} catch (Exception e) {
			showError(e);
		}
	}

	protected void executeCallback() {
		if(this.m_callback != null && this.m_callback.trim().length() > 0){
			this.runNativeCallback(this.m_callback);
		}
	}
	protected final native void runNativeCallback(String callback)/*-{
		try{
			eval('$wnd.'+callback);
		}catch(Exception){
		}
	}-*/;
	public void setRequestData(JSONValue jo) {
		this.m_requestdata = jo;
	}
	public void showError(Throwable exception) {
		wait_image.setVisible(false);
		html.setHTML("<h1>Error:</h1><p>"+exception.getMessage()+"</p>");
	}

	@Override
	public void setChannels(String channels) {
		if(FieldVerifier.exists(channels)){
			String[] list = channels.split(" ");
			for(String str: list){
				this.channels.add(str);
			}
		}
	}
	public void setCallback(String attribute) {
		this.m_callback = attribute;
	}
}
