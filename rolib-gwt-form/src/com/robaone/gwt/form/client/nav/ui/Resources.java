package com.robaone.gwt.form.client.nav.ui;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Resources extends ClientBundle {

	@Source("ajax-loader.gif")
	ImageResource ajaxLoader();

}
