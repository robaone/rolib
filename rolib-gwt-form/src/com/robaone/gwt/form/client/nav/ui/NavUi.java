package com.robaone.gwt.form.client.nav.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class NavUi extends Composite {

	private static NavUiUiBinder uiBinder = GWT.create(NavUiUiBinder.class);

	interface NavUiUiBinder extends UiBinder<Widget, NavUi> {
	}

	public NavUi() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	SimplePanel panel;
	ULPanel ul;

	public NavUi(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		ul = new ULPanel();
		panel.setWidget(ul);
	}

	public void load(String src){
		String[] test = {"item1","item2"};
		for(String str: test){
			ul.add(new Label(str));
		}
	}
}
