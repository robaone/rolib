package com.robaone.gwt.form.client;

import com.robaone.gwt.form.client.layout.LayoutUi;
import com.robaone.gwt.form.client.nav.ui.ContentUi;
import com.robaone.gwt.form.client.table.ui.DataTableUi;
import com.robaone.gwt.form.client.ui.FormUi;
import com.robaone.gwt.form.shared.FieldVerifier;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FormManager implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	/*
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	 */
	 public static EventBus EVENT_BUS = GWT.create(SimpleEventBus.class);
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		createEventMethod();
		populate();
	}
	public static void populate() {
		loadLayouts();
		loadForms();
		loadDataGrids();
		loadContentLoaders();

	}
	public native void createEventMethod()/*-{
		$wnd.form_manager_event = function(channel,message){
			@com.robaone.gwt.form.client.FormManager::handleEvent(Ljava/lang/String;Ljava/lang/String;)(channel,message);
		}
	}-*/;
	public static void handleEvent(String channel,String message){
		System.out.println("Channel("+channel+") = "+message);
		EVENT_BUS.fireEvent(new ChannelEvent(channel,message));
	}
	public static void loadContentLoaders(String parentid) {
		Element root = RootPanel.get(parentid).getElement();
		NodeList<Element> nodes = root.getElementsByTagName("div");
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			loadContent(e);
		}
	}
	public static void loadLayouts(){
		loadLayouts(null);
	}
	public static void loadLayouts(String parentid) {
		Element root = null;
		if(FieldVerifier.exists(parentid)){
			root = RootPanel.get(parentid).getElement();
		}else{
			root = RootPanel.get().getElement();
		}
		NodeList<Element> nodes = root.getElementsByTagName("div");
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			loadLayout(e);
		}
	}
	public static void loadContentLoaders() {
		NodeList<Element> nodes = RootPanel.get().getElement().getElementsByTagName("div");
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			loadContent(e);
		}
	}
	private static void loadLayout(Element e){
		String id = e.getId();
		String loaded = e.getAttribute("loaded");
		String channels = e.getAttribute("channels");
		if(FieldVerifier.exists(id)){
			if(id.startsWith("form_layout_") && !FieldVerifier.exists(loaded)){
				if(FieldVerifier.exists(e.getAttribute("commands"))){
					ContentLoadManager man = new ContentLoadManager(new LayoutUi());
					man.setChannels(channels);
					man.setCallback(e.getAttribute("callback"));
					man.execute();
					RootPanel.get(id).add(man.getContent());
					e.setAttribute("loaded", "true");
				}else{
					String src = e.getAttribute("src");
					if(!FieldVerifier.exists(src)){
						src = e.getAttribute("url");
					}
					LayoutUi layout = new LayoutUi();
					layout.setChannels(channels);
					layout.setCallback(e.getAttribute("callback"));
					RootPanel.get(id).add(layout);
					layout.load(src, null);
					e.setAttribute("loaded", "true");
				}
			}
		}
	}
	private static void loadContent(Element e) {
		String id = e.getId();
		String loaded = e.getAttribute("loaded");
		String channels = e.getAttribute("channels");
		if((id.startsWith("page_loader_app_container_") || id.startsWith("form_content_")) && !FieldVerifier.exists(loaded)){
			if(FieldVerifier.exists(e.getAttribute("commands"))){
				ContentLoadManager man = new ContentLoadManager(new DashboardAppController(null));
				man.setChannels(channels);
				man.setCommandsVariable(e.getAttribute("commands"));
				String src = e.getAttribute("src");
				if(!FieldVerifier.exists(src)){
					src = e.getAttribute("url");
				}
				man.setRPCSource(src);
				man.setCallback(e.getAttribute("callback"));
				man.execute();
				RootPanel.get(e.getId()).add(man.getContent());
				e.setAttribute("loaded", "true");
			}else if(FieldVerifier.exists(e.getAttribute("command_src"))){
				ContentLoadManager man = new ContentLoadManager(new ContentUi());
				man.setChannels(channels);
				man.setCommandSource(e.getAttribute("command_src"));
				String src = e.getAttribute("src");
				if(!FieldVerifier.exists(src)){
					src = e.getAttribute("url");
				}
				man.setRPCSource(src);
				man.setCallback(e.getAttribute("callback"));
				man.execute();
				RootPanel.get(e.getId()).add(man.getContent());
				e.setAttribute("loaded", "true");
			}else{
				String src = e.getAttribute("src");
				if(!FieldVerifier.exists(src)){
					src = e.getAttribute("url");
				}
				ContentUi content = new ContentUi();
				content.setChannels(channels);
				content.setCallback(e.getAttribute("callback"));
				RootPanel.get(e.getId()).add(content);
				e.setAttribute("loaded", "true");
				content.load(src,null);
			}
		}
	}
	public static void loadDataGrids(String parentid){
		NodeList<Element> nodes = RootPanel.get(parentid).getElement().getElementsByTagName("div");
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			loadGrid(e);
		}
	}
	public static void loadDataGrids() {
		NodeList<Element> nodes = RootPanel.get().getElement().getElementsByTagName("div");
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			loadGrid(e);
		}
	}
	private static void loadGrid(Element e) {
		if(e.getId().startsWith("form_datagrid_") && (e.getAttribute("loaded") == null || e.getAttribute("loaded").length() == 0)){
			String src = e.getAttribute("src");
			if(!FieldVerifier.exists(src)){
				src = e.getAttribute("url");
			}
			String fields = e.getAttribute("fields");
			String onrowclick = e.getAttribute("onrowclick");
			String channels = e.getAttribute("channels");
			String callback = e.getAttribute("callback");
			e.removeAttribute("onrowclick");
			if(e.getAttribute("commands") != null && e.getAttribute("commands").length() > 0){
				ContentLoadManager man = new ContentLoadManager(new DataTableUi(fields));
				man.setChannels(channels);
				if(e.getAttribute("commands").startsWith("[") || e.getAttribute("commands").startsWith("{")){
					//Inline commands
				}else{
					man.setCommandsVariable(e.getAttribute("commands"));
				}
				man.setRPCSource(src);
				man.setCallback(callback);
				man.execute();
				RootPanel.get(e.getId()).add(man.getContent());
				e.setAttribute("loaded", "true");
			}else if(FieldVerifier.exists(e.getAttribute("command_src"))){
				ContentLoadManager man = new ContentLoadManager(new DataTableUi(fields));
				man.setChannels(channels);
				man.setCommandSource(e.getAttribute("command_src"));
				man.setCallback(callback);
				man.execute();
				RootPanel.get(e.getId()).add(man.getContent());
				e.setAttribute("loaded", "true");
			}else{
				DataTableUi grid = new DataTableUi(fields);
				grid.setChannels(channels);
				grid.setRowClickAction(onrowclick);
				grid.setCallback(callback);
				RootPanel.get(e.getId()).add(grid);
				grid.load(src,null);
				e.setAttribute("loaded", "true");
			}
		}
	}
	public static void loadForms() {
		loadForms(null);
	}
	public static void loadForms(String parentid) {
		NodeList<Element> nodes = null;
		if(FieldVerifier.exists(parentid)){
			nodes = RootPanel.get(parentid).getElement().getElementsByTagName("div");
		}else{
			nodes = RootPanel.get().getElement().getElementsByTagName("div");
		}
		for(int i = 0; i < nodes.getLength();i++){
			Element e = nodes.getItem(i);
			String channels = e.getAttribute("channels");
			if(e.getId().startsWith("form_manager_") && (e.getAttribute("loaded") == null || e.getAttribute("loaded").length() == 0)){
				String url = e.getAttribute("url");
				if(url == null || url.length() == 0){
					url = e.getAttribute("src");
				}
				if(url != null && url.length() > 0){
					e.removeAttribute("url");
					FormUi form = new FormUi();
					form.setChannels(channels);
					form.setCallback(e.getAttribute("callback"));
					String class_name = null;
					try{
						class_name = e.getAttribute("styleName");
						if(class_name != null && class_name.length() > 0){
							form.setStyleName(class_name);
						}
						e.removeAttribute("class");
					}catch(Exception e2){}
					try{
						String addedClass = e.getAttribute("addStyleName");
						if(addedClass != null && addedClass.length() > 0){
							form.addStyleName(addedClass);
						}
						e.removeAttribute("addStyleName");
					}catch(Exception e2){}
					try{
						String action = e.getAttribute("action");
						if(action != null && action.length() > 0){
							form.setAction(action);
						}
					}catch(Exception e2){}
					try{
						String onsuccess = e.getAttribute("onsuccess");
						if(onsuccess != null && onsuccess.length() > 0){
							form.setOnSuccess(onsuccess);
						}
					}catch(Exception e2){}
					try{
						String authurl = e.getAttribute("authurl");
						if(authurl != null && authurl.length() > 0){
							form.setAuthenticationUrl(authurl);
						}
					}catch(Exception e2){}
					if(e.getAttribute("commands") != null && e.getAttribute("commands").length() > 0){
						ContentLoadManager man = new ContentLoadManager(form);
						man.setCommandsVariable(e.getAttribute("commands"));
						String src = e.getAttribute("src");
						if(!FieldVerifier.exists(src)){
							src = e.getAttribute("url");
						}
						man.setRPCSource(src);
						man.execute();
						RootPanel.get(e.getId()).add(man.getContent());
						e.setAttribute("loaded", "true");
					}else{
						RootPanel.get(e.getId()).getElement().appendChild(form.getElement());
						//RootPanel.get(e.getId()).add(form);
						form.load(url,null);
						e.setAttribute("loaded", "true");
						
					}
				}
			}
		}
	}
}
