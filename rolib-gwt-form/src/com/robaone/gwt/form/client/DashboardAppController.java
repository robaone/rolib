package com.robaone.gwt.form.client;

import java.util.ArrayList;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.robaone.gwt.form.shared.FieldVerifier;
/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 *
 */
public class DashboardAppController extends Composite implements
ValueChangeHandler<String>, LoaderInterface {
	RequestBuilder request;
	SimplePanel panel = new SimplePanel();
	ArrayList<String> channels = new ArrayList<String>();
	private String m_parentid;
	private String m_command_src;
	private String m_commands;
	private String m_source;
	private JSONValue m_requestdata;
	private String m_callback ;
	public DashboardAppController(RequestBuilder rb){
		request = rb;
		//History.addValueChangeHandler(this);
		this.initWidget(panel);
	}
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		load(event.getValue());
	}
	public void load(String val){
		String value = val;
		try{
			request.sendRequest(value,getCallback(value));
		}catch(Exception e){
			showError(e);
		}
	}
	private RequestCallback getCallback(final String value) {
		return new RequestCallback(){

			@Override
			public void onResponseReceived(Request request, Response response) {
				try{
					if(response.getStatusCode() == 200){
						String txt = response.getText();
						HTML body = new HTML(txt);
						panel.setWidget(body);
						FormManager.loadForms(m_parentid);
						FormManager.loadDataGrids(m_parentid);
						FormManager.loadContentLoaders(m_parentid);
					}else{
						throw new Exception(response.getStatusText());
					}
				}catch(Exception e){
					onError(request,e);
				}
			}

			@Override
			public void onError(Request request, Throwable exception) {
				showError(exception);
			}
			
		};
	}
	@Override
	public void load(String m_rpc_source, JSONValue jorequest) throws Exception {
		request = new RequestBuilder(RequestBuilder.POST,m_rpc_source);
		this.load(jorequest.toString());
		
	}
	@Override
	public void showError(Throwable exception) {
		Label error = new Label(exception.getMessage());
		panel.setWidget(error);
	}
	@Override
	public void setChannels(String channels) {
		if(FieldVerifier.exists(channels)){
			String[] list = channels.split(" ");
			for(String str: list){
				this.channels.add(str);
			}
		}
	}
	@Override
	public void setCommandSource(String source) {
		this.m_command_src = source;
	}
	@Override
	public void setCommands(String source) {
		this.m_commands = source;
	}
	@Override
	public void setSource(String source) {
		this.m_source = source;
	}
	@Override
	public void setRequestData(JSONValue jo) {
		this.m_requestdata = jo;
	}
	@Override
	public void setCallback(String callback) {
		this.m_callback = callback;
	}

}
