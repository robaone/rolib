package com.robaone.gwt.form.client;

import com.google.gwt.event.shared.EventHandler;


public interface ChannelMessageHandler extends EventHandler {

	public void setSource(String channel, String message);

	public void setFields(String channel, String message);

	public void setCommandSource(String channel, String message);

	public void refresh(String channel, String message);

	public void hide(String channel, String message);

	public void show(String channel, String message);

	public void setCommands(String channel, String message);

}
