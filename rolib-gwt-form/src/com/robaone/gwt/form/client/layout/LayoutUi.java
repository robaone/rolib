package com.robaone.gwt.form.client.layout;

import java.util.ArrayList;

import com.google.gwt.xml.client.Node;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.XMLParser;
import com.robaone.gwt.form.client.ChannelEvent;
import com.robaone.gwt.form.client.ChannelMessageHandler;
import com.robaone.gwt.form.client.ContentLoadManager;
import com.robaone.gwt.form.client.FormManager;
import com.robaone.gwt.form.client.LoaderInterface;
import com.robaone.gwt.form.client.nav.ui.ContentUi;
import com.robaone.gwt.form.client.table.ui.DataTableUi;
import com.robaone.gwt.form.client.ui.FormUi;
import com.robaone.gwt.form.shared.FieldVerifier;

public class LayoutUi extends Composite implements LoaderInterface {
	FlowPanel panel = new FlowPanel();
	ArrayList<String> channels = new ArrayList<String>();
	private JSONValue m_requestdata;
	private String m_source;
	private String m_command_src;
	private String m_commands;
	private String m_callback;
	private static int total = 0;
	public LayoutUi(){
		this.initWidget(panel);
		bind();
	}
	public void resetLoadTotal(){
		total = 0;
	}
	public void setChannels(String channels_list){
		if(FieldVerifier.exists(channels_list)){
			String[] list = channels_list.split(" ");
			for(String str : list){
				channels.add(str);
			}
		}
	}
	public boolean isListening(String channel){
		for(String str : this.channels){
			if(str.equals(channel)){
				return true;
			}
		}
		return false;
	}
	private void bind() {
		FormManager.EVENT_BUS.addHandler(ChannelEvent.TYPE, new ChannelMessageHandler(){

			@Override
			public void setSource(String channel, String message) {
				if(isListening(channel)){
					System.out.println("LayoutUi::setSource(\""+channel+"\",\""+message+"\")");
					LayoutUi.this.setSource(message);
				}
			}

			@Override
			public void setFields(String channel, String message) {
				if(isListening(channel)){
					System.out.println("LayoutUi::setFields(\""+channel+"\",\""+message+"\")");
				}
			}

			@Override
			public void setCommandSource(String channel, String message) {
				if(isListening(channel)){
					System.out.println("LayoutUi::setCommandSource(\""+channel+"\",\""+message+"\")");
					LayoutUi.this.setCommandSource(message);
				}
			}

			@Override
			public void refresh(String channel, String message) {
				if(isListening(channel)){
					System.out.println("LayoutUi::refresh(\""+channel+"\",\""+message+"\")");
					LayoutUi.this.load();
				}
			}

			@Override
			public void hide(String channel, String message) {
				if(isListening(channel)){
					System.out.println("LayoutUi::hide(\""+channel+"\",\""+message+"\")");
					LayoutUi.this.setVisible(false);
				}
			}

			@Override
			public void show(String channel, String message) {
				if(isListening(channel)){
					System.out.println("LayoutUi::show(\""+channel+"\",\""+message+"\")");
					LayoutUi.this.setVisible(true);
				}
			}

			@Override
			public void setCommands(String channel, String message) {
				if(isListening(channel)){
					System.out.println("LayoutUi::setCommands(\""+channel+"\",\""+message+"\")");
					LayoutUi.this.setCommands(message);
				}
			}

		});
	}

	protected void load() {
		this.resetLoadTotal();
		if(FieldVerifier.exists(this.m_command_src) || FieldVerifier.exists(this.m_commands)){
			ContentLoadManager man = new ContentLoadManager(this);
			man.setCommandSource(this.m_command_src);
			man.setCommandsVariable(this.m_commands);
			man.execute();
		}else{
			this.load(this.m_source,this.m_requestdata);
		}
	}
	public void setRequestData(JSONValue jorequest){
		this.m_requestdata = jorequest;
	}
	public void setSource(String source){
		this.m_source = source;
	}
	@Override
	public void load(String m_rpc_source, JSONValue jorequest) {
		this.setRequestData(jorequest);
		this.setSource(m_rpc_source);
		try{
			RequestBuilder rb = new RequestBuilder(RequestBuilder.POST,m_rpc_source);
			rb.sendRequest(jorequest == null ? null : jorequest.toString(), new RequestCallback(){

				@Override
				public void onResponseReceived(Request request,
						Response response) {
					try{
						if(response.getStatusCode() == 200){
							System.out.println("Reading xml file");
							// parse the XML document into a DOM
							String xml = response.getText();
							System.out.println("LayoutUi::load::onResponseReceived(..)\n  <<< "+xml);
							Document messageDom = XMLParser.parse(xml);
							/**
							 * Retrieve the root
							 */
							Node layoutui = messageDom.getElementsByTagName("layoutui").item(0);
							/**
							 * Build the objects
							 */
							incrementTotal();
							panel.clear();
							build(layoutui,panel);
						}else{
							throw new Exception(response.getStatusText());
						}
					}catch(Exception e){
						onError(request,e);
					}
				}

				@Override
				public void onError(Request request, Throwable exception) {
					showError(exception);
				}
				
			});
		}catch(Exception e){
			showError(e);
		}
	}

	protected void incrementTotal() throws Exception {
		total++;
		if(total > 5){
			throw new Exception("Recursion found");
		}
	}

	@Override
	public void showError(Throwable exception) {
		panel.clear();
		panel.add(new Label("Error: "+exception.getMessage()));
		exception.printStackTrace();
	}

	protected void build(Node layoutui, Panel parent) {
		for(int i = 0; i < layoutui.getChildNodes().getLength();i++){
			Node n = layoutui.getChildNodes().item(i);
			String name = n.getNodeName();
			String commands = null;
			String command_src = null;
			String src = null;
			String fields = null;
			String onrowclick = null;
			String channels = null;
			String styleName = null;
			String addStyleName = null;
			try{addStyleName = n.getAttributes().getNamedItem("addStyleName").getNodeValue();}catch(Exception e){}
			try{styleName = n.getAttributes().getNamedItem("styleName").getNodeValue();}catch(Exception e){}
			try{channels = n.getAttributes().getNamedItem("channels").getNodeValue();}catch(Exception e){}
			try{onrowclick = n.getAttributes().getNamedItem("onrowclick").getNodeValue();}catch(Exception e){}
			try{fields = n.getAttributes().getNamedItem("fields").getNodeValue();}catch(Exception e){}
			try{src = n.getAttributes().getNamedItem("src").getNodeValue();}catch(Exception e){}
			try{commands = n.getAttributes().getNamedItem("commands").getNodeValue();}catch(Exception e){}
			try{command_src = n.getAttributes().getNamedItem("command_src").getNodeValue();}catch(Exception e){}
			/**
			 * Match the name to an object
			 */
			if("simplePanel".equals(name)){
				/**
				 * Create a simple Panel
				 */
				SimplePanel p = new SimplePanel();
				if(FieldVerifier.exists(styleName)){
					p.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					p.addStyleName(addStyleName);
				}
				parent.add(p);
				build(n,p);
			}else if("flowPanel".equals(name)){
				/**
				 * Create a flow Panel object
				 */
				FlowPanel fl = new FlowPanel();
				if(FieldVerifier.exists(styleName)){
					fl.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					fl.addStyleName(addStyleName);
				}
				parent.add(fl);
				build(n,fl);
			}else if("horizontalPanel".equals(name)){
				/**
				 * Create a horizontal Panel object
				 */
				HorizontalPanel h = new HorizontalPanel();
				if(FieldVerifier.exists(styleName)){
					h.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					h.addStyleName(addStyleName);
				}
				parent.add(h);
				build(n,h);
			}else if("verticalPanel".equals(name)){
				/**
				 * Create a vertical Panel object
				 */
				VerticalPanel h = new VerticalPanel();
				if(FieldVerifier.exists(styleName)){
					h.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					h.addStyleName(addStyleName);
				}
				parent.add(h);
				build(n,h);
			}else if("layoutui".equals(name)){
				/**
				 * Create a layout object
				 */
				LayoutUi l = new LayoutUi();
				if(FieldVerifier.exists(styleName)){
					l.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					l.setStyleName(addStyleName);
				}
				l.setChannels(channels);
				if(FieldVerifier.exists(commands)){
					ContentLoadManager man = new ContentLoadManager(l);
					man.execute();
				}else if(FieldVerifier.exists(command_src)){
					ContentLoadManager man = new ContentLoadManager(l);
					man.execute();
				}else{
					l.load(src, null);
				}
				parent.add(l);
			}else if("formui".equals(name)){
				/**
				 * Create a form object
				 */
				FormUi f = new FormUi();
				if(FieldVerifier.exists(styleName)){
					f.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					f.setStyleName(addStyleName);
				}
				f.setChannels(channels);
				if(FieldVerifier.exists(commands)){
					ContentLoadManager man = new ContentLoadManager(f);
					man.execute();
				}else if(FieldVerifier.exists(command_src)){
					ContentLoadManager man = new ContentLoadManager(f);
					man.execute();
				}else{
					f.load(src, null);
				}
				parent.add(f);
			}else if("contentui".equals(name)){
				/**
				 * Create a content object
				 */
				ContentUi c = new ContentUi();
				if(FieldVerifier.exists(styleName)){
					c.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					c.setStyleName(addStyleName);
				}
				c.setChannels(channels);
				if(FieldVerifier.exists(commands)){
					ContentLoadManager man = new ContentLoadManager(c);
					man.setCommandsVariable(commands);
					man.setRPCSource(src);
					man.execute();
				}else if(FieldVerifier.exists(command_src)){
					ContentLoadManager man = new ContentLoadManager(c);
					man.setCommandSource(command_src);
					man.setRPCSource(src);
					man.execute();
				}else{
					c.load(src, null);
				}
				parent.add(c);
			}else if("datatableui".equals(name)){
				/**
				 * Create a data grid object
				 */
				DataTableUi d = new DataTableUi(fields);
				if(FieldVerifier.exists(styleName)){
					d.setStyleName(styleName);
				}
				if(FieldVerifier.exists(addStyleName)){
					d.setStyleName(addStyleName);
				}
				d.setChannels(channels);
				d.setRowClickAction(onrowclick);
				if(FieldVerifier.exists(commands)){
					ContentLoadManager man = new ContentLoadManager(d);
					man.setCommandsVariable(commands);
					man.setRPCSource(src);
					man.execute();
				}else if(FieldVerifier.exists(command_src)){
					ContentLoadManager man = new ContentLoadManager(d);
					man.setCommandSource(command_src);
					man.setRPCSource(src);
					man.execute();
				}else{
					d.load(src, null);
				}
				parent.add(d);
			}
		}
	}
	@Override
	public void setCommandSource(String source) {
		this.m_command_src = source;
	}
	@Override
	public void setCommands(String source) {
		this.m_commands = source;
	}
	@Override
	public void setCallback(String callback) {
		this.m_callback = callback;
	}
}
