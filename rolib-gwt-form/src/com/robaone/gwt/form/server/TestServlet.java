package com.robaone.gwt.form.server;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.robaone.api.json.DSResponse;

/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 *
 */
public class TestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3852351352596664242L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			String mode = request.getParameter("mode");
			if("html".equals(mode)){
				String request_data = printRequest(request);
				response.setContentType("text/html");
				response.getOutputStream().print("<h2>This is a transformed page</h2>");
				response.getOutputStream().print("<h3>Page = "+request.getParameter("page")+"</h3>");
				response.getOutputStream().print("<div id=\"form_manager_inline\" src=\"/test.xml\"></div>");
				response.getOutputStream().print("<pre>"+new JSONObject(request_data).toString(3)+"</pre>");
			}else if("xml".equals(mode)){
				response.setContentType("text/xml");
				response.getOutputStream().print("<?xml version=\"1.0\" ?><formui><title>This is a form</title><field name=\"field1\" type=\"text\"/></formui>");
			}else{
				response.setContentType("text/plain");
				DSResponse<String> dsr = new DSResponse<String>();
				dsr.getResponse().setStatus(0);
				dsr.getResponse().addData("This is a test");
				JSONObject jo = new JSONObject(dsr);
				//System.out.println(jo.toString(3));
				response.getOutputStream().print(jo.toString(3));
			}
		} catch (JSONException e) {
			System.out.println("Error:"+e.getMessage());
		}

	}

	private String printRequest(HttpServletRequest request) throws JSONException {
		JSONObject r = new JSONObject(request.getParameterMap());
		System.out.println("TestServlet.printRequest(..)");
		System.out.println(r.toString());
		try{
			String jsonstr = IOUtils.toString(request.getInputStream());
			System.out.println(jsonstr);
			return jsonstr;
		}catch(Exception e){}
		return r.toString();
	}


}
