package com.robaone.gwt.form.client.ui;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Widget;
/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class CheckFieldUi extends Composite implements FormField {

	private static CheckFieldUiUiBinder uiBinder = GWT
			.create(CheckFieldUiUiBinder.class);

	interface CheckFieldUiUiBinder extends UiBinder<Widget, CheckFieldUi> {
	}

	public CheckFieldUi(String name) {
		initWidget(uiBinder.createAndBindUi(this));
		this.setName(name);
	}
	@UiField FlowPanel content;
	private String m_name;
	private String m_stylename;
	private String m_addedname;
	@Override
	public void setStyleName(String name){
		content.setStyleName(name+"-panel");
		this.m_stylename = name;
	}
	@Override
	public void addStyleName(String name){
		content.addStyleName(name);
		this.m_addedname = name;
	}
	public void add(final String checkvalue,boolean value, String label){
		CheckBox box = new CheckBox();
		final Hidden hidden = new Hidden(this.getName());
		if(value){
			hidden.setDefaultValue(checkvalue);
			content.add(hidden);
		}
		box.addValueChangeHandler(new ValueChangeHandler<Boolean>(){

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if(event.getValue().booleanValue()){
					hidden.setDefaultValue(checkvalue);
					if(content.getWidgetIndex(hidden) == -1){
						content.add(hidden);
					}
				}else{
					hidden.setDefaultValue(null);
					if(content.getWidgetIndex(hidden) > -1){
						content.remove(hidden);
					}
				}
			}
			
		});
		if(this.m_stylename != null){
			box.setStyleName(this.m_stylename);
		}
		if(this.m_addedname != null){
			box.addStyleName(this.m_addedname);
		}
		box.setName("");
		box.setText(label == null ? checkvalue : label);
		box.setValue(value);
		content.add(box);
	}
	
	public String[] getValues(){
		Vector<String> retval = new Vector<String>();
		for(int i = 0; i < content.getWidgetCount();i++){
			Widget w = content.getWidget(i);
			if(w instanceof CheckBox){
				CheckBox box = (CheckBox)w;
				if(box.getValue()){
					retval.add(box.getText());
				}
			}
		}
		return retval.toArray(new String[0]);
	}

	public String getName() {
		return this.m_name;
	}
	public void setName(String name){
		this.m_name = name;
	}

	public void setValues(String[] values, String[] items) {
		for(int i = 0; i < items.length;i++){
			String item = items[i];
			JSONObject jo = JSONParser.parseStrict(item).isObject();
			String label = null;
			String itemvalue = null;
			try{
				itemvalue = jo.get("value").isString().stringValue();
				label = jo.get("label").isString().stringValue();
			}catch(Exception e){
			}
			boolean match = false;
			for(int j = 0; values != null && j < values.length;j++){
				String val = values[j];
				if(val.equals(itemvalue)){
					match = true;
					break;
				}
			}
			this.add(itemvalue, match,label);
		}
	}

	@Override
	public void setError(boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addKeyUpHandler(KeyUpHandler handler) {
		// TODO Auto-generated method stub
		
	}
}
