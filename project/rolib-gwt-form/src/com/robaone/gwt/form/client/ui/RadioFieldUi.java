package com.robaone.gwt.form.client.ui;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;
/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 *
 */
public class RadioFieldUi extends Composite  implements FormField {

	private static RadioFieldUiBinder uiBinder = GWT
			.create(RadioFieldUiBinder.class);

	interface RadioFieldUiBinder extends UiBinder<Widget, RadioFieldUi> {
	}
	@UiField FlowPanel content;
	@UiField Hidden hidden;
	String name;
	private String m_stylename;
	private String m_addedstyle;
	public RadioFieldUi(String name) {
		initWidget(uiBinder.createAndBindUi(this));
		hidden.setName(name);
		this.setName(name);
	}

	@Override
	public void setStyleName(String name){
		content.setStyleName(name);
		this.m_stylename = name;
	}
	@Override
	public void addStyleName(String name){
		content.addStyleName(name);
		this.m_addedstyle = name;
	}
	public void setValue(String value,String[] items){
		for(int i = 0; i < items.length;i++){
			JSONObject jo = JSONParser.parseStrict(items[i]).isObject();
			String label = null;
			String itemvalue = null;
			try{
				itemvalue = jo.get("value").isString().stringValue();
				label = jo.get("label").isString().stringValue();
			}catch(Exception e){
				
			}
			RadioButton radio = new RadioButton(this.getName()+"_g", label == null ? itemvalue : label);
			final String currentValue = itemvalue;
			radio.addValueChangeHandler(new ValueChangeHandler<Boolean>(){

				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					if(event.getValue().booleanValue()){
						hidden.setDefaultValue(currentValue);
					}
				}
				
			});
			if(this.m_stylename != null){
				radio.setStyleName(this.m_stylename);
			}
			if(this.m_addedstyle != null){
				radio.addStyleName(this.m_addedstyle);
			}
			if(value != null && value.equals(itemvalue)){
				radio.setValue(true);
				hidden.setValue(value);
			}
			content.add(radio);
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String str){
		name = str;
	}

	public String[] getValues() {
		Vector<String> retval = new Vector<String>();
		for(int i = 0; i < content.getWidgetCount();i++){
			Widget w = content.getWidget(i);
			if(w instanceof RadioButton){
				RadioButton radio = (RadioButton)w;
				if(radio.getValue()){
					retval.add( radio.getText());
				}
			}
		}
		return retval.toArray(new String[0]);
	}

	public void setValue(String value, JSONArray array) {
		for(int i = 0; i < array.size();i++){
			RadioButton radio = new RadioButton(this.getName()+"_g",array.get(i).isString().stringValue());
			if(value != null && value.equals(array.get(i).isString().stringValue())){
				radio.setValue(true);
				radio.setName(getName());
			}
			content.add(radio);
		}
	}

	@Override
	public void setError(boolean b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addKeyUpHandler(KeyUpHandler handler) {
		// TODO Auto-generated method stub
		
	}

}
