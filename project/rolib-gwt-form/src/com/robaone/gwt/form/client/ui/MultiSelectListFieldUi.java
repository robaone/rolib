package com.robaone.gwt.form.client.ui;

import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 *
 */
public class MultiSelectListFieldUi extends Composite implements FormField  {

	private static ListFieldUiBinder uiBinder = GWT
			.create(ListFieldUiBinder.class);

	interface ListFieldUiBinder extends UiBinder<Widget, MultiSelectListFieldUi> {
	}

	protected ListBox list = new ListBox(true);
	private String m_name;
	@UiField FlowPanel content;

	public MultiSelectListFieldUi(String name) {
		initWidget(uiBinder.createAndBindUi(this));
		content.add(list);
		this.setName(name);
	}

	@Override
	public void setStyleName(String name){
		super.setStyleName(name);
		list.setStyleName(name);
	}
	@Override
	public void addStyleName(String name){
		super.addStyleName(name);
		list.addStyleName(name);
	}
	public String[] getValues() {
		Vector<String> retval = new Vector<String>();
		int selected = list.getSelectedIndex();
		String val = list.getValue(selected);
		if(val == null){
			val = list.getItemText(selected);
		}
		try{retval.add(val);}catch(Exception e){}
		return retval.toArray(new String[0]);
	}

	public void setValue(String[] values,String[][] items){
		for(int i = 0; i < items.length;i++){
			if(items[i].length > 1){
				list.addItem(items[i][0], items[i][1]);
				for(String v : values){
					if(v != null && items[i][1].equals(v)){
						list.setItemSelected(i, true);
					}	
				}
			}else{
				list.addItem(items[i][0]);
				for(String v : values){
					if(v != null && items[i][0].equals(v)){
						list.setItemSelected(i, true);
					}
				}
			}
		}
	}

	public void setValue(String[] values,String[] items){
		for(int i = 0; i < items.length;i++){
			JSONObject jo = JSONParser.parseStrict(items[i]).isObject();
			String label = null;
			String value = null;
			try{
				value = jo.get("value").isString().stringValue();
				label = jo.get("label").isString().stringValue();
				list.addItem(label, value);
			}catch(Exception e){
				list.addItem(value);
			}
			if(values != null){
				for(String v : values){
					if(v != null && value.equals(v)){
						list.setItemSelected(i, true);
					}
				}
			}
		}
	}

	public void setName(String m_name) {
		this.m_name = m_name;
		this.list.setName(m_name);
	}

	public String getName() {
		return m_name;
	}

	public void setValue(String value, JSONArray items) {
		int selectedindex = -1;
		for(int i = 0;i < items.size();i++){
			JSONValue jv = items.get(i);
			if(jv.isString() != null){
				String name = jv.isString().stringValue();
				list.addItem(name);
				if(value != null && value.equals(name)){
					selectedindex = i;
				}
			}else if(jv.isObject() != null){
				String name = jv.isObject().get(FormUi.NAME).isString().stringValue();
				String val = jv.isObject().get(FormUi.VALUE).isString().stringValue();
				list.addItem(name,val);
				if(value!= null && value.equals(val)){
					selectedindex = i;
				}
			}
		}
		if(selectedindex > -1){
			list.setSelectedIndex(selectedindex);
		}
	}

	@Override
	public void setError(boolean b) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addKeyUpHandler(KeyUpHandler handler) {
		// TODO Auto-generated method stub

	}
	public ListBox getListBox(){
		return this.list;
	}

}
