package com.robaone.gwt.form.server;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.robaone.json.DSResponse;

/**
 * <pre>   Copyright Mar 23, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel Robateau
 *
 */
public class TestServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3852351352596664242L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			printRequest(request);
			response.setContentType("text/plain");
			DSResponse<String> dsr = new DSResponse<String>();
			try{
				if(request.getParameter("username").equals("arobateau@microdg.com")){
					dsr.getResponse().setStatus(0);
				}else{
					dsr.getResponse().setStatus(1);
					dsr.getResponse().addError("username", "Invalid username");
				}
			}catch(Exception e){
				dsr.getResponse().setStatus(2);
				dsr.getResponse().setError(e.getClass()+": "+e.getMessage());
			}
			response.getOutputStream().write(new JSONObject(dsr).toString().getBytes());
		} catch (JSONException e) {
			System.out.println("Error:"+e.getMessage());
		}
		
	}

	private void printRequest(HttpServletRequest request) throws JSONException {
		JSONObject r = new JSONObject(request.getParameterMap());
		System.out.println(r.toString(3));
	}
	

}
