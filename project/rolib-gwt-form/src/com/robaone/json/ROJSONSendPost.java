package com.robaone.json;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
/**
 * <pre>   Copyright Mar 21, 2012 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class ROJSONSendPost {
	public static void sendPost(String urlstr,Map parameters,ROJSONLineProcessor lp) throws Exception {
		try{ // Construct data 
			String data = "";
			for(int i = 0; i < parameters.keySet().size();i++){
				String key = (String)parameters.keySet().toArray()[i];
				if(i > 0) data += "&";
				if(parameters.get(key) == null)
					data += key + "=";
				else
					data += key + "=" + URLEncoder.encode((String)parameters.get(key), "UTF-8");
			}
			// Send data 
			URL url = new URL(urlstr); 
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write(data); wr.flush(); 
			// Get the response 
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			String line; 
			
			while ((line = rd.readLine()) != null) { 
				lp.process(line); 
			} 
			wr.close(); 
			rd.close(); 
		} catch (Exception e) { 
			throw e;
		} 
	}

}

