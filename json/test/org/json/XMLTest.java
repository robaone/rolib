package org.json;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class XMLTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	private Properties m_properties;
	private XML xml;

	@Before
	public void setUp() throws Exception {
		m_properties = new Properties();
		m_properties.load(this.getClass().getResourceAsStream("xmltest.properties"));
		setXml(new XML());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEscape() {
		getXml();
		String amp = XML.escape("&");
		assertEquals("&amp;",amp);
		getXml();
		String lt = XML.escape("<");
		assertEquals("&lt;",lt);
		getXml();
		String gt = XML.escape(">");
		assertEquals("&gt;",gt);
		getXml();
		String quot = XML.escape("\"");
		assertEquals("&quot;",quot);
	}

	@Test
	public void testNoSpace() {
		try {
			getXml();
			XML.noSpace("abc");
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getClass().getName()+": "+e.getMessage());
		}
		
		try {
			getXml();
			XML.noSpace("ab c");
			fail("Should show erro");
		}catch(JSONException e){
			assertTrue(true);
		}
	}

	@Test
	public void testToJSONObject() {
		try {
			getXml();
			JSONObject json = XML.toJSONObject("<tag>value</tag>");
			String value = json.getString("tag");
			assertEquals("value",value);
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getClass().getName()+": "+e.getMessage());
		}
	}

	@Test
	public void testToStringObject() {
		String name = "json1";
		JSONObject o = getJSON(name);
		try {
			getXml();
			String str = XML.toString(o);
			assertEquals("<tag>value</tag>",str);
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getClass().getName()+": "+e.getMessage());
		}
	}

	private JSONObject getJSON(String name) {
		JSONObject o = null;
		try {
			o = new JSONObject((String)m_properties.get(name));
		} catch (JSONException e1) {
			e1.printStackTrace();
			fail(e1.getClass().getName()+": "+e1.getMessage());
		}
		return o;
	}

	@Test
	public void testToStringObjectString() {
		JSONObject o = getJSON("json4");
		try {
			String str = XML.toString(o, "test");
			assertEquals("<test><_1234-usr_at_domain.com>value</_1234-usr_at_domain.com></test>",str);
		} catch( JSONException e1){
			e1.printStackTrace();
			fail(e1.getClass().getName()+": "+e1.getMessage());
		}
	}

	public XML getXml() {
		return xml;
	}

	public void setXml(XML xml) {
		this.xml = xml;
	}

}
