package com.sohvac.test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Properties;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CodeGeneratorHandlerImplHandlerTest {
	CodeGeneratorHandlerImplHandler handler;
	Properties props;
	@Before
	public void setUp() throws Exception {
		handler = new CodeGeneratorHandlerImplHandler();
		props = new Properties();
		props.setProperty("output.folder", "/home/arobateau/temp");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() throws JSONException {
		JSONObject request = new JSONObject();
		request.put("name", "TestService");
		request.put("package", "com.sample.test");
		request.put("handler", "TestServiceHandlerImpl");
		request.put("jmscorrelationid", "abc");
		handler.setRequest(request);
		handler.setProperties(props);
		handler.run(null);
		JSONObject response = handler.getResponse();
		System.out.println(response);
		assertNotNull(response);
		assertTrue(new File(response.getString("body")).exists());
	}

}
