package com.sohvac.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.json.JSONObject;

import com.robaone.gen.RabbitMQMicroserviceGenerator;
import com.robaone.jms.RabbitMQConnectionManager;
import com.robaone.util.Zip;

public class CodeGeneratorHandlerImplHandler extends
		RabbitMQCodeGeneratorServiceHandler {
	private RabbitMQMicroserviceGenerator generator;
	private Zip compresser;
	@Override
	public void run(RabbitMQConnectionManager connectionManager) {
		JSONObject response = new JSONObject();
		try{
			String[] args = {
				this.getProperties().getProperty("output.folder")+"/"+this.getRequest().getString("jmscorrelationid")	
			};
			this.getGenerator().setArgs(args);
			this.getGenerator().setScriptInputStream(new ByteArrayInputStream(this.getRequest().get("body").toString().getBytes()));
			this.getGenerator().run();
			if(this.getGenerator().getException() != null){
				throw this.getGenerator().getException();
			}else{
				File zipFile = createZip(args[0]);
				response.put("body", zipFile.toString());
				response.put("jmstype", "response");
				response.put("jmscontent_type", "application/octet-stream");
				response.put("type", "BytesMessage");
			}
		}catch(Exception e){
			try{
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				response.put("jmstype", "error");
				response.put("body", sw.toString());
				response.put("jmscontent_type", "text/plain");
			}catch(Exception e1){}
		}
		this.setResponse(response);
	}
	protected File createZip(String string) throws Exception {
		Zip zip = this.getCompresser();
		zip.setOutputFolder(new File(this.getProperties().getProperty("output.folder")));
		File zipFile = zip.zipFolder(new File(string));
		return zipFile;
	}
	public RabbitMQMicroserviceGenerator getGenerator() {
		if(this.generator == null){
			this.setGenerator(new RabbitMQMicroserviceGenerator());
		}
		return generator;
	}
	public void setGenerator(RabbitMQMicroserviceGenerator generator) {
		this.generator = generator;
	}
	public Zip getCompresser() {
		if(this.compresser == null){
			this.setCompresser(new Zip());
		}
		return compresser;
	}
	public void setCompresser(Zip compresser) {
		this.compresser = compresser;
	}

}
