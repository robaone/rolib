package com.sohvac.test;

import java.util.Properties;

import org.json.JSONObject;

import com.robaone.jms.ROJsonMessageHandler;

abstract public class RabbitMQCodeGeneratorServiceHandler implements ROJsonMessageHandler {
	private Properties properties;
	private JSONObject request;
	private JSONObject response;

	@Override
	public void setRequest(JSONObject message_jo) {
		this.request = message_jo;
	}

	public JSONObject getRequest() {
		return this.request;
	}

	@Override
	public JSONObject getResponse() {
		return this.response;
	}

	protected void setResponse(JSONObject resp) {
		this.response = resp;
	}

	public void setProperties(Properties configProperties) {
		this.properties = configProperties;
	}

	public Properties getProperties() {
		return this.properties;
	}

}
